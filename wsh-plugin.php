<?php
/**
 * Plugin Name: WSH Plugin
 * Description: A WordPress plugin template.
 * Author: West Shore Home Digital Marketing
 * Author URI: https://westshorehome.com
 * Version: 1.0
 */

// Plugin Security
defined('ABSPATH') or die('Sorry, you are not allowed to access this page.');

// Composer check
if (!file_exists(__DIR__ . '/vendor/autoload.php')) {
  add_action('admin_notices', function() {
    echo "<div class='error'><p>Please run <kbd>composer install</kbd> prior to using this plugin.</p></div>";
  });

  return;
}

require_once('vendor/autoload.php');

$wsh_plugin = new WSHPlugin\WSHPlugin(__FILE__);