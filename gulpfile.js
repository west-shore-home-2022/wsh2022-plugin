/*
Gulpfile.js file for the tutorial:
Using Gulp, SASS and Browser-Sync for your front end web development - DESIGNfromWITHIN
http://designfromwithin.com/blog/gulp-sass-browser-sync-front-end-dev

Steps:

1. Install gulp globally:
npm install --global gulp

2. Type the following after navigating in your project folder:
npm init

3. Type the following after making package.json:
npm install --save-dev gulp@3.9.1 gulp-sass gulp-uglify gulp-autoprefixer gulp-rename gulp-notify gulp-clean-css gulp-concat gulp-plumber gulp-imagemin gulp-sourcemaps @babel/core@7.4.5 @babel/preset-env@7.4.5 gulp-babel@8.0.0
*/

var gulp = require('gulp');  
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var minifycss = require('gulp-clean-css');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');

var onError = function(err) {
  notify.onError({
    title: "Error!",
    message: "<%= error.message %>",
    sound: "Beep"
  })(err);
  
  this.emit('end');
};

gulp.task('css', function() {
  return gulp.src([
      'assets/css/backend.css',
      'assets/css/frontend.css'
    ])
    .pipe(plumber({errorHandler: onError}))
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(gulp.dest('assets/css'));
});

gulp.task('js', function() {
  return gulp.src([
      'assets/js/backend.js'
    ])
    .pipe(plumber({errorHandler: onError}))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'));
});

gulp.task('sass', function() {
  return gulp.src([
      'assets/sass/**/*.scss',
      'assets/scss/**/*.scss'
    ])
    .pipe(sourcemaps.init())
    .pipe(plumber({errorHandler: onError}))
    .pipe(sass())
    .pipe(rename({suffix: '.min'}))
    .pipe(minifycss())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('assets/css'));
});

gulp.task('babel', function () {
  return gulp.src('assets/vue/es6/*.js')
    .pipe(sourcemaps.init())
    .pipe(rename({suffix: '.min'}))
    .pipe(babel())
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('assets/vue'));
});

gulp.task('watch', function() {
  gulp.watch([
    'assets/css/backend.css',
    'assets/css/frontend.css'
  ], ['css']);
  
  gulp.watch([
    'assets/js/backend.js'
  ], ['js']);
  
  gulp.watch([
    'assets/sass/**/*.scss',
    'assets/scss/**/*.scss'
  ], ['sass']);
  
  gulp.watch([
    'assets/vue/es6/*.js'
  ], ['babel']);
  
  //gulp.watch('template-parts/*.php').on('change');
  //gulp.watch('*.php').on('change');
});

gulp.task('default', ['css', 'js', 'sass', 'babel', 'watch']);
