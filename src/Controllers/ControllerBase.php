<?php

namespace WSHPlugin\Controllers;

use WSHWordpress\Controllers\Controller;

/**
 * Base controller
 */
abstract class ControllerBase extends Controller {}