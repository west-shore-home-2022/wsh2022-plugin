<?php

namespace WSHPlugin;

use WSHWordpress\Plugins\Plugin;

class WSHPlugin extends Plugin
{
  /**
   * Initialize the plugin
   */
  public function init()
  {
    // Plugin
    parent::init();
  }
}