<?php

if (!defined('ABSPATH') || !defined('WP_UNINSTALL_PLUGIN')) {
  return false;
}

// Autoload the framework
require __DIR__ . '/vendor/autoload.php';

// Drop the plugin tables
global $wpdb;

//$wpdb->query('SET FOREIGN_KEY_CHECKS = 0');
//$wpdb->query('DROP TABLE IF EXISTS `wsh_appointments`;');
//$wpdb->query('SET FOREIGN_KEY_CHECKS = 1');
