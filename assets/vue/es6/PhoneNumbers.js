(function($) {
  $(function() {
    var component = 'phone_numbers',
        plugin = wsh_twilio;
    
    var data = {
      plugin: plugin,
      pagination: component,
      phone_numbers: {
        singular: 'phone_number',
        api: {
          get: plugin.urls.rest + 'records/',
          export: plugin.urls.rest + 'records.export/',
          delete: plugin.urls.rest + 'record.delete/',
          save: plugin.urls.rest + 'record.save/',
          active: plugin.urls.rest + 'record.active/'
        },
        model: {
          id: null,
          active: 1
        }
      }
    };

    //----------------------------------
    
    if ($('#tmpl-' + component).length) {
      var Component = Vue.extend({
        name: component,
        mixins: [WSH.Component],
        template: '#tmpl-' + component,

        data: function() {
          return data;
        },
        
        methods: {
          init: function() {
            var self = this;

            self._init();
            
            // Handle form submission page responses
            if (typeof logs === 'object') {
              setTimeout(() => {
                self.handleResponse({
                  response: logs
                });
              }, 1000);
            }
            
            self.phone_numbers.search();
          }
        }
      });
      
      Component = new Component().$mount('#tab-' + component);
      
      var name = Component.getMethodSuffix(component);
      WSH.Apps = WSH.Apps || {};
      WSH.Apps[name] = Component;
    }
  });
})(jQuery);