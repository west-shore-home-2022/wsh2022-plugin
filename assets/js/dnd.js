/**
 * Drag n Drop
 *
 * @author Ed Rodriguez
 * @version 1.0
 * @created 2017-10-13
 */
var DnD = {
  $dnd: $(),
  $dropzone: $(),

  formdata: {              
    content_type: 'text/plain'
  },

  hasDnD: function() {
    var div = document.createElement('div');
    return ('draggable' in div) || ('ondragstart' in div && 'ondrop' in div);
  },

  hasFormData: function() {
    return !! window.FormData;
  },

  init: function() {
    // DRAG n DROP
    if (DnD.hasDnD() && DnD.hasFormData()) {
      $('.dnd-yes').show();
      $('.dnd-no').hide();

      DnD.$dnd = $('.dnd');
      DnD.$dropzone = $('.dropzone', DnD.$dnd);

      // BODY DROP
      $('body').on('dragenter', function(e){
          e.preventDefault();
      }).on('dragover', function(e){
          e.preventDefault();
      }).on('dragleave', function(e){
          e.preventDefault();
      }).on('drop', function(e){
          e.preventDefault();
      });

      // DROPZONE
      DnD.$dropzone
      .off('dragenter')
      .on('dragenter', DnD.dragenter)
      .off('dragover')
      .on('dragover', DnD.dragover)
      .off('dragleave')
      .on('dragleave', DnD.dragleave)
      .off('drop')
      .on('drop', DnD.drop);
    }else{
      $('.dnd-yes').hide();
      $('.dnd-no').show();
    }
  },

  dragenter: function(e) {
    e.preventDefault();

    $(this).addClass('dragover');
  },

  dragover: function(e) {
    e.preventDefault();

    if (!$(this).hasClass('dragover')) {
      $(this).addClass('dragover');
    }
  },

  dragleave: function(e) {
    e.preventDefault();

    $(this).removeClass('dragover');
  },

  drop: function(e) {
    var errors = [],
        files = e.originalEvent.target.files || e.originalEvent.dataTransfer.files;

    e.preventDefault();

    $(this).removeClass('dragover');

    if (files.length) {
      $.each(files, function(index, file) {
        // @todo Define allowed file types
        if (1 || file.name.match(/\.(jpg|pdf)$/i)) {
          // @todo Set values

          var $file = DnD.$dropzone.find('.dnd-file'),
              $info = DnD.$dropzone.find('.dnd-info'),
              reader = new FileReader(),
              elem = document.createElement('img');
              elem.file = file;

          $file.find('.file-name').text(file.name);
          $file.find('.file-size').text(DnD.filesize(file.size, 2));
          $file.find('.file-type').text(file.type.replace(/^(image|application)\//i, ''));

          $file.data('file', file);

          $file.show();
          $info.hide();

          document.body.appendChild(elem);

          reader.onload = (function(img, $row) { 
            return function(e){
              var src = typeof e.target.result === 'string' ? e.target.result : null;

              // Only show image based thumbnails
              if (src && /^data:image\//i.test(src)) {
                $file.find('.file-thumb').attr('src', src).show();
              } else {
                $file.find('.file-thumb').hide();
              }
            }; 
          })(elem, $file);

          reader.readAsDataURL(file);
        } else {
          Resources.error('Invalid file format: <strong>'+file.name+'</strong>');
        }
      });
    }
  },

  filesize: function(bytes, decimals) {
    if (bytes == 0) return '0 Byte';
    var k = 1024; // 1000 or 1024 for binary
    var dm = decimals || 0;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }
};