(function($) {
  $(function() {
    // CMS - Keep session alive
    if (typeof cms.refreshSession === 'function') {
      var url = WSH.urls.rest + 'session.refresh/';
      
      //cms.refreshSession(url);
    }
    
    // Heartbeat - Keep session alive
    // 2018-06-07 | erodriguez | Disabled in favor of cms refresh session
    if (false) {
      $(document).on('heartbeat-send', function(e, data) {
        data.refresh = 1;
      })
      .on('heartbeat-tick', function(e, data) {

        // Heartbeat Listener
        if (cms.exists('WSH.nonces', 'object')) {
          
          // Update expired nonces or add new nonces
          for (var nonce in data.nonces) {
            if (typeof WSH.nonces[nonce] !== 'string' 
              || WSH.nonces[nonce] !== data.nonces[nonce]) { 

              WSH.nonces[nonce] = data.nonces[nonce];
            }
          }

          if (cms.exists('WSH.nonces.wpnonce', 'string')) {
            $.ajaxSetup({
              headers: {'X-WP-Nonce': WSH.nonces.wpnonce}
            });
          }
        }
      });
    }
  });
})(jQuery);