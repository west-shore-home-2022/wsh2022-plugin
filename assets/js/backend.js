(function($) {
  $(function() {
    // Make sure .disabled buttons are properly disabled.
    $('button.disabled').prop('disabled', true);

    /**
     * Set up the bits that use WP_API.
     * Make sure the WP-API nonce is always set on AJAX requests.
     */
    if (typeof wpApiSettings !== 'undefined') {
      $.ajaxSetup({
        headers: { 'X-WP-Nonce': wpApiSettings.nonce }
      });
    }
    
    // Open Settings
    $('.btn-settings').on('click', function(e) {
      $modal = $('#cms-modal-settings');
      $modal.modal('show');
    });
    
    // Save Settings
    $('.btn-save-settings').on('click', function(e) {
      Reviews.updateSettings();
    });
  });
})(jQuery);