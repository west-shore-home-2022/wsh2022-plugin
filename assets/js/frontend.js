(function($) {
  $(function() {
    var $popup = $('.test-popup'),
        movePopup = function(e) {
          var div = $popup[0];
          div.style.top = (e.clientY - 15) + 'px';
          div.style.left = (e.clientX - ($popup.width() / 2)) + 'px';
        };

    // Tracking
    //cms.track();

    // Test Popup Close
    $('.test-close', $popup).on('click', function(e) {
      var $btn = $(this),
          $popup = $btn.closest('.test-popup');

      $popup.hide();
    });

    // Test Popup Move
    $popup.on('mousedown', function(e) {
      window.addEventListener('mousemove', movePopup, true);
    });

    $(document).on('mouseup', function(e) {
      window.removeEventListener('mousemove', movePopup, true);
    });
  });
})(jQuery);