/*
Gulpfile.js file for the tutorial:
Using Gulp, SASS and Browser-Sync for your front end web development - DESIGNfromWITHIN
http://designfromwithin.com/blog/gulp-sass-browser-sync-front-end-dev

Steps:

1. Type the following after navigating in your project folder:
npm init

2. Type the following after making package.json:
npm install --save-dev gulp@^3.9.1 gulp-sass gulp-uglify gulp-autoprefixer gulp-rename gulp-notify gulp-clean-css gulp-concat gulp-plumber gulp-sourcemaps @babel/core @babel/preset-env @babel/plugin-transform-destructuring gulp-babel
*/

var gulp = require('gulp');  
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var minifycss = require('gulp-clean-css');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var babel = require('gulp-babel');

var onError = function(err) {
  notify.onError({
    title: "Error!",
    message: "<%= error.message %>",
    sound: "Beep"
  })(err);
  
  this.emit('end');
};

gulp.task('css', function() {
  gulp.src('assets/vendor/bootstrap/3.3.7/css/overrides.css')
  .pipe(plumber({errorHandler: onError}))
  .pipe(rename({suffix: '.min'}))
  .pipe(minifycss())
  .pipe(gulp.dest('assets/vendor/bootstrap/3.3.7/css'));

  gulp.src([
    'assets/css/backend.css'
  ])
  .pipe(plumber({errorHandler: onError}))
  .pipe(rename({suffix: '.min'}))
  .pipe(minifycss())
  .pipe(gulp.dest('assets/css'));

  return true;
});

gulp.task('js', function() {
  gulp.src('assets/vendor/bootstrap/3.3.7/bootstrap-hack.js')
  .pipe(plumber({errorHandler: onError}))
  .pipe(rename({suffix: '.min'}))
  .pipe(uglify())
  .pipe(gulp.dest('assets/vendor/bootstrap/3.3.7'));
    
  return gulp.src([
      'assets/js/backend.js',
      'assets/js/cms.js',
      'assets/js/jquery-extended.js'
    ])
    .pipe(plumber({errorHandler: onError}))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('assets/js'));
});

gulp.task('sass', function() {
  return gulp.src('assets/sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(plumber({errorHandler: onError}))
    .pipe(sass())
    .pipe(minifycss())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('assets/css'));
});

gulp.task('babel', function () {
  return gulp.src('assets/vue/es6/*.js')
    .pipe(sourcemaps.init())
    .pipe(plumber({errorHandler: onError}))
    .pipe(rename({suffix: '.min'}))
    .pipe(babel())
    .pipe(uglify())
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("assets/vue"));
});

gulp.task('watch', function() {
  gulp.watch([
    'assets/vendor/bootstrap/3.3.7/css/overrides.css',
    'assets/css/backend.css'
  ], ['css']);
  
  gulp.watch([
    'assets/vendor/bootstrap/3.3.7/bootstrap-hack.js',
    'assets/js/backend.js',
    'assets/js/cms.js',
    'assets/js/jquery-extended.js'
  ], ['js']);
  
  gulp.watch([
    'assets/sass/**/*.scss'
  ], ['sass']);
  
  gulp.watch([
    'assets/vue/es6/*.js'
  ], ['babel']);
});

gulp.task('default', ['css', 'js', 'sass', 'babel', 'watch']);
