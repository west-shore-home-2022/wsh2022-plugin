String.prototype.toCapitalCase = function() {
  var re = /\s/;
  var words = this.split(re);

  re = /(\S)(\S+)/;
  for (i = words.length - 1; i >= 0; i--) {
    re.exec(words[i]);
    words[i] = RegExp.$1.toUpperCase() + RegExp.$2.toLowerCase();
  }

  return words.join(' ');
};

var cms = cms || {};

(function(cms, $) {
  cms.debugLevel = 5;
  cms.debugLevels = [
    'error',
    'warn',
    'debug',
    'log',
    'info'
  ];
  
  cms.init = function() {
    // Tracking
    $(document).on('click', '[data-track]', function(e){
        var btn = $(this),
            params = btn.data('track');

        cms.track(params);
    });
  };
  
  /**
   * Verifies if a namespace exists
   * 
   * @param string namespace
   * @param object [container]
   * @param string [type]
   * @returns boolean
   */
  cms.exists = function(namespace, container, type) {
    found = false;
    
    if (typeof(namespace) === 'string' && namespace.length) {
      var paths = namespace.split('.'), 
          last = paths.length - 1,
          exit = false;
  
      if (typeof container === 'string' && typeof type !== 'string') {
        type = container;
      }
      
      if (typeof container !== 'object') {
        container = window;
      }
      
      path = container;
      
      for (var i = 0; i < paths.length; i++) {
        if (!exit) {
          if (i === last) {
            if (typeof type === 'string') {
              if (typeof path[paths[i]] === type) {
                found = true;
              }
            } else if (typeof path[paths[i]] !== 'undefined') {
              found = true;
            }
          } else {
            if (typeof path[paths[i]] === 'object') {
              path = path[paths[i]];
            } else {
              exit = true;
            }
          }
        }
      }
    }
    
    return found;
  };
  
  /**
   * Capitalize words in a string
   * 
   * @param string str
   * @returns string
   */
  cms.capitalize = function(str) {
    return str.replace(/\b\w/g, function(char) { 
      return char.toUpperCase(); 
    });
  };

  /**
   * Formats amounts as money.
   *
   * @param number mnt The amount to format.
   * @return string The money formatted amount.
   */
  cms.formatMoney = function (mnt, commas) {
    // Possibly the same results as Number.toFixed(2)
    mnt -= 0;
    mnt = (Math.round(mnt * 100)) / 100;
    mnt = (mnt == Math.floor(mnt)) ? mnt + '.00'
        : ( (mnt * 10 == Math.floor(mnt * 10)) ?
    mnt + '0' : mnt);
    mnt = (typeof(commas) !== 'undefined') ? cms.addCommas(mnt) : mnt;
    return mnt;
  };
  cms.money = cms.formatMoney;

  /**
   * Adds commas to numbers.
   *
   * @param number|string num The number to add commas to.
   * @return string A comma seperated numerical string.
   */
  cms.addCommas = function (num) {
    num = (typeof(nStr) === 'number') ? String(num) : num;
    num += '';
    x = num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
  };
  cms.commas = cms.addCommas;

  /**
   * Determines if an email is valid based on its format.
   *
   * @usage Widely Used
   * @param string email The email to validate.
   * @return boolean True if the email is valid.
   */
  cms.isValidEmail = function (email) {
    if (typeof(email) !== 'string') {
      return false;
    }

    var at = email.lastIndexOf('@');

    // Make sure the at (@) sybmol exists and
    // it is not the first or last character
    if (at < 1 || (at + 1) === email.length) {
      return false;
    }

    // Make sure there aren't multiple periods together
    if (/(\.{2,})/.test(email)) {
      return false;
    }

    // Break up the local and domain portions
    var local = email.substring(0, at);
    var domain = email.substring(at + 1);

    // Check lengths
    if (local.length < 1 || local.length > 64 || domain.length < 4 || domain.length > 255) {
      return false;
    }

    // Make sure local and domain don't start with or end with a period
    if (/(^\.|\.$)/.test(local) || /(^\.|\.$)/.test(domain)) {
      return false;
    }

    // Check for quoted-string addresses
    // Since almost anything is allowed in a quoted-string address,
    // we're just going to let them go through
    if (!/^"(.+)"$/.test(local)) {
      // It's a dot-string address...check for valid characters
      if (!/^[\-a-zA-Z0-9!#$%*\/?|\^{}`~&'+=_\.]*$/.test(local)) {
        return false;
      }
    }

    // Make sure domain contains only valid characters and at least one period
    if (!/^[\-a-zA-Z0-9\.]*$/.test(domain) || domain.indexOf(".") === -1) {
      return false;
    }

    return true;
  };

  /**
   * Return a value
   * 
   * @param mixed val
   * @param mixed def
   * @returns mixed
   */
  cms.value = function (val, def) {
    if (typeof val === 'undefined') {
      return def;
    }

    return val;
  };

  /**
   * Crops a string
   * 
   * @param string str
   * @param integer max
   * @param boolean elipses
   * @returns string
   */
  cms.crop = function(str, max, elipses) {
    if(typeof str === 'string' && typeof max === 'number' && max > 0){
      var length = str.length;

      if(length > max){
        str = str.substring(0, max-1);

        if(typeof elipses !== 'undefined'){
          str = str.trim() + '...';
        }
      }
    }

    return str;
  };

  /**
   * Return a formatted filesize
   * 
   * @param integer bytes
   * @param [integer] decimals
   * @returns string
   */
  cms.filesize = function(bytes, decimals) {
    if(bytes == 0) return '0 Byte';
    var k = 1000; // or 1024 for binary
    var dm = decimals + 1 || 3;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  };

  /**
   * Determine the mouse's position
   * 
   * @param object e
   * @param [integer] pad
   * @returns object
   */
  cms.mouse = function(e, pad) {
    var mouse = {
      top: 0,
      left: 0
    };

    if (typeof e === 'object' && typeof e.pageX !== 'undefined') {
      mouse.top = e.pageY;
      mouse.left = e.pageX;

      var $body = $('#wpbody');

      // Compensate for Wordpress body offsets
      if ($body.length) {
        body = $body.offset();

        mouse.top -= body.top;
        mouse.left -= body.left;
      }
    }

    if (typeof pad === 'number') {
      mouse.top += pad;
      mouse.left += pad;
    }

    return mouse;
  }
  
  /**
   * Sets up the breakpoint sizes
   */
  cms.sizes = {
    'xs': {start:480,end:767},
    'sm': {start:768,end:991},
    'md': {start:992,end:1199},
    'lg': {start:1200,end:1399},
    'xl': {start:1400,end:1599},
    'xxl': {start:1600,end:1799},
    'yl': {start:1800,end:1919},
    'zl': {start:1920,end:null}
  };
  
  /**
   * Checks if a breakpoint size is currently present
   * 
   * @param integer size
   * @returns boolean
   */
  cms.size = function(size) {
    var width = cms.winWidth(),
        obj = cms.sizes[size];

    if (obj.start && obj.end) {
      return width >= obj.start && width <= obj.end;
    } else if (obj.start) {
      return width >= obj.start;
    }
  };
  
  /**
   * Checks for specific breakpoint sizes
   * 
   * @returns boolean
   */
  cms.xs = function() {return cms.size('xs');};
  cms.sm = function() {return cms.size('sm');};
  cms.md = function() {return cms.size('md');};
  cms.lg = function() {return cms.size('lg');};
  cms.xl = function() {return cms.size('xl');};
  cms.xxl = function() {return cms.size('xxl');};
  cms.yl = function() {return cms.size('yl');};
  cms.zl = function() {return cms.size('zl');};
  
  /**
   * Get the window's current width
   * 
   * @returns integer
   */
  cms.winWidth = function() {
    return $(window).width() + cms.scrollbarWidth();
  };

  /**
   * Checks if the mobile breakpoint is currently present
   * 
   * @returns boolean
   */
  cms.isMobile = function() {
    return cms.winWidth() <= cms.sizes.sm.end;
  };
  
  /**
   * Checks if the desktop breakpoint is currently present
   * 
   * @returns boolean
   */
  cms.isDesktop = function() {
    return !cms.isMobile();
  };

  /**
   * Determine the scrollbar width
   * 
   * @returns integer
   */
  cms.scrollbarWidth = function() { 
    var div = $('<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;"></div>'); 
    // Append our div, do our calculation and then remove it 
    $('body').append(div); 
    var w1 = $('div', div).innerWidth(); 
    div.css('overflow-y', 'scroll'); 
    var w2 = $('div', div).innerWidth(); 
    $(div).remove(); 
    
    return (w1 - w2); 
  };
  
  /**
   * Get a query string value
   * 
   * @param string name
   * @param mixed [value] Optional default value
   * @returns string
   */
  cms.querystring = function(name, value) {
    value = typeof value === 'undefined' ? null : value; 
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    
    return ((results == null) ? value : (results[1] || 0));
  };
  
  cms.timeout_session_refresh = null;
  
  /**
   * Refreshes a session to prevent it from timing out
   * 
   * @param string url
   * @param integer time
   */
  cms.refreshSession = function(url, time) {
    if (typeof url === 'string') {
      time = typeof time === 'number' ? time : 600000; // Default 10 mins

      clearTimeout(cms.timeout_session_refresh);

      cms.timeout_session_refresh = setTimeout(function() {
        $.ajax({
          url: url,
          cache: false,
          beforeSend: function(xhr) {
            xhr.setRequestHeader('X-WP-Nonce');
          },
          complete: function(response) {
            if (typeof response.responseJSON.successes === 'object') {
              var data = response.responseJSON.successes[0];

              if (typeof data === 'object') {
                time = data.refresh.milliseconds;

                // Update expired nonces or add new nonces
                if (cms.exists('WSH.nonces') && cms.exists('nonces', data, 'object')) {
                  for (var nonce in data.nonces) {
                    if (typeof WSH.nonces[nonce] !== 'string' 
                      || WSH.nonces[nonce] !== data.nonces[nonce]) { 

                      WSH.nonces[nonce] = data.nonces[nonce];
                    }
                  }
                  
                  if (cms.exists('WSH.nonces.wpnonce')) {
                    $.ajaxSetup({
                      headers: {'X-WP-Nonce': WSH.nonces.wpnonce}
                    });
                  }
                }
                
                console.log('Next session refresh will occur in ' + data.refresh.minutes + ' minutes');
              }
            }

            cms.refreshSession(url, time);
          },
          error: function(xhr, text, error) {
            console.log('Unable to refresh session');
            
            // Admin page refreshes
            if (/\/wp-admin/i.test(location.pathname)) {
              //cms.reload();
            }
          }
        });
      }, time);
    }
  };
  
  /**
   * Refreshes the browser
   * 
   * @param mixed params
   */
  cms.refresh = function(params) {
    cms.reload(params);
  };

  /**
   * Reloads the browser
   * 
   * @param mixed params
   */
  cms.reload = function(params) {
    var querystring = '';
    
    if (typeof params === 'string') {
      querystring = !params.match(/^\?/) ? '?' + params : params;
      
      location.href = location.pathname + querystring;
    } else if (typeof params === 'object') {
      
      for (var key in params) {
        if ((typeof params[key]).match(/^(string|number|boolean)$/)) {
          querystring += '&'+key+'='+escape(params[key]);
        }
      }
      
      querystring = querystring.replace(/^\&/, '?');
      
      location.href = location.pathname + querystring;
    } else {
      location.href = location.href;
    }
  };
  
  /**
   * Disable a button
   * 
   * @param object btn
   * @param string text
   */
  cms.disableBtn = function(btn, text) {
    var $btn = btn instanceof jQuery ?  btn : $(btn);

    text = text || 'Save';

    $btn.find('.icon').show();
    $btn.find('.text').html(text);
    $btn.prop('disabled', true);
  };

  /**
   * Enable a button
   * 
   * @param object btn
   * @param string text
   */
  cms.enableBtn = function(btn, text) {
    var $btn = btn instanceof jQuery ?  btn : $(btn);

    text = text || 'Save';

    $btn.find('.icon').hide();
    $btn.find('.text').html(text);
    $btn.prop('disabled', false);
  };
  
  cms.handleResponse = function(options) {
    var self = this,
        opts = $.extend({}, options),
        $modal = $('.modal:visible'),
        res = {
          errors: [],
          successes: [],
          warnings: [],
          infos: []
        };

    // Complete
    if (typeof opts.complete !== 'function') {
      opts.complete = function () {};
    }

    // Success
    if (typeof opts.success === 'string') {
      res.successes.push(opts.success);
    }
    if (typeof opts.success !== 'function') {
      opts.success = function () {};
    }

    // Error
    if (typeof opts.error === 'string') {
      res.errors.push(opts.error);
    }
    if (typeof opts.error !== 'function') {
      opts.error = function () {};
    }

    // Warning
    if (typeof opts.warning === 'string') {
      res.warnings.push(opts.warning);
    }
    if (typeof opts.warning !== 'function') {
      opts.warning = function () {};
    }

    // Info
    if (typeof opts.info === 'string') {
      res.infos.push(opts.info);
    }
    if (typeof opts.info !== 'function') {
      opts.info = function () {};
    }

    // Response
    if (typeof opts.response !== 'undefined') {
      if (typeof opts.response !== 'object') {
        res.errors.push('Unable to parse server response');
      } else {
        for (var prop in opts.response) {
          res[prop] = res[prop].concat(opts.response[prop]);
        }
      }
    }

    var callback = function () {
      opts.complete.apply(this, [res]);

      // Infos
      if (res.infos.length) {
        $.each(res.infos, function (i, msg) {
          if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
            self.info(msg);
          }
        });

        opts.info.apply(this, [res]);
      }

      // Warnings
      if (res.warnings.length) {
        $.each(res.warnings, function (i, msg) {
          if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
            self.warning(msg);
          }
        });

        opts.warning.apply(this, [res]);
      }

      // Errors
      if (res.errors.length) {
        $.each(res.errors, function (i, msg) {
          if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
            self.error(msg);
          }
        });

        opts.error.apply(this, [res]);
      }

      // Successes
      if (res.successes.length) {
        $.each(res.successes, function (i, msg) {
          if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
            self.success(msg);
          }
        });

        opts.success.apply(this, [res]);
      }
    };

    if ($modal.length) {
      setTimeout(function () {
        if (typeof opts.close === 'undefined') {
            $modal.modal('hide');
        }

        callback.apply(this);
      }, 1000);
    } else {
      callback.apply(this);
    }
  },

  cms.console = function() {
    var self = this;

    if (arguments.length === 2) {
      var type = arguments[0],
          msg = arguments[1][0],
          args = Array.prototype.slice.call(arguments[1]);

      if (typeof console[type] === 'function'
          && (self.debugLevels.indexOf(type) + 1) <= self.debugLevel) {

          console[type].apply(console, args);
      }

      var $alerts = $('.modal:visible .alerts'),
          alerts = {
            debug: undefined,
            log: undefined,
            info: 'info',
            warn: 'warning',
            error: 'danger',
            success: 'success'
          };

      if (!$alerts.length) {
        $alerts = $('.alerts:first');
      }

      if ($alerts.length && typeof alerts[type] !== 'undefined') {
        var html = '\
          <div class="alert alert-' + alerts[type] + ' alert-dismissable" style="display:none">\
            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>\
            <strong>' + self.capitalize(type) + '!</strong> ' + msg + '\
          </div>\
        ';

        var $elem = $(html);

        $alerts.append($elem);

        $elem.slideDown();

        setTimeout(function() {
          $elem.slideUp(function() {
            $elem.remove();
          });
        }, 4000);
      }
    }
  };

  cms.debug = function() { cms.console('debug', arguments); };
  cms.log = function() { cms.console('log', arguments); };
  cms.error = function() { cms.console('error', arguments); };
  cms.info = function() { cms.console('info', arguments); };
  cms.warning = function() { cms.console('warn', arguments); };
  cms.warn = function() { cms.console('warn', arguments); };
  cms.success = function() { cms.console('success', arguments); };
  
  /**
   * Opens a vimeo modal
   * 
   * @param integer id
   */
  cms.preview = function(type, url, title) {
    var $modal = $('#cms-modal-preview');
    
    if ($modal.length) {
      $modal.modal('hide');
      $modal.find('.preview').hide();
      $preview = $modal.find('.preview-' + type);
      $preview.show();
      $modal.find('.modal-title').html('Preview');

      if (typeof title !== 'undefined') {
        $modal.find('.modal-title').html(title);
      }

      // Setup Preview
      if (type === 'vimeo') {
        if (/^(\d+)$/.test(url) || /^http(s)?:\/\//i.test(url)) {
          if (/^(\d+)$/.test(url)) {
            url = 'https://player.vimeo.com/video/' + url;
          }

          $preview.find('iframe').attr('src', url);
        }
      } else if (type === 'pdf') {
        if (/^http(s)?:\/\//i.test(url)) {
          $preview.find('embed').attr('src', url);
          
          // Fixes a reloading issue
          var content = $preview.html();
          
          $preview.html(content);
        }
      }
      
      $modal.modal('show');
    }
  };
  
  cms.name = function(name) {
    if (typeof name === 'string') {
      name = name.replace(/(_|-)/g, ' ', name);
      name = name.toCapitalCase();
    }
    
    return name;
  };
  
  cms.track = function(params) {
    var url = location.href,
        data = {
          title: document.title,
          url: location.href,
          referrer: document.referrer,
          depth: screen.colorDepth,
          width: screen.width,
          height: screen.height
        };

    // Parse params
    if (typeof params === 'string' && /:/.test(params)) {
      params = params.trim();
      
      if (!params.startsWith('{')) {
        params = '{' + params;
      }
      
      if (!params.endsWith('}')) {
        params += '}';
      }
      
      try {
        eval('params = ' + params + ';');
      } catch (error) {
        console.error(error);
      }
    }
    
    if (typeof params === 'object') {
      $.extend(data, params);
    }
    
    if (typeof WSH === 'object' 
      && typeof WSH.urls === 'object' 
      && typeof WSH.urls.rest !== 'undefined') {

      url = WSH.urls.rest + 'tracking';
    } else {
      data.api = 'tracking';
    }

    $.ajax({
      type: 'POST',
      url: url,
      data: data
    });
  };
  
  /**
   * Storage
   * A utility that interacts with local storage
   * 
   * @author Ed Rodriguez
   */
  cms.storage = {
    key: 'app',

    /**
     * Returns the current timestamp
     * 
     * @returns number
     */
    now: function() {
      return (new Date).getTime();
    },

    /**
     * Returns a unique number
     * 
     * @returns number
     */
    id: function() {
      return this.now();
    },

    /**
     * Returns a unique number
     * 
     * @returns number
     */
    uuid: function() {
      return this.now();
    },

    /**
     * Get data from local storage
     * 
     * @param string name
     * @param mixed _default
     * @returns object
     */
    get: function(name, _default) {
      _default = _default || null;
      
      var value = _default,
          data = localStorage.getItem(this.key);

      data = typeof data === 'string' ? JSON.parse(data) : {};
      name = typeof name === 'string' ? name : '';

      if (name) {
        var found = true,
            paths = name.split(/\./),
            node = data,
            prop = paths.slice(-1)[0];

        paths = paths.slice(0, paths.length - 1);

        paths.forEach(function(path) {
          if (typeof node[path] === 'object') {
            node = node[path];
          } else {
            found = false;
          }
        });

        if (found && typeof node[prop] !== 'undefined') {
          value = node[prop];
        }
      } else {
        value = data;
      }

      return value;
    },

    /**
     * Alias function for save
     * 
     * @param object data
     * @param string [path]
     * @returns boolean
     */
    set: function(name, value) {
      return this.save(name, value);
    },

    /**
     * Saves data to local storage
     * 
     * @param object data
     * @param string [path]
     * @returns boolean
     */
    save: function(name, value) {
      var data = this.get(),
          node = data,
          success = false,
          now = this.now();

      name = typeof name === 'string' ? name : '';

      if (name) {
        var paths = name.split(/\./),
            prop = paths.slice(-1)[0];

        paths = paths.slice(0, paths.length - 1);

        paths.forEach(function(path) {
          if (typeof node[path] === 'object') {
            node = node[path];
          } else {
            node = {};
          }
        });

        node[prop] = value;

        //cms.log(name, paths, prop);
      } else {
        data = value;
      }

      data.created_at = typeof data.created_at !== 'undefined' ? data.created_at : now;
      data.updated_at = now;

      data = JSON.stringify(data);
      localStorage.setItem(this.key, data);

      var _data = JSON.stringify(this.get());
      success = data === _data;

      return success;
    }
  };
  
  $(function() {
    cms.init();
  });
})(cms, jQuery);