var $ = jQuery;

(function($) {
  /**
   * Serializes an array of forms into a single data object.
   * 
   * @return object An object with key/value pairs.
   */
  $.fn.serializeObject = function(){
    var o = {}, a = this.serializeArray(), h = $('[data-hint]', this);

    $.each(a, function(){
      if(o[this.name] !== undefined){
        if(!o[this.name].push){
          o[this.name] = [o[this.name]];
        }

        o[this.name].push(this.value || '');
      }else{
        o[this.name] = this.value || '';
      }

      var hint = h.filter('[name="'+this.name+'"]');

      if(hint.length === 1 && o[this.name] === hint.attr('data-hint')){
        o[this.name] = '';
      }
    });

    return o;
  };
  

  /**
   * Converts an element into a form for serialization.
   *
   * @usage Widely Used
   * @link http://stackoverflow.com/questions/742810/clone-isnt-cloning-select-values
   * @return object An object with key/value pairs.
   */
  $.fn.serializeElement = function(){
      var $src = $(this),
          $clone = $('<form></form>').append($src.clone()),
          fields = ['textarea', 'select', 'input[type="text"]', 'input[type="hidden"]', 'input[type="checkbox"]', 'input[type="radio"]'],
          data = {};

      for(var i=0; i<fields.length; i++){
          var selector = fields[i],
              $items = $src.find(selector);

          $items.each(function(index){
              var $field = $(this);

              if($field.get(0).tagName.match(/^(input)$/i) && typeof($field.attr('type')) !== 'undefined' && $field.attr('type').match(/^(checkbox|radio)$/i)){
                  $clone.find(selector).eq(index).attr('checked', $field.attr('checked'));
              }else{
                  $clone.find(selector).eq(index).val($field.val());
              }
          });
      }

      data = $clone.serializeObject();

      if($src.attr('data-defaults')){
          var defaults = $.cms.parseJson($src.attr('data-defaults'));
          data = $.extend(defaults||{}, data);
      }

      return data;
  };
})(jQuery);