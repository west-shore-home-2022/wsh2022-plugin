(function($) {
  $(function() {
    var backend = {
      init: function() {
        // Make sure .disabled buttons are properly disabled.
        $('button.disabled').prop('disabled', true);

        /**
         * Set up the bits that use WP_API.
         * Make sure the WP-API nonce is always set on AJAX requests.
         */
        if (cms.exists('WSH.nonces.wpnonce')) {
          $.ajaxSetup({
            headers: {'X-WP-Nonce': WSH.nonces.wpnonce}
          });
        }
        
        // Update modals to display properly with page scrolls
        $('body').on('show.bs.modal', '.modal', function() {
          var $modal = $(this);
          
          $modal.find('.alerts').html('').hide();
          
          var height = $modal.height() + 1000;
          
          $modal.css({
            position: 'absolute',
            height: height
          });
          
          $modal.removeClass('fade');
        });
        
        $('body').on('shown.bs.modal', '.modal', function() {
          var $modal = $(this),
              offset = $modal.offset();
          
          $modal.css('display', 'none');
          offset.top = $(document).scrollTop() + 40;
          $modal.fadeIn();
          
          $modal.offset(offset);
        });
      }
    };
    
    backend.init();
  });
})(jQuery);