CREATE TABLE IF NOT EXISTS `wsh_zips` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zip` VARCHAR(20) NOT NULL,
  `city` VARCHAR(100) NULL,
  `state` VARCHAR(100) NULL,
  `county` VARCHAR(100) NULL,
  `country` VARCHAR(100) NULL,
  `latitude` VARCHAR(30) NULL,
  `longitude` VARCHAR(30) NULL,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `zips_zip_unique` (`zip`)
) ENGINE=InnoDB DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;