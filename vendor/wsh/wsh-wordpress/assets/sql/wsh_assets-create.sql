CREATE TABLE IF NOT EXISTS `wsh_assets` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` VARCHAR(255) NOT NULL,
  `assets` TEXT NOT NULL,
  `attributes` TEXT NULL,
  `action` ENUM('replace', 'combine-head', 'combine-body', 'combine-replace') NOT NULL,
  `type` ENUM('backend', 'frontend', 'global') NOT NULL DEFAULT 'frontend',
  `minify` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `expire` VARCHAR(255) NULL,
  `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
  `created_at` TIMESTAMP NULL,
  `updated_at` TIMESTAMP NULL
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;