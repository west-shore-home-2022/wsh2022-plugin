var WSH = WSH || {},
    plugin = WSH;

/**
 * The Record Base Class
 * 
 * @param object child
 * @param object component
 * @param object dataset
 */
plugin.RecordBase = function(child, component, dataset) {
  var self = this;
  
  self.edit = function() { component.doShow(dataset, child); };
  self.show = function() { component.doShow(dataset, child); };
  self.delete = function() { component.doDelete(dataset, child); };
  self.duplicate = function() { component.doDuplicate(dataset, child); };
  self.toggle = function() { component.doActive(dataset, child); };
  self.revert = function(params) { component.doRevert(dataset, child, params); };
  self.save = function(e) { component.doSave(dataset, e); };
  self.status = function() { component.doStatus(dataset, child); };
  self.name = function(lowercase) { return dataset.name(lowercase); };
  self.title = function(lowercase) { return dataset.title(lowercase); };
  
  self.active = function(value) {
    if (child.active !== 'undefined') {
      if (typeof value !== 'undefined') {
        child.active = value;
      }

      return child.active;
    }

    return null;
  };

  self.update = function(record) {
    for (var prop in record) {
      if ($.isArray(record[prop]) 
        || !/^(object|function)$/i.test(typeof record[prop]) 
        || (typeof dataset.fillable !== 'undefined' 
          && $.isArray(dataset.fillable) 
          && $.inArray(prop, dataset.fillable) > -1)) {
  
        child[prop] = record[prop];
      }
    }
  };
};

/**
 * The Record Class
 * 
 * Examples of Usage:
 * 
 * record.$.edit();
 * record.parent.edit();
 * record.__proto__.edit();
 * 
 * @param object component
 * @param object dataset
 * @param object record
 */
plugin.Record = function(component, dataset, record) {
  var self = this,
      parent = new plugin.RecordBase(self, component, dataset);
  
  $.extend(self, record);

  for (var prop in self) {
    component.$set(self, prop, self[prop]); 
  }
  
  self.$ = parent;
  self.parent = parent;
  self.__proto__ = parent;
};