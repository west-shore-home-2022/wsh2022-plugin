(function($) {
  $(function() {
    var component = 'assets',
        plugin = WSH;
    
    var data = {
      plugin: plugin,
      pagination: component,
      mode: {
        attribute: ''
      },
      attribute: {
        id: null,
        name: '',
        value: ''
      },
      attribute_revert: null,
      attribute_model: {
        id: null,
        name: '',
        value: ''
      },
      attributes: [],
      assets: {
        singular: 'asset',
        api: {
          get: plugin.urls.rest + 'records/',
          export: plugin.urls.rest + 'records.export/',
          delete: plugin.urls.rest + 'record.delete/',
          save: plugin.urls.rest + 'record.save/',
          active: plugin.urls.rest + 'record.active/'
        },
        model: {
          id: null,
          attributes: [],
          action: 'replace',
          type: 'frontend',
          minify: 0,
          active: 1
        },
        prepRecords: function(records) {
          var self = this;

          records.forEach((record, i) => {
            record = self.prepRecord(record);
          });

          return records;
        },
        prepRecord: function(record) {
          record.attributes = record.attributes ? JSON.parse(record.attributes) : [];

          return record;
        },
        onBeforeShow: function(data, $form) {
          var self = this;
          
          self.component.mode.attribute = '';
          self.component.attribute = Object.assign({}, self.component.attribute_model);
          self.component.attributes = data && data.attributes ? data.attributes : [];

          return data; 
        },
        onBeforeSave: function(data, $form) {
          var self = this;
          
          data.record.attributes = $.isArray(self.component.attributes) ? JSON.stringify(self.component.attributes) : [];

          return data; 
        }
      }
    };

    //----------------------------------
    
    if ($('#tmpl-' + component).length) {
      var Component = Vue.extend({
        name: component,
        mixins: [plugin.Component],
        template: '#tmpl-' + component,

        data: function() {
          return data;
        },
        
        methods: {
          init: function() {
            var self = this;

            self._init();
            
            // Handle form submission page responses
            if (typeof logs === 'object') {
              setTimeout(() => {
                self.handleResponse({
                  response: logs
                });
              }, 1000);
            }
            
            self.assets.search();
          },
          
          addAttribute: function() {
            var self = this;
            
            self.attribute = Object.assign({}, self.attribute_model);
            
            self.mode.attribute = 'add';
          },
          
          editAttribute: function(attribute) {
            var self = this;
            
            self.attribute = attribute;
            self.attribute_revert = Object.assign({}, attribute);
            
            self.mode.attribute = 'edit';
          },
          
          deleteAttribute: function(attribute) {
            var self = this;
              
            if (confirm('You are about to delete this attribute.')) {
              self.attributes = self.attributes.filter((item) => {
                return item.id !== attribute.id;
              });
            }
          },
          
          saveAttribute: function() {
            var self = this,
                attribute = self.attribute;
            
            // Validate Attribute Name
            attribute.name = attribute.name.trim();
            
            if (!attribute.name.length) {
              self.error('Name is required');
              
              return;
            }
            
            // Validate Attribute Search
            attribute.value = attribute.value.trim();
            
            if (!attribute.value.length) {
              self.error('Value is required');
              
              return;
            }
            
            // Add Attribute
            if (!attribute.id) {
              attribute.id = (new Date).getTime();
              self.attributes.push(attribute);
            }
            
            // Edit Attribute
            else {
              self.attributes.forEach((item) => {
                if (item.id === attribute.id) {
                  item = $.extend(item, attribute);
                }
              });
            }
            
            self.mode.attribute = '';
          },
          
          cancelAttribute: function() {
            var self = this;
              
            if (self.attribute.id) {
              self.attribute = Object.assign({}, self.attribute_revert);
              
              self.attributes.forEach((item) => {
                if (item.id === self.attribute.id) {
                  item = $.extend(item, self.attribute);
                }
              });
            }
            
            self.mode.attribute = '';
          }
        }
      });
      
      Component = new Component().$mount('#tab-' + component);
      
      var name = Component.getMethodSuffix(component);
      plugin.Apps = plugin.Apps || {};
      plugin.Apps[name] = Component;
    }
  });
})(jQuery);