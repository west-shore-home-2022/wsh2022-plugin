(function($) {
  $(function() {
    var component = 'leads',
        plugin = WSH;
    
    var data = {
      plugin: plugin,
      pagination: component,
      leads: {
        singular: 'lead',
        api: {
          get: plugin.urls.rest + 'records/',
          export: plugin.urls.rest + 'records.export/',
          delete: plugin.urls.rest + 'record.delete/',
          save: plugin.urls.rest + 'record.save/',
          active: plugin.urls.rest + 'record.active/'
        },
        model: {
          id: null,
          active: 1
        }
      }
    };

    //----------------------------------
    
    if ($('#tmpl-' + component).length) {
      var Component = Vue.extend({
        name: component,
        mixins: [plugin.Component],
        template: '#tmpl-' + component,

        data: function() {
          return data;
        },
        
        methods: {
          init: function() {
            var self = this;
            
            self._init();
            
            // Handle form submission page responses
            if (typeof logs === 'object') {
              setTimeout(() => {
                self.handleResponse({
                  response: logs
                });
              }, 1000);
            }
            
            self.leads.search();
          },
          
          showImport: function() {
            this.showModal('#cms-modal-lead-import');
          },
          
          doImport: function(e) {
            var self = this,
                $btn = $(e.target),
                $form = $btn.closest('form'),
                $file = $('input[name="file"]', $form),
                file = $file.val(),
                $modal = $('#cms-modal-loading'),
                errors = [];

            if (!file || !/\.csv$/i.test(file)) {
              errors.push('Please select a csv to import');
            }

            if (!errors.length) {
              $('.modal').modal('hide');

              $modal.find('.message').html('Importing...');

              self.showModal($modal);

              $form[0].submit();
            }

            if (errors.length) {
              self.handleResponse({
                response: {errors: errors},
                close: false
              });
            }
          }
        }
      });
      
      Component = new Component().$mount('#tab-' + component);
      
      var name = Component.getMethodSuffix(component);
      plugin.Apps = plugin.Apps || {};
      plugin.Apps[name] = Component;
    }
  });
})(jQuery);