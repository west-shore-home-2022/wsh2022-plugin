var WSH = WSH || {},
    plugin = WSH;

/**
 * The Component Class
 */
plugin.Component = {
  delimiters: ['${', '}'],
  
  props: [],
  
  created: plugin.Base.created,

  // 1) beforeCreate
  beforeCreate: function() {
    if (this._beforeCreate) { this._beforeCreate() };
  },
  
  // 2) created
  created: function() {
    if (this._created) { this._created() };
  },
  
  // 3) beforeMount
  beforeMount: function() {
    if (this._beforeMount) { this._beforeMount() };
  },
  
  // 4) mounted
  mounted: function() {
    if (this._mounted) { this._mounted() };
  },
  
  // 5) beforeUpdate
  beforeUpdate: function() {
    if (this._beforeUpdate) { this._beforeUpdate() };
  },
  
  // 6) updated
  updated: function() {
    if (this._updated) { this._updated() };
  },

  // 7) beforeDestroy
  beforeDestroy: function() {
    if (this._beforeDestroy) { this._beforeDestroy() };
  },
  
  // 8) destroyed
  destroyed: function() {
    if (this._destroyed) { this._destroyed() };
  },
  
  filters: $.extend(plugin.Base.filters, {
    name: function(str, lowercase) {
      var self = this;

      str = str.replace(/(_|-)/, ' ');

      lowercase = lowercase || null;

      if (!lowercase) {
        str = str.replace(/\b\w/g, l => l.toUpperCase());
      }

      return str;
    }
  }),

  methods: $.extend(plugin.Base.methods, {
    _beforeMount: function() {
      var self = this;

      if (typeof self.init === 'function') {
        self.init();
      }
    },
    
    _mounted: function() {
      var self = this;

      self._setup();
    },

    _beforeUpdate: function() {
      var self = this;
    },
    
    _updated: function() {
      var self = this;

      self._setup();
    },

    _setup: function() {
      var self = this;
      
      // Set up Datepicker
      if ($.fn.datepicker) {
        $('input[id].datepicker', self.$el).each(function(i, input) {
          var $input = $(input),
              id = $input.attr('id');

          //console.log('[' + id + ']', $input.val());
          
          if (!$input.hasClass('datepicker-alias')) {
            plugin.Data.Plugins[id] = $input.val();
          }
        });
      }

      self.applySorting();

      // Set up WYSIWYG editors
      $('textarea.form-editor', self.$el).each(function(i, elem) {
        var $textarea = $(elem),
            dataset = self,
            props = $textarea.data('dataset').split('.'),
            value = $textarea.val(),
            found = true;
        
        // Build dataset form namespace
        $.each(props, function(i, prop) {
          if (found && typeof dataset[prop] === 'object') {
            dataset = dataset[prop];
            found = true;
          } else {
            found = false;
          }
        });
        
        $textarea.trumbowyg({
          btns: [
            ['viewHTML'],
            ['undo', 'redo'], // Only supported in Blink browsers
            ['formatting'],
            ['strong', 'em', 'del'],
            ['superscript', 'subscript'],
            ['link'],
            ['insertImage'],
            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ['unorderedList', 'orderedList'],
            ['horizontalRule'],
            ['removeformat']
            //['fullscreen']
          ] 
        });

        $textarea.trumbowyg('html', value);

        // Update model after redraw
        $textarea.prev().off('blur').on('blur', function() {
          var $div = $(this),
              $textarea = $div.next(),
              prop = $textarea.attr('id').replace(/^.*-/, '');

          if (typeof dataset[prop] !== 'undefined') {
            dataset[prop] = $textarea.val();
          } else {
            console.error('Unable to find property ' + $textarea.data('dataset') + '.' + prop);
          }
        });
      });

      // Set up Valid
      if ($.fn.valid) {
        $('[data-valid]', self.$el).valid();
      }

      // Set up Datepicker
      if ($.fn.datepicker) {
        $('input.datepicker', self.$el).each(function(i, input) {
          var $input = $(input),
              id = $input.attr('id');

          if (id && typeof plugin.Data.Plugins[id] !== 'undefined') {
            //console.log('[' + id + ']', plugin.Data.Plugins[id]);
            
            $input.val(plugin.Data.Plugins[id]);
          }
          
          $input.datepicker({
            dateFormat: 'm/d/y'
          });
        });
      }
    },
    
    /**
     * Calls a callback after all ajax calls have completed
     * 
     * @param function callback
     */
    afterAjax: function(callback) {
      var self = this;
      
      if (typeof callback === 'function') {
        new Promise(function(resolve, reject) {
          function checkActive() {
            if ($.active <= 0) {
              resolve();
            } else {
              setTimeout(checkActive, 200);
            }
          }

          checkActive();
        }).then(val => new Promise(function(resolve, reject) {
          callback.apply(self);
        }));
      }
    },
    
    // Apply click on copy button
    copyToClipboard: function(e, type) {
      var self = this,
          $btn = $(e.target),
          $parent = $btn.parent(),
          $elem = $('.copy', $parent);

      if ($elem.length) {
        var success = self._copyToClipboard($elem[0]);

        if (success) {
          self.info(type + ' copied...');
        } else {
          self.error('Unable to copy ' + type.toLowerCase() + '!');
        }
      }
    },

    /**
      * Copy a input or textarea to the clipboard
      *
      * @param object elem
      * @return boolean
      */
    _copyToClipboard: function(elem) {
      var success = false;

      if (elem) {
        // Create hidden text element, if it doesn't already exist
        var targetId = '_hiddenCopyText_';
        var isInput = elem.tagName === 'INPUT' || elem.tagName === 'TEXTAREA';
        var origSelectionStart, origSelectionEnd;

        if (isInput) {
          // Can just use the original source element for the selection and copy
          target = elem;
          origSelectionStart = elem.selectionStart;
          origSelectionEnd = elem.selectionEnd;
        } else {
          // Must use a temporary form element for the selection and copy
          target = document.getElementById(targetId);

          if (!target) {
            var target = document.createElement('textarea');

            target.style.position = 'absolute';
            target.style.left = '-9999px';
            target.style.top = '0';
            target.id = targetId;
            document.body.appendChild(target);
          }

          target.textContent = elem.textContent;
        }

        // Select the content
        var currentFocus = document.activeElement;

        target.focus();
        target.setSelectionRange(0, target.value.length);

        // Copy the selection
        try {
          success = document.execCommand('copy');
        } catch(e) {
          success = false;
        }

        // Restore original focus
        if (currentFocus && typeof currentFocus.focus === 'function') {
          currentFocus.focus();
        }

        if (isInput) {
          // Restore prior selection
          elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
          // Clear temporary content
          target.textContent = '';
        }
      }

      return success;
    },
    
    /**
     * Sets up records on init
     */
    _init: function() {
      var self = this;

      // Set up data
      plugin.Data = plugin.Data || {};
      plugin.Data.Plugins = plugin.Data.Plugins || {};
      
      // Build datasets
      for (var prop in self) {
        if (typeof self[prop] === 'object' && self[prop]) {
          var dataset = self[prop];

          if (typeof dataset.singular === 'string') {
            self[prop] = new plugin.Dataset(self, dataset);
            self.$set(self[prop], 'plural', prop);
          }
        }
      }

      // Set tab click event
      var selector = self.pagination;
      
      if (typeof selector !== 'undefined') {
        $('a[href="#tab-' + selector + '"]').on('click', {selector: selector}, function(e) {
          self.doSearch(self[e.data.selector]);
        });
      }
      
      // Runs a shared init call once across all components
      plugin.Apps = plugin.Apps || {};
      plugin.Apps.Components = plugin.Apps.Components || {};
      
      plugin.Apps.Components.count = plugin.Apps.Components.count || 0;
      plugin.Apps.Components.count++;
      
      var debug = false, checkInterval = 200;
      
      function log() {
        if (debug) {
          console.log.apply(console, arguments);
        }
      }
      
      if (plugin.Apps.Components.count >= $('template[id]').length) {
        log('Components:', plugin.Apps.Components.count);
      
        if (typeof plugin.Apps.Components.init === 'undefined') {
          plugin.Apps.Components.init = true;
          
          var tabs = cms.querystring('tab');
          
          // Activate the first tab if tab(s) not specified
          if (!tabs) {
            var $tab = $('.cms [data-toggle="tab"]:first');

            if ($tab.length) {
              $tab.trigger('click');
            }
          }
          
          if (typeof Promise !== 'undefined') {
            // Run promise cahin
            new Promise(function(resolve, reject) {
              // Trigger Tab
              log('Part 1: Start');

              function checkActive() {
                if ($.active <= 0) {
                  log('Part 1: Processing');
                    
                  if (tabs) {
                    tabs = unescape(tabs).split(/,/);

                    var count = 1;

                    tabs.forEach(function(tab, i) {
                      var $tab = $('[data-tab="' + tab + '"]:first');

                      if (!$tab.length) {
                        $tab = $('a[href="#tab-' + tab + '"]:first'); 
                      }

                      if ($tab.length === 1) {
                        if (!$tab.closest('li').hasClass('active')) {
                          $tab.trigger('click');
                        }
                      }

                      if (count >= tabs.length) {
                        resolve(tab);
                      }

                      count++;
                    });
                  } else {
                    resolve();
                  }
                } else {
                  log('--');
                  setTimeout(checkActive, checkInterval);
                }
              }

              checkActive();
            }).then(tab => new Promise(function(resolve, reject) {
              // Trigger Left Nav or Edit Button
              log('Part 2: Start');

              function checkActive() {
                if ($.active <= 0) {
                  log('Part 2: Processing');

                  var  id = cms.querystring('id');

                  if (id && tab) {
                    var found = false,
                        method = 'tab' + tab.toLowerCase().toCapitalCase();
                    
                    $.each(plugin.Apps, function(app) {
                      if (typeof plugin.Apps[app][method] === 'function') {
                        found = true;
                        plugin.Apps[app][method].apply(plugin.Apps[app], [id]);
                      }
                    });
                    
                    // Call Custom Method
                    if (!found) {
                    
                      // Trigger Left Menus
                      var $btn = $('.tab-pane.active .pane-left label[data-id="' + id + '"]');

                      if (!$btn.length) {
                        $btn = $('.tab-pane.active tr[data-id="' + id + '"] .btn-edit');
                      }

                      if ($btn.length) {
                        $btn.trigger('click');
                      }
                    }
                    
                    resolve();
                  }
                } else {
                  log('--');
                  setTimeout(checkActive, checkInterval);
                }
              }

              checkActive();
            }));
          }
        }
      }
    },

    init: function() {
      var self = this;
      self._init();
    },
    
    /**
     * Apply sorting to a table
     */
    applySorting: function() {
      var self = this;

      for (var prop in self) {
        if (typeof self[prop] === 'object' && self[prop]) {
          if (typeof self[prop].plural === 'string') {
            var dataset = self[prop],
                $table = $('.table-' + dataset.plural, self.$el);

            $table.find('th[data-sort]').off('click')
              .on('click', {dataset: dataset, component: self}, self.tableSort)
              .addClass('cursor').attr('title', 'Sort');
          }
        }
      }
    },

    name: function(str, lowercase) {
      var self = this;

      return self.$options.filters.name(str, lowercase);
    },

    /*-- Record Methods -----------------------------------------------*/

    /**
     * Search records
     * 
     * @param object dataset
     * @param object [params]
     * @param integer [page]
     */
    doSearch: function(dataset, params, page) {
      page = page || 1;
      params = params || {};
      dataset.page = page;
      
      // Params Override
      if (typeof dataset.onParams === 'function') {
        params = dataset.onParams(params);
      }

      this.callFunction('get', dataset, [params]);
    },
    
    /**
     * Shows records by page
     * 
     * @param integer page
     * @param object [dataset]
     * @param object [params]
     */
    doPage: function(page, dataset, params) {
      page = page || 1;
      params = params || {};
      dataset.page = page;
      
      // Params Override
      if (typeof dataset.onParams === 'function') {
        params = dataset.onParams(params);
      }
      
      this.callFunction('get', dataset, [params]);
    },
    
    /**
     * Export records
     * 
     * @param object dataset
     * @param object [params]
     */
    doExport: function(dataset, params) {
      params = params || {};

      // Params Override
      if (typeof dataset.onParams === 'function') {
        params = dataset.onParams(params);
      }
      
      this.callFunction('export', dataset, [params]);
    },

    /**
     * Show a record
     * 
     * @param object dataset
     * @param object record
     */
    doShow: function(dataset, record) {
      this.callFunction('show', dataset, [record]);
    },

    /**
     * Previous record
     * 
     * @param object dataset
     */
    doPrev: function(dataset) {
      this.callFunction('prev', dataset);
    },

    /**
     * Next record
     * 
     * @param object dataset
     */
    doNext: function(dataset) {
      this.callFunction('next', dataset);
    },

    /**
     * Delete a record
     * 
     * @param object dataset
     * @param object record
     */
    doDelete: function(dataset, record) {
      this.callFunction('delete', dataset, [record]);
    },

    /**
     * Duplicate a record
     * 
     * @param object dataset
     * @param object record
     */
    doDuplicate: function(dataset, record) {
      this.callFunction('duplicate', dataset, [record]);
    },

    /**
     * Save a record
     * 
     * @param object dataset
     * @param object [e]
     */
    doSave: function(dataset, e) {
      this.callFunction('save', dataset, [e]);
    },
    
    /**
     * Revert a record
     * 
     * @param object dataset
     * @param object record
     */
    doRevert: function(dataset, record, params) {
      this.callFunction('revert', dataset, [record, params]);
    },

    /**
     * Toggle a record's active state
     * 
     * @param object dataset
     * @param object record
     */
    doActive: function(dataset, record) {
      this.callFunction('active', dataset, [record]);
    },
    
    /**
     * Set a record's status
     * 
     * @param object dataset
     * @param object record
     */
    doStatus: function(dataset, record) {
      this.callFunction('status', dataset, [record]);
    },

    /**
     * Dynamically call a function
     * 
     * @param string prefix
     * @param object dataset
     * @param object [args]
     */
    callFunction: function(prefix, dataset, args) {
      var self = this,
          plural = ['get', 'export'],
          isPlural = plural.indexOf(prefix) > -1,
          method = prefix + (isPlural ? self.getMethodSuffix(dataset.plural) : self.getMethodSuffix(dataset.singular));

      args = args || [];

      if (typeof self[method] === 'function') { 
        self[method].apply(self, args);
      } else {
        var suffix = isPlural ? 'Records' : 'Record';

        method = prefix + suffix;

        if (typeof self[method] === 'function') { 
          args.unshift(dataset);
          self[method].apply(self, args);
        }
      }
    },
    
    /**
     * Get records
     * 
     * @param object dataset
     * @param object [params]
     */
    getRecords: function(dataset, params) {
      var self = this,
          name = dataset.name(1),
          search = $('.search-' + dataset.plural + ' [name="search"]', self.$el).val(),
          url = self.plugin.urls.rest + dataset.plural + '/',
          query = [
            '_page=' + dataset.page,
            '_count=' + dataset.count
          ];

      search = dataset.search_term ? dataset.search_term : search;
      
      url = dataset.value('api.get', url);
      
      // Merge custom params
      if (typeof params === 'object') {
        for (var param in params) {
          if (!/(function|object)/.test(typeof params[param])) {
            query.push(param + '=' + encodeURI(params[param]));
          }
        }
      }

      dataset.loading = true;

      // Handle Searches
      if (search) {
        search = encodeURI(search.trim());

        if (search.length) {
          query.push('_search=' + search);
        }
      }

      // Common
      query.push('_model=' + dataset.singular);
      
      // Sorting
      if (dataset.sort && dataset.order) {
        query.push('_sort=' + dataset.sort);
        query.push('_order=' + dataset.order);
      }

      // All
      if (dataset.all) {
        query.push('_all=' + dataset.all);
      }
      
      if (query.length) {
        url += !/\?/.test(url) ? '?' : '&'; 
        url += query.join('&');
      }

      const key = btoa(url),
            handleResponse = function(response, text, xhr) {
              self.handleResponse({
                response: response,
                close: function() {},
                complete: function(response) {
                  dataset.loading = false;
                },
                success: function(response) {
                  var records = [];

                  if (response.successes.length) {
                    // Save cache if enabled
                    if (dataset.cache.enabled) {
                      var cache = {
                        value: response,
                        created: (new Date).getTime()
                      };

                      // Save cache by cache type
                      if (/memory/i.test(dataset.cache.type)) {
                        if (typeof dataset.cache.memory[key] === 'undefined') {
                          dataset.cache.memory[key] = cache;
                        }
                      } else if (/local(storage)?/i.test(dataset.cache.type)) {
                        var storage = cms.storage.get('cache', {});
                        
                        if (typeof storage[key] === 'undefined') {
                          storage[key] = cache;

                          cms.storage.save('cache', storage);
                        }
                      }
                    }

                    if (typeof response.successes[0].records !== 'undefined') {
                      records = response.successes[0].records;

                      dataset.pages = response.successes[0].pages;
                      dataset.total = response.successes[0].total;
                    } else {
                      records = response.successes[0];
                    }
                  }

                  if (typeof dataset.prepRecords === 'function') {
                    records = dataset.prepRecords(records);
                  }

                  if (typeof dataset.onResponse === 'function') {
                    dataset.onResponse(response);
                  }

                  dataset.setRecords(records);

                  if (typeof params.success === 'function') {
                    params.success.apply(this, [dataset]);
                  }
                }
              });
            };
      
      // Cache
      let storage = null,
          cache = null,
          expired = false;
      
      // Retrieve cache by cache type if enabled
      if (dataset.cache.enabled) {
        if (/memory/i.test(dataset.cache.type)) {
          storage = dataset.cache.memory;
        } else if (/local(storage)?/i.test(dataset.cache.type)) {
          storage = cms.storage.get('cache', {});
        }

        // Set cache
        if (storage && typeof storage[key] !== 'undefined') {
          cache = storage[key];
        }

        // Determine if cache has expired by expire date
        if (cache && typeof cache.created === 'number' && typeof dataset.cache.expire === 'number') {
          let now = (new Date).getTime(),
              {created} = cache,
              expire = (dataset.cache.expire * ((1000 * 1) * 60)) + created;
          
          expired = expire <= now;
        }
        
        // Expire by url ?reload
        expired = !expired ? /((\?|&)(reload|nocache))/.test(location.search) : expired;
        
        // Cache expired
        if (expired) {
          console.log('Cache Expired');

          // Clear up memory if expired by cache type
          delete storage[key];

          if (/memory/i.test(dataset.cache.type)) {
            dataset.cache.memory = storage;
          } else if (/local(storage)?/i.test(dataset.cache.type)) {
            cms.storage.save('cache', storage);
          }

          cache = null;
        }
      }
      
      // Cached response
      if (cache) {
        console.log('Cache Found', cache.created, url);
        
        handleResponse(cache.value);
      }
      // Retrieve response
      else {
        if (dataset.cache.enabled) {
          console.log('Cache Not Found', url);
        }
        
        $.ajax({
          url: url,
          type: 'get',
          dataType: 'json',
          success: handleResponse,
          error: function(xhr, text, error) {
            self.handleResponse({
              response: xhr.responseJSON,
              close: function() {},
              complete: function(response) {
                dataset.loading = false;
              },
              error: 'Unable to retrieve ' + name
            });
          }
        });
      }
    },

    /**
     * Toggles the record's active status
     * 
     * @param object dataset
     * @param object record
     * @param [object] e
     */
    activeRecord: function(dataset, record, e) {
      var self = this,
          url = self.plugin.urls.rest + dataset.singular + '.active/';
  
      url = dataset.value('api.active', url);

      if (typeof e === 'object') {
        e.stopPropagation();
      }

      if (typeof dataset === 'object' && record) {
        var active_old = record.$.active(),
            active_new = active_old == 1 ? '0' : '1';

        record.$.active(active_new);

        self.$forceUpdate();

        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          data: {
            id: record.id,
            active: active_new,
            _model: dataset.singular
          },
          success: function(response) {
            self.handleResponse({
              response: response,
              close: function() {},
              complete: function(response) {},
              success: function(response) {
                record.$.active(active_new);
                self.$forceUpdate();
              },
              error: function() {
                record.$.active(active_old);
                self.$forceUpdate();
              }
            });
          },
          error: function() {
            self.handleResponse({
              close: function() {},
              complete: function(response) {},
              error: function() {
                record.$.active(active_old);
                self.$forceUpdate();
              }
            });
          }
        });
      }
    },

    /**
     * Updates the record's status
     * 
     * @param object dataset
     * @param object record
     * @param [object] e
     */
    statusRecord: function(dataset, record, e) {
      var self = this,
          url = self.plugin.urls.rest + dataset.singular + '.status/';
  
      url = dataset.value('api.status', url);
      
      if (typeof e === 'object') {
        e.stopPropagation();
      }

      if (typeof dataset === 'object' && record) {
        $modal = $('#cms-modal-loading');
        $modal.find('.message').html('Saving...');

        self.showModal($modal);

        $.ajax({
          url: url,
          type: 'post',
          dataType: 'json',
          data: {
            id: record.id,
            status: record.status,
            _model: dataset.singular
          },
          success: function(response) {
            self.handleResponse({
              response: response,
              complete: function() {},
              success: function(response) {}
            });
          },
          error: function() {
            self.handleResponse({
              complete: function() {}
            });
          }
        });
      }
    },

    getCurrent: function(dataset) {
      let index = 0,
          {record, records} = dataset,
          json = JSON.stringify(record);
      
      records.forEach(function(_record, i) {
        let json2 = JSON.stringify(_record);
        
        if (json2 === json) {
          index = i;
        }
      });
      
      return index;
    },
    
    /**
     * Previous record
     * 
     * @param object dataset
     */
    prevRecord: function(dataset) {
      let self = this,
          {record, records, page, pages:totalPages} = dataset;
      
      if (typeof record === 'object') {
        let current = self.getCurrent(dataset),
          prev = current - 1,
          prevPage = page - 1;
        
        //console.log('prevRecord', prev, prevPage, totalPages);
        
        if (prev < 0) {
          if (prevPage >= 1) {
            dataset.search({
              success: function(dataset) {
                dataset.show(dataset.records[dataset.records.length - 1]);
              }
            }, prevPage);
          }
        } else {
          dataset.revert({close: false});
          dataset.show(records[prev]);
        }
      }
    },

    /**
     * Next record
     * 
     * @param object dataset
     */
    nextRecord: function(dataset) {
      let self = this,
          {record, records, page, pages:totalPages} = dataset;
      
      if (typeof record === 'object') {
        let current = self.getCurrent(dataset),
            next = current + 1,
            nextPage = page + 1;
        
        //console.log('nextRecord', next, nextPage, totalPages);
        
        if (next > (records.length - 1)) {
          if (nextPage <= totalPages) {
            dataset.search({
              success: function(dataset) {
                dataset.show(dataset.records[0]);
              }
            }, nextPage);
          }
        } else {
          dataset.revert({close: false});
          dataset.show(records[next]);
        }
      }
    },

    /**
     * Revert a record
     * 
     * @param object dataset
     * @param object [record]
     */
    revertRecord: function(dataset, record, params) {
      record = record || dataset.record;
      
      var $modal = $('.modal:visible'), close = true;

      if (typeof record === 'object') {
        if (0) {
          // Old way (simple objects)
          Object.assign(record, this.clone(dataset._record));
        } else {
          // New Way (complex objects)
          // Maintains the record's object & function members
          for (var prop in dataset._record) {
            if ($.isArray(dataset._record[prop]) || !/^(object|function)$/i.test(typeof dataset._record[prop])) {
              record[prop] = dataset._record[prop];
            }
          }
        }
      }
      
      dataset.editing = false;
      
      if (typeof params === 'object') {
        if (typeof params.close !== 'undefined') {
          close = params.close;
        }
      }
      
      if (close) {
        $modal.modal('hide');
      }
    },

    /**
     * Delete a record
     * 
     * @param object dataset
     * @param object record
     * @param object [e]
     */
    deleteRecord: function(dataset, record, e) {
      var self = this,
          url = self.plugin.urls.rest + dataset.singular + '.delete/';
  
      url = dataset.value('api.delete', url);

      if (typeof e === 'object') {
        e.stopPropagation();
      }

      if (typeof dataset === 'object' && record) {
        var name = self.name(dataset.singular, true);

        if (confirm('You are about to delete this ' + name + '.')) {
          $.ajax({
            url: url,
            type: 'post',
            dataType: 'json',
            data: {
              id: record.id,
              _model: dataset.singular
            },
            success: function(response) {
              self.handleResponse({
                response: response,
                close: function() {},
                complete: function() {},
                success: function(response) {
                  var id = response.successes[0],
                      index = undefined;

                  if (dataset.records.length > 1) {
                    dataset.records.forEach(function(item, i) {
                      if (item.id == id) {
                        index = i;
                      }
                    });

                    if (typeof index !== 'undefined') {
                     dataset.records.splice(index, 1);
                    }
                  } else {
                    self.doSearch(dataset);
                  }

                  // Only close loading modals
                  var $modal = $('.modal');

                  if ($modal.length && /loading/i.test($modal.attr('id'))) {
                    $('.modal').modal('hide');
                  }
                }
              });
            },
            error: function() {
              self.handleResponse({
                close: function() {},
                complete: function() {}
              });
            }
          });
        }
      }
    },

    /**
     * Duplicate a record
     * 
     * @param object dataset
     * @param object record
     * @param object [e]
     */
    duplicateRecord: function(dataset, record, e) {
      var self = this;

      if (typeof e === 'object') {
        e.stopPropagation();
      }

      if (typeof dataset === 'object' && record) {
        record = self.clone(record);
        record.id = null;

        self.events.record = record;
        self.doShow(dataset, record);
      }
    },

    /**
     * Show the record
     * 
     * @param object dataset
     * @param object record
     */
    showRecord: function(dataset, record) {
      var self = this,
          $modal = $('#cms-modal-' + dataset.singular);

      if (typeof record === 'object' && !(record instanceof MouseEvent)) {
        dataset.record = record;
      } else {
        dataset.record = self.clone(dataset.model);
      }

      dataset._record = self.clone(dataset.record);

      dataset.editing = true;
      
      if ($modal.length) {
        $modal.find('.nav li:first a').trigger('click');
        $modal.find('.input-search').val('');
        $modal.find('input[type="file"]').val('');
      
        // Before showing record
        if (typeof dataset.onBeforeShow === 'function') {
          dataset.onBeforeShow.apply(dataset, [record, $modal]);
        } else {
          var method = 'beforeShow' + self.getMethodSuffix(dataset.singular);

          if (typeof self[method] === 'function') {
            self[method].apply(self, [dataset, record, $modal]);
          } 
        }
        
        self.showModal($modal);
      }
    },

    /**
     * Save a account record
     * 
     * @param object dataset
     * @param object [e]
     */
    saveRecord: function(dataset, e) {
      var self = this,
          url = self.plugin.urls.rest + dataset.singular + '.save/',
          $modal = $('.modal:visible'),
          add = !dataset.record.id,
          $btn = $(e.target),
          $form = $btn.closest('.form'),
          $files = $form.find('[type="file"]'),
          tag = $btn[0].tagName,
          formdata = new FormData();

      url = dataset.value('api.save', url);

      // Get values from form editors
      $('textarea.form-editor', $modal).each(function(i, elem) {
        var $textarea = $(elem),
            id = $textarea.attr('id'),
            prop = id.replace(/^.*-/, ''),
            value = $textarea.trumbowyg('html');

        if (typeof dataset.record[prop] !== 'undefined') {
          dataset.record[prop] = value;
        }
      });

      // Prep data
      var data = {
        record: self.clone(dataset.record),
        _model: dataset.singular
      };
      
      // Before saving record
      if (typeof dataset.onBeforeSave === 'function') {
        data = dataset.onBeforeSave.apply(dataset, [data, $form]);
      } else {
        var method = 'beforeSave' + self.getMethodSuffix(dataset.singular);

        if (typeof self[method] === 'function') {
          data = self[method].apply(self, [dataset, data, $form]);
        } 
      }
      
      if (typeof data !== 'object' || !data) {
        return;
      }
      
      // Remove columns
      delete data.record.deleted_at;
      delete data.record.$;
      
      if (!/button/i.test(tag)) {
        $btn = $btn.closest('button');
        tag = $btn[0].tagName;
      }

      // Add files to formdata
      $files.each(function() {
        var $file = $(this),
            name = $file.attr('name');
        
        // Add files to formdata
        if ($file[0].files.length === 1) {
          formdata.append(name, $file[0].files[0]);
        }
      });

      // Convert data to formdata
      for (var prop in data) {
        var value = data[prop];
        value = typeof value === 'object' ? JSON.stringify(value) : value;
        formdata.append(prop, value);
      }

      data = formdata;

      self.disableBtn($btn, 'Saving...');

      $.ajax({
        url: url,
        type: 'post',
        dataType: 'json',
        data: data,
        processData: false,
        contentType: false,
        success: function(response) {
          self.handleResponse({
            response: response,
            close: function() {},
            complete: function() {
              self.enableBtn($btn);
            },
            success: function(response) {
              var record = response.successes[0];

              if (typeof dataset.prepRecord === 'function') {
                record = dataset.prepRecord(record);
              }
              
              if (add) {
                dataset.prepend(record);
              } else {
                dataset.record.$.update(record);
              }

              dataset.editing = false;
              
              // After saving record
              if (typeof dataset.onAfterSave === 'function') {
                dataset.onAfterSave.apply(dataset);
              } else {
                $('.modal').modal('hide');
              }
            }
          });
        },
        error: function() {
          self.handleResponse({
            close: function() {},
            complete: function() {
              self.enableBtn($btn);
            }
          });
        }
      });
    },

    /**
     * Export records
     * 
     * @param object dataset
     * @param object [params]
     */
    exportRecords: function(dataset, params) {
      var self = this,
          name = dataset.name(1),
          search = $('.search-' + dataset.plural + ' [name="search"]', self.$el).val(),
          url = self.plugin.urls.rest + dataset.plural + '.export/',
          $modal = $('#cms-modal-loading'),
          query = [];

      search = dataset.search_term ? dataset.search_term : search;

      url = dataset.value('api.export', url);

      // Merge custom params
      if (typeof params === 'object') {
        for (var param in params) {
          query.push(param + '=' + encodeURI(params[param]));
        }
      }

      // Handle Searches
      if (search) {
        search = encodeURI(search.trim());

        if (search.length) {
          query.push('_search=' + search);
        }
      }

      // Common
      query.push('_model=' + dataset.singular);
      query.push('_plural=' + dataset.plural);
      
      // Sorting
      if (dataset.sort && dataset.order) {
        query.push('_sort=' + dataset.sort);
        query.push('_order=' + dataset.order);
      }

      if (query.length) {
        url += !/\?/.test(url) ? '?' : '&'; 
        url += query.join('&');
      }

      $modal.find('.message').html('Exporting...');
      self.showModal($modal, {width: 250});

      $.ajax({
        url: url,
        type: 'get',
        dataType: 'json',
        success: function(response) {
          self.handleResponse({
            response: response,
            close: function() {
              $modal.modal('hide');
            },
            success: function(response) {
              var url = response.successes[0].url;

              location.href = url;
            }
          });
        },
        error: function() {
          self.handleResponse({
            close: function() {
              $modal.modal('hide');
            },
            error: 'Unable to export ' + name
          });
        }
      });
    }
  })
};