/**
 * Loading Component
 * This Vue component is used for displaying loading
 * 
 * @author Ed Rodriguez
 * @created 2019-04-29
 * @param string message
 */
Vue.component('loading', {
  props: {
    message: {
      default: 'Loading...',
      type: String
    }
  },

  template: `
    <div class="loading">
      <div class="animation"></div>
      <div class="message">
        {{ message }}
      </div>
    </div>
  `
});

/**
 * Search Component
 * This Vue component is used for all dataset search needs
 * 
 * @author Ed Rodriguez
 * @created 2019-04-29
 * @param object dataset The pagination's dataset
 * @param function [route] Optional route overriding
 * @param string [name] Optional dataset name
 * @param object [params] Optional page request params
 */
Vue.component('search', {
  props: ['dataset', 'route', 'name', 'params'],
  
  data() {
    return {
      route: 0,
      name: 'dataset',
      dataset: null,
      params: {}
    };
  },
  
  beforeMount() {
    this.init();
  },
  
  updated() {
    this.init();
  },
  
  methods: {
    /**
     * Sets up the search component
     */
    init() {
      let {dataset} = this;
      
      if (dataset) {
        this.name = !this.name && dataset.plural ? dataset.plural : this.name;
      }
    },
    
    /**
     * Process dataset searching
     */
    process() {
      let {dataset} = this,
          params = [this.params];
      
      if (!dataset.loading) {
        if (typeof this.route === 'function') {
          this.route.apply(dataset, params);
        } else if (typeof dataset.search === 'function') {
          dataset.search.apply(dataset, params);
        }
      }
    },
    
    /**
     * Trigger searching
     * 
     * @param MouseEvent e
     */
    search(e) {
      let {target} = e,
          {keyCode} = e,
          {dataset} = this,
          submit = keyCode === 13;
      
      if (/div/i.test(target.tagName)) {
        let value = $(target).text();
        
        dataset.search_term = value;
      } else {
        submit = true;
      }
            
      if (submit) {
        e.preventDefault();
        e.stopPropagation();

        this.process();
        
        return false;
      }
    },
    
    /**
     * Prevent line breaks
     * 
     * @param MouseEvent e
     */
    nobreaks(e) {
      if (e.keyCode === 13) {
        e.preventDefault();
        e.stopPropagation();
      }
    }
  },

  template: `
    <div :class="'search search-' + name">
      <div class="input-group">
        <div class="form-control input-search" contenteditable="true" autocomplete="false" @keypress="nobreaks" @keyup="search"></div>
        <span class="input-group-btn">
          <button class="btn btn-info btn-search" type="button" @click="search">
            <span class="glyphicon glyphicon-search"></span>
            <span class="hidden-xs d-none d-sm-inline">Search</span>
          </button>
        </span>
      </div>
      <slot></slot>
    </div>
  `
});

/**
 * Pagination Component
 * This Vue component is used for all dataset pagination needs
 * 
 * @author Ed Rodriguez
 * @created 2019-04-25
 * @param object dataset The pagination's dataset
 * @param function [route] Optional route overriding
 * @param string [name] Optional dataset name
 * @param object [params] Optional page request params
 */
Vue.component('pagination', {
  props: ['dataset', 'route', 'name', 'params'],
  
  data() {
    return {
      route: 0,
      name: 'dataset',
      page: 1,
      pages: 0,
      max: 10,
      first: 0,
      last: 0,
      dataset: null,
      less: false,
      more: false,
      params: {}
    };
  },
  
  beforeMount() {
    this.init();
  },
  
  updated() {
    this.init();
  },
  
  methods: {
    /**
     * Sets up and builds the pagination
     */
    init() {
      let {dataset, max} = this;
      
      if (dataset) {
        this.name = !this.name && dataset.plural ? dataset.plural : this.name;
        
        let page = dataset.page,
            pages = dataset.pages,
            middle = Math.ceil(max / 2),
            before = middle - 1,
            after = (max - before) - 1,
            last_first = (pages - max) + 1;

        last_first = last_first <= 0 ? 1 : last_first;

        // Real Middle
        middle = page < middle ? middle : page;

        let first = middle - before,
            last = middle + after;

        // Last First
        first = first > last_first ? last_first : first;

        // Last Last
        last = last > pages ? 0 : last;

        let less = first > 1,
            more = (pages - max) >= first;
            
        first = first - 1;
        
        this.first = first;
        this.last = last;
        this.page = page;
        this.pages = pages;
        this.less = less;
        this.more = more;
      }
    },
    
    /**
     * Processes page requests through a route function or a dataset component
     */
    process() {
      let {dataset} = this,
          params = [this.page, dataset, this.params];
      
      if (!dataset.loading) {
        if (typeof this.route === 'function') {
          this.route.apply(dataset, params);
        } else if (dataset.component) {
          dataset.component.doPage.apply(dataset, params);
        }
      }
    },
    
    /**
     * Loads the previous page
     * 
     * @param MouseEvent e
     */
    prev(e) {
      e.preventDefault();
      e.stopPropagation();
      
      this.page--;
      this.page = this.page <= 0 ? 1 : this.page;

      this.process();
    },
    
    /**
     * Loads the next page
     * 
     * @param MouseEvent e
     */
    next(e) {
      e.preventDefault();
      e.stopPropagation();
      
      this.page++;
      this.page = this.page > this.pages ? this.pages : this.page;

      this.process();
    },
    
    /**
     * Loads the specified page
     * 
     * @param integer page
     * @param MouseEvent e
     */
    load(page, e) {
      e.preventDefault();
      e.stopPropagation();

      this.page = page;

      this.process();
    },
    
    /**
     * Retrieves the classes for each page link
     * 
     * @param integer page
     * @return string
     */
    classes(page) {
      let classes = 'page';
      
      if (this.page === page) {
        classes += ' selected';
      }
      
      return classes;
    }
  },
  
  template: `
    <div v-if="dataset && dataset.pages > 1" :class="'text-center pagination-' + name">
      <div class="pagination-pages-wrap">
        <ul class="pagination pagination-pages">
          <li class="prev-next prev" @click="prev">
            <a><i class="fa fa-chevron-left"></i></a>
          </li>

          <li class="page etc" v-if="less" @click="load(1, $event)"><a>...</a></li>
  
          <li v-for="num in max" :class="classes(first + num)" v-if="(first + num) <= pages">
            <a :data-page="first + num" @click="load(first + num, $event)">{{ first + num }}</a>
          </li>

          <li class="page etc" v-if="more" @click="load(pages, $event)"><a>...</a></li>
  
          <li class="prev-next next" @click="next">
            <a><i class="fa fa-chevron-right"></i></a>
          </li>
        </ul>
      </div>
      <br />
    </div>
  `
});

/**
 * Previous Next Component
 * This Vue component is used for all dataset previous/next record needs
 * 
 * @author Ed Rodriguez
 * @created 2020-08-20
 * @param object dataset The dataset
 * @param string [name] Optional dataset name
 */
Vue.component('prevnext', {
  props: ['dataset', 'name'],
  
  data() {
    return {
      name: 'dataset',
      dataset: null
    };
  },
  
  beforeMount() {
    this.init();
  },
  
  updated() {
    this.init();
  },
  
  methods: {
    /**
     * Sets up the prevnext component
     */
    init() {
      let {dataset} = this;
      
      if (dataset) {
        this.name = !this.name && dataset.plural ? dataset.plural : this.name;
      }
    },
    
    /**
     * Trigger previous
     * 
     * @param MouseEvent e
     */
    prev(e) {
      let {dataset} = this;
      
      e.preventDefault();
      e.stopPropagation();

      dataset.prev();
    },

    /**
     * Trigger next
     * 
     * @param MouseEvent e
     */
    next(e) {
      let {dataset} = this;
      
      e.preventDefault();
      e.stopPropagation();

      dataset.next();
    }
  },

  template: `
    <div :class="'prevnext prevnext-' + name">
      <a href="#" @click="prev" title="Previous">&lt;</a>
      <a href="#" @click="next" title="Next">&gt;</a>
      <slot></slot>
    </div>
  `
});