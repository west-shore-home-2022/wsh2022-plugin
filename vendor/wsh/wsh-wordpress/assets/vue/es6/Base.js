var WSH = WSH || {},
    plugin = WSH;

/**
 * The Vue Base Class
 */
plugin.Base = {
  search: null,
  term: null,
  alertTimeout: null,
  debugLevel: 3,
  debugLevels: [
    'error',
    'warn',
    'debug',
    'log',
    'info'
  ],

  paginators: {},

  mounted: function() {},

  beforeMount: function() {                
    var self = this;

    if (self.constructor.name === 'VueComponent') {
      self.$root.log('Component Created:', self.$options.name);
      self.setup();
    } else {
      self.init();
    }
  },

  created: function() {},

  beforeUpdate: function() {},
  
  updated: function() {
    var self = this;

    if (self.constructor.name === 'VueComponent') {
      self.$root.log('Component Updated:', self.$options.name);
      self.setup();
    }
  },

  filters: {
    formatDate: function(date, pattern) {
      pattern = pattern || 'MM/DD/YY';

      return moment(date).format(pattern);
    },

    formatName: function(str) {
      if (typeof str === 'string') {
        str = str.replace(/(_|-)/, ' ', name);
        str = str.replace(/\b\w/g, l => l.toUpperCase());
      }

      return str;  
    },

    /**
     * Adds commas to numbers.
     *
     * @param number|string num The number to add commas to.
     * @return string A comma seperated numerical string.
     */
    formatNumber: function(num, decimals) {
      if (typeof num !== 'string' && typeof num !== 'number') {
        return num;
      }
      
      decimals = decimals || 0;
      num = typeof num === 'number' ? String(num) : num;
      num += '';

      var x = num.split('.'),
          x1 = x[0],
          x2 = x.length > 1 ? x[1] : '',
          rgx = /(\d+)(\d{3})/;

      // Add Commas
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }

      var result = x1;

      // Append decimal value
      if (typeof decimals === 'number' && decimals > 0) {
        while (x2.length < decimals) {
          x2 += '0';
        }
        
        result += '.' + x2.substr(0, decimals);
      }
      
      return result;
    },
    
    formatPhone: function(phone, pattern) {
      pattern = pattern || '($1) $2-$3';
      
      var val = String(phone);
      val = val.trim();

      if (pattern && val) {
        phone = phone.replace(/^\+1/, '');
        phone = phone.replace(/[^0-9]/g, '');
        
        if (/^\d+$/.test(phone)) {
          if (phone.length === 11 && /^1/.test(phone)) {
            phone = phone.replace(/^1/, '');
          }
          
          if (phone.length === 10) {
            val = phone.replace(/^(\d{3})(\d{3})(\d{4})$/, pattern);
          }
        }
      }
      
      return val;
    },
    
    formatTime: function(date, pattern) {
      pattern = pattern || 'h:mma';

      return moment(date).format(pattern);
    },

    /**
     * Determines if a date occurs after an amount of time
     *  
     * @param string date
     * @param integer amount
     * @param string type
     * @returns boolean
     */
    after: function(date, amount, type) {
      return typeof moment === 'function'
              && typeof date === 'string' 
              && typeof amount === 'number'
              && typeof type === 'string'
              && /^\d{4}-\d{2}-\d{2}/.test(date)
              && moment(date).isAfter(moment().subtract(amount, type));
    },
    
    capitalize: function(str) {
      return str.replace(/\b\w/g, l => l.toUpperCase());
    },
    
    crop: function(str, max, elipses) {
      if (typeof str === 'string' && typeof max === 'number' && max > 0) {
        var length = str.length;

        if (length > max) {
          str = str.substring(0, max - 1);

          if(typeof elipses !== 'undefined'){
            str = str.trim() + '...';
          }
        }
      }

      return str;
    },

    nl2br: function(str) {
      return str.replace(/\n/g, '<br />');
    },
    
    replace: function(str, search, replace) {
      var regexp = typeof search === 'string' ? new RegExp(search, 'gi') : search;
      
      return str.replace(regexp, replace, str);
    }
  },

  methods: {
    init: function() {
      var self = this;

      // Set Tabs
      self.$router.options.routes.forEach(function(route) {
        if (typeof route.tab === 'boolean' && route.tab) {
          self.tabs.push(route);
        }
      });

      // Set Page
      if (self.$route.path === '/') {
        self.go({
          page: self.page,
          route: self.type
        });
      } else {
        self.fetchData();
      }
    },

    clone: function(data) {
      return JSON.parse(JSON.stringify(data));
    },

    disableBtn: function(btn, text) {
      var $btn = btn instanceof jQuery ?  btn : $(btn),
          $text = $btn.find('.text');

      if (typeof $text.data('disable') !== 'undefined') {
        text = $text.data('disable');
      } else {
        text = text || 'Save';
      }

      $btn.find('.icon').show();
      $text.html(text);
      $btn.prop('disabled', true);
    },

    enableBtn: function(btn, text) {
      var $btn = btn instanceof jQuery ?  btn : $(btn),
          $text = $btn.find('.text');

      if (typeof $text.data('enable') !== 'undefined') {
        text = $text.data('enable');
      } else {
        text = text || 'Save';
      }

      $btn.find('.icon').hide();
      $text.html(text);
      $btn.prop('disabled', false);
    },

    openModal: function($modal, opts) {
      return this.showModal($modal, opts);
    },
    
    /**
     * Shows a static modal
     * 
     * @param jQuery $modal
     * @param object [opts] Custom options
     */
    showModal: function($modal, opts) {
      if (typeof $modal === 'string') {
        $modal = $($modal);
      }
      
      if ($modal && $modal instanceof jQuery && $modal.length === 1) {
        // Show modal
        $modal.modal({
          backdrop: 'static',
          keyboard: false
        }).on('mousedown', function() {
          $modal.data('bs.modal').ignoreBackdropClick = true;
        });
        
        // Clear any saved plugin data
        $('input[id], select[id], textarea[id]', $modal).each(function(i, elem) {
          var $elem = $(elem),
              id = $elem.attr('id');
          
          if (typeof plugin.Data.Plugins[id] !== 'undefined') {
            $elem.val('');

            delete plugin.Data.Plugins[id];
          }
        });
        
        // Customize modal with opts
        if (typeof opts === 'object') {
          if (typeof opts.width !== 'undefined') {
            opts.width = String(opts.width);
            opts.width = /^\d+$/.test(opts.width) ? opts.width + 'px' : opts.width;
            
            $('.modal-dialog', $modal).width('auto').css('max-width', opts.width);
          }
        }
      }
      
      return $modal;
    },
    
    /**
     * Uploads a file using chunks
     * 
     * @param object options
     */
    uploadFile: function(options) {
      var self = this,
          $modal = $('#cms-modal-loading'),
          defaults = {
            file: undefined,
            rest_url: '/',
            size: 1048576, //1024*1024; size of one chunk
            progress: function(percent) {
              percent = Math.round(percent);
              
              //console.log('Progress: ' + percent + '%');
              
              if ($modal.length) {
                var $message = $modal.find('.message');
                $message.html('Uploading: ' + percent + '%');
              }
            },
            success: function(tmp) {
              console.log('Temp File: ' + tmp);
            },
            error: function(errors) {
              $.each(errors, function(i, msg) {
                if (typeof msg === 'string') {
                  self.error(msg);
                }
              });
            }
          };
      
      var opts = $.extend(defaults, options),
          tmp = undefined;
      
      if (opts.file) {
        var loaded = 0,
            progress = 0,
            name = opts.file.name,
            total = opts.file.size, // total size of file
            start = 0, // starting position
            reader = new FileReader(),
            blob = opts.file.slice(start, opts.size); // a single chunk in starting of step size
        
        if ($modal.length) {
          var $message = $modal.find('.message');
          $message.html('Uploading...');

          $modal.modal({
            backdrop: 'static',
            keyboard: false
          }).on('mousedown', function() {
            $modal.data('bs.modal').ignoreBackdropClick = true;
          });
        }

        reader.readAsBinaryString(blob); // reading that chunk. when it read it, onload will be invoked

        reader.onload = function(e) {            
          var data = {
            tmp: tmp,
            name: name,
            size: opts.size,
            chunk: btoa(reader.result)
          };

          $.ajax({
            url: opts.rest_url + 'upload.chunk/',
            type: 'POST',
            data: data,
            dataType: 'json'
          }).done(function(res) {
            var errors = [];
            
            if (typeof res.errors === 'object') {
              errors = res.errors;
            }
          
            if (!errors.length && (typeof res.successes !== 'object' || !res.successes.length)) {
              errors.push('Unable to upload file');
            }
          
            if (errors.length) {
              opts.error.apply(this, [errors]);
            } else {
              var success = res.successes[0];
              
              tmp = success.tmp;
              
              loaded += opts.size;

              progress = (loaded / total) * 100;

              if (loaded <= total) {
                opts.progress.apply(self, [progress]);
                
                blob = opts.file.slice(loaded, loaded + opts.size);
                reader.readAsBinaryString(blob);
              } else {
                loaded = total;
                
                opts.progress.apply(self, [100]);
                
                if ($modal.length) {
                  $modal.modal('hide');
                }
                
                opts.success.apply(self, [tmp]);
              }
            }
          }).fail(function() {
            var errors = [];
            
            errors.push('Failed to upload file');
            
            opts.error.apply(this, [errors]);
          });  
        };
          
      } else {
        opts.success.apply(self, []);
      }
    },
    
    findElement: function(name) {
      var self = this,
          $element = $();
  
      if (/^(alerts)$/i.test(name)) {
         $element = $('.modal:visible .alerts');

        // Alerts override
        if (typeof self.$alerts !== 'undefined' && self.$alerts instanceof jQuery && self.$alerts.length) {
          $element = self.$alerts;
        }

        // Alerts fallback
        if (!$element.length) {
          $element = $('.cms .alerts:first');
        }
      }
      
      return $element;
    },
    
    handleResponse: function(options) {
      var self = this,
          defaults = {alertTimeout: undefined},
          opts = $.extend(defaults, options),
          $modal = $('.modal:visible'),
          res = {
            errors: [],
            successes: [],
            warnings: [],
            infos: []
          };

      // Reset alerts
      var $alerts = self.findElement('alerts');
      
      $alerts.hide(function() {
        $alerts.html('');
      });
      
      // Alerts timeout
      if (typeof opts.alertTimeout === 'number') {
        self.alertTimeout = opts.alertTimeout;
      }
      
      // Complete
      if (typeof opts.complete !== 'function') {
        opts.complete = function() {};
      }

      // Success
      if (typeof opts.success === 'string') {
        res.successes.push(opts.success);
      }
      if (typeof opts.success !== 'function') {
        opts.success = function() {};
      }

      // Error
      if (typeof opts.error === 'string') {
        res.errors.push(opts.error);
      }
      if (typeof opts.error !== 'function') {
        opts.error = function() {};
      }

      // Warning
      if (typeof opts.warning === 'string') {
        res.warnings.push(opts.warning);
      }
      if (typeof opts.warning !== 'function') {
        opts.warning = function() {};
      }

      // Info
      if (typeof opts.info === 'string') {
        res.infos.push(opts.info);
      }
      if (typeof opts.info !== 'function') {
        opts.info = function() {};
      }

      // Response
      if (typeof opts.response !== 'undefined') {
        if (typeof opts.response !== 'object') {
          res.errors.push('Unable to parse server response');
        } else {
          if (typeof opts.response.code === 'string') {
            if (opts.response.code === 'rest_cookie_invalid_nonce') {
              res.errors.push('Your session has expired');
              
              var $modal = $('#cms-modal-loading');
              
              // Try reloading page
              if ($modal.length) {
                self.error('Your session has expired');
                
                setTimeout(function() {
                  var $message = $modal.find('.message');
                  $message.html('Reloading page...');

                  self.showModal($modal);
                  cms.reload();
                }, 2000);
                
                return;
              }
            }
          } else {
            for (var prop in opts.response) {
              res[prop] = res[prop].concat(opts.response[prop]);
            }
          }
        }
      }

      var callback = function() {
        opts.complete.apply(this, [res]);

        /* Show Alerts
        -----------------------------------------*/
        
        // Successes
        if (res.successes.length) {
          $.each(res.successes, function(i, msg) {
            if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
              self.success(msg);
            }
          });
        }
        
        // Infos
        if (res.infos.length) {
          $.each(res.infos, function(i, msg) {
            if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
              self.info(msg);
            }
          });
        }

        // Warnings
        if (res.warnings.length) {
          $.each(res.warnings, function(i, msg) {
            if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
              self.warning(msg);
            }
          });
        }

        // Errors
        if (res.errors.length) {
          $.each(res.errors, function(i, msg) {
            if (typeof msg === 'string' && !/^\d+$/.test(msg)) {
              self.error(msg);
            }
          });
        }
        
        delete self.alertTimeout;
                    
        /* Apply Callbacks
        -----------------------------------------*/
        
        // Infos
        if (res.infos.length) {
          opts.info.apply(this, [res]);
        }

        // Warnings
        if (res.warnings.length) {
          opts.warning.apply(this, [res]);
        }

        // Errors
        if (res.errors.length) {
          opts.error.apply(this, [res]);
        }
        
        // Successes
        if (res.successes.length) {
          opts.success.apply(this, [res]);
        }
      };

      if ($modal.length) {
        setTimeout(function() {
          // Close loading modals
          $('#cms-modal-loading').modal('hide');
          
          if (typeof opts.close === 'undefined') {
            $modal.modal('hide');
          }

          callback.apply(this);
        }, 1000);
      } else {
        callback.apply(this);
      }
    },

    console: function() {
      var self = this;

      if (arguments.length === 2) {
        let type = arguments[0],
            msg = arguments[1][0],
            args = Array.prototype.slice.call(arguments[1]);

        if (typeof console[type] === 'function'
          && (plugin.Base.debugLevels.indexOf(type) + 1) <= plugin.Base.debugLevel) {

          console[type].apply(console, args);
        }

        var messages = messages = $.isArray(msg) ? msg : [msg],
            $alerts = self.findElement('alerts'),
            alerts = {
              debug: undefined,
              log: undefined,
              info: 'info',
              warn: 'warning',
              error: 'danger',
              success: 'success'
            };
        
        if ($alerts.length && typeof alerts[type] !== 'undefined') {
          messages.forEach(function(msg, i) {
            var html = '\
              <div class="alert alert-' + alerts[type] + ' alert-dismissable" style="display:none">\
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>\
                <strong>' + self.$options.filters.capitalize(type) + '!</strong> ' + msg + '\
              </div>\
            ';

            var $elem = $(html);

            $alerts.show().append($elem);

            $elem = $alerts.find('> .alert-' + alerts[type] + ':last');

            $elem.slideDown();
            
            if (/^(undefined|number)$/i.test(typeof self.alertTimeout)) {
              var alertTimeout = typeof self.alertTimeout === 'number' ? self.alertTimeout : 4000;

              if (alertTimeout > 0) {
                setTimeout(function() {
                  $elem.slideUp(function() {
                    $elem.remove();

                    if (!$alerts.children().length) {
                      $alerts.hide();
                    }
                  });
                }, alertTimeout);
              }
            }
          });

          // Scroll to alerts if modal visible
          if ($alerts.closest('.modal:visible').length) {
            if (/^(auto|scroll)$/i.test($('.modal').css('overflowY'))) {
              $('.modal').animate({scrollTop: 0});
            } else {
              $('html, body').animate({scrollTop: 0});
            }
          }
        }
      }
    },

    debug: function() { this.console('debug', arguments); },
    log: function() { this.console('log', arguments); },
    error: function() { this.console('error', arguments); },
    info: function() { this.console('info', arguments); },
    warning: function() { this.console('warn', arguments); },
    warn: function() { this.console('warn', arguments); },
    success: function() { this.console('success', arguments); },

    settings: function() {
      var self = this;

      plugin.Base.search = $('.search select[name="search"]', self.$el).val();
      plugin.Base.term = $('.search input[name="term"]', self.$el).val();
    },

    getType: function() {
      return this.$options.filters.capitalize(this.$options.type);
    },

    setup: function() {
      var self = this;

      self.$root.tab = this;

      self.log('Component Setup:', this.$options.name);

      if (plugin.Base.term) {
        $('.search select[name="search"]').val(plugin.Base.search);
        $('.search input[name="term"]').val(plugin.Base.term);
      }

      if (typeof self.afterupdate === 'function') {
        self.log('afterupdate');

        self.afterupdate.apply(self);
        self.afterupdate = undefined;
      }
    },

    showLoading: function(msg) {
      msg = msg || 'Loading...';

      this.section = 'loading';
      this.loading = msg;
    },

    debounceSearch: _.debounce(function(e) {
      var self = this;
      self.doSearch();
    }, 1250),

    /**
     * Triggers a specified route
     *
     * Examples:
     * self.go(1);
     * self.go({
     *    page: 1,
     *    company: 1234
     * });
     * self.go({
     *    page: 2,
     *    route: 'reports'
     * });
     *
     * @param options
     * @param route
     */
    go: function(options) {
      options = typeof options !== 'object' ? {page: options} : options;

      var self = this,
          route = typeof options.route === 'undefined' ? self.type : options.route;

      // Get route by other methods
      if (typeof route !== 'object') {
        var routes = self.$router.options.routes;

        if (typeof route === 'string') {
          var found = false;

          $.each(routes, function(i, item) {
            var regexp = new RegExp(route, 'i');

            if (!found && regexp.test(item.name)) {
              route = item;
              found = true;
            }
          });
        } else if (typeof route === 'number' && routes.length >= route) {
          route = routes[route - 1];
        }
      }

      if (typeof route === 'object') {
        var uri = route.path,
            items = uri.split(/\//);

        // Replace dynamic uri placeholders
        $.each(items, function(i, item) {
          if (/^:/.test(item)) {
            var value = item,
                found = false,
                opts = $.extend(plugin.Base, options),
                props = item.replace(/^:/i, '').split(/_/),
                data = opts;

            $.each(props, function(i, prop) {
              if (typeof data[prop] !== 'undefined') {
                data = data[prop];
              }
            });

            if (typeof data === 'object' && /^(number|string)$/i.test(typeof data.id)) {
              value = data.id;
            }

            if (/^(number|string|boolean)$/i.test(typeof data)) {
              value = data;
            }

            var regexp = new RegExp(item, 'i');
            uri = uri.replace(regexp, value);
          }
        });

        self.settings();
        self.$router.push(uri);
      }
    },

    setPage: function(e) {
      var self = this,
          $btn = $(e.target),
          page = $btn.data('page');

      self.go(page);
    },

    prev: function(e) {
      var self = this;
      self.page--;
      self.page = self.page <= 0 ? 1 : self.page;

      self.go(self.page);
    },

    next: function(e) {
      var self = this;
      self.page++;
      self.page = self.page > self.pages ? self.pages : self.page;

      self.go(self.page);
    },

    showTab: function(route) {
      var self = this,
          tab = self.tab,
          section = route.name.toLowerCase(),
          isTab = section === tab.$options.name;

      if (isTab) {
        if (tab.section !== section) {
          tab.section = section;
          tab.page = 1;

          if (tab.chart) {
            tab.chart = tab.chart.destroy();
          }
        }
      } else {
        $('.search input[name="term"]', self.$el).val('');

        plugin.Base.search = undefined;
        plugin.Base.term = undefined;

        self.go({
          page: 1,
          route: route
        });
      }
    },

    updateTab: function() {
      if (this.tab.page !== 'loading') {
        this.tab.page = this.tab.$options.name;
      }
    },
    
    /**
     * Formats a method suffix name
     * 
     * @param {type} type
     * @returns {String}
     */
    getMethodSuffix: function(type) {
      type = type.replace(/(_|-)/, ' ');

      var self = this,
          name = self.$options.filters.capitalize(type);

      return name.replace(/\s/, '');
    },

    /**
     * Adds sorting to a table by its data
     */
    tableSort: function(e) {
      var component = e.data.component,
          dataset = e.data.dataset;
      
      var method = 'doSearch' + component.getMethodSuffix(dataset.plural);
      
      e.preventDefault();
      e.stopPropagation();
      
      if (typeof dataset === 'object' 
        && typeof dataset.sort !== 'undefined'
        && typeof dataset.order !== 'undefined') {
  
        var $th = $(e.target),
            $thead = $th.closest('thead'),
            $icon = $('<i class="glyphicon"></i>'),
            order = 'ASC',
            arrow = 'down';

        if ($th.length) {
          var tag = $th[0].tagName;
          
          if (!/^(th)$/i.test(tag)) {
            $th = $th.closest('th');
            
            tag = $th[0].tagName;
            console.log(tag);
          }
          
          var sort = $th.data('sort');
          
          if (dataset.sort === sort) {
            order = dataset.order === 'ASC' ? 'DESC' : 'ASC';
          }

          arrow = order !== 'ASC' ? 'up' : arrow;
          $icon.addClass('glyphicon-chevron-' + arrow); 

          $('.glyphicon-chevron-up, .glyphicon-chevron-down', $thead).remove();
          $th.prepend($icon);

          dataset.sort = sort;
          dataset.order = order;

          if (typeof component[method] === 'function') { 
            component[method]();
          } else {
            component.doSearch(dataset);
          }
        }
      }
    }
  }
};