(function($) {
  $(function() {
    var component = 'tools',
        plugin = WSH;
    
    var data = {
      plugin: plugin,
      pagination: component,
      rule: {
        id: null,
        name: '',
        search: '',
        replace: ''
      },
      rule_revert: null,
      rule_model: {
        id: null,
        name: '',
        search: '',
        replace: ''
      },
      rules: [],
      mode: {
        attribute: ''
      },
      attribute: {
        id: null,
        name: '',
        value: ''
      },
      attribute_revert: null,
      attribute_model: {
        id: null,
        name: '',
        value: ''
      },
      attributes: [],
      assets: {
        singular: 'asset',
        api: {
          get: plugin.urls.rest + 'records/',
          export: plugin.urls.rest + 'records.export/',
          delete: plugin.urls.rest + 'record.delete/',
          save: plugin.urls.rest + 'record.save/',
          active: plugin.urls.rest + 'record.active/'
        },
        model: {
          id: null,
          attributes: [],
          action: 'replace',
          type: 'frontend',
          minify: 0,
          active: 1
        },
        prepRecords: function(records) {
          var self = this;

          records.forEach((record, i) => {
            record = self.prepRecord(record);
          });

          return records;
        },
        prepRecord: function(record) {
          record.attributes = record.attributes ? JSON.parse(record.attributes) : [];

          return record;
        },
        onBeforeShow: function(data, $form) {
          var self = this;
          
          self.component.mode.attribute = '';
          self.component.attribute = Object.assign({}, self.component.attribute_model);
          self.component.attributes = data && data.attributes ? data.attributes : [];

          return data; 
        },
        onBeforeSave: function(data, $form) {
          var self = this;
          
          data.record.attributes = $.isArray(self.component.attributes) ? JSON.stringify(self.component.attributes) : [];

          return data; 
        }
      },
      tracking: {
        singular: 'tracking',
        api: {
          get: plugin.urls.rest + 'records/',
          export: plugin.urls.rest + 'records.export/',
          delete: plugin.urls.rest + 'record.delete/',
          save: plugin.urls.rest + 'record.save/',
          active: plugin.urls.rest + 'record.active/'
        },
        model: {
          id: null,
          active: 1
        }
      }
    };

    //----------------------------------
    
    if ($('#tmpl-' + component).length) {
      var Component = Vue.extend({
        name: component,
        mixins: [plugin.Component],
        template: '#tmpl-' + component,

        data: function() {
          return data;
        },
        
        updated: function() {
          var self = this;
          
          self._updated();
          
          // Select first tab
          if (!$('.pane-top .nav-tabs li.active').length) {
            $('.pane-top .nav-tabs li:first a', self.$el).trigger('click');
          }
        },
        
        methods: {
          init: function() {
            var self = this;

            self._init();
            
            // Handle form submission page responses
            if (typeof logs === 'object') {
              setTimeout(() => {
                self.handleResponse({
                  response: logs
                });
              }, 1000);
            }
            
            if (typeof search_replace === 'object') {
              self.rules = search_replace;
            }
            
            self.assets.search();
            self.tracking.search();
          },
          
          addRule: function() {
            var self = this,
                $modal = $('#cms-modal-rule').modal();
            
            self.rule = Object.assign({}, self.rule_model);
            
            self.openModal($modal);
          },
          
          editRule: function(rule) {
            var self = this,
                $modal = $('#cms-modal-rule').modal();
            
            self.rule = rule;
            self.rule_revert = Object.assign({}, rule);
            
            self.openModal($modal);
          },
          
          deleteRule: function(rule) {
            var self = this;
              
            if (confirm('You are about to delete this rule.')) {
              self.rules = self.rules.filter((item) => {
                return item.id !== rule.id;
              });
            }
          },
          
          saveRule: function() {
            var self = this,
                rule = self.rule,
                $modal = $('#cms-modal-rule').modal();
            
            // Validate Rule Name
            rule.name = rule.name.trim();
            
            if (!rule.name.length) {
              self.error('Name is required');
              return;
            }
            
            // Validate Rule Search
            rule.search = rule.search.trim();
            
            if (!rule.search.length) {
              self.error('Search is required');
              return;
            }
            
            // Add Rule
            if (!rule.id) {
              rule.id = (new Date).getTime();
              self.rules.push(rule);
            }
            
            // Edit Rule
            else {
              self.rules.forEach((item) => {
                if (item.id === rule.id) {
                  item = $.extend(item, rule);
                }
              });
            }
            
            $modal.modal('hide');
          },
          
          cancelRule: function() {
            var self = this,
                $modal = $('#cms-modal-rule').modal();
              
            if (self.rule.id) {
              self.rule = Object.assign({}, self.rule_revert);
              
              self.rules.forEach((item) => {
                if (item.id === self.rule.id) {
                  item = $.extend(item, self.rule);
                }
              });
            }
            
            $modal.modal('hide');
          },
          
          savePagespeed: function(e) {
            var self = this,
                $btn = $(e.target),
                data = {
                  settings: {
                    pagespeed: {
                      remove_all_scripts: '0',
                      remove_all_styles: '0',
                      remove_all_images: '0',
                      remove_body_html: '0',
                      remove_head_html: '0',
                      search_replace: []
                    }
                  }
                };

            self.rules.forEach((rule) => {
              var item = {};
              
              item.id = rule.id;
              item.name = rule.name;
              item.search = rule.search;
              item.replace = rule.replace;
              
              data.settings.pagespeed.search_replace.push(item);
            });
            
            data.settings = $.extend(data.settings, $btn.closest('.form-pagespeed').serializeElement());
            
            console.log('Settings:', data.settings);

            data.settings = btoa(JSON.stringify(data.settings));
              
            $('.modal').modal('hide');

            var $modal = $('#cms-modal-loading');
            $modal.find('.message').html('Saving...');

            self.showModal($modal);

            $.ajax({
              url: self.plugin.urls.rest + 'settings.save/',
              type: 'post',
              dataType: 'json',
              data: data,
              success: function(response) {
                self.handleResponse({
                  response: response,
                  success: 'Settings updated'
                });
              },
              error: function() {
                self.handleResponse({
                  error: 'Unable to update settings'
                });
              }
            });
          },
          addAttribute: function() {
            var self = this;
            
            self.attribute = Object.assign({}, self.attribute_model);
            
            self.mode.attribute = 'add';
          },
          
          editAttribute: function(attribute) {
            var self = this;
            
            self.attribute = attribute;
            self.attribute_revert = Object.assign({}, attribute);
            
            self.mode.attribute = 'edit';
          },
          
          deleteAttribute: function(attribute) {
            var self = this;
              
            if (confirm('You are about to delete this attribute.')) {
              self.attributes = self.attributes.filter((item) => {
                return item.id !== attribute.id;
              });
            }
          },
          
          saveAttribute: function() {
            var self = this,
                attribute = self.attribute;
            
            // Validate Attribute Name
            attribute.name = attribute.name.trim();
            
            if (!attribute.name.length) {
              self.error('Name is required');
              
              return;
            }
            
            // Validate Attribute Search
            attribute.value = attribute.value.trim();
            
            if (!attribute.value.length) {
              self.error('Value is required');
              
              return;
            }
            
            // Add Attribute
            if (!attribute.id) {
              attribute.id = (new Date).getTime();
              self.attributes.push(attribute);
            }
            
            // Edit Attribute
            else {
              self.attributes.forEach((item) => {
                if (item.id === attribute.id) {
                  item = $.extend(item, attribute);
                }
              });
            }
            
            self.mode.attribute = '';
          },
          
          cancelAttribute: function() {
            var self = this;
              
            if (self.attribute.id) {
              self.attribute = Object.assign({}, self.attribute_revert);
              
              self.attributes.forEach((item) => {
                if (item.id === self.attribute.id) {
                  item = $.extend(item, self.attribute);
                }
              });
            }
            
            self.mode.attribute = '';
          }
        }
      });
      
      Component = new Component().$mount('#tab-' + component);
      
      var name = Component.getMethodSuffix(component);
      plugin.Apps = plugin.Apps || {};
      plugin.Apps[name] = Component;
    }
  });
})(jQuery);