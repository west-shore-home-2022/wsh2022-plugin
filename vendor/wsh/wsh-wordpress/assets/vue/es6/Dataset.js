var WSH = WSH || {},
    plugin = WSH;

/**
 * The Dataset Class
 * 
 * @param object component
 * @param object dataset
 */
plugin.Dataset = function(component, dataset) {
  var self = this;
  
  var options = {
    page: 1,
    count: 30,
    total: 0,
    pages: null,
    records: [],
    record: null,
    cache: {
      enabled: false,
      expire: 60, // Minutes (1 Hour)
      type: 'localStorage',
      memory: {}
    },
    _record: null,
    all: null,
    sort: null,
    order: null,
    loading: false,
    editing: false,
    search_term: null,
    component: component,

    model: {
      id: null,
      active: 1
    },

    search: function(params, page) { component.doSearch(self, params, page); },
    add: function() { component.doShow(self); },
    prev: function() { component.doPrev(self); },
    next: function() { component.doNext(self); },
    edit: function(record) { component.doShow(self, record); },
    show: function(record) { component.doShow(self, record); },
    delete: function(record) { component.doDelete(self, record); },
    duplicate: function(record) { component.doDuplicate(self, record); },
    toggle: function(record) { component.doActive(self, record); },
    status: function(record) { component.doStatus(self, record); },
    export: function(params) { component.doExport(self, params); },
    revert: function(params) { component.doRevert(self, null, params); },
    save: function(e) { component.doSave(self, e); },
    name: function(lowercase) { return component.$options.filters.name(self.plural, lowercase); },
    title: function(lowercase) { return component.$options.filters.name(self.singular, lowercase); },
    
    prepend: function(record) { self.addRecord(record, 'prepend'); },
    append: function(record) { self.addRecord(record, 'append'); },
    
    addRecord: function(record, action) {
      action = action || 'prepend';
      
      if (typeof record === 'object') {
        if (action === 'prepend') {
          self.records.unshift(new plugin.Record(component, self, record));
        } else {
          self.records.push(new plugin.Record(component, self, record));
        }
      }
    },
    
    setRecords: function(items) {
      var records = [];
      
      if ($.isArray(items)) {
        $.each(items, function(i, item) {
          records.push(new plugin.Record(component, self, item));
        });
      }
      
      self.records = records;
    },
    
    value: function(props, _default) {
      _default = _default || null;
      
      var value = _default;
      
      props = props.split('.');
      
      var found = true, temp = self;
      
      props.forEach(function(prop, i) {
        if (found) {
          if (typeof temp[prop] === 'undefined') {
            found = false;
          } else {
            temp = temp[prop];
          }
        }
      });
      
      if (found) {
        value = temp;
      }
      
      return value;
    }
  };
  
  $.extend(true, this, options, dataset);

  this.record = component.clone(this.model);

  for (var prop in this) {
    component.$set(this, prop, this[prop]); 
  }
};