(function($) {
  $(function() {
    var component = 'tracking',
        plugin = WSH;
    
    var data = {
      plugin: plugin,
      pagination: component,
      tracking: {
        singular: 'tracking',
        api: {
          get: plugin.urls.rest + 'records/',
          export: plugin.urls.rest + 'records.export/',
          delete: plugin.urls.rest + 'record.delete/',
          save: plugin.urls.rest + 'record.save/',
          active: plugin.urls.rest + 'record.active/'
        },
        model: {
          id: null,
          active: 1
        }
      }
    };

    //----------------------------------
    
    if ($('#tmpl-' + component).length) {
      var Component = Vue.extend({
        name: component,
        mixins: [plugin.Component],
        template: '#tmpl-' + component,

        data: function() {
          return data;
        },
        
        methods: {
          init: function() {
            var self = this;
            
            self._init();
            
            // Handle form submission page responses
            if (typeof logs === 'object') {
              setTimeout(() => {
                self.handleResponse({
                  response: logs
                });
              }, 1000);
            }
            
            self.tracking.search();
          }
        }
      });
      
      Component = new Component().$mount('#tab-' + component);
      
      var name = Component.getMethodSuffix(component);
      plugin.Apps = plugin.Apps || {};
      plugin.Apps[name] = Component;
    }
  });
})(jQuery);