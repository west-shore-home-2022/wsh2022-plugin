/*
 * Allows only valid characters to be entered into input boxes.
 * Originally based off of jquery.numeric.js by Sam Collett
 *
 * Validation Config Params:
 *    - chars
 *    - pattern
 *    - md5
 *    - example
 *
 * @version 2.0
 * @author Ed Rodriguez
 * @created 2010
 * @revised 2013-10-21 | edr | Added validation configurations
 * @revised 2014-03-20 | edr | Added md5 validation
 * @example $('.numeric').valid();
 * @example $('.numeric').valid('0123456789');
 * @example $('.numeric').valid('/[0-9]/');
 * @required
 *    - jQuery
 *    - JSON
 * @minify true
 */
$(function() {
  $.fn.valid = function($valid) {
    
    if (this.length) {
      var validations = {
        date: {
          chars: '/([0-9]|\\/)/',
          pattern: '/^\\d{1,2}\\/\\d{1,2}\\/(\\d{2}|\\d{4})$/i',
          example: 'MM/DD/YYYY'
        },
        decimal: {
          chars: '.0123456789'
        },
        email: {
          custom: 'isValidEmail',
          example: 'me@example.com'
        },
        'float': 'decimal',
        integer: {
          chars: '-0123456789'
        },
        money: {
          chars: '/([0-9]|\\.)/',
          pattern: '/^[0-9]*[0-9]{1}(\\.[0-9]{2})?$/i',
          example: '1.50'
        },
        number: 'numeric',
        numeric: {
          chars: '0123456789'
        },
        phone: {
          chars: '/([0-9]|-)/',
          pattern: '/^((\\d{1}-\\d{3}-)?\\d{3}-\\d{3}-\\d{4}|(\\d{1}\\d{3})?\\d{3}\\d{3}\\d{4})$/i',
          example: '111-222-3333'
        },
        phone_international: {
          chars: '-+()0123456789 '
        },
        zip: {
          chars: '/([0-9]|[a-z]|\ )/i',
          pattern: '/^([0-9]{5}|[a-z]{1}[0-9]{1}[a-z]{1}\ ?[0-9]{1}[a-z]{1}[0-9]{1})$/i',
          example: '33607 (US), T0E 1S2 (CA)'
        },
        zip_international: {
          chars: '-0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        }
      };
      
      $valid = typeof $valid === 'string' ? $valid : '';
      
      var scope = this;
      
      scope.$keypress = function(e) {
        var valid = null;
        
        if (typeof e === 'object') {
          if (typeof e.data.valid === 'string') {
            valid = {
              chars: e.data.valid
            };
          } else if (typeof e.data.valid === 'object' && typeof e.data.valid.chars === 'string') {
            valid = e.data.valid;
          }
        }
           
        if (valid) { 
          // Chars Validation
          if (valid.chars !== '') {
            
            var allow = false,
            key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0,
            c = String.fromCharCode(key);
            
            // Debugging
            //console.log('key', key);
            
            // allow enter/return key (only when in an input box)
            if (key === 13 && this.nodeName.toLowerCase() === 'input') {
              return true;
            } else if (key === 13) {
              return false;
            }
            
            // allow Ctrl+A
            if ((e.ctrlKey && key == 97 /* firefox */) || (e.ctrlKey && key == 65) /* opera */) return true;
            // allow Ctrl+X (cut)
            if ((e.ctrlKey && key == 120 /* firefox */) || (e.ctrlKey && key == 88) /* opera */) return true;
            // allow Ctrl+C (copy)
            if ((e.ctrlKey && key == 99 /* firefox */) || (e.ctrlKey && key == 67) /* opera */) return true;
            // allow Ctrl+Z (undo)
            if ((e.ctrlKey && key == 122 /* firefox */) || (e.ctrlKey && key == 90) /* opera */) return true;
            // allow or deny Ctrl+V (paste), Shift+Ins
            if ((e.ctrlKey && key == 118 /* firefox */) || (e.ctrlKey && key == 86) /* opera */
            || (e.shiftKey && key == 45)) return true;
            
            var detect = false;
            
            if (/^\//.test(valid.chars) && /\/(m|i|g)*?$/.test(valid.chars)) {
              var pattern = eval(valid.chars);
              
              if (pattern.test(c)) {
                allow = true;
              } else {
                detect = true;
              }
            } else if (valid.chars.indexOf(c) >= 0) {
              allow = true;
            } else {
              detect = true;
            }
            
            if (detect) {
              allow = false;
              
              // Obsolete old browser code
              if (false) {
                // For detecting special keys (Ex: backspace)
                // IE does not support 'charCode' and ignores them in keypress anyway
                if ($.inArray(key, [8,9,13,35,36,37,39,46]) < 0) {
                  allow = false; 
                } else {
                  if (typeof e.charCode !== 'undefined') {
                    // special keys have 'keyCode' and 'which' the same (Ex: backspace)
                    if (e.keyCode === e.which && e.which !== 0) {
                      allow = true;
                    }
                    // or keyCode != 0 and 'charCode'/'which' = 0
                    else if (e.keyCode !== 0 && e.charCode === 0 && e.which === 0) {
                      allow = true;
                    }
                  }
                }
              }
            }
            
            return allow;
          }
        }
        
        return true;
      };
      
      scope.$validate = function(e) {
        var elem = $(this), errors = [], value = elem.val();
        
        if (typeof e === 'object' && typeof e.data.valid === 'object') {
          var valid = e.data.valid, error = false;
          
          // Pattern Validation
          if (!error && typeof valid.pattern === 'string') {
            var pattern = eval(valid.pattern);
            error = !pattern.test(value);
          }
          
          // Custom Validation
          if (!error && typeof valid.custom !== 'undefined') {
            if (typeof valid.custom === 'function') {
              error = !valid.custom.apply(this, [value]);
            } else if (typeof valid.custom === 'string' && typeof window[valid.custom] === 'function') {
              error = !window[valid.custom].apply(this, [value]);
            }
          }
          
          // Md5 Validation
          if (!error && typeof valid.md5 === 'string') {
            if (typeof md5 !== 'function') {
              errors.push('MD5 javascript didn\'t load properly');
            }
            
            if (!errors.length && typeof valid.md5 === 'string' && valid.md5 !== '') {
              var array = valid.md5.split(/:/);
              array.shift();
              
              var value1 = array.pop(),
              value2 = value;
      
              if ($.inArray('upper', array) >= 0) {
                value2 = value2.toUpperCase();
              }
              
              if ($.inArray('lower', array) >= 0) {
                value2 = value2.toLowerCase();
              }
              
              value2 = md5(value2);
              error = (value1 !== value2);
            }
          }
          
          if (error) {
            var field = elem.attr('name');
            
            field = ucwords(field.replace(/_/gi,' '));
            errors.push('The '+field+' entered is invalid.');
            
            if (typeof elem.attr('data-valid-example') !== 'undefined') {
              errors.push('<strong>Example:</strong> '+elem.attr('data-valid-example'));
            } else if (typeof valid.example === 'string') {
              errors.push('<strong>Example:</strong> '+valid.example);
            }
          }
        }
        
        return errors;
      };
      
      // SETUP
      this.each(function() {
        var elem = $(this),
            valid = (typeof $(this).attr('data-valid') !== 'undefined') ? $(this).attr('data-valid') : $valid;
        
        valid = (typeof validations[valid] !== 'undefined') ? validations[valid] : valid;
        
        valid = (typeof valid === 'string' && typeof validations[valid] !== 'undefined') ? validations[valid] : valid;
        
        if (typeof valid === 'string') {
          // configs
          if (/^\{/.test(valid) && /\}$/.test(valid)) {
            valid = JSON.parse(valid);
          }
          // patterns
          else if (/^\/\^/.test(valid) || /\$\/(m|i|g)*?$/.test(valid)) {
            valid = {pattern:valid};
          }
          // md5
          else if (/^md5:/i.test(valid)) {
            valid = {md5:valid};
          }
          // chars
          else {
            valid = {chars:valid};
          }
        }
        
        if (typeof valid === 'object') {
          elem.on('validate', {valid:valid}, scope.$validate);
          elem.on('keypress', {valid:valid}, scope.$keypress);
        }
      });
    }
    
    return this;
  }
  
  if ($.fn.valid) {
    $('[data-valid]').valid();
  }
});