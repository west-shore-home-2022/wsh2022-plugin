(function($) {
  var id = 'bootstrap-loaded';

  var getURL = function(filename) {
    var scripts = document.getElementsByTagName('script');

    if (scripts && scripts.length > 0) {
      for (var i in scripts) {
        if (scripts[i].src && scripts[i].src.match(new RegExp(filename+'(\\.min)?\\.js'))) {
          return scripts[i].src.replace(new RegExp('(.*)'+filename+'(\\.min)?\\.js.*'), '$1');
        }
      }
    }
  };

  if (!document.getElementById(id)) {
    var url = getURL('bootstrap-hack'),
        head = document.getElementsByTagName('head')[0];
    
    var wrapper = document.createElement('link');
    wrapper.id = id;
    wrapper.rel = 'stylesheet';
    wrapper.type = 'text/css';
    wrapper.href = url + 'css/bootstrap-wrapper.css';
    wrapper.media = 'all';
    head.appendChild(wrapper);
    
    var overrides = document.createElement('link');
    overrides.onload = function () {
      setTimeout(function () {
        $('.bootstrap-wrapper').fadeIn();

        if ($.fn.summernote) {
          $('.summernote').summernote({height: 200});
        }
      }, 300);
    }
    overrides.rel = 'stylesheet';
    overrides.type = 'text/css';
    overrides.href = url + 'css/overrides.css';
    overrides.media = 'all';
    head.appendChild(overrides);
  }
})(jQuery);
