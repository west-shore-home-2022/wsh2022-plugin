<?php
  require_once('wp-load.php');

  global $_plugin;
  
  if ($_plugin && class_exists('WSHWordpress\Api\Lead')) {
    $api = new WSHWordpress\Api\Lead($_plugin);
    $response = $api->add();

    if ($response) {
      $code = $api->hasErrors() ? 500 : 200;

      http_response_code($code);
      header('content-type: application/json');
      die(json_encode($response));
    }
  }
