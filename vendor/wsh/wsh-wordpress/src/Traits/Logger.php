<?php

namespace WSHWordpress\Traits;

use WSHWordpress\Utils\Utils;

/**
 * A logger trait
 */
trait Logger
{
    /**
     * The logs array holds all types of logs
     *
     * [
     *   [
     *     'type' => '{success, alert, warning, error}',
     *     'messages' => [
     *       [
     *         'message' => 'Message 1',
     *         'functions' => {TRACE},
     *         'file' => {FILE},
     *         'line' => {LINE},
     *         'date' => {DATE}
     *       ],
     *       [
     *         'message' => 'Message 2',
     *         'functions' => {TRACE},
     *         'file' => {FILE},
     *         'line' => {LINE},
     *         'date' => {DATE}
     *       ],
     *       [
     *         'message' => 'Message 3',
     *         'functions' => {TRACE},
     *         'file' => {FILE},
     *         'line' => {LINE},
     *         'date' => {DATE}
     *       ]
     *     ]
     *   ]
     * ]
     *
     * @var array
     */
    public $logs = [];

    /**
     * Converts specified log type(s) into an object based item array for piping purposes
     *
     * @return object The errors object
     */
    public function getLogsArray($type)
    {
        $array = [];
        $messages = $this->logs($type);
        $count = 0;

        foreach ($messages as $message) {
            $obj = Utils::Object();

            foreach ($message as $key=>$value) {
                $obj->{$key} = $value;
            }

            $array[$count] = $obj;
            $count++;
        }

        return $array;
    }

    /**
     * Adds a log to the Logs array bassed on type and if it already exists.
     *
     * @param string $type The message category type to store the message under
     * @param string $message The message to store
     * @return boolean True if the log was added
     */
    public function addLog($type, $message)
    {
        $message = (is_string($message)) ? trim($message) : $message;

        if ((is_string($message) && !empty($message)) || !is_string($message)) {
            $type = trim($type);
            $logs = $this->logs;
            $found = false;

            // Update log type with new message
            for ($i = 0; $i < count($logs); $i++) {
                if (is_array($logs[$i])) {
                    if ($logs[$i]['type'] == $type) {
                        if (is_array($logs[$i]['messages'])) {
                            $new = [];
                            $new['message'] = $message;
                            $new['functions'] = $this->getCaller('trace-function', '2-5');
                            $new['file'] = basename($this->getCaller('file'));
                            $new['line'] = $this->getCaller('line');
                            $new['date'] = date('Y-m-d H:i:s');
                            $logs[$i]['messages'][] = $new;
                        }

                        $found = true;

                        break;
                    }
                }
            }

            // Add new log type
            if (!$found) {
                $new = [];
                $new['message'] = $message;
                $new['functions'] = $this->getCaller('trace-function', '2-5');
                $new['file'] = basename($this->getCaller('file'));
                $new['line'] = $this->getCaller('line');
                $new['date'] = date('Y-m-d H:i:s');
                $logs[] = [
                    'type' => $type,
                    'messages' => [$new]
                ];
            }

            $this->logs = $logs;
            //print_r($logs);
            //echo('<br /><br />');

            return true;
        }

        return false;
    }

    /**
     * Clears all current logs
     *
     * @param string $type The optional log type(s)
     */
    public function clearLogs($type = '')
    {
        $type = trim($type);
        $type = $type ? $type : 'error, success, warning, info';

        $types = Utils::trimExplode('/,|\|/', $type, 1);

        foreach ($types as $type) {
            if (empty($type)) {
                $this->logs = [];
            } else {
                $logs = $this->logs;
                $new = [];

                foreach ($logs as $set) {
                    if ($set['type'] != $type) {
                        array_push($new, $set);
                    }
                }

                $this->logs = $new;
            }
        }
    }

    /**
     * Returns plain logs stripped of any html tags
     *
     * @param string $type An optional log category type
     * @return string A text list of plain logs
     */
    public function getPlainLogs($type = '', $delimiter = "\\n")
    {
        $type = trim(strtolower($type));
        $output = '';
        $logs = $this->logs;

        foreach ($logs as $log) {
            if (is_array($log)) {
                $class = $log['type'];
                $show = false;

                if (empty($type) && $class != 'sql') {
                    $show = true;
                } else if ($type == $class) {
                    $show = true;
                }

                if ($show) {
                    $messages = $log['messages'];

                    if (is_array($messages)) {
                        foreach ($messages as $message) {
                            if (is_string($message['message'])) {
                                $output .= $message['message'].$delimiter;
                            }
                        }
                    }
                }
            }
        }

        return strip_tags($output);
    }

    /**
     * Returns true if there are error logs.
     *
     * @return boolean True if error logs found
     */
    public function hasErrors($type = 'error')
    {
        return $this->hasLogs($type);
    }

    /**
     * Returns true if there are error or warning logs.
     *
     * @return boolean True if error or warning logs found
     */
    public function hasErrorsWarnings($type = 'error, warning')
    {
        return $this->hasLogs($type);
    }

    /**
     * Returns true if there are alert logs.
     *
     * @return boolean True if alert logs found
     */
    public function hasAlerts($type = 'alert')
    {
        return $this->hasLogs($type);
    }

    /**
     * Returns true if there are success logs.
     *
     * @return boolean True if success logs found
     */
    public function hasSuccesses($type = 'success')
    {
        return $this->hasLogs($type);
    }

    /**
     * Returns true if there are any logs in general or any logs for the specified type
     *
     * @param string $type An optional log category type
     * @return boolean True if any logs were found
     */
    public function hasLogs($type = '')
    {
        $types = Utils::trimExplode('/,|\|/', $type, 1);
        $logs = $this->logs;
        $found = false;

        if (!$types) {
            $found = (count($logs) > 0) ? true : $found;
        } else {
            foreach ($types as $type) {
                foreach ($logs as $log) {
                    if (is_array($log)) {
                        $name = strtolower($log['type']);

                        if ($name == $type) {
                            if (is_array($log['messages']) && count($log['messages']) > 0) {
                                $found = true;
                                break;
                            }
                        }
                    }
                }
            }
        }

        return $found;
    }

    /**
     * Sets the logs
     *
     * @param array $value The logs to set
     */
    public function setLogs($value)
    {
        $this->logs = $value;
    }

    /**
     * Add to logs
     *
     * @param array $value The logs to add
     */
    public function addLogs($value)
    {
        if (is_array($value)) {
            $value = array_merge($this->logs, $value);
            $this->logs = $value;
        }
    }

    /**
     * Gets the logs by type
     *
     * @param string $type The optional type of log to retrieve
     * @return array The log's array
     */
    public function logs($type = '')
    {
        $data = [];
        $types = Utils::trimExplode('/,|\|/', $type, 1);

        if (!$types) {
            return $this->logs;
        }

        foreach ($types as $type) {
            if (!empty($type)) {
                $logs = $this->logs;

                foreach ($logs as $log) {
                    if (is_array($log)) {
                        $name = trim(strtolower($log['type']));

                        if ($name == $type && is_array($log['messages'])) {
                            $data = $data + $log['messages'];
                        }
                    }
                }

            }
        }

        return $data;
    }

    /**
     * Ajax response
     */
    public function response()
    {
      $ajax = isset($this->ajax) && !$this->ajax ? false : true;

      $response = $this->json('', 'ajax', 0);

      if ($ajax) {
        header('Content-Type: application/json');
        $json = json_encode($response);
        die($json);
      }

      return $response;
    }

    /**
     * Gets the json logs by type
     *
     * @param string $type The optional type of log to retrieve
     * @return array The log's array
     */
    public function json($type = '', $mode = 'simple', $encode = 1)
    {
        $data = [];
        $types = Utils::trimExplode('/,|\|/', $type, 1);

        if (!$types || $type === 'all') {
            $data = $this->logs;
        } else {
            foreach ($types as $type) {
                if (!empty($type)) {
                    $logs = $this->logs;

                    foreach ($logs as $log) {
                        if (is_array($log)) {
                            $name = trim(strtolower($log['type']));

                            if ($name == $type && is_array($log['messages'])) {
                                $data = $data + $log['messages'];
                            }
                        }
                    }

                }
            }
        }

        if ($mode === 'simple') {
            foreach ($data as &$item) {
                if ($item['messages']) {
                    foreach ($item['messages'] as &$message) {
                        $keys = array_keys($message);

                        foreach ($keys as $key) {
                            if ($key !== 'message') {
                                unset($message[$key]);
                            }
                        }

                    }
                }
            }
        } else if ($mode === 'ajax') {
            $obj = (object) [
                'errors' => [],
                'successes' => [],
                'warnings' => [],
                'infos' => []
            ];

            foreach ($data as &$item) {
                if ($item['messages']) {
                    foreach ($item['messages'] as &$message) {

                        if ($item['type'] === 'success') {
                            $obj->successes[] = $message['message'];
                        } else if ($item['type'] === 'error') {
                            $obj->errors[] = $message['message'];
                        } else if ($item['type'] === 'warning') {
                            $obj->warnings[] = $message['message'];
                        } else if ($item['type'] === 'info') {
                            $obj->infos[] = $message['message'];
                        }
                    }
                }
            }

            $data = $obj;
        }

        return $encode ? json_encode($data) : $data;
    }

    /**
     * Searches the trace and returns information about a parent caller
     * identified by the depth for debugging and logging purposes.
     *
     * Possible Values:
     * function, line, file, class, object, type, args
     *
     * @param string $prop The caller property to retrieve
     * @param integer $depth The caller depth in the backtrace
     * @return mixed The property of the caller object
     */
    public function getCaller($prop = 'file', $depth = 2, $delimiter = ' < ')
    {
        $object = ($prop == 'object') ? true : false;
        $trace = @debug_backtrace($object);

        if (preg_match('/^trace-/', $prop)) {
            $array = explode('-', $prop);
            $prop = $array[1];
            $history = '';

            $start = 0;
            $end = $depth;

            if (is_string($depth) && strpos($depth, '-') !== false) {
                $array = explode('-', $depth);
                $start = $array[0];
                $end = $array[1];
            }

            for ($i=$start; $i<$end; $i++) {
                // @todo Need to resolve why adding a dynamic item outputs errors
                if ($i == $start) {
                    @$history .= $trace[$i][$prop];
                } else {
                    @$history .= $delimiter.$trace[$i][$prop];
                }
            }

            return $history;
        } else {
            if (count($trace) >= ($depth+1)) {
                return $trace[$depth][$prop];
            }
        }

        return null;
    }

    /**
     * Converts warning logs into an object based item array for piping purposes
     *
     * @return object The errors object
     */
    public function warnings($logs = null)
    {
        if ($logs && is_array($logs)) {
          $this->addLogs($logs);
        }

        return $this->getLogsArray('warning');
    }

    /**
     * Converts error logs into an object based item array for piping purposes
     *
     * @return object The errors object
     */
    public function errors($logs = null)
    {
        if ($logs && is_array($logs)) {
          $this->addLogs($logs);
        }

        return $this->getLogsArray('error');
    }

    /**
     * Converts alert logs into an object based item array for piping purposes
     *
     * @return object The errors object
     */
    public function alerts($logs = null)
    {
        if ($logs && is_array($logs)) {
          $this->addLogs($logs);
        }

        return $this->getLogsArray('alert');
    }

    /**
     * Converts info logs into an object based item array for piping purposes
     *
     * @return object The errors object
     */
    public function infos($logs = null)
    {
        if ($logs && is_array($logs)) {
          $this->addLogs($logs);
        }

        return $this->getLogsArray('info');
    }

    /**
     * Converts alert logs into an object based item array for piping purposes
     *
     * @return object The errors object
     */
    public function successes($logs = null)
    {
        if ($logs && is_array($logs)) {
          $this->addLogs($logs);
        }

        return $this->getLogsArray('success');
    }

    /**
     * Adds a warning message to the logs
     *
     * @param string $message The warning message
     */
    public function warning($message)
    {
        $this->addLog('warning', $message);
    }

    /**
     * Adds a warning message to the logs
     *
     * @param string $message The warning message
     */
    public function warn($message)
    {
        $this->addLog('warning', $message);
    }

    /**
     * Adds an error message to the logs
     *
     * @param string $message The error message
     */
    public function error($message)
    {
        $this->addLog('error', $message);
    }

    /**
     * Adds an alert message to the logs
     *
     * @param string $message The alert message
     */
    public function alert($message)
    {
        $this->addLog('alert', $message);
    }

    /**
     * Adds an action message to the logs
     *
     * @param string $message The action message
     */
    public function action($message)
    {
        $this->addLog('action', $message);
    }

    /**
     * Adds an info message to the logs
     *
     * @param string $message The warning message
     */
    public function info($message)
    {
        $this->addLog('info', $message);
    }

    /**
     * Adds a success message to the logs
     *
     * @param string $message The success message
     */
    public function success($message)
    {
        $this->addLog('success', $message);
    }
}
