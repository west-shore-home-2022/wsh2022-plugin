<?php

namespace WSHWordpress\Traits;

use WSHWordpress\Tools\QueryBuilder;
use WSHWordpress\Collections\Collection;

/**
 * Common Model Traits
 *
 * @author Ed Rodriguez
 */
trait ModelTrait
{
  /**
   * Create a new Collection instance
   *
   * @param array [$input]
   * @return object
   */
  public function newCollection($input = [])
  {
    return new Collection($input);
  }

  /**
   * Returns associative table relationship query builder
   *
   * Examples:
   * Resource Model: $this->belongsToMany('\WSHPortal\Models\File', 'wsh_resources_files', 'resources_id', 'files_id');
   * User Model: $this->belongsToMany('\WSHPortal\Models\Company', 'wsh_companies_users', 'users_id', 'companies_id');
   *
   * @param string $related
   * @param string [$table]
   * @param string [$foreignKey]
   * @param string [$otherKey]
   * @return object The Query Builder
   */
  public function belongsToMany($related, $table = null, $foreignKey = null, $otherKey = null)
  {
    // For detaching
    $fk = (object) [
      'table' => $table,
      'column' => $foreignKey,
      'value' => $this->id,
      'other' => $otherKey
    ];

    return (new QueryBuilder($related))->belongsToMany($fk);
  }

  /**
   * Returns associative column relationship query builder
   *
   * Examples:
   * Conversation Model: $this->columnToMany('\WSHPortal\Models\User', 'user_ids');
   *
   * @param string $related
   * @param string $column
   * @param string [$foreignKey]
   * @return object The Query Builder
   */
  public function columnToMany($related, $column, $foreignKey = 'id')
  {
    // For detaching
    $fk = (object) [
      'column' => $foreignKey,
      'values' => $this->{$column}
    ];

    return (new QueryBuilder($related))->columnToMany($fk);
  }

  /**
   * Returns associative table relationship query builder
   *
   * Example
   * User Model: $this->hasOne('\WSHPortal\Models\Profile', 'users_id');
   *
   * @param type $related
   * @param string [$foreignKey]
   * @return object
   */
  public function hasOne($related, $foreignKey = null)
  {
    $fk = (object) [
      'column' => $foreignKey,
      'value' => $this->id
    ];

    return (new QueryBuilder($related))->hasOne($fk);
  }

  /**
   * Returns associative table relationship query builder
   *
   * Example:
   * User Model: $this->hasMany('\WSHPortal\Models\Sales', 'user_id');
   *
   * @param type $related
   * @param string [$foreignKey]
   * @param string [$localKey]
   * @return object
   */
  public function hasMany($related, $foreignKey = null, $localKey = 'id')
  {
    $fk = (object) [
      'column' => $foreignKey,
      'value' => $this->{$localKey}
    ];

    return (new QueryBuilder($related))->hasMany($fk);
  }

  /**
   * Returns associative table relationship query builder
   *
   * Example:
   * Sale Model: $this->belongsTo('\WSHPortal\Models\User', 'user_id');
   *
   * @param type $related
   * @param integer [$localKey]
   * @return object
   */
  public function belongsTo($related, $localKey = null)
  {
    $fk = (object) [
      'value' => $this->{$localKey}
    ];

    return (new QueryBuilder($related))->belongsTo($fk);
  }

  /**
   * Wraps query builder methods
   *
   * @return object|array The query builder, record or records
   */
  public static function __callStatic($method, $arguments)
  {
    $builder = new QueryBuilder(get_called_class());

    if (method_exists($builder, $method)) {
      return call_user_func_array([$builder, $method], $arguments);
    }

    return null;
  }
}
