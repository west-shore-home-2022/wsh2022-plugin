<?php

namespace WSHWordpress\Collections;

/**
 * File Collection
 *
 * @author erodriguez
 */
class FileCollection extends Collection
{
  /**
   * Get the downloads total for all files
   *
   * @return integer
   */
  public function downloads()
  {
    $value = 0;

    if ($this->count()) {
      foreach ($this as $file) {
        $value += $file->downloads;
      }
    }

    return $value;
  }

  /**
   * Get the size total for all files
   *
   * @return integer
   */
  public function size()
  {
    $value = 0;

    if ($this->count()) {
      foreach ($this as $file) {
        $value += $file->size;
      }
    }

    return $value;
  }
}
