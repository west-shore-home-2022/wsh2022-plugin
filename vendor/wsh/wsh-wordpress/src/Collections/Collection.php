<?php

namespace WSHWordpress\Collections;

use ArrayObject;
use WSHWordpress\Traits\Logger;

/**
 * Description of Collection
 */
class Collection extends ArrayObject
{
  use Logger;

  public function __construct($input = [], $flags = 0)
  {
    parent::__construct($input, $flags);
  }

  /**
   * Filters the current collection using the supplied arguments
   * 
   * Examples:
   * $filtered = $collection->where('active', 1);
   * $filtered = $collection->where([
   *   'active' => 1,
   *   'first_name' => 'Ed'
   * ]);
   * $filtered = $collection->where('active', '=', 1);
   * $filtered = $collection->where('active', 1)->where('first_name', 'LIKE', 'Ed%');
   * 
   * @param array|string $arg1
   * @param mixed [$arg2]
   * @param mixed [$arg2]
   * @return Collection
   */
  public function where($arg1, $arg2 = null, $arg3 = null)
  {
    $array = [];
    $iterator = $this->getIterator();

    // Filter collection
    while($iterator->valid()) {
      $index = $iterator->key();
      $row = $iterator->current();

      $match = true;

      // 1) Filter by array
      if (is_array($arg1) && is_null($arg2)  && is_null($arg3)) {
        foreach ($arg1 as $key => $value) {
          if (!isset($row->{$key}) || $row->{$key} != $value) {
            $match = false;
            break;
          }
        }
      }
      // 2) Filter by operator
      else if (is_string($arg1)) {
        $operators = ['===', '==', '=', '!=', '<>', '>', '<', '<=', '>=', 'LIKE'];
  
        if (is_string($arg2) && in_array($arg2, $operators)) {
          $key = $arg1;
          $operator = $arg2;
          $value = $arg3;
          $match = true;
          
          if (!isset($row->{$key})) {
            $match = false;
          } else {
            if ($operator === '===') {
              $match = $row->{$key} === $value;
            } else if ($operator === '==' || $operator === '=') {
              $match = $row->{$key} == $value;
            } else if ($operator === '!=' || $operator === '<>') {
              $match = $row->{$key} != $value;
            } else if ($operator === '>') {
              $match = $row->{$key} > $value;
            } else if ($operator === '<') {
              $match = $row->{$key} < $value;
            } else if ($operator === '<=') {
              $match = $row->{$key} <= $value;
            } else if ($operator === '>=') {
              $match = $row->{$key} >= $value;
            } else if (preg_match('/^(LIKE)$/i', $operator)) {
              $val = $value;
              $val = preg_replace('/^%/', '', $val);
              $val = preg_replace('/%$/', '', $val);
              $pattern = preg_quote($val, '/');

              // Starts with
              if (!preg_match('/^%/', $value) && preg_match('/%$/', $value)) {
                $match = preg_match('/^' . $pattern . '/i', $row->{$key});
              }
              // Ends with
              else if (preg_match('/^%/', $value) && !preg_match('/%$/', $value)) {
                $match = preg_match('/' . $pattern . '$/i', $row->{$key});
              }
              // Has pattern
              else {
                $match = preg_match('/' . $pattern . '/i', $row->{$key});
              }
            }
          }
        }
        // 3) Filter by equals
        else if (is_string($arg1) && is_null($arg3)) {
          $key = $arg1;
          $value = $arg2;
          $match = true;

          if (!isset($row->{$key})) {
            $match = false;
          } else {
            $match = $row->{$key} == $value;
          }
        }
      }

      if ($match) {
        $array[] = clone $row;
      }

      $iterator->next();
    }
    
    $class = get_class($this);
    
    return new $class($array);
  }

  /**
   * Deletes each record in the collection
   *
   * @todo Refactor to delete where ids are in the primary key
   * @return boolean
   */
  public function delete()
  {
    foreach ($this as $row) {
      if (!$row->delete()) {
        $this->addLogs($row->logs());
      }
    }

    return !$this->hasErrors();
  }

  /**
   * Gets the first row
   *
   * @return object
   */
  public function first()
  {
    $row = null;

    if ($this->count()) {
      $row = $this[0];
    }

    return $row;
  }

  /**
   * Gets the last row
   *
   * @return object
   */
  public function last()
  {
    $row = null;

    if ($this->count()) {
      $row = $this[$this->count() - 1];
    }

    return $row;
  }

  /**
   * Converts the collection to an array
   *
   * @param boolean|array $cleanup
   * @return array
   */
  public function toArray($cleanup = false)
  {
    $array = (array) $this;

    // Cleanup - Boolean
    if (is_bool($cleanup) && $cleanup) {
      foreach ($array as &$item) {
        unset($item->table);
        unset($item->config);
        unset($item->logs);
      }
    }

    // Cleanup - Array
    if (is_array($cleanup) && $cleanup) {
      $columns = $cleanup;

      foreach ($array as &$item) {
        $item = clone $item;
        $cols = array_keys(get_object_vars($item));

        foreach ($cols as $column) {
          if (!in_array($column, $columns)) {
            unset($item->{$column});
          }
        }
      }
    }

    return $array;
  }

  /**
   * Retrieves all of the values for a given key
   *
   * @param string $key
   */
  public function pluck($key)
  {
    $array = (array) $this;

    $values = [];

    foreach ($array as $item) {
      if (isset($item->{$key})) {
        $values[] = $item->{$key};
      }
    }

    return $values;
  }

  /**
   * Checks if the array has a value
   *
   * @param mixed $value
   * @return boolean
   */
  public function has($value)
  {
    $found = false;

    foreach ($this as $item) {
      if (is_object($item)) {
        $idColumn = $item->idColumn();

        if (is_numeric($value)
            && property_exists($item, $idColumn)
            && $item->{$idColumn} === $value) {
          $found = true;
        } elseif (is_object($value)
            && property_exists($value, $idColumn)
            && property_exists($item, $idColumn)
            && $value->{$idColumn} === $item->id) {
          $found = true;
        }
      }

      if ($found) {
        break;
      }
    }

    return $found;
  }
}
