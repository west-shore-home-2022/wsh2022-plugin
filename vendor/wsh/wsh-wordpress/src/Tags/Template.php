<?php

namespace WSHWordpress\Tags;

use WSHWordpress\Tags\Tag;
use WSHWordpress\Utils\Utils;

/**
 * Template tag
 * 
 * Usage:
 * [template file="template-parts/gallery"]
 */
class Template extends Tag
{
  public static $active = true;

  protected $name = 'template';

  public $defaults = [
    'file' => null,
    'attributes' => '',
    'index' => null,
    'lazyload' => false,
    'cache' => false
  ];

  public function emit()
  {
    global $post;

    $plugin = $this->plugin;

    $tags = $this->tags();

    foreach ($tags as $tag) {
      $args = $tag->args;

      $file = $args->file;

      if (!$file) {
        continue;
      }

      // Lazy loading
      $file_value = $file;
      $file = $plugin->path($file);

      $lazyload = !isset($_GET['lazyload']) || Utils::getBool($_GET['lazyload']);

      // Prevent lazyloading when a bot is detected
      if ($lazyload) {
        $agent = @$_SERVER['HTTP_USER_AGENT'];

        $bots = [
          'googlebot',
          'google-structured-data-testing-tool',
          'bingbot',
          'linkedinbot',
          'mediapartners-google'
        ];

        $found = false;

        foreach ($bots as $bot) {
          $pattern = '/' . preg_quote($bot, '/') . '/i';

          if (is_string($agent) && strlen($agent) && preg_match($pattern, $agent)) {
            $found = true;
            break;
          }
        }

        $lazyload = $found ? false : $lazyload;
      }

      if (Utils::getBool($args->lazyload) && $lazyload) {
        $args->uri = $plugin->path($file_value, 1);
        $file = preg_replace('/\.[a-z0-9]+/i', '-placeholder$0', $file);
      }

      if (!file_exists($file)) {
        continue;
      }

      // Build tag attributes
      $args->attributes .= " data-template-lazyload=\"{$args->uri}\"";

      if ($args->cache) {
        $args->attributes .= " data-template-cache=\"1\"";
      }

      if (is_numeric($args->index)) {
        $args->attributes .= " data-template-index=\"{$args->index}\"";
      }

      // Attribute: Post ID
      if (isset($post) && is_object($post)) {
        $args->attributes .= " data-template-post-id=\"{$post->ID}\"";
      }

      $args->attributes = trim($args->attributes);

      $value = $this->render($file, $args);

      $this->replace($tag, $value);
    }

    return $this->output;
  }
}