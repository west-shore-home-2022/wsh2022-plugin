<?php

namespace WSHWordpress\Tags;

use WSHWordpress\Traits\Logger;
use WSHWordpress\Utils\Utils;

class Tag
{
  use Logger;

  public $plugin;
  protected $name = '';
  protected $output = '';
  public $defaults = [];

  public function __construct($plugin, $output)
  {
    $this->plugin = $plugin;
    $this->output = $output;
  }

  public function tags()
  {
    $pattern = '/\[' . $this->name . '(\s*[^\s]+=["\'].*["\']\s*)*\]/Uxis';
    $tags = [];

    preg_match_all($pattern , $this->output, $tags);

    $items = [];

    foreach ($tags[0] as $i => $tag) {
      // Fixes a match all bug
      // $args = $tags[1][$i]; // Buggy
      $args = trim($tag);
      $args = preg_replace('/^\[' . $this->name . '/i', '', $args);
      $args = preg_replace('/\]$/', '', $args);
      $args = trim($args);

      $args = $this->args($args);

      $item = (object) [
        'syntax' => $tag,
        'args' => $args
      ];

      $items[] = $item;
    }

    $tags = $items;

    return $tags;
  }

  public function args($str)
  {
    $args = [];

    $matches = [];
    $pattern = '/[a-z0-9_]+=("|\').*("|\')/Uxis';
    
    preg_match_all($pattern, $str, $matches);

    foreach ($matches[0] as $arg) {
      $arg = preg_replace('/("|\')$/', '', $arg);
      $arg = preg_split('/=("|\')/', $arg, 2);

      if (count($arg) === 2) {
        $key = strtolower($arg[0]);
        $value = $arg[1];

        // Handles booleans
        if (is_string($value) && preg_match('/(true|false|yes|no)/i', $value)) {
          $value = Utils::getBool($value);
        }

        $args[$key] = $value;
      }
    }

    $args = array_merge($this->defaults, $args);

    return (object) $args;
  }

  /**
   * Render the template and return the output.
   *
   * @return string
   */
  public function render($_file, $_args)
  {
    $output = '';
    
    if (file_exists($_file)) {
      if (1) {
        ob_start();

        // Initialize template variables
        $_args = is_object($_args) ? get_object_vars($_args) : $_args;
        $_args = is_array($_args) ? $_args : [];

        foreach ($_args as $_name => $_value) {
          // Handle passed global variables
          if (preg_match('/^\$[a-z0-9_]+$/', $_value)) {
            $_value = preg_replace('/^\$/', '', $_value);

            $_value = isset($GLOBALS[$_value]) ? $GLOBALS[$_value] : null;
          }

          $$_name = $_value;
        }

        include $_file;

        $output = ob_get_clean();
      } else {
        $output = file_get_contents($file, true);
      }
    }

    return $output;
  }

  /**
   * Replace tag with value in output
   */
  public function replace($tag, $value)
  {
    $tag = is_object($tag) ? $tag->syntax : $tag;
    $pattern = '/' . preg_quote($tag, '/') .'/';

    $this->output = preg_replace($pattern, $value, $this->output);
  }
}