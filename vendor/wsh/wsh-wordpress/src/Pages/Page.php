<?php

namespace WSHWordpress\Pages;

use WSHWordpress\Traits\Logger;
use WSHWordpress\Utils\Utils;

class Page
{
  use Logger;

  protected $wpdb = null;
  protected $plugin = null;
  protected $assets = [];
  protected $params = [];

  public function __construct($plugin)
  {
    global $wpdb, $page;

    $this->wpdb = $wpdb;
    $this->plugin = $plugin;
    $this->params = $_REQUEST;

    add_filter('document_title_parts', [$this, 'setTitle']);
    $this->setup();

    if (!$page) {
      $page = $this;
    }
  }

  /**
   * Sets up a page
   */
  public function setup()
  {
    $this->assets();
  }

  /**
   * Loads page assets
   */
  public function assets()
  {
    $assets = Utils::trimExplode(',', $this->assets);

    if ($assets) {
      $this->plugin->assets($assets, 'group:page');
    }
  }

  /**
   * Sets the page title
   *
   * @param array $title_parts
   * @return array
   */
  public function setTitle($title_parts)
  {
    if (isset($this->title)) {
      $title_parts['title'] = $this->title;
    }

    return $title_parts;
  }

  /**
   * Get a property value from the API params
   *
   * @param string $prop
   * @param [mixed] $default
   * @return mixed
   */
  protected function value($prop, $default = '')
  {
    return $this->plugin->find($prop, $default, $this->params);
  }

  /**
   * Gets the record's wp column values
   *
   * @param object $record
   * @param [string] $table
   * @return array
   */
  public function values($record, $table = '')
  {
    $wpdb = $this->wpdb;
    $values = get_object_vars($record);

    if ($table) {
      $columns = $wpdb->get_col("DESC {$table}", 0);

      foreach ($values as $column => $value) {
        if (!in_array($column, $columns)) {
          unset($values[$column]);
        }
      }
    }

    return $values;
  }

  /**
   * Validates required fields
   *
   * @param object $record
   * @param string $type
   */
  public function validateRequired($record, $type)
  {
    if (isset($this->required) && isset($this->required[$type])) {
      $required = $this->required[$type];
      $record = is_array($record) ? (object) $record : $record;

      foreach ($required as $require) {
        if (!isset($record->{$require})) {
          $name = Utils::getName($require);
          $this->error("{$name} is a required field");
        } else {
          $value = $record->{$require};
          if (is_string($value)) {
            $value = trim($value);

            if (!strlen($value)) {
              $name = Utils::getName($require);
              $this->error("{$name} is a required field");
            }
          }
        }
      }
    }
  }

  /**
   * Checks if the user is authorized
   *
   * @return boolean
   */
  public function auth()
  {
    return current_user_can('editor') || current_user_can('administrator');
  }

  /**
   * Checks and determines if a record has been updated
   *
   * @param object $row The record to check
   * @param array $values The new values
   * @return boolean
   */
  public function hasUpdates($row, $values)
  {
    $bool = false;

    foreach ($values as $prop => $value) {
      if ($row->{$prop} != $value) {
        $bool = true;
        break;
      }
    }

    return $bool;
  }

  /**
   * Checks if a nonce is valid
   *
   * @param type $action The nonce action
   * @return boolean
   */
  public function nonce($action = 'wp_rest')
  {
    $bool = false;

    if (isset($_REQUEST['_wpnonce'])) {
      $bool = wp_verify_nonce($_REQUEST['_wpnonce'], $action);
    }

    return $bool;
  }

  /**
   * Check the frontend and backend for a template
   *
   * @param type $filename
   * @return type
   */
  public function path($filename)
  {
    $plugin = $this->plugin;
    $path = null;

    // Frontend
    $file = $plugin->themeRoot . $filename;

    if (file_exists($file)) {
      $path = $file;
    } else {
      // Backend
      $file = $plugin->templateRoot . $filename;

      if (file_exists($file)) {
        $path = $file;
      }
    }

    $path = $path ? preg_replace('/(\/|\\\)/', DS, $path) : $path;

    return $path;
  }
}