<?php

namespace WSHWordpress\Models;

/**
 * Tracking model
 *
 * @author Ed Rodriguez
 */
class Tracking extends ModelBase
{
  public $config = "
    table: wsh_tracking
    sort: id
    order: DESC
    columns:
      search:
        - title
        - uri
        - ip
        - type
    required:
      - title
  ";
}
