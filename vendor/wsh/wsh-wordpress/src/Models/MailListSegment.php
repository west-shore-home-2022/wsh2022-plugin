<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Mail List Segment model
 *
 * @author Ed Rodriguez
 */
class MailListSegment extends ModelBase
{
  public $table = 'wsh_mail_lists_segments';

  /**
   * Gets the segment's mail list
   *
   * @return object
   */
  public function mail_list()
  {
    return $this->belongsTo('\WSHWordpress\Models\MailList', 'list_id');
  }

  /**
   * Gets the mail list segment's users
   *
   * @return object
   */
  public function users()
  {
    return $this->belongsToMany('\WSHPortal\Models\User', 'wsh_mail_lists_segments_users', 'mail_list_segment_id', 'user_id');
  }
}
