<?php

namespace WSHWordpress\Models;

/**
 * Ip model
 *
 * @author Ed Rodriguez
 */
class Ip extends ModelBase
{
  public $config = "
    table: wsh_ips
    sort: ip
    order: ASC
    columns:
      search:
        - ip
        - abusive
        - country
    required:
      - ip
  ";
}