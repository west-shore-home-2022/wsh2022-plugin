<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\CompaniesUsers;
use WSHWordpress\Models\Company;
use WSHWordpress\Utils\Utils;

/**
 * User model
 *
 * @author Ed Rodriguez
 */
class User extends ModelBase
{
  public $table = '{$prefix}users';

  public $id = null;
  public $username = null;
  public $email = null;
  public $name = null;
  public $firstname = null;
  public $first_name = null;
  public $lastname = null;
  public $last_name = null;
  public $created_at = null;
  public $roles = [];

  /**
   * Builds the user
   *
   * @param object|id $user
   */
  public function __construct($user = null)
  {
    parent::__construct($user);

    if ($this->ID) {
      // Sets up additional WordPress user information and aliases
      $this->id = $this->ID;

      $wpuser = get_user_by('ID', $this->id);

      $roles = $wpuser->roles;
      $user = $wpuser->data;

      $meta = (object) get_user_meta($this->id);

      $this->username = $user->user_login;
      $this->email = $user->user_email;
      // Bug Fix: Prevents errors when meta is returned incomplete during the request lifecycle
      $this->firstname = isset($meta->first_name) ? array_shift($meta->first_name) : null;
      $this->lastname = isset($meta->last_name) ? array_shift($meta->last_name) : null;
      $this->first_name = $this->firstname;
      $this->last_name = $this->lastname;
      $this->name = trim($this->firstname . ' ' . $this->lastname);
      $this->created_at = $user->user_registered;
      $this->roles = $roles;
    }
  }

  /**
   * Determines if a user is active
   *
   * @return boolean
   */
  public function active()
  {
    $active = true;

    $company = $this->company;
    $profile = $this->profile;

    $active = $company->isEmpty() || $company->active;
    $active = $active && !$profile->active ? false : $active;

    return $active;
  }

  /**
   * Attempt to get the user's name and fallback to the username
   *
   * @return string
   */
  public function name()
  {
    $name = $this->name;

    if (!$name) {
      $name = $this->username;
    }

    return $name;
  }

  /**
   * Checks if the user is part of a company
   *
   * @param integer $id
   */
  public function company()
  {
    return $this->companies->count() ? $this->companies->first() : new Company;
  }

  /**
   * Checks if the user is part of a company
   *
   * @param integer $id
   */
  public function hasCompany($id)
  {
    $found = 0;

    foreach ($this->companies as $company) {
      if ($company->id === $id) {
        $found = true;
        break;
      }
    }

    return $found;
  }

  /**
   * Sets the user to company relationship
   *
   * @param object|integer $company
   */
  public function setCompany($company)
  {
    $id = $company;

    if (is_object($company) && isset($company->id)) {
      $id = $company->id;
    }

    if ($id && is_numeric($id)) {
      // Build record
      $row = CompaniesUsers::where('users_id', $this->id)->first();
      $row = $row ? $row : new CompaniesUsers;

      $row->companies_id = $id;
      $row->users_id = $this->id;

      if (!$row->isEmpty()) {
        // Update record
        if ($row->update()) {
          $this->error('Unable to update company relationship');
          $this->addLogs($row->logs());
        }
      } else {
        // Add record
        if (!$row->add()) {
          $this->error('Unable to add company relationship');
          $this->addLogs($row->logs());
        }
      }
    }
  }

  /**
   * Checks if a user is of a certain type
   *
   * @param string $types
   * @return type
   */
  public function is($types = null)
  {
    $bool = false;

    if ($types) {
      $types = is_string($types) ? Utils::trimExplode('|', $types) : $types;

      if (is_array($types) && $types) {
        $admin = preg_grep('/^(administrator)$/i', $this->roles);
        $editor = preg_grep('/^(editor|administrator)$/i', $this->roles);

        if (count($types) === 1) {
          $type = array_shift($types);

          if (preg_match('/admin/i', $type)) {
            $bool = $admin;
          } elseif (preg_match('/editor/i', $type)) {
            $bool = $editor;
          }
        } else {
          foreach ($types as $type) {
            if (preg_match('/admin/i', $type)) {
              $bool = $admin;
            } elseif (preg_match('/editor/i', $type)) {
              $bool = $editor;
            }

            if ($bool) {
              break;
            }
          }
        }
      }
    }

    return $bool;
  }

  /**
   * Builds dynamic properties for daisy chaining
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name)
  {
    if (in_array($name, ['companies', 'company'])) {
      $value = $this->getValue($name);

      // Try to get meta from wp_usermeta table
      if (0 && is_null($value)) {
        if ($meta = get_user_meta($this->id, $name, true)) {
          $value = $meta;
        }
      }

      return $value;
    }

    return parent::__get($name);
  }
}
