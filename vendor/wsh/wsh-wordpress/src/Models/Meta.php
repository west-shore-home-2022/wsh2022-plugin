<?php

namespace WSHWordpress\Models;

/**
 * Meta model
 *
 * @author Ed Rodriguez
 */
class Meta extends ModelBase
{
  public $table = 'wsh_meta';
}
