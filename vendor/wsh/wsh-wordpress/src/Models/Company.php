<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\File;

/**
 * Company model
 *
 * @author Ed Rodriguez
 */
class Company extends ModelBase
{
  public $table = 'wsh_companies';

  /**
   * Get the logo
   *
   * @return object
   */
  public function logo()
  {
    return new File($this->logo_id);
  }

  /**
   * Builds dynamic properties for daisy chaining
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name)
  {
    if (in_array($name, [])) {
      return $this->getValue($name);
    }

    return parent::__get($name);
  }
}
