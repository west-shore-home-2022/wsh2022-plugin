<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Asset Model
 *
 * @author Ed Rodriguez
 */
class Asset extends ModelBase
{
  public $config = "
    table: wsh_assets
    sort: id
    order: DESC
    columns:
      search:
        - name
        - assets
        - action
        - type
        - expire
    required:
      - name
      - assets
      - action
      - minify
      - type
      - active
  ";
}