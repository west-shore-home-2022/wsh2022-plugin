<?php

namespace WSHWordpress\Models;

use WSHWordpress\Traits\Logger;
use WSHWordpress\Traits\ModelTrait;
use WSHWordpress\Plugins\Plugin;
use WSHWordpress\Components\Config;
use WSHWordpress\Tools\QueryBuilder;
use WSHWordpress\Components\Meta as MetaComponent;
use WSHWordpress\Utils\Utils;

/**
 * Model base
 *
 * @author Ed Rodriguez
 */
class ModelBase
{
  use Logger;
  use ModelTrait;

  public $table = null;
  public $config = null;

  public function __construct($data = [])
  {
    global $wpdb;

    // Initialize config
    $this->config = new Config($this->config);

    $table = $this->table();

    if (is_numeric($data) && $table) {
      $id = $data;

      $sql = "SELECT * FROM `{$table}` WHERE {$this->idColumn} = '{$id}'";

      $row = $wpdb->get_row($sql);

      if ($row && is_object($row)) {
        $this->consume($row);
      }
    } else {
      $this->consume($data);
    }
  }

  /**
   * Determines if the model is empty
   *
   * @return boolean
   */
  public function isEmpty()
  {
    return !is_numeric($this->id) || $this->id <= 0;
  }

  /**
   * Returns a string with line breaks
   *
   * @param string $column
   * @return string
   */
  public function nl2br($name)
  {
    $value = null;

    if (isset($this->{$name}) && is_string($this->{$name})) {
      $value = nl2br($this->{$name});
    }

    return $value;
  }

  /**
   * Returns a date based on type
   *
   * @param string $name The name of the date field
   * @return string $type A date based on type
   */
  public function date($name, $type='relative')
  {
    if (isset($this->{$name})) {
      return Utils::date($this->{$name}, $type);
    }

    return $this->{$name};
  }

  /**
   * Cleans up a property for safe html markup insertion or cleans up an overall object
   *
   * @param string [$prop] The property name
   * @return string
   */
  public function cleanup($prop = null)
  {
    $value = null;

    if (!is_null($prop)) {
      // Cleans up a property for safe html markup insertion
      if (isset($this->{$prop}) && (is_string($prop) || is_numeric($prop))) {
        $value = htmlspecialchars($this->{$prop});
      }
    } else {
      // Cleans up the object by removing excluded properties
      $excludes = $this->getExcludes();

      $obj = (object) [];
      $columns = array_keys(get_object_vars($this));

      foreach ($columns as $column) {
        if (!in_array($column, $excludes)) {
          $obj->{$column} = $this->{$column};
        }
      }

      return $obj;
    }

    return $value;
  }

  /**
   * URL encodes a property
   *
   * @param string $prop The property name
   * @return string
   */
  public function urlencode($prop)
  {
    $value = null;

    if (isset($this->{$prop}) && (is_string($prop) || is_numeric($prop))) {
      $value = urlencode($this->{$prop});
    }

    return $value;
  }

  /**
   * Gets the record's wp column values
   *
   * @param object $record
   * @param [string] $table The table name
   * @return array
   */
  public function values($record, $table = null)
  {
    $values = is_object($record) ? get_object_vars($record) : $record;

    if ($table && is_array($values)) {
      $columns = $this->columns($table);

      foreach ($values as $column => $value) {
        if (!is_numeric($column) && !in_array($column, $columns)) {
          unset($values[$column]);
        }
      }
    }

    return $values;
  }

  /**
   * Checks and determines if a record has been updated
   *
   * @param object $row The record to check
   * @param array $values The new values
   * @return boolean
   */
  public function hasUpdates($values, $row = null)
  {
    $bool = false;

    $values = is_object($values) ? get_object_vars($values) : $values;

    if (is_null($row)) {
      $row = $this;
    }

    if ($row && is_array($values)) {
      $columns = $row->columns;

      foreach ($values as $column => $value) {
        if (in_array($column, $columns) && $row->{$column} != $value) {
          $bool = true;
          break;
        }
      }
    }

    return $bool;
  }

  /**
   * Determines if the specified data is a partial instance of a record
   *
   * @param array|object $data
   * @return boolean
   */
  public function isPartial($data)
  {
    $bool = false;

    $data = is_object($data) ? get_object_vars($data) : $data;

    if (is_array($data)) {
      foreach ($this->columns as $column) {
        if (!array_key_exists($column, $data)) {
          $bool = true;
          break;
        }
      }
    }

    return $bool;
  }

  /**
   * Return the model's property excludes
   *
   * @return array
   */
  public function getExcludes()
  {
    return ['config', 'meta', 'logs', 'table'];
  }

  /**
   * Consumes passed data into the object
   *
   * @param object|array $data
   */
  public function consume($data = null)
  {
    global $wpdb;

    $table = $this->table();
    $data = is_object($data) ? get_object_vars($data) : $data;
    $data = is_array($data) ? $data : [];

    // For data which contain an ID determine & consume partial records
    // by fetching the complete record first
    if ($table && isset($data[$this->idColumn]) && is_numeric($data[$this->idColumn]) && $data[$this->idColumn] > 0) {
      if ($this->isPartial($data)) {
        $id = $data[$this->idColumn];

        $sql = "SELECT * FROM `{$table}` WHERE {$this->idColumn} = '{$id}'";

        $row = $wpdb->get_row($sql);

        if ($row && is_object($row)) {
          $this->consume($row);
        }
      }
    }

    // Prefill
    if (!isset($data[$this->idColumn])) {
      foreach ($this->columns as $column) {
        if (!property_exists($this, $column)) {
          $this->{$column} = null;
        }
      }
    }

    $excludes = $this->getExcludes();

    foreach ($data as $prop => $value) {
      if (!is_numeric($prop) && !in_array($prop, $excludes)) {
        $this->{$prop} = $value;
      }
    }
  }

  /**
   * Saves a model
   *
   * @param function $callback_success
   * @param function $callback_error
   * @param function $callback_init
   * @return array
   */
  public function save($callback_success = null, $callback_error = null, $callback_init = null)
  {
    global $wpdb;

    $table = $this->table();

    // Trigger init callback
    if (is_callable($callback_init)) {
      call_user_func_array($callback_init, [&$this]);
    }

    $mode = $this->isEmpty() ? 'add' : 'update';

    if (!$table) {
      $this->error('No table is defined');
    }

    if (!$this->hasErrors()) {
      $now = date('Y-m-d H:i:s');

      // Prepare model and cleanup nulls if needed with defaults
      foreach ($this->schema as $column) {
        $prop = $column->Field;

        if (property_exists($this, $prop)) {
          if (is_null($this->{$prop}) && $column->Null === 'NO') {
            if (isset($column->Default) && is_string($column->Default)) {
              if ($column->Type === 'timestamp') {
                $this->{$prop} = $now;
              } else {
                $this->{$prop} = $column->Default;
              }
            }
          }
        }
      }

      // Process mode
      if ($mode === 'update') {
        if (in_array('updated_at', $this->columns)) {
          $this->updated_at = $now;
        }

        if (in_array('updated_by', $this->columns)) {
          $user_id = get_current_user_id();

          if ($user_id) {
            $this->updated_by = $user_id;
          }
        }

        $values = get_object_vars($this->object());

        // @todo Prevent errors when saving unchanged models
        if (!$wpdb->update($table, $values, [$this->idColumn => $this->id]) && $wpdb->last_error) {
          $this->error($wpdb->last_error);
        }
      } elseif ($mode === 'add') {
        if (in_array('created_at', $this->columns) && !$this->created_at) {
          $this->created_at = $now;
        }

        if (in_array('created_by', $this->columns) && !$this->created_by) {
          $this->created_by = Plugin::$user_id;
        }

        if (in_array('updated_at', $this->columns) && !$this->updated_at) {
          $this->updated_at = $now;
        }

        if (in_array('updated_by', $this->columns) && !$this->updated_by) {
          $this->updated_by = Plugin::$user_id;
        }

        $values = get_object_vars($this->object());

        if (!$wpdb->insert($table, $values)) {
          $this->error($wpdb->last_error);
        } else {
          $this->id($wpdb->insert_id);
        }
      }
    }

    if (!$this->hasErrors()) {
      // Trigger success callback
      if (is_callable($callback_success)) {
        call_user_func_array($callback_success, [&$this]);
      }

      do_action("wsh_{$mode}", $this);
    } else {
      // Trigger error callback
      if (is_callable($callback_error)) {
        call_user_func_array($callback_error, [&$this]);
      }
    }

    return !$this->hasErrors();
  }

  /**
   * Adds a record
   *
   * @param array|object $values
   * @return array
   */
  public function add()
  {
    if ($this->isEmpty()) {
      $this->save();
    } else {
      $this->error('The record was alreay added');
    }

    return !$this->hasErrors();
  }

  /**
   * Updates a record
   *
   * @return array
   */
  public function update()
  {
    if (!$this->isEmpty()) {
      $this->save();
    } else {
      $this->error('The record was empty');
    }

    return !$this->hasErrors();
  }

  /**
   * Deletes a model
   *
   * @param function $callback_success
   * @param function $callback_error
   * @param function $callback_init
   * @return boolean
   */
  public function delete($callback_success = null, $callback_error = null, $callback_init = null)
  {
    global $wpdb;

    $table = $this->table();

    // Trigger init callback
    if (is_callable($callback_init)) {
      call_user_func_array($callback_init, [&$this]);
    }

    if (!$this->hasErrors() && !isset($table)) {
      $this->error('No table is defined');
    }

    if ($this->hasErrors()) {
      return false;
    }

    if ($this->hasColumn('deleted_at')) {
      // Soft Deletes
      if (in_array('deleted_by', $this->columns)) {
        $user_id = get_current_user_id();

        if ($user_id) {
          $this->deleted_by = $user_id;
        }
      }

      $this->deleted_at = date('Y-m-d H:i:s');
      $this->save();
    } elseif ($this->isEmpty()) {
      // Hard Deletes without id
      if (!$this->hasColumn($this->idColumn)) {
        $values = get_object_vars($this->object());

        if (!$wpdb->delete($table, $values)) {
          $this->error('Unable to delete record');
        }
      } else {
        $this->error('Unable to delete record');
      }
    } else {
      // Hard Deletes with id
      if (!$wpdb->delete($table, [$this->idColumn => $this->id])) {
        $this->error('Unable to delete record');
      }
    }

    if (!$this->hasErrors()) {
      // Trigger success callback
      if (is_callable($callback_success)) {
        call_user_func_array($callback_success, [&$this]);
      }

      do_action('wsh_delete', $this);
    } else {
      // Trigger error callback
      if (is_callable($callback_error)) {
        call_user_func_array($callback_error, [&$this]);
      }
    }

    return !$this->hasErrors();
  }

  /**
   * Restores a soft deleted record
   *
   * @return boolean
   */
  public function restore()
  {
    $bool = false;

    if (!$this->isEmpty()) {
      if ($this->hasColumn('deleted_at')) {
        if (isset($this->deleted_at)) {
          $this->deleted_at = null;

          return $this->save();
        } else {
          $bool = true;
        }
      }
    }

    return $bool;
  }

  /**
   * Determine if a record was trashed
   *
   * @return boolean
   */
  public function trashed()
  {
    $bool = false;

    if (!$this->isEmpty()) {
      if ($this->hasColumn('deleted_at')) {
        if (isset($this->deleted_at)) {
          $bool = true;
        }
      }
    }

    return $bool;
  }

  /**
   * Gets or sets the id
   *
   * @param integer $id
   * @return integer
   */
  public function id($id = null)
  {
    if ($id && is_numeric($id)) {
      $this->{$this->idColumn} = $id;
    }

    return $this->{$this->idColumn};
  }

  /**
   * Gets the primary key id column name
   *
   * @return type
   */
  public function idColumn()
  {
    $name = 'id';

    $schema = $this->schema;

    foreach ($schema as $column) {
      if ($column->Key === 'PRI') {
        $name = $column->Field;
        break;
      }
    }

    return $name;
  }

  /**
   * Gets the model's table name
   *
   * @global object $wpdb
   * @return string
   */
  public function table()
  {
    global $wpdb;

    $table = null;

    if (isset($this->table)) {
      $table = $this->table;
      $table = preg_replace('/\{(\$)?prefix\}/i', $wpdb->prefix, $table);

      $this->table = $table;
    } else {
      $table = $this->config->get('table');
      $table = preg_replace('/\{(\$)?prefix\}/i', $wpdb->prefix, $table);

      $this->config->set('table', $table);
    }

    return $table;
  }

  /**
   * Gets the model's table name
   *
   * @global object $wpdb
   * @return string
   */
  public function required()
  {
    $required = [];

    if (isset($this->required)) {
      $required = $this->required;
    }

    return $required;
  }

  /**
   * Validates required fields
   *
   * @param object $record
   * @param string $type
   */
  public function validate()
  {
    if (isset($this->required)) {
      $required = $this->required;

      foreach ($required as $require) {
        if (!isset($this->{$require})) {
          $name = Utils::getName($require);
          $this->error("{$name} is a required field");
        } else {
          $value = $this->{$require};

          if (is_string($value)) {
            $value = trim($value);

            if (!strlen($value)) {
              $name = Utils::getName($require);
              $this->error("{$name} is a required field");
            }
          }
        }
      }
    }
  }

  /**
   * Gets the model's schema
   *
   * @return array
   */
  public function schema()
  {
    global $wpdb;

    $table = $this->table();

    $schema = [];

    if ($table) {
      if (!isset(Plugin::$config->schemas[$table])) {
        $sql = "DESCRIBE {$table}";
        $schema = $wpdb->get_results($sql);
        Plugin::$config->schemas[$table] = $schema;
      } else {
        $schema = Plugin::$config->schemas[$table];
      }
    }

    return $schema;
  }

  /**
   * Get the model's columns
   *
   * Stores schemas in the plugin's config to limit subsequent database queries
   *
   * @return array
   */
  public function columns($type = null)
  {
    global $wpdb;

    $columns = [];

    $table = $this->table();

    // Get custom columns sets
    if ($type) {
      $prop = "columns.{$type}";

      $value = $this->config->get($prop);

      $columns = $value ? $value : $columns;

      if (is_string($columns)) {
        $columns = Utils::trimExplode('/(,|\|)/', $columns);
      }

      $columns = is_array($columns) ? $columns : [];
    }

    if (!$columns && $table) {
      foreach ($this->schema as $column) {
        $columns[] = $column->Field;
      }
    }

    return $columns;
  }

  /**
   * Get a defined sql statement
   *
   * @param string $type
   * @return string
   */
  public function sql($type = null)
  {
    $sql = null;

    $method = 'sql' . ucwords($type);

    if (method_exists($this, $method)) {
      $sql = $this->{$method}();
    }

    return $sql;
  }

  /**
   * Checks if a model has a column
   *
   * @param string $column The column name
   * @param string [$table] The table name
   * @return boolean
   */
  public function hasColumn($column)
  {
    return $column && in_array($column, $this->columns);
  }

  /**
   * Returns the meta component
   *
   * @param string [$table]
   * @return \WSHWorpdress\Components\Meta
   */
  public function meta($table = null)
  {
    return new MetaComponent($this, $table);
  }

  /**
   * Returns the simple WordPress object
   *
   * @params string|array $columns
   * @return object
   */
  public function object($columns = [])
  {
    $obj = (object) [];
    $columns = is_string($columns) ? Utils::trimExplode(',', $columns) : $columns;
    $columns = $columns ? $columns : $this->columns;

    foreach ($columns as $column) {
      if (property_exists($this, $column)) {
        $value = $this->{$column};
        
        // Convert string values into certain identified data type values
        if (is_string($value)) {
          if (preg_match('/^\d+$/', $value)) {
            $value = intval($value);
          } else if (preg_match('/^\d+\.\d*$/', $value)) {
            $value = floatval($value);
          }
        }
        
        $obj->{$column} = $value;
      }
    }

    return $obj;
  }

  /**
   * Builds dynamic properties for daisy chaining
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name)
  {
    if (in_array($name, ['id', 'columns', 'idColumn', 'schema', 'meta']) || method_exists($this, $name)) {
      return $this->getValue($name);
    }

    return null;
  }

  public function getValue($name)
  {
    $var = '_' . $name;

    $value = (isset($this->{$var})) ? $this->{$var} : method_exists($this, $name) ? $this->{$name}() : (null);

    if (is_object($value) && $value instanceof QueryBuilder) {
      $value = $value->get();
    }

    return $value;
  }

  public function onBeforeSave(&$record){}
  public function onAfterSave(&$record){}
  public function onExtend(){}
}
