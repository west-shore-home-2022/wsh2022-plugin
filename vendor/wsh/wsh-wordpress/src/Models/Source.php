<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Source model
 *
 * @author Ed Rodriguez
 */
class Source extends ModelBase
{
  public $config = "
    table: wsh_sources
    sort: id
    order: DESC
    columns:
      search:
        - source
        - phone
        - name
        - type
        - notes
    required:
      - source
      - name
      - type
  ";
}