<?php

namespace WSHWordpress\Models;

/**
 * Post model
 *
 * @author Ed Rodriguez
 */
class Post extends ModelBase
{
  public $table = '{$prefix}posts';
}
