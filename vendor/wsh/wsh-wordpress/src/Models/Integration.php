<?php

namespace WSHWordpress\Models;

/**
 * Integration model
 *
 * @author Ed Rodriguez
 */
class Integration extends ModelBase
{
  public $table = 'wsh_integrations';
}
