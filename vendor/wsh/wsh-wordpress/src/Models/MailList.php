<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Mail List model
 *
 * @author Ed Rodriguez
 */
class MailList extends ModelBase
{
  public $table = 'wsh_mail_lists';

  /**
   * Gets the mail list's segments
   *
   * @return object
   */
  public function segments()
  {
    return $this->hasMany('\WSHWordpress\Models\MailListSegment', 'list_id', 'list_id');
  }

  /**
   * Gets the list's users
   *
   * @return object
   */
  public function users()
  {
    return $this->belongsToMany('\WSHPortal\Models\User', 'wsh_mail_lists_users', 'mail_list_id', 'user_id');
  }
}
