<?php

namespace WSHWordpress\Models;

/**
 * Zip model
 *
 * @author Ed Rodriguez
 */
class Zip extends ModelBase
{
  public $config = "
    table: wsh_zips
    sort: zip
    order: ASC
    columns:
      search:
        - zip
        - city
        - state
        - country
    required:
      - zip
      - city
      - state
      - country
      - latitude
      - longitude
  ";
}
