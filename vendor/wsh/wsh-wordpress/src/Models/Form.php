<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Form model
 *
 * @author Ed Rodriguez
 */
class Form extends ModelBase
{
  public $table = 'wsh_forms';
}
