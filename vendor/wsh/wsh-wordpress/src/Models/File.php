<?php

namespace WSHWordpress\Models;

use WSHWordpress\Utils\CryptUtils;
use WSHWordpress\Utils\FileUtils;
use WSHWordpress\Collections\FileCollection;

/**
 * File model
 *
 * @author Ed Rodriguez
 */
class File extends ModelBase
{
  public $table = 'wsh_files';

  /**
   * Gets the image api base url
   *
   * @param string [$uri] URI to append
   * @param boolean [$encrypt=true]
   * @return string
   */
  public function url($uri = null, $encrypt = true)
  {
    global $_plugin;

    $url = null;

    if ($uri) {
      if ($encrypt) {
        $uri = CryptUtils::encrypt($uri);

        if ($this->extension) {
          $uri .= ".{$this->extension}";
        }
      }

      $slug = $_plugin->config('slug');

      $url = site_url() . "/wp-json/{$slug}/image/{$uri}";
    }

    return $url;
  }

  /**
   * Return a maxwidth image api url
   *
   * @param integer [$max=150]
   * @param boolean [$encrypt=true]
   * @return string
   */
  public function maxwidth($max = 150, $encrypt = true)
  {
    $uri = null;

    if (!$this->isEmpty() && preg_match('/^(gif|jpg|png)$/i', $this->extension)) {
      $uri = "cmds/maxwidth-{$max}/id/{$this->id}/";
    } elseif ($this->file) {
      $this->extension = FileUtils::getExt($this->file);
      $uri = "cmds:maxwidth-{$max},uri:{$this->urlencode('file')}";
    }

    return $this->url($uri, $encrypt);
  }

  /**
   * Return a square image api url
   *
   * @param integer [$max=50]
   * @param boolean [$encrypt=true]
   * @return string
   */
  public function square($max = 50, $encrypt = true)
  {
    $uri = null;

    if (!$this->isEmpty() && preg_match('/^(gif|jpg|png)$/i', $this->extension)) {
      $uri = "cmds/square-{$max}/id/{$this->id}/";
    } elseif ($this->file) {
      $this->extension = FileUtils::getExt($this->file);
      $uri = "cmds:square-{$max},uri:{$this->urlencode('file')}";
    }

    return $this->url($uri, $encrypt);
  }

  /**
   * Return a square image api url
   *
   * @param integer $width
   * @param integer $height
   * @param boolean [$encrypt=true]
   * @return string
   */
  public function maxwidthheight($width = null, $height = null, $encrypt = true)
  {
    $uri = null;

    if (!$this->isEmpty() && preg_match('/^(gif|jpg|png)$/i', $this->extension)) {
      $uri = "cmds/maxwidthheight-{$width}-{$height}/id/{$this->id}/";
    } elseif ($this->file) {
      $this->extension = FileUtils::getExt($this->file);
      $uri = "cmds:maxwidthheight-{$width}-{$height},uri:{$this->urlencode('file')}";
    }

    return $this->url($uri, $encrypt);
  }

  /**
   * Builds dynamic properties for daisy chaining
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name)
  {
    if (in_array($name, [])) {
      $var = '_' . $name;

      return (isset($this->{$var})) ? $this->{$var} : $this->{$name}();
    }

    return parent::__get($name);
  }

  /**
   * Create a new Collection instance
   *
   * @param array [$input]
   * @return object
   */
  public function newCollection($input = [])
  {
    return new FileCollection($input);
  }
}
