<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Companies Users model
 *
 * @author Ed Rodriguez
 */
class CompaniesUsers extends ModelBase
{
  public $table = 'wsh_companies_users';
}
