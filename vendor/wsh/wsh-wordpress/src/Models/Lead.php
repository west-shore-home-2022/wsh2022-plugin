<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Lead model
 *
 * @author Ed Rodriguez
 */
class Lead extends ModelBase
{
  public $config = "
    table: wsh_leads
    sort: id
    order: DESC
    columns:
      search:
        - first_name
        - last_name
        - address
        - city
        - state
        - zip
        - phone
        - email
    required:
      - first_name
      - last_name
      - zip
      - phone
      - email
  ";
}