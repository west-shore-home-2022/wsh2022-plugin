<?php

namespace WSHWordpress\Models;

/**
 * Conversation model
 *
 * @author Ed Rodriguez
 */
class Conversation extends ModelBase
{
  public $table = 'wsh_conversations';


  /**
   * Gets the conversation's users
   *
   * @return object
   */
  public function users()
  {
    return $this->columnToMany('\WSHPortal\Models\User', 'user_ids');
  }

  /**
   * Gets the conversation's unsubscribed users
   *
   * @return object
   */
  public function unsubscribed()
  {
    return $this->columnToMany('\WSHPortal\Models\User', 'unsubscribed_ids');
  }

  /**
   * Builds dynamic properties for daisy chaining
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name)
  {
    if (in_array($name, [])) {
      $value = $this->getValue($name);

      return $value;
    }

    return parent::__get($name);
  }
}
