<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Mail list segment/user model
 *
 * @author Ed Rodriguez
 */
class MailListSegmentUser extends ModelBase
{
  public $table = 'wsh_mail_lists_segments_users';
}
