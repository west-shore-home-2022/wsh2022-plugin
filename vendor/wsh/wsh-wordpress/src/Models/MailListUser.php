<?php

namespace WSHWordpress\Models;

use WSHWordpress\Models\ModelBase;

/**
 * Mail List/Users model
 *
 * @author Ed Rodriguez
 */
class MailListUser extends ModelBase
{
  public $table = 'wsh_mail_lists_users';
}
