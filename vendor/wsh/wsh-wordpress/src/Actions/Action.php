<?php

namespace WSHWordpress\Actions;

use WSHWordpress\Traits\Logger;

/**
 * Action Base
 *
 * @author Ed Rodriguez
 */
class Action
{
  use Logger;

  public $plugin;
  public $priority = 10;
  public $accepted_args = 1;

  public function __construct($plugin)
  {
    $this->plugin = $plugin;
  }
}
