<?php

namespace WSHWordpress\Tests;

/**
 * Test Base
 *
 * @author Ed Rodriguez
 */
class Test
{
  public $plugin;

  public function __construct($plugin)
  {
    $this->plugin = $plugin;
  }
}
