<?php

namespace WSHWordpress\Tests;

use WSHWordpress\Tests\Test;
use WSHWordpress\Models\User;

/**
 * Query Builder Tests
 *
 * @author Ed Rodriguez
 */
class QueryBuilderTest extends Test
{
  public static $active = false;

  public function testFirstOrNew()
  {
    $plugin = $this->plugin;

    $model = User::firstOrNew([
      'user_email' => 'ryan.kemp@westshorehome.com'
    ]);

    echo('<pre>' . print_r($model, 1) . '</pre>');
  }
}
