<?php

namespace WSHWordpress\Tests;

use WSHWordpress\Tests\Test;
use WSHWordpress\Models\User;
use WSHWordpress\Utils\EmailUtils;

/**
 * Config Tests
 *
 * @author Ed Rodriguez
 * @created 2019-09-27
 */
class ConfigTest extends Test
{
  public static $active = true;

  public function __construct($plugin)
  {
    parent::__construct($plugin);
  }

  public function testEmailConfig()
  {
    $plugin = $this->plugin;

    $data = $plugin->load('emails', false);

    echo '<pre>' . print_r($data, 1) . '</pre>';
  }
  
  public function _testEmailOverrides()
  {
    $plugin = $this->plugin;

    $to = 'test@test.com';

    $data = (object) ['id' => 1, 'first_name' => 'Test', 'last_name' => 'Test', 'zip' => '11111', 'phone' => '111-111-1111', 'email' => 'test@test.com'];

    // Send user email notification
    $params = [
      'to' => $to,
      'config' => 'admin.lead_notification',
      'data' => get_object_vars($data),
      'debug' => 1
    ];

    $success = EmailUtils::mail($plugin, $params);

    echo '<pre>' . print_r($data, 1) . '</pre>';
  }
}
