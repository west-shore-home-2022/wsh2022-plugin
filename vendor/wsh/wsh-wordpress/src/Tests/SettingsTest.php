<?php

namespace WSHWordpress\Tests;

use WSHWordpress\Tests\Test;
use WSHWordpress\Models\User;
use WSHWordpress\Utils\EmailUtils;

/**
 * Settings Tests
 *
 * @author Ed Rodriguez
 * @created 2019-09-27
 */
class SettingsTest extends Test
{
  public static $active = true;

  public function __construct($plugin)
  {
    parent::__construct($plugin);
  }

  public function testSettings()
  {
    $plugin = $this->plugin;

    $data = $plugin->settings->get();

    echo '<pre>' . print_r($data, 1) . '</pre>';
  }
  
  public function _testResetSettings()
  {
    $plugin = $this->plugin;

    $data = $plugin->settings->get();
    
    echo '<pre>' . print_r($data, 1) . '</pre>';
    
    $plugin->settings->delete('remove_all_scripts');
    $plugin->settings->delete('remove_all_styles');
    $plugin->settings->delete('remove_all_images');
    $plugin->settings->delete('remove_body_html');
    $plugin->settings->delete('remove_head_html');
    
    $data = $plugin->settings->get();
    
    echo '<pre>' . print_r($data, 1) . '</pre>';
  }
}
