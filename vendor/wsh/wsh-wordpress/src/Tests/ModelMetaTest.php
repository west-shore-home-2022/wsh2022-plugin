<?php

namespace WSHWordpress\Tests;

use WSHWordpress\Tests\Test;
use WSHWordpress\Models\User;

/**
 * Model Meta Tests
 *
 * @author Ed Rodriguez
 * @created 2018-05-17
 */
class ModelMetaTest extends Test
{
  public static $active = true;

  protected $key = 'test1';
  protected $value = '11111111';
  protected $user = null;

  public function __construct($plugin)
  {
    parent::__construct($plugin);

    $this->user = User::find(1);
  }

  /**
   * Tests setting meta
   */
  public function testSetMeta()
  {
    $result = $this->user->meta->set($this->key, $this->value) ? 'Passed' : 'Failed';

    echo('<pre>' . print_r("<strong>$result:</strong> $this->key = $this->value", 1) . '</pre>');
  }

  /**
   * Tests getting meta
   */
  public function testGetMeta()
  {
    $result = $this->user->meta->get($this->key);

    echo('<pre>' . print_r("<strong>$this->key:</strong> $result", 1) . '</pre>');
  }

  /**
   * Tests getting meta via the magic method
   */
  public function testGetMetaByMagicMethod()
  {
    $result = $this->user->meta->{$this->key};

    echo('<pre>' . print_r("<strong>$this->key:</strong> $result", 1) . '</pre>');
  }

  /**
   * Tests deleting meta
   */
  public function _testDeleteMeta()
  {
    $result = $this->user->meta->delete($this->key) ? 'Passed' : 'Failed';

    echo('<pre>' . print_r("<strong>$result:</strong> <del>$this->key</del>", 1) . '</pre>');
  }

  /**
   * Tests getting all meta
   */
  public function testGetAllMeta()
  {
    $result = $this->user->meta->get();

    echo('<pre><strong>All Meta</strong><br />' . print_r($result, 1) . '</pre>');
  }
}
