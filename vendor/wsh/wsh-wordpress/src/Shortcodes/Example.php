<?php

namespace WSHWordpress\Shortcodes;

use WSHWordpress\Controllers\Controller;

class Example extends Controller
{
  /**
   * Run the shortcode
   *
   * @param array $attrs The attributes
   * @return string
   */
  public function run($attrs)
  {
    $defaults = [];

    $attrs = shortcode_atts($defaults, $attrs);

    $shortcode = $this->plugin->shortcode('example');

    return $shortcode->render();
  }

  /**
   * Get a formatted error message
   *
   * @param string $message The error message
   * @return string
   */
  public function error($message)
  {
    return '';
  }
}
