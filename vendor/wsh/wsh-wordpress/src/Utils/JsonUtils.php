<?php

namespace WSHWordpress\Utils;

/**
 * JSON specific utilities
 *
 * @author Ed Rodriguez
 * @created 2018-07-18
 */
class JsonUtils extends UtilsBase
{
  /**
   * Validates if the JSON decoding was successful
   *
   * @param array|object $data The PHP array or object to validate
   * @param string [$type=object|array] The JSON return type to check
   * @return boolean True if the JSON decoded correctly
   */
  public static function isValid($data, $type = null)
  {
    $type = !isset($type) || !is_string($type) ? null : strtolower(trim($type));

    if ($type == 'object') {
      return isset($data) && is_object($data);
    } elseif ($type == 'array') {
      return isset($data) && is_array($data);
    }

    return isset($data) && (is_array($data) || is_object($data));
  }

  /**
   * Alias: Validates if the JSON array decoding was successful
   *
   * @param array|object $data The PHP array to validate
   * @return boolean True if the JSON array decoded
   */
  public static function isArray($data)
  {
    return self::isValid($data, 'array');
  }

  /**
   * Alias: Validates if the JSON object decoding was successful
   *
   * @param array|object $data The PHP object to validate
   * @return boolean True if the JSON object decoded
   */
  public static function isObject($data)
  {
    return self::isValid($data, 'object');
  }

  /**
   * Attempt to determine if a string is formatted like JSON
   *
   * @param string $string
   * @return boolean
   */
  public static function isJson($string)
  {
    $string = trim($string);

    return preg_match('/^(\{|\[)/', $string) && preg_match('/(\}|\])$/', $string);
  }

  /**
   * JSON encode wrapper
   *
   * @param object|array $value
   * @return string
   */
  public static function encode($value, $options = 0)
  {
    return json_encode($value, $options);
  }

  /**
   * Attempts to decode JSON
   *
   * @param string $json The string to decode
   * @param boolean [$assoc=false] When true, returned objects will be converted to associative arrays
   * @return mixed object or array if valid JSON, error string if invalid JSON
   */
  public static function decode($json, $assoc = false)
  {
    $json = is_string($json) && preg_match('/^\s*(\{|\[)\s*\\\\"/', $json) ? stripslashes($json) : $json;
    
    $error = null;
    $result = json_decode($json, $assoc);

    if (function_exists('json_last_error')) {
      switch (json_last_error()) {
        case JSON_ERROR_NONE:
          $error = null; // JSON is valid // No error has occurred
          break;
        case JSON_ERROR_DEPTH:
          $error = 'The maximum stack depth has been exceeded.';
          break;
        case JSON_ERROR_STATE_MISMATCH:
          $error = 'Invalid or malformed JSON.';
          break;
        case JSON_ERROR_CTRL_CHAR:
          $error = 'Control character error, possibly incorrectly encoded.';
          break;
        case JSON_ERROR_SYNTAX:
          $error = 'Syntax error, malformed JSON.';
          break;
        // PHP >= 5.3.3
        case JSON_ERROR_UTF8:
          $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
          break;
        // PHP >= 5.5.0
        case JSON_ERROR_RECURSION:
          $error = 'One or more recursive references in the value to be encoded.';
          break;
        // PHP >= 5.5.0
        case JSON_ERROR_INF_OR_NAN:
          $error = 'One or more NAN or INF values in the value to be encoded.';
          break;
        case JSON_ERROR_UNSUPPORTED_TYPE:
          $error = 'A value of a type that cannot be encoded was given.';
          break;
        default:
          $error = 'Unknown JSON error occured.';
          break;
      }
    } elseif (!self::isValid($result)) {
      $error = 'Unable to decode JSON';
    }

    if ($error) {
      return $error;
    }

    return $result;
  }
}