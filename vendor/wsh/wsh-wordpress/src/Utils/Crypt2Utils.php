<?php

namespace WSHWordpress\Utils;

use Exception;

/**
 * Encrypts string randomly
 *
 * @author erodriguez
 * @created 2018-01-08
 */
class Crypt2Utils extends Utils
{
  const METHOD = 'aes-256-ctr';
  const KEY = 'asdk233445lhjj8899wer345ihvcdfvrfjfv342';

  /**
   * Encrypts a message
   *
   * @param string $message
   * @param string $key
   * @return string
   */
  public static function encrypt($message, $key = null)
  {
    $key = $key ? $key : self::KEY;

    $nonceSize = openssl_cipher_iv_length(self::METHOD);
    $nonce = openssl_random_pseudo_bytes($nonceSize);

    $ciphertext = openssl_encrypt(
      $message,
      self::METHOD,
      $key,
      OPENSSL_RAW_DATA,
      $nonce
    );

    // Bin2hex
    $output = bin2hex($nonce.$ciphertext);

    return $output;
  }

  /**
   * Decrypts a message
   *
   * @param string $message
   * @param string $key
   * @return string
   */
  public static function decrypt($message, $key = null)
  {
    $key = $key ? $key : self::KEY;

    // Hex2bin
    $message = self::hex2bin($message);

    if (is_null($message)) {
      throw new Exception('Encryption failure');
    }

    $nonceSize = openssl_cipher_iv_length(self::METHOD);
    $nonce = mb_substr($message, 0, $nonceSize, '8bit');
    $ciphertext = mb_substr($message, $nonceSize, null, '8bit');

    $plaintext = openssl_decrypt(
      $ciphertext,
      self::METHOD,
      $key,
      OPENSSL_RAW_DATA,
      $nonce
    );

    return $plaintext;
  }

  /**
   * The opposite of the built in php function bin2hex
   *
   * @return string The decoded bin2hex value
   */
  public static function hex2bin($h)
  {
      if (!is_string($h)) {
          return null;
      }

      $r = '';
      for ($a=0; $a<strlen($h); $a+=2) {
        $r .= @chr(hexdec($h{$a}.$h{($a+1)}));
      }

      return $r;
  }
}
