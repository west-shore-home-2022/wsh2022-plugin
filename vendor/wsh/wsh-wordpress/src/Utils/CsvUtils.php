<?php

namespace WSHWordpress\Utils;

/**
 * Description of CsvUtils
 *
 * @author Ed Rodriguez
 */
class CsvUtils extends Utils
{
  /**
   * Read CSV file and build dataset
   *
   * @param string $file
   * @return array
   */
  public static function read($file)
  {
    if (file_exists($file)) {
      $fh = fopen($file, 'r');

      while (!feof($fh)) {
        $dataset[] = fgetcsv($fh, 1024);
      }

      fclose($fh);
    }

    $dataset = self::convert($dataset);

    return $dataset;
  }

  /**
   * Gets a determined amount of data from a csv file.
   *
   * @param $file
   * @param int $amount
   * @param int $start
   * @return array
   */
  public static function getData($file, $amount = 10, $start = 1)
  {
    $end = ($start + $amount) - 1;
    $columns = self::getColumns($file);
    $results = [];

    $fh = fopen($file, 'r');
    if ($fh) {
      $count = 0;
      while (!feof($fh)) {
        if ($line = fgetcsv($fh)) {
          if ($count >= $start && (!isset($end) || $count <= $end)) {
            $obj = self::combine($line, $columns);
            $results[] = $obj;
          }

          $count++;

          if (isset($end) && $count > $end) {
            break;
          }
        }
      }
      fclose($fh);
    }

    return $results;
  }

  /**
   * Combine line values with column names
   *
   * @param $line
   * @param $columns
   * @return object
   */
  public static function combine($line, $columns)
  {
    $obj = [];

    $count = 0;
    foreach ($columns as $column) {
      if (isset($line[$count])) {
        $obj[$column] = $line[$count];
      } else {
        $obj[$column] = null;
      }

      $count++;
    }

    return (object) $obj;
  }

  /**
   * Only gets the csv columns for large files in order to limit strain on the server
   *
   * @param string $file The csv file path
   * @return array The csv columns
   */
  public static function getColumns($file)
  {
    $columns = [];

    $fh = fopen($file, 'r');
    if ($fh) {
      if ($line = fgetcsv($fh)) {
        $columns = $line;
      }
      fclose($fh);
    }

    return $columns;
  }

  /**
   * Get the total number of csv rows
   *
   * @param string $file The csv file path
   * @return integer
   */
  public static function getTotalRows($file)
  {
    $total = 0;

    $memory_limit = ini_get('memory_limit');

    ini_set('memory_limit', '-1');

    if ($file = file($file)) {
      $total = count($file);
      unset($file);
    }

    ini_set('memory_limit', $memory_limit);

    // Adjust for the columns row
    if ($total > 0) {
      $total--;
    }

    return $total;
  }

  /**
   * Converts the data from a csv dataset to associative arrays
   *
   * Example:
   * from
   * [['Zip', 'IMB']['33624', '1234567890'],...]
   * to
   * [['Zip' => '33624', 'IMB' => '1234567890'],...]
   *
   * @param string|array $dataset The dataset to convert
   * @param [$columns] The original columns
   * @param [$valid] The valid columns
   * @return array
   */
  public static function convert($dataset, $columns = [], $valid = [])
  {
    $items = [];

    // Convert csv strings to an array of rows
    if (is_string($dataset)) {
      $dataset = preg_split('/\R/', trim($dataset));
      $dataset = array_map('str_getcsv', $dataset);
    }

    $dataset = !is_array($dataset) || ($dataset && !is_array($dataset[0]))  ? [$dataset] : $dataset;
    $columns = !$columns ? array_shift($dataset) : $columns;

    // Clean up columns
    $columns = array_map('trim', $columns);

    // Convert CSV
    foreach ($dataset as $data) {
      $item = [];

      $isEmpty = true;

      foreach ($columns as $index => $column) {
        if (!$valid || in_array($column, $valid)) {
          $value = null;

          if (isset($data[$index])) {
            $value = $data[$index];
          }

          if ($isEmpty && !is_null($value)) {
            $isEmpty = false;
          }

          $item[$column] = $value;
        }
      }

      if (!$isEmpty) {
        $items[] = $item;
      }
    }

    return $items;
  }

  /**
   * Normalizes csv file line breaks
   *
   * @param string $file The csv file
   */
  public static function normalize($file)
  {
    $normalize = false;

    if (file_exists($file)) {

      $limit = 1048576 * 25; // 25 MB filesize limit

      if (filesize($file) <= $limit) {

        // Detect the encoding for line breaks
        ini_set('auto_detect_line_endings', true);

        // Check if the file has old mac breaks
        $fh = fopen($file, 'r');
        while (!feof($fh)) {
          $line = fgets($fh);

          if (preg_match('/\\r$/', $line)) {
            $normalize = true;
            break;
          }
        }

        fclose($fh);

        // Normalizes the file if needed
        if ($normalize) {

          // Read CSV file and build data
          $data = [];
          $fh = fopen($file, 'r');
          while (!feof($fh)) {
            $data[] = fgetcsv($fh);
          }

          fclose($fh);

          // Write CSV file
          $fh = fopen($file, 'w+');
          foreach ($data as $line) {
            if (is_array($line)) {
              fputcsv($fh, $line, ',', '"');
            }
          }

          fclose($fh);
        }
      }
    }
  }

  /**
   * Optimizes csv files by removing undefined columns
   *
   * @param string $src The csv source
   * @param string $dest The csv destination
   * @param array $columns The columns to keep (order not important)
   * @return string The optimized csv file path
   */
  public static function optimize($src, $dest = null, $columns = [])
  {
    $optimize = true;
    $path = null;

    if ($src && $dest && $columns && file_exists($src)) {

      $limit = 1048576 * 25; // 25 MB filesize limit

      if (filesize($src) <= $limit) {

        // Determine destination file path
        if ($dest && is_string($dest)) {
          $dest = trim($dest);

          // Check if destination is relative
          if (!preg_match('/^' . preg_quote(DS, '/') . '/', $dest) && !preg_match('/:/', $dest)) {
            $dir = dirname($src) . DS . $dest;
            $dir .= !preg_match('/' . preg_quote(DS, '/') . '$/', $dir) ? DS : '';
            // @todo Dynamically create directories if they don't exist
            $filename = basename($src);
            $dest = $dir . $filename;
          }
        }

        // Auto detect line breaks
        ini_set('auto_detect_line_endings', true);

        // Check if we have to optimize or if it has already been done
        if (file_exists($dest) && filemtime($dest) > filemtime($src)) {

          // Check if the columns have been updated
          $fh = fopen($dest, 'r');
          $cols = fgetcsv($fh);
          fclose($fh);

          // Skip optimization if the columns haven't changed
          if ($cols && is_array($cols)) {

            // Check for missing columns
            $column_mismatch = false;
            foreach ($columns as $column) {
              if (!in_array($column, $cols)) {
                $column_mismatch = true;
                break;
              }
            }

            // Check for uneeded columns
            if (!$column_mismatch) {
              foreach ($cols as $col) {
                if (!in_array($col, $columns)) {
                  $column_mismatch = true;
                  break;
                }
              }
            }

            // If no missing columns then no need to optimize
            if (!$column_mismatch) {
              $optimize = false;
              $path = $dest;
            }
          }
        }

        // Perform csv file optimization
        if ($optimize) {
          // Read CSV file
          $data = [];
          $fh = fopen($src, 'r');
          while (!feof($fh)) {
            $data[] = fgetcsv($fh);
          }
          fclose($fh);

          if (count($data)) {
            // Set columns
            $cols = $data[0];

            // Setup
            $optimized = [];
            $columns2 = [];

            // Build optimized columns
            foreach ($cols as $index => $col) {
              foreach ($columns as $column) {
                if ($col === $column) {
                  $columns2[$index] = $col;
                }
              }
            }

            ksort($columns2);
            $optimized[] = $columns2;

            // Remove column row from data
            array_shift($data);

            // Iterate data records
            foreach ($data as $record) {

              $record2 = [];

              // Build optimized record
              foreach ($columns2 as $index => $col) {
                if (isset($record[$index])) {
                  $record2[$index] = $record[$index];
                }
              }

              // If optimized records has the right amount of optimized columns then add it to optimized
              if (count($record2) === count($columns2)) {
                ksort($record2);

                // Add record to optimized
                $optimized[] = $record2;
              }
            }

            // Check if optimized has records
            if ($optimized && count($optimized) > 1) {

              $fh = fopen($dest, 'w+');
              foreach ($optimized as $line) {
                if (is_array($line)) {
                  fputcsv($fh, $line, ',', '"');
                }
              }

              fclose($fh);

              $path = $dest;
            }
          }
        }
      }
    }

    return $path;
  }
}
