<?php

namespace WSHWordpress\Utils;

/**
 * Common utilities
 *
 * @author Ed Rodriguez
 * @version 1.0
 */
class UtilsDate extends UtilsBase
{
  public static function getTime()
  {
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $start = $time;

    return $start;
  }

  public static function now($type = '')
  {
    $type = strtolower(trim($type));

    if ($type == 'mysql') {
      return date('Y-m-d H:i:s');
    }

    return self::modifyTime();
  }

  public static function modifyTime($unixtime = '', $hr = 0, $min = 0, $sec = 0, $mon = 0, $day = 0, $yr = 0)
  {
    $unixtime = (empty($unixtime)) ? mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('Y')) : $unixtime;
    $dt = localtime($unixtime, true);

    $unixnewtime = mktime(
      $dt['tm_hour'] + $hr, $dt['tm_min'] + $min, $dt['tm_sec'] + $sec,
      $dt['tm_mon'] + 1 + $mon, $dt['tm_mday'] + $day, $dt['tm_year'] + 1900 + $yr
    );

    return $unixnewtime;
  }
  /**
   * Returns an array of Week Days
   *
   * @return boolean An array of Week Days
   */
  public static function getWeekDays($abbr = 0)
  {
    if ($abbr === 1) {
      return ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    } elseif ($abbr === 2) {
      return ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    }

    return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  }

  /**
   * Returns an array of Months
   *
   * @return boolean An array of Months
   */
  public static function getMonths($abbr = 0)
  {
    if ($abbr === 1) {
      return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    }

    return ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  }

  /**
   * Returns the year from a raw database date string
   *
   * @param string $date The raw database date string
   * @return integer The year
   */
  public static function getYear($date = '', $pattern = 'Y')
  {
    return self::formatDate($pattern, $date);
  }

  /**
   * Returns the month from a raw database date string
   *
   * @param string $date The raw database date string
   * @return integer The month
   */
  public static function getMonth($date = '', $pattern = 'm')
  {
    return self::formatDate($pattern, $date);
  }

  /**
   * Returns the day from a raw database date string
   *
   * @param string $date The raw database date string
   * @return integer The day
   */
  public static function getDay($date = '', $pattern = 'd')
  {
    return self::formatDate($pattern, $date);
  }

  /**
   * Returns the hours from a raw database date string
   *
   * @param string $date The raw database date string
   * @return integer The hours
   */
  public static function getHour($date = '', $pattern = 'H')
  {
    return self::formatDate($pattern, $date);
  }

  /**
   * Returns the minutes from a raw database date string
   *
   * @param string $date The raw database date string
   * @return integer The minutes
   */
  public static function getMinute($date = '', $pattern = 'i')
  {
    return self::formatDate($pattern, $date);
  }

  /**
   * Returns the seconds from a raw database date string
   *
   * @param string $date The raw database date string
   * @return integer The seconds
   */
  public static function getSecond($date = '', $pattern = 's')
  {
    return self::formatDate($pattern, $date);
  }

  /**
   * Returns the meridiem from a raw database date string
   *
   * @param string $date The raw database date string
   * @return integer The meridiem
   */
  public static function getMeridiem($date = '', $pattern = 'a')
  {
    return self::formatDate($pattern, $date);
  }

  public static function getDatesBetween($start, $end)
  {
    $array = [];
    $start_unix = strtotime($start);
    $end_unix = strtotime($end);

    while ($start_unix <= $end_unix) {
      $array[] = date('Y-m-d', $start_unix);
      $start_unix = strtotime('+1 day', $start_unix);
    }

    return $array;
  }

  /**
   * Returns the month's name
   *
   * @param string $month The month to get the name
   * @return string The month's name
   */
  public static function monthAsText($month)
  {
    $month_name = null;
    $months = self::getMonths();

    for ($i=0; $i <= count($months); $i++) {
      if ($month == $i) {
        $month_name = $months[$i];
      }
    }

    return $month_name;
  }

  /**
   * Transforms a date into a relative human readable date from current server time
   *
   * @param date $datefrom The date to transform
   * @param string $type The type of translation to perform
   * @param date $dateto The cut off date
   * @return object An relative human readable date
   */
  public static function date($datefrom, $type = 'relative', $dateto = -1)
  {
    if ($type == 'relative') {
      $datefrom = self::getUnixDate($datefrom);

      if ($datefrom <= 0) { return 'from a long time ago'; }
      if ($dateto == -1) { $dateto = time(); }

      $difference = $dateto - $datefrom;

      if ($difference < 60) {
        $interval = 's';
      } elseif ($difference >= 60 && $difference<60*60) {
        $interval = 'n';
      } elseif ($difference >= 60*60 && $difference<60*60*24) {
        $interval = 'h';
      } elseif ($difference >= 60*60*24 && $difference<60*60*24*7) {
        $interval = 'd';
      } elseif ($difference >= 60*60*24*7 && $difference < 60*60*24*30) {
        $interval = 'ww';
      } elseif ($difference >= 60*60*24*30 && $difference < 60*60*24*365) {
        $interval = 'm';
      } elseif ($difference >= 60*60*24*365) {
        $interval = 'y';
      }

      switch ($interval) {
        case 'm':
          $months_difference = floor($difference / 60 / 60 / 24 / 29);

          while (mktime(date('H', $datefrom), date('i', $datefrom),
            date('s', $datefrom), date('n', $datefrom)+($months_difference),
            date('j', $dateto), date('Y', $datefrom)) < $dateto) {
            $months_difference++;
          }

          $datediff = $months_difference;

          if ($datediff == 12) { $datediff--; }

          $res = $datediff == 1 ? "$datediff month ago" : "$datediff
          months ago";
          break;
        case 'y':
          $datediff = floor($difference / 60 / 60 / 24 / 365);
          $res = $datediff == 1 ? "$datediff year ago" : "$datediff
          years ago";
          break;
        case 'd':
          $datediff = floor($difference / 60 / 60 / 24);
          $res = $datediff == 1 ? "$datediff day ago" : "$datediff
          days ago";
          break;
        case 'ww':
          $datediff = floor($difference / 60 / 60 / 24 / 7);
          $res = $datediff == 1 ? "$datediff week ago" : "$datediff
          weeks ago";
          break;
        case 'h':
          $datediff = floor($difference / 60 / 60);
          $res = $datediff == 1 ? "$datediff hour ago" : "$datediff
          hours ago";
          break;
        case 'n':
          $datediff = floor($difference / 60);
          $res = $datediff == 1 ? "$datediff minute ago" :
          "$datediff minutes ago";
          break;
        case 's':
          $datediff = $difference;
          $res = $datediff == 1 ? "$datediff second ago" :
          "$datediff seconds ago";
          break;
      }

      return $res;
    } else {
      return self::formatDate($type, $datefrom);
    }

    return $datefrom;
  }

  /**
   * Formats a specified date. Used for multiple date types.
   * http://us.php.net/date
   *
   * @param string $pattern The format pattern
   * @param string $date The raw date
   * @param string $type The raw date type
   * @return string The formatted date
   */
  public static function formatDate($pattern, $date = '', $type = 'mysql')
  {
    if (empty($date)) {
      return '';
    }

    $type = strtolower(trim($type));

    if ($type == 'mysql') {
      $date = self::getUnixDate($date);
    }

    $date = date($pattern, $date);

    return $date;
  }

  public static function isToday($date = '')
  {
    if (!empty($date)) {
      $date = self::formatDate('m/d/Y', $date);

      if (date('m/d/Y') == $date) {
        return true;
      }
    }

    return false;
  }

  public static function isAfter($params='')
  {
    $date = '';
    $debug = 0;
    $on = false;

    // PARAMS
    $locals = ['date', 'on', 'debug'];
    $params = self::prepParams($params);

    // SINGLE ARGUMENT PARAM SETUP (if needed)
    if ($params) {
      $keys = array_keys($params);

      if (!array_key_exists('date', $params)) {
        $key = trim($keys[0]);

        if (strlen($params[$key]) > 0 && !in_array($key, $locals)) {
          $date = $params[$key];
        }
      }
    }

    // LOCAL PARAM SETUPS
    foreach ($params as $key=>$value) {
      if (!is_numeric($key) && in_array($key, $locals)) {
        $$key = $value;
      }
    }

    $date = trim($date);

    if (!empty($date)) {
      if ($debug) {
        echo(date('m/d/Y H:i:s').' - Now<br />'."\n");
      }

      //$now = mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('Y'));
      $now = (strpos($date, ' ') !== false || !preg_match('/(-|\/)/i', $date)) ? mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')) : mktime(0, 0, 0, date('m'), date('d'), date('Y'));
      $unix = self::getUnixDate($date, $debug);

      if ($debug) {
        echo $now.' - Now<br />'."\n";
        echo $unix.' - Date<br />'."\n";
      }

      if (($on && $now >= $unix) || ($now > $unix)) {
        return true;
      }
    }

    return(false);
  }

  public static function isBefore($params = '')
  {
    $date = '';
    $debug = 0;
    $on = false;

    // PARAMS
    $locals = ['date', 'on', 'debug'];
    $params = Utils::prepParams($params);

    // SINGLE ARGUMENT PARAM SETUP (if needed)
    if ($params) {
      $keys = array_keys($params);

      if (!array_key_exists('date', $params)) {
        $key = trim($keys[0]);

        if (strlen($params[$key]) > 0 && !in_array($key, $locals)) {
          $date = $params[$key];
        }
      }
    }

    // LOCAL PARAM SETUPS
    foreach ($params as $key=>$value) {
      if (!is_numeric($key) && in_array($key, $locals)) {
        $$key = $value;
      }
    }

    $date = trim($date);

    if (!empty($date)) {
      if ($debug) {
        echo date('m/d/Y H:i:s').' - Now<br />'."\n";
      }

      //$now = mktime(date('H'),date('i'),date('s'),date('m'),date('d'),date('Y'));
      $now = (strpos($date, ' ') !== false || !preg_match('/(-|\/)/i', $date)) ? mktime(date('H'), date('i'), date('s'), date('m'), date('d'), date('Y')) : mktime(0, 0, 0, date('m'), date('d'), date('Y'));
      $unix = self::getUnixDate($date, $debug);

      if ($debug) {
        echo $now.' - Now<br />'."\n";
        echo $unix.' - Date<br />'."\n";
      }

      if (($on && $now <= $unix) || ($now < $unix)) {
        return true;
      }
    }

    return false;
  }

  public static function inRange($date1 = '', $date2 = '')
  {
    if (!empty($date1) && !empty($date2)) {
      if (self::isAfter(['date' => $date1, 'on' => 1]) && self::isBefore(['date' => $date2, 'on' => 1])) {
        return true;
      }
    }

    return false;
  }

  /**
   * Returns a unix date from the specified mysql date.
   *
   * @param string $date The mysql date
   * @return string The unix date
   */
  public static function getUnixDate($date='', $debug=0)
  {
    $date = trim($date);

    if (is_numeric($date)) {
      return($date);
    }

    $patterns = [];
    $patterns[] = PatternUtils::pattern('datetime');
    $patterns[] = PatternUtils::pattern('mysql_datetime');
    $patterns[] = PatternUtils::pattern('time');

    $count = 1;

    foreach ($patterns as $pattern) {
      $matches = array();
      preg_match_all($pattern, $date, $matches);

      if (isset($matches['datetime']) && count($matches['datetime']) > 0) {
        $matches = self::keyArray($matches);

        // DATE SETUP
        $year = (isset($matches['year']) && $matches['year'] && !empty($matches['year'][0])) ? $matches['year'][0] : date('Y');
        $month = (isset($matches['month']) && $matches['month'] && !empty($matches['month'][0])) ? $matches['month'][0] : date('m');
        $day = (isset($matches['day']) && $matches['day'] && !empty($matches['day'][0])) ? $matches['day'][0] : date('d');

        // TIME SETUP
        $hour = (isset($matches['hour']) && $matches['hour'] && !empty($matches['hour'][0])) ? $matches['hour'][0] : 0;
        $minute = (isset($matches['minute']) && $matches['minute'] && !empty($matches['minute'][0])) ? $matches['minute'][0] : 0;
        $second = (isset($matches['second']) && $matches['second'] && !empty($matches['second'][0])) ? $matches['second'][0] : 0;

        // MERIDIAN SETUP (The default is AM because it matches up to military time the best)
        $meridian = 'a';

        if (isset($matches['meridian']) && $matches['meridian'] && !empty($matches['meridian'][0])) {
          $meridian = preg_replace('/m/i', '', $matches['meridian'][0]);
        } else {
          $meridian = ($hour == 12) ? 'p' : $meridian;
        }

        $meridian = trim(strtolower($meridian));

        if ($meridian == 'p' && $hour < 12) {
          $hour = $hour + 12;
        } elseif ($meridian == 'a' && $hour == 12) {
          $hour = 0;
        }

        // DEBUGGING
        //echo '<xmp>'.print_r($matches, 1).'</xmp><hr />';
        if ($debug) {
          echo '<!-- Date Pattern: '.$count.' -->'."\n";
          echo $month.'/'.$day.'/'.$year.' '.$hour.':'.$minute.':'.$second.' - Date<br />'."\n";
        }

        return mktime($hour, $minute, $second, $month, $day, $year);
      }

      $count++;
    }

    //echo $date.'<br />';
    return $date;
  }

  /**
   * Returns the last day of the month
   *
   * @param interger $time The strtotime to base it on
   * @return integer The last day of the month
   */
  public static function getLastday($time = 'today')
  {
    $time = trim($time);
    $time = empty($time) ? 'today' : $time;

    return $lastday = date('t', strtotime($time));
  }

  public static function getLastMonths ($count = 12, $date = '')
  {
    $months = [];

    if ($count > 0) {
      $date = trim($date);
      $date = empty($date) ? date('Y-m-d') : $date;
      $time = Utils::getUnixDate($date);
      $months[date('m', $time)] = date('Y', $time);

      // Last 12 Months
      for ($i = 1; $i < $count; $i++) {
        $hour = date('H', $time);
        $minute = date('i', $time);
        $second = date('s', $time);
        $month = date('n', $time);
        $day = date('j', $time);
        $year = date('Y', $time);

        if ($month == 1) {
          $month = 12;
          $year = $year - 1;
        } else {
          $month = $month - 1;
        }

        $time = mktime($hour, $minute, $second, $month, $day, $year);
        $months[date('m', $time)] = date('Y', $time);
      }
    }

    return $months;
  }

  public static function convertTimeZone($date, $from, $format='Y-m-d H:i:s', $to='America/New_York')
  {
    $obj = new DateTime($date, new DateTimeZone($from));
    $obj->setTimezone(new DateTimeZone($to));

    return $obj->format($format);
  }

/**
 * Converts a local timezone datetime to another timezone datetime.
 *
 * <br><b>Usage</b><br>
 * <code>
 * $datetime = UtilsDate::convertLocalTime('2012-06-06T16:20:43+0000');
 * </code>
 *
 * ----
 *
 * @param string $datetime The datetime to convert.
 * @param string [$timezone=America/New_York] The timezone to convert it too.
 * @return date A mysql formatted date.
 */
  public static function convertLocalTime($datetime, $timezone='America/New_York')
  {
      date_default_timezone_set($timezone);

      return date('Y-m-d H:i:s', strtotime($datetime));
  }
}