<?php

namespace WSHWordpress\Utils;

class CacheUtils extends Utils
{
    private static $reload = false;

    /**
     * Return true if caching is disabled
     * Detects if a browser hard refresh was present in the request headers.
     *
     * @note Ignores ajax requests
     * @return boolean True if caching is disabled
     */
    public static function nocache()
    {
        $headers = function_exists('getallheaders') ? getallheaders() : [];

        /*
            If-Modified-Since: (Missing for all)
            If-None-Match: (Missing for all)
            Pragma: no-cache (IE has)
            Cache-Control: no-cache (Firefox has)
        */
        if (!isset($headers['X-Requested-With'])
            && !isset($headers['If-Modified-Since'])
            && !isset($headers['If-None-Match'])) {

            if (
                isset($headers['Cache-Control'])
                && preg_match('/no-cache/i', $headers['Cache-Control'])
            ) {
                return true;
            }
        }

        // QueryString (?reload or ?nocache)
        if (isset($_REQUEST['reload']) || isset($_REQUEST['nocache'])) {
            return true;
        }

        // Static
        if (self::$reload) {
            return true;
        }

        // Global
        if (isset($GLOBALS['cms.reload'])) {
            return true;
        }

        return false;
    }
}