<?php

namespace WSHWordpress\Utils;

use WSHWordpress\Models\Zip;

/**
 * Zip Utilities
 *
 * @author erodriguez
 */
class ZipUtils extends Utils
{
  /**
   * Gets zip information from local first then remote if needed
   *
   * @param integer $zip
   * @return object
   */
  public static function getZip($zip)
  {
    $_zip = null;

    $zip = $zip ? trim($zip) : $zip;

    if ($zip && is_numeric($zip) && strlen($zip) === 5) {
      $_zip = Zip::where('zip', $zip)->first();

      if (!$_zip) {
        $_zip = new Zip;
      }

      // Update zip if needed
      if ($_zip && empty($_zip->country)) {
        $result = GoogleUtils::geocode($zip);

        if ($result) {
          $components = $result->address_components;

          // Zip
          $_zip->zip = $zip;

          foreach ($components as $component) {
            if ($component->types) {
              // City
              if (in_array('locality', $component->types)) {
                $_zip->city = $component->long_name;
              }

              // State
              if (in_array('administrative_area_level_1', $component->types)) {
                $_zip->state = $component->short_name;
              }

              // County
              if (in_array('administrative_area_level_2', $component->types)) {
                $_zip->county = $component->long_name;
              }

              // Country
              if (in_array('country', $component->types)) {
                $_zip->country = $component->short_name;
              }
            }
          }

          // Lat & Long
          $_zip->latitude = $result->geometry->location->lat;
          $_zip->longitude = $result->geometry->location->lng;

          // Only save if in US
          if ($_zip->country === 'US') {
            $_zip->save();
          }
        }
      }
    }

    if ($_zip && empty($_zip->country)) {
      $_zip = null;
    }

    return $_zip;
  }

  public static function milesByZips($zipFrom, $zipTo) {
    $miles = null;

    $zipFrom = self::getZip($zipFrom);

    if (!$zipFrom) {
      return $miles;
    }

    $zipTo = self::getZip($zipTo);

    if ($zipTo) {
      $distance = self::distance($zipFrom->latitude, $zipFrom->longitude, $zipTo->latitude, $zipTo->longitude);

      if ($distance) {
        $miles = $distance->miles;
      }
    }

    return $miles;
  }

  /**
   * Calculates the great-circle distance between two points, with the Haversine formula.
   *
   * @param float $latitudeFrom Latitude of start point in [deg decimal]
   * @param float $longitudeFrom Longitude of start point in [deg decimal]
   * @param float $latitudeTo Latitude of target point in [deg decimal]
   * @param float $longitudeTo Longitude of target point in [deg decimal]
   * @param float $earthRadius Mean earth radius in [m]
   * @return float Distance between points in [m] (same as earthRadius)
   */
  public static function distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
  {
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);

    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;

    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

    $meters = $angle * $earthRadius;
    $kilometers = $meters / 1000;
    $miles = $meters * 0.000621371;

    $obj = (object) [
      'miles' => round($miles, 2),
      'meters' => round($meters, 2),
      'kilometers' => round($kilometers, 2),
    ];

    return $obj;
  }

  /**
   * Calculate distance between lat & long
   *
   * @param float $lat1 The zip1 latitude
   * @param float $long1 The zip1 longitude
   * @param float $lat2 The zip2 latitude
   * @param float $long2 The zip2 longitude
   * @param string [$unit] The unit to return the distance in
   * @return integer The distance
   */
  public static function distance2($lat1, $lon1, $lat2, $lon2, $unit = 'm')
  {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if (preg_match('/^(k|kilometers)$/i', $unit)) {
      return ($miles * 1.609344);
    } else if (preg_match('/^(n|nautical)$/i', $unit)) {
      return ($miles * 0.8684);
    } else if (preg_match('/^(m|miles)$/i', $unit)) {
      return $miles;
    } else {
      return $miles;
    }
  }
}
