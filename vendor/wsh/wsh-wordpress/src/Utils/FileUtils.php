<?php

namespace WSHWordpress\Utils;

use WSHWordpress\Models\File;

/**
 * File specific utilities
 *
 * @author Ed Rodriguez
 * @version 1.0
 */
class FileUtils extends Utils
{
    /**
     * Merges successful chunked uploads
     * 
     * @param object $plugin
     */
    public function mergeChunked($plugin = null)
    {
      if ($plugin && isset($_REQUEST['_FILES'])) {
        $files = $_REQUEST['_FILES'];

        $files = is_string($files) ? json_decode(stripslashes(trim($files))) : $files;
        
        if (is_object($files)) {
          $files = get_object_vars($files);
          
          foreach ($files as $key => $item) {
            if (!isset($_FILES[$key]) && isset($item->tmp_name)) {
              $tmp_name = $plugin->cacheRoot . 'chunks' . DS . $item->tmp_name;
              
              if (file_exists($tmp_name)) {
                $name = preg_replace('/-\d+\.tmp$/i', '', basename($tmp_name));
                $type = filetype($tmp_name);
                $size = filesize($tmp_name);
                
                $file = (object) [
                  'name' => $name,
                  'type' => $type,
                  'tmp_name' => $tmp_name,
                  'error' => 0,
                  'size' => $size
                ];

                $_FILES[$key] = $file;
              }
            }
          }
        }
      }
    }
    
    /**
     * Creates a directory and sets its permissions
     *
     * @param string [$path] The path to the directory
     * @param string|array $params Encapsulated parameters
     *      {@param string [mode] The chmod mode}
     *      {@param string [owner] The chown owner}
     *      {@param string [group] The chgroup group}
     * @return boolean True if the directory was successfully created
     */
    public static function createDir ($path = '', $params = '')
    {
      $success = false;

      // CONFIGURATION
      $owner = null;
      $group = null;
      $mode = null;

      if (empty($mode)) {
        if (!empty($owner) && $owner == $group) {
          $mode = '0775';
        } else {
          $mode = '0755';
        }
      }

      $locals = ['mode', 'owner', 'group'];
      $params = Utils::prepParams($params);
      
      foreach ($params as $key=>$value) {
        if (in_array($key, $locals)) {
          $$key = $value;
        }
      }

      $mode = octdec(trim($mode));

      if (preg_match('/' . preg_quote(DIRECTORY_SEPARATOR, '/') . '$/', $path)) {
        $dirname = $path;
      } else {
        $pathinfo = pathinfo($path);
        $dirname = $pathinfo['dirname'] . DIRECTORY_SEPARATOR;
      }

      if (is_dir($dirname)) {
        $success = true;
      } else {
        if (@mkdir($dirname, $mode, true)) {
          if (isset($mode) && !empty($mode)) {
            @chmod($dirname, $mode);
          }

          if (!empty($owner) && !empty($owner)) {
            @chown($dirname, $owner);
          }

          if (!empty($group) && !empty($group)) {
            @chgrp($dirname, $group);
          }

          $success = true;
        }
      }

      return $success;
    }

    /**
     * Returns a safe filename for upload purposes
     *
     * @todo Add max name length checks
     * @param string [$path] The file path
     * @param string [$delimiter=_] The delimiter to use
     * @param boolean [$lowercase=0] Whether to apply lowercase
     * @return string Returns a safe filename
     */
    public static function prepName($path = '', $delimiter = '_', $lowercase = 0)
    {
      $path = trim($path);
      $pathinfo = pathinfo($path);
      $dirname = $pathinfo['dirname'];
      $dirname = $dirname == '.' ? '' : $dirname.'/';
      $extension = strtolower(@$pathinfo['extension']);
      $origName = self::getFilename($pathinfo['basename']);
      $origName = $lowercase ? strtolower($origName) : $origName;
      $origName = str_replace(' ', $delimiter, $origName);
      $filename = '';

      for ($i=0; $i<strlen($origName); $i++) {
        if (preg_match('([0-9]|[a-z]|[A-Z]|_|-|\.|\[|\])', $origName[$i])) {
          $filename .= $origName[$i];
        }
      }

      // PREVENTS NAME FROM STARTING WITH DOT
      $filename = preg_match('/^\./', $filename) ? substr($filename, 1) : $filename;

      $filename = empty($filename) ? 'Untitled' : $filename;
      if (!empty($extension)) {
        $filename .= '.' . $extension;
      }

      $path = $dirname . $filename;

      $path = preg_replace('/(\/|\\\)/i', DIRECTORY_SEPARATOR, $path);

      return $path;
    }

    /**
     * Returns a unique filename by appending a bracketed increment
     *
     * @param string [$path] The file path
     * @return string A modified unique path
     */
    public static function uniqueName($path = '')
    {
      $path = self::prepName($path);

      if (file_exists($path)) {
        $i = 1;
        
        while (file_exists($path)) {
          $path = preg_replace('/(\[\d+\])?(\.[a-z0-9]+)$/i', "[{$i}]$2", $path);
          $i++;
        }
      }

      return $path;
    }

    /**
     * Returns the file's extension
     * 
     * Example: jpg, gif, png
     * 
     * @param string [$path] The file's path
     * @param boolean [$lowercase=1] Whether to lowercase
     * @return string The file's extension
     */
    public static function getExt($path = '', $lowercase = 1)
    {
      $path = trim($path);
      $pathinfo = pathinfo($path);
      $extension = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
      $extension = self::getBool($lowercase) ? strtolower($extension) : $extension;

      return $extension;
    }
    
    /**
     * Gets the file's name
     *
     * @param string $path The file's path
     * @return string The file's name
     */
    public static function getFilename($path)
    {
      $path = trim($path);
      $pathinfo = pathinfo($path);
      $filename = $pathinfo['basename'];
      $extension = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
      $filename = !empty($extension) ? substr($filename, 0, (strlen($filename) - (strlen($extension)+1))) : $filename;

      return $filename;
    }

    /**
     * Appends a suffix to a file's name
     *
     * @param string $path The file's path
     * @param boolean $suffix The suffix to append
     * @return string The file's path with an appended suffix
     */
    public static function addSuffix($path, $suffix)
    {
      $path = trim($path);
      $pathinfo = pathinfo($path);
      $dirname = $pathinfo['dirname'];
      $dirname = $dirname == '.' ? '' : $dirname.'/';
      $basename = $pathinfo['basename'];
      $extension = $pathinfo['extension'];
      $filename = self::getFilename($basename).$suffix;

      return $dirname.$filename.'.'.$extension;
    }

    /**
     * Returns a formatted filesize
     *
     * 1 KiB and 1 KB is 1024−10001024−1000 bytes
     * 
     * @param integer [$size] The byte size
     * @return string The size of the file in (bytes, KiB, MiB, GiB...)
     */
    public static function getFileSize($size = '', $decimals = null, $bytes = 1000)
    {
      if (!is_numeric($size) && is_string($size) && !empty($size)) {
        $size = @filesize($size);
      }

      if (is_numeric($size)) {
        $type = 'bytes';

        $sizes = 'KB, MB, GB, TB, PB, EB, ZB, YB, BB';

        if ($bytes === 1024) {
          $sizes = 'KiB, MiB, GiB, TiB, PiB, EiB, ZiB, YiB, BiB';
        }

        $sizes = Utils::trimExplode(',', $sizes);
        $sizes = array_reverse($sizes);

        $count = count($sizes);
        foreach ($sizes as $key) {
          $val = pow($bytes, $count);

          if ($size >= $val) {
            $size = $size/$val;
            $type = $key;
            break;
          }

          $count--;
        }

        if (is_numeric($decimals)) {
          $size = round($size, $decimals);
        }

        return $size.' '.$type;
      }

      return '';
    }
    
    /**
     * Uploads a file
     * 
     * Uploaded File Example
     * ------------------------
     * name: Penguins.jpg
     * type: image/jpeg
     * tmp_name: C:\xampp\tmp\php2CD5.tmp
     * error: 0
     * size: 777835
     * 
     * @param type $params
     * @return object
     */
    public static function upload($params = '')
    {
      global $_plugin;

      // Required params
      $field = null;
      $caller = null;
      $plugin = null;
      
      // Default params
      $file = new File;
      $file->active = 1;
      $path = null;
      $max = null;
      $extensions = null;
      $replace = false;
      
      $locals = Utils::trimExplode(',', 'field, caller, plugin, file, path, max, extensions, replace');
      
      $params = self::prepParams($params);
      
      foreach ($params as $key => $value) {
        if (in_array($key, $locals)) {
          $$key = $value;
        }
      }
      
      $plugin = $plugin ? $plugin : $_plugin;

      // Upload file
      if ($field && $caller && $plugin && isset($_FILES[$field])) {
        $upload = (object) $_FILES[$field];
        $ext = self::getExt($upload->name);

        // Validate server upload errors
        if (!$caller->hasErrors() && $upload->error) {
          $caller->error('An error occured while uploading');
          $caller->error($upload->error);
        }

        // Validate upload size
        if (!$caller->hasErrors()) {  
          if (!$upload->size) {
            $caller->error('Please select a file to upload');
          } elseif (is_numeric($max) && $upload->size > $max) {
            $caller->error('Your file was too large');
          }
        }
        
        // Validate valid upload extensions
        if (!$caller->hasErrors() && isset($extensions) && !in_array($ext, $extensions)) {
          $caller->error('The uploaded file extension type is not allowed');
        }

        // Upload file
        if (!$caller->hasErrors()) {
          $filename = $upload->name;
          
          // Replace current file
          if ($replace) {
            
            $replaced = false;
            $ext2 = self::getExt($replace);

            if ($replace && $ext === $ext2) {
              $replaced = true;
            } else {
              $old = $plugin->siteRoot . $replace;

              if (file_exists($old)) {
                // @todo Save 301 redirect records here
                unlink($old);
              }
            }

            // Replace or upload
            $filename = $replaced ? basename($replace) : $filename;
          }

          // Build destination path
          $path = trim($path);
          
          if ($path) {
            $path = preg_replace('/(\/|\\\)+/', DS, $path);
            $path .= !preg_match('/' . preg_quote(DS, '/') . '$/', $path) ? DS : '';
          }
          
          $dest = $plugin->uploadRoot . $path . $filename;
          
          $dest = FileUtils::prepName($dest);
          
          // Make unique if new upload
          if (!$replace) {
            $dest = FileUtils::uniqueName($dest);
          }

          $dir = dirname($dest);
          
          // Copy original uploaded file over
          if (self::createDir($dest)) {
            if (is_writeable($dir)) {
              $success = false;
              
              // Move file
              if (is_uploaded_file($upload->tmp_name)) {
                $success = move_uploaded_file($upload->tmp_name, $dest);
              } else {
                $success = rename($upload->tmp_name, $dest);
              }
              
              // Handle success
              if ($success) {
                $file->file = preg_replace('/(\/|\\\)+/', '/', preg_replace('/^' . preg_quote($plugin->siteRoot, '/') . '/i', '/', $dest));
                $file->name = !$file->name ? basename($file->file) : $file->name;
                $file->size = filesize($dest);
                $file->type = filetype($dest);
                $file->extension = $ext;
                $file->active = !isset($file->active) || is_null($file->active) ? 1 : $file->active;
                
                // Save file
                if (!$file->save()) {
                  $caller->addLogs($file->logs());
                }
              } else {
                $caller->error('Unable to copy original upload to directory');
              }
            } else {
              $caller->error('Unable to write to directory');
              $caller->error($dir);
            }
          } else {
            $caller->error('Upload directory missing');
            $caller->error($dir);
          }
        }
      }

      return $file;
    }
}