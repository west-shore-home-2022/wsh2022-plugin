<?php

namespace WSHWordpress\Utils;

/**
 * Regular Expression Patterns specific utilities
 *
 * @author Ed Rodriguez
 */
class PatternUtils extends Utils
{
  /**
   * @var array Should contain an associative array of regular expression patterns
   */
  public static $patterns = [
    'tags_opened'=>[
      '5.3.7'=>'
        /
          (?P<tags>
            <(?P<namespace> [a-z0-9_-]+):(?P<name> (?2))
              (?P<attributes>
                [^>]*?
              )
            >
            (?P<content>
              .*?
            )
            <\/\2:\3>
          )
      /xis',
      'default'=>'
        /
          (?P<tags>
            <(?P<namespace> [a-z0-9_-]+):(?P<name> [a-z0-9_-]+)
              (?P<attributes>
                [^>]*?
              )
            >
            (?P<content>
              .*?
            )
            <\/\2:\3>
          )
      /xis',
    ],
    'tags_closed'=>[
      '5.3.7'=>'
      /
        (?P<tags>
          <(?P<namespace> [a-z0-9_-]+):(?P<name> (?2))
            (?P<attributes>
              (.
                (?!
                  <\/\2:\3>
                )
              )*?
            )
          \/>
        )
      /xis',
      'default'=>'
      /
        (?P<tags>
          <(?P<namespace> [a-z0-9_-]+):(?P<name> [a-z0-9_-]+)\s
            (?P<attributes>.*)
          \/>
        )
      /Uxis',
    ],
    'html_tags_opened'=>[
      'default'=>'
        /
          (?P<tags>
            <{$tag}
              (?P<attributes>
                [^>]*?
              )
            >
            (?P<content>
              .*?
            )
            <\/{$tag}>
          )
      /xis',
    ],
    'html_tags_closed'=>[
      '5.3.7'=>'
      /
        (?P<tags>
          <(?P<name> {$tag})
            (?P<attributes>
              (.
                (?!
                  <\/{$tag}>
                )
              )*?
            )
          \/>
        )
      /xis',
      'default'=>'
      /
        (?P<tags>
          <(?P<name> {$tag})\s
            (?P<attributes>.*)
          \/>
        )
      /Uxis',
    ],
    'css_paths'=>'
    /
      (url\s*?\(|\@import\s*)
      \s*?
      (\'|")*?
      (.*)
      (\'|")*
      \s*?
      \)
    /Uxis',
    'attributes'=>'
    /
      (?P<attribute>
        (?P<name>
          [a-z0-9_-]+
        )
        =
        ("|\')
          (?P<value>
            .*?
          )
       (\3)
      )
    /xis',
    /*
        Date Patterns:
        6-4-09
        6-4-2009
        06-04-09
        06-04-2009
        6/4/09
        6/4/2009
        06/04/09
        06/04/2009
        etc...
        Time Patterns:
        09:30:30
        09:30
        9:30
        9
        etc...
        Date & Time Examples:
        06/04/2009 9:30
        06-04-09 9
        06-04-09 9p
        06/04/2009 9 pm
        06-04-09 9:30 p
        etc...
     */
    'datetime'=>'
    /
      (?P<datetime>
        ^(?P<date>
          (?P<month> \d{1,2})
          (?: -|\/)
          (?P<day> \d{1,2})
          (?: -|\/)
          (?P<year> \d{2,4})
        )
        (?: \ )?
        (?P<time>
          (?:
            (?: \ )?
            (?P<hour> [0-2]{0,1}[0-9]{1,1})
            (?:
              :(?P<minute> [0-5]{1,1}[0-9]{1,1})
              (?:
                :(?P<second> [0-5]{1,1}[0-9]{1,1})
              )?
            )?
            (?: \ )?
            (?P<meridian> am|pm|a|p)?
          )?
        )?
      )
    /xis',
    'email'=>'
    /
      (
        (
          (?<!\"|\'|\.)
          \b((\w+\.)*\w+@(\w+\.)*\w+(\w+\-\w+)*\.\w+)\b
        )
      )
    /xi',
    'email2'=>'
    /
      (
        (
          (?<!\"|\'|\.|\:|\=)
          \b(((\w+\.)*\w+)@((\w+\.)*\w+(\w+\-\w+)*\.\w+))(?!\<\/a\>)\b
        )
      )
    /xi',
    /*
        MySQL Datetime Patterns:
        2009-04-22 16:14:00
        2009-06-04
     */
    'mysql_datetime'=>'
    /
      (?P<datetime>
        ^(?P<date>
            (?P<year> \d{4,4})
            (?: -(?P<month> \d{1,2}))?
            (?: -(?P<day> \d{1,2}))?
        )
        (?: \ )?
        (?P<time>
          (?:
            (?: \ )?
            (?P<hour> [0-2]{0,1}[0-9]{1,1})
            (?:
              :(?P<minute> [0-5]{1,1}[0-9]{1,1})
              (?:
                :(?P<second> [0-5]{1,1}[0-9]{1,1})
              )?
            )?
            (?: \ )?
            (?P<meridian> am|pm|a|p)?
          )?
        )?
      )
    /xis',
    'http'=>"
    /
      (
        (?<! \"|\'|\">|\'>)
        (
          http(s?):\/\/
          [a-z0-9;\/\?:@=\&\$\-_\.\+!*'\(\),~%]+
          \/?
        )
      )
    /xi",
    'http2'=>"
    /
      (
        (
          (?<!\"|\')
          (\b((http(s?):\/\/))
          ([a-z0-9;\/\?:@=\&\$\-_\.\+!*'\(\),~%]+)\b(\/)?)
        )
      )
    /xi",
    'params'=>'
    /
      (?P<param>
        (?P<name>
          [a-z0-9_-]+
        )
        \s*(?<!\\\)
        (?: :|=)
        \s*
        (?P<value>
          .*?
        )
      )
      (?=(?1)|$)
    /xis',
    /*
        Time Patterns:
        09:30
        09:30:30
        9pm
        9:00 p
        etc...
     */
    'time'=>'
    /
      (?P<datetime>
        ^(?P<time>
          (?P<hour> [0-2]{0,1}[0-9]{1,1})
          (?:
            :(?P<minute> [0-5]{1,1}[0-9]{1,1})
            (?:
              :(?P<second> [0-5]{1,1}[0-9]{1,1})
            )?
          )?
          (?: \ )?
          (?P<meridian> am|pm|a|p)?
        )
      )
    /xis',
    'www'=>"
    /
      (
        (?<! \/|\@|\"|\'|<wbr\s\/>)
        (
          www\.
          [a-z0-9;\/\?:@=\&\$\-_\.\+!*'\(\),~%]+
          \/?
        )
      )
    /xi",
    'www2'=>"
    /
      (
        (
          (?<!\/|\@|\"|\')
          (\b((www\.))
          ([a-z0-9;\/\?:@=\&\$\-_\.\+!*'\(\),~%]+)\b)
        )
      )
    /xi",
  ];

  /**
   * Alias: Gets patterns based on PHP version since
   * PCRE varies from version to version
   *
   * @param string|array The encapsulated params
   *      {@param string [name] The pattern's name}
   * @return string Returns a regular expression pattern
   */
  public static function pattern($params = '')
  {
    return self::getPattern($params);
  }

  /**
   * Gets patterns based on PHP version since
   * PCRE varies from version to version
   *
   * @param string|array The encapsulated params
   *      {@param string [name] The pattern's name}
   * @return string Returns a regular expression pattern
   */
  public static function getPattern($params = '')
  {
    $name = '';
    $pattern = '';
    $locals = ['name'];
    $params = Utils::prepParams($params);
    
    foreach ($params as $key => $value) {
      if (in_array($key, $locals)) {
        $$key = $value;
      }
    }

    // FIRST ARGUMENT
    if (strlen($name) <= 0 && isset($params[0])) {
      $name = $params[0];
    }

    if (!empty($name)) {
      $patterns = self::$patterns;

      if (isset($patterns[$name])) {
        if (is_array($patterns[$name])) {
          $version = phpversion();
          foreach ($patterns[$name] as $key => $value) {
            if (version_compare(PHP_VERSION, $key) <= 0) {
              $pattern = $value;
              break;
            }
          }

          if (empty($pattern) && isset($patterns[$name]['default'])) {
            $pattern = $patterns[$name]['default'];
          }
        } else {
          $pattern = $patterns[$name];
        }
      }
    }

    if (!empty($pattern) && count($params) > 1) {
      foreach ($params as $key => $value) {
        $pattern = preg_replace('/\{\$' . preg_quote($key, '/') . '\}/i', $value, $pattern);
      }
    }

    return $pattern;
  }
}