<?php

namespace WSHWordpress\Utils;

use WSHWordpress\Config\YamlConfig;

/**
 * Wufoo specific utilities
 *
 * @author Ed Rodriguez
 * @created 2019-07-31
 */
class WufooUtils extends Utils
{
  public static $config = '
    -
      prop: first_name
      pattern: /^(first|first name):?$/i
    -
      prop: last_name
      pattern: /^(last|last name):?$/i
    -
      prop: first_name
      pattern: /^(name|full name):?$/i
      name: true
    -
      prop: last_name
      pattern: /^(name|full name):?$/i
      name: true
    -
      prop: address
      pattern: /^(address|street address):?$/i
    -
      prop: city
      pattern: /^(city):?$/i
    -
      prop: state
      pattern: /^(state):?$/i
    -
      prop: zip
      pattern: /^(zip|zip code|postal code):?$/i
    -
      prop: phone
      pattern: /^(phone|phone number):?$/i
    -
      prop: email
      pattern: /^(email|email address):?$/i
    -
      prop: product
      pattern: /(product of interest)/i
    -
      prop: comments
      pattern: /(comments)/i
    -
      prop: source
      pattern: /(source)/i
    -
      prop: referrer
      pattern: /(referrer)/i
  ';

  /********************************
   * Wufoo form field methods
   ********************************/

  /**
   * Get the first name
   *
   * @return string
   */
  public static function firstname()
  {
    return self::value('first_name');
  }
  
  /**
   * Get the last name
   *
   * @return string
   */
  public static function lastname()
  {
    return self::value('last_name');
  }
  
  /**
   * Get the email
   *
   * @return string
   */
  public static function email()
  {
    return self::value('email');
  }
  
  /**
   * Get the phone
   *
   * @return string
   */
  public static function phone()
  {
    return self::value('phone');
  }
  
  /**
   * Get the address
   *
   * @return string
   */
  public static function address()
  {
    return self::value('address');
  }
  
  /**
   * Get the city
   *
   * @return string
   */
  public static function city()
  {
    return self::value('city');
  }
  
  /**
   * Get the state
   *
   * @return string
   */
  public static function state()
  {
    return self::value('state');
  }
  
  /**
   * Get the zip
   *
   * @return string
   */
  public static function zip()
  {
    return self::value('zip');
  }
  
  /**
   * Get the product
   *
   * @return string
   */
  public static function product()
  {
    return self::value('product');
  }
  
  /**
   * Get the comments
   *
   * @return string
   */
  public static function comments()
  {
    return self::value('comments');
  }
  
  /**
   * Get the source
   *
   * @return string
   */
  public static function source()
  {
    return self::value('source');
  }
  
  /**
   * Get the referrer
   *
   * @return string
   */
  public static function referrer()
  {
    return self::value('referrer');
  }
  
  /**
   * Get the Wufoo form name
   *
   * @return string
   */
  public static function formname()
  {
    return self::form('name');
  }
  
  /**
   * Get the Wufoo form hash
   *
   * @return string
   */
  public static function formhash()
  {
    return self::form('hash');
  }
  
  /**
   * Get the Wufoo form id
   *
   * @return string
   */
  public static function formid()
  {
    return self::id();
  }
  
  /********************************
   * Utility methods
   ********************************/

  /**
   * Get Wufoo form properties
   *
   * Props: name, description, redirectmessage, url, email, ispublic, language,
   * startdate, enddate, entrylimit, datecreated, dateupdated, hash, array
   *
   * @param string [$prop]
   * @return mixed
   */
  public static function form($prop = '')
  {
    $value = null;
    $form = isset($_POST['FormStructure']) ? $_POST['FormStructure'] : null;

    if ($form) {
      $form = JsonUtils::decode($form);

      if (JsonUtils::isObject($form)) {
        $temp = get_object_vars($form);
        $form = (object) array_combine(array_map('strtolower', array_keys($temp)), $temp);

        if ($prop) {
          if (isset($form->{$prop})) {
            $value = $form->{$prop};
          }
        } else {
          $value = $form;
        }
      }
    }

    return $value;
  }

  /**
   * Get the Wufoo form field mapping
   *
   * @param array [$data]
   * @return array
   */
  public static function mapping($data = [])
  {
    $map = [];

    $data = $data ? $data : $_POST;

    $fields = isset($data['FieldStructure']) ? $data['FieldStructure'] : null;

    if ($fields) {
      $fields = JsonUtils::decode($fields);

      if (JsonUtils::isObject($fields) && isset($fields->Fields)) {
        $fields = $fields->Fields;

        // Build Names Lookup
        foreach ($fields as $field) {
          $map[$field->ID] = $field->Title;

          if (isset($field->SubFields)) {
            foreach ($field->SubFields as $subfield) {
              $map[$subfield->ID] = $subfield->Label;
            }
          }
        }
      }
    }

    return $map;
  }

  /**
   * Get a Wufoo form field value
   *
   * @param string $prop
   * @param string|array [$config]
   * @param array [$data]
   * @return mixed
   */
  public static function value($prop, $config = '', $data = [])
  {
    global $_plugin;

    $plugin = $_plugin ? $_plugin : null;

    $value = null;

    $map = self::mapping($data);

    if ($map) {
      // 1) Load config from YAML
      if (!$config && $plugin) {
        $_config = $plugin->load('wufoo', false);

        if ($_config && isset($_config->config)) {
          $config = $_config->config;
        }
      }

      // 2) Load config from local static variable
      $config = $config ? $config : self::$config;

      // 3) Parse config if needed
      if (is_string($config)) {
        if (preg_match('/^(\{|\[)/', $config)) {
          if (class_exists('JsonUtils')) {
            $config = JsonUtils::decode($config);
          } else {
            $config = json_decode($config);
          }
        } else if (class_exists('WSHWordpress\Config\YamlConfig')) {
          $config = YamlConfig::parse($config);
        }
      }

      if (is_array($config)) {
        $data = $data ? $data : array_merge($_POST, $_GET);

        foreach ($config as $item) {
          $item = is_array($item) ? (object) $item : $item;

          if (is_object($item) && isset($item->prop) && $item->prop === $prop) {
            $field = null;

            // Find field
            foreach ($map as $_field => $name) {
              if (preg_match($item->pattern, $name)) {
                $field = $_field;
                break;
              }
            }

            if ($field && isset($data[$field])) {
              $value = $data[$field];

              // Split the name field
              if (isset($item->name) && $item->name) {
                $name = self::name($value);

                if (isset($name->{$prop})) {
                  $value = $name->{$prop};
                }
              }

              break;
            }
          }
        }
      }
    }

    return $value;
  }

  /**
   * Get the Wufoo form entry id
   *
   * @return integer
   */
  public static function id()
  {
    return isset($_POST['EntryId']) ? $_POST['EntryId'] : null;
  }

  /**
   * Wufoo name field value callback
   *
   * @param string $value
   * @param string $default_first
   * @param string $default_last
   * @return object
   */
  public static function name($value, $default_first = 'No First Name', $default_last = 'No Last Name')
  {
    // Prep name
    $name = htmlspecialchars(trim($value));
    $name = preg_split('/\s+/', $name);

    $first_name = $name ? array_shift($name) : '';
    $last_name = $name ? implode(' ', $name) : '';

    // Handle no names
    $first_name = !preg_match('/^[a-z]/i', $first_name) ? $default_first : $first_name;
    $last_name = !preg_match('/^[a-z]/i', $last_name)? $default_last : $last_name;

    return (object) [
      'first_name' => $first_name,
      'last_name' => $last_name
    ];
  }

  /**
   * Protects an array
   *
   * @param array $array
   * @param [integer] $show The length to protect
   * @param [array] $keys The optional keys to protect otherwise protect all keys
   * @return array
   */
  public static function protectArray($array, $show = 4, $keys = [])
  {
    $data = [];

    if (isset($array) && is_array($array)) {
      foreach ($array as $key => $value) {
        if (!$keys) {
          $data[$key] = $this->protectString($value, $show);
        } else if (in_array($key, $keys)) {
          $data[$key] = $this->protectString($value, $show);
        } else {
          $data[$key] = $value;
        }
      }
    }

    return $data;
  }
  
  /**
   * Protects a string by only showing a portion of the string
   *
   * @param string $str The string to protect
   * @param integer $show The amount to show
   * @return string
   */
  public static function protectString($str, $show = 4)
  {
    if (!is_string($str)) {
      return $str;
    }
    
    $length = strlen($str);
    
    if ($length) {
      $hint = substr($str, 0, $show);
      
      if ($length >= $show) {
        $hint .= str_repeat('-', $length - $show);
      }
      
      return $hint;
    }

    return $str;
  }

  /**
   * Submit form to Wufoo
   *
   * @param string $url
   * @param array $inputs
   * @param array [$protect]
   * @param object [$logger]
   * @return boolean
   */
  public static function submit($url, $inputs, $protect = [], $logger = false)
  {
    $success = false;

    // Prep inputs
    foreach ($inputs as $key => &$value) {
      if (is_string($value)) {
        $value = htmlspecialchars($value);
      }
    }

    if ($logger) {
      $json = self::protectArray($inputs, 4, $protect);
      $json = json_encode($json, JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
      $plugin->info($json);
    }

    $query = http_build_query($inputs);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($inputs));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $error = curl_error($ch);

    curl_close($ch);

    $success = $status && ($status == 200 || $status == 302);
    
    if ($logger) {
      if ($success) {
        $logger->success("Lead successfully submitted to Wufoo");
      } else {
        $logger->error("Unable to submit lead to Wufoo", true);
        $logger->error("Result: " . print_r($result, 1));
        $logger->error("Status: " . print_r($status, 1));
        $logger->error("Error: " . print_r($error, 1));
      }
    }
    
    return $success;
  }
}