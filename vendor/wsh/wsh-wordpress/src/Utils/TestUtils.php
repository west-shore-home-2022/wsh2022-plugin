<?php

namespace WSHWordpress\Utils;

class TestUtils extends Utils
{
  /**
   * Formats data for html output
   *
   * @param array $data
   * @param boolean [$die]
   * @return string
   */
  public static function html($data, $die = true)
  {
    $html = "<!DOCTYPE html>
      <html>
      <head>
        <style>
          body {
            background: #EEE;
            font-family: 'Roboto',sans-serif,verdana,arial;
            line-height: 20px;
            font-size: 13px;
            padding: 20px;
          }
          hr {
            margin: 20px 0;
            border: 1px dashed #AAA;
            border-bottom: 0;
          }
          fieldset {
            background: #FFF;
            margin-bottom: 20px;
            padding: 20px;
            border: 1px solid #AAA;
          }
          legend {
            font-weight: normal;
            font-size: 16px;
            padding: 0 10px;
          }
          pre {
            background: #F5F5F5;
            border: 1px solid #DDD;
            overflow: auto;
            padding: 20px;
            max-height: 400px;
          }
        </style>
      </head>
      <body>
    ";

    foreach ($data as $name => $item) {
      $html .= "
        <fieldset>
          <legend>{$name}</legend>
      ";

      if (is_array($item)) {
        $keys = array_keys($item);

        $assoc = !is_numeric($keys[0]);

        if ($assoc) {
          foreach ($item as $key => $val) {
          $val = is_array($val) || is_object($val) || (is_string($val) && preg_match('/\n/', $val)) ? '<pre>' . print_r($val, 1) . '</pre>' : "{$val}<br />";
            $html .= "<b>{$key}:</b> {$val}";
          }
        } else {
          $html .= '<pre>' . print_r($item, 1) . '</pre>';
        }
      } else {
        $html .= '<pre>' . print_r($item, 1) . '</pre>';
      }

      $html .= "</fieldset>";
    }

    $html .= "
      </body>
    </html>";

    if ($die) {
      header('Content-Type: text/html');
      die($html);
    }

    return $html;
  }
}