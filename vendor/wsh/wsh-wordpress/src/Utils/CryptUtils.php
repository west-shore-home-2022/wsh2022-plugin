<?php

namespace WSHWordpress\Utils;

use Exception;

/**
 * Encryption specific utilities
 *
 * The encryption function takes the ascii value of each character in the string
 * and adds it an associated ascii value in the key. If the result is greater than
 * 255(ascii maximum) it loops around by subtracting 256 from the combined value.
 * The key is looped around as well so if we have a 5 character key then on character
 * 6 of the message the function goes back to the first character of the key. The
 * decryption function works pretty much the same but in reverse. It takes the
 * encrypted message and subtracts from each character's ascii value the associated
 * key ascii value. If the result is less than zero it adds 256 to compensate for
 * the looping from the encryption function. Here is a picture of the encryption process.
 * 
 * Notes:
 * Here are some possible 2 way encoding techniques for cleaner database storage:
 * bin2hex, base64_encode (33% larger), and convert_uuencode (35% larger). If the
 * current encryption was to be wrapped in one of these, this would allow it to be stored
 * in a non UTF-8 table column.
 *
 * Encryption Types:
 * 1. bin2hex (preffered type)
 * 2. base64_encode
 * 3. convert_uuencode
 *
 * Researched Approaches:
 * 1. mcrypt (two way encryption - requires php plugin)
 * 2. md5 (one way encryption)
 * 3. crypt (one way encryption)
 * 4. openssl_encrypt (two way encryption)
 *    *the preferred way for handling encryption as of php 5.3
 * 
 * @author Ed Rodriguez
 * @version 1.0
 * @copyright Copyright (c) 2010
 */
class CryptUtils extends Utils
{

  /**
   * The encryption key variable used for all encryptions
   * 
   * @var string
   */
  protected $key = 'asdf890BN';
  protected $type = '1';
  public static $instance;

  public static function getInstance()
  {
    if (!isset(self::$instance)) {
      $class = __CLASS__;
      self::$instance = new $class();
    }

    return self::$instance;
  }

  /**
   * Alias: Returns an encrypted string based on the configuration key
   * 
   * @param string $str The string to encrypt
   * @param mixed [$params] Optional parameters
   * @return string An encrypted string
   */
  public static function encode($str, $params = '')
  {
    return self::encrypt($str, $params);
  }

  /**
   * Returns an encrypted string based on the configuration key
   *
   * @see WSHWordpress\Utils\CryptUtils::encrypt()
   * @param string $str The string to encrypt
   * @param mixed [$params] Optional parameters
   * @return string An encrypted string
   */
  public static function encrypt($str, $params = '')
  {
    // PARAMS
    $locals = ['key', 'type'];

    $params = Utils::prepParams($params);
    foreach ($params as $name=>$value) {
      if (in_array($name, $locals)) {
        $$name = $value;
      }
    }

    // CONVERT ARRAY to STRING
    if (is_array($str)) {
      $convert = '';

      if ($str) {
        foreach ($str as $name=>$value) {
          if (!is_numeric($name) && (is_string($value) || is_bool($value) || is_numeric($value))) {
            $convert .= "{$name}:{$value},";
          }
        }

        $convert = preg_replace('/,$/', '', $convert);
      }

      $str = $convert;
    }

    // SETUP
    $key = (!isset($key)) ? self::getInstance()->key : trim($key);
    $type = (!isset($type)) ? self::getInstance()->type : trim($type);

    $keylength = strlen($key);
    $strlength = strlen($str);
    $encstring = '';

    for ($i=0; $i<=$strlength - 1; $i++) {
      $msgord = ord(substr($str, $i, 1));
      $keyord = ord(substr($key, $i % $keylength, 1));

      if ($msgord + $keyord <= 255) {
        $encstring .= chr($msgord + $keyord);
      }

      if ($msgord + $keyord > 255) {
        $encstring .= chr(($msgord + $keyord)-256);
      }
    }

    // TYPE TO RETURN
    switch ($type) {
      case '1':
        return bin2hex($encstring);
        break;
      case '2':
        return base64_encode($encstring);
        break;
      case '3':
        return convert_uuencode($encstring);
        break;
    }

    return $encstring;
  }

  /**
   * Alias: Returns a decrypted string based on the configuration key
   *
   * @see cms\utils\CryptUtils::decrypt()
   * @param string $str The string to decrypt
   * @param mixed [$params] Optional parameters
   * @return string A decrypted string
   */
  public static function decode($str, $params = '')
  {
    return self::decrypt($str, $params);
  }

  /**
   * The opposite of the built in php function bin2hex
   * 
   * @return string The decoded bin2hex value
   */
  public static function hex2bin($h)
  {
    if (!is_string($h)) {
      return null;
    }

    $r = '';
    
    for ($a=0; $a<strlen($h); $a+=2) {
      $r .= @chr(hexdec($h{$a}.$h{($a+1)}));
    }

    return $r;
  }

  /**
   * Returns a decrypted string based on the configuration key
   * 
   * @param string $str The string to decrypt
   * @param mixed [$params] Optional parameters
   * @return string A decrypted string
   */
  public static function decrypt($str, $params = '')
  {
    // PARAMS
    $locals = array('key', 'type');
    $params = Utils::prepParams($params);

    foreach ($params as $name=>$value) {
      if (in_array($name, $locals)) {
        $$name = $value;
      }
    }

    // SETUP
    $key = (!isset($key)) ? self::getInstance()->key : trim($key);
    $type = (!isset($type)) ? self::getInstance()->type : trim($type);

    // TYPE SETUP
    switch ($type) {
      case '1':
        $str = self::hex2bin($str);
        break;
      case '2':
        $str = base64_decode($str);
        break;
      case '3':
        $str = convert_uudecode($str);
        break;
    }

    $keylength = strlen($key);
    $strlength = strlen($str);
    $decstring = '';

    for ($i=0; $i<=$strlength - 1; $i++) {
      $msgord = ord(substr($str, $i, 1));
      $keyord = ord(substr($key, $i % $keylength, 1));
      
      if ($msgord - $keyord >= 0) {
        $decstring .= chr($msgord - $keyord);
      }
      
      if ($msgord + $keyord < 0) {
        $decstring .= chr(($msgord - $keyord) + 256);
      }
    }

    return $decstring;
  }

  /**
   * Encrypts a URL param in the shortest possible way
   * 
   * @param string $str The string to encrypt
   * @return string An encrypted string
   */
  public static function urlEncrypt($str)
  {
    return urlencode(base64_encode($str));
  }

  /**
   * Decrypts a URL param in the shortest possible way
   * 
   * @param string $str The string to decrypt
   * @return string A decrypted string
   */
  public static function urlDecrypt($str)
  {
    return base64_decode(urldecode($str));
  }

  /**
   * Encrypts arrays
   * 
   * @param array $array
   * @return string An encrypted string
   */
  public static function encryptArray($array)
  {
    return self::encrypt(JsonUtils::encode($array));
  }

  /**
   * Decrypts arrays
   * 
   * @param array $array
   * @return array A decrypted array
   */
  public static function decryptArray($array)
  {
    return JsonUtils::decode(self::decrypt($array));
  }

  /**
   * Encrypts objects
   * 
   * @param object $obj
   * @return string An encrypted string
   */
  public static function encryptObject($obj)
  {
    return self::encrypt(JsonUtils::encode($obj));
  }

  /**
   * Decrypts objects
   * 
   * @param object $obj
   * @return object A decrypted object
   */
  public static function decryptObject($obj)
  {
    return JsonUtils::decode(self::decrypt($obj));
  }

  /**
   * Encrypts param in the shortest possible way
   * 
   * @param string [$params]
   * @return string An encrypted string
   */
  public static function encryptParams($params =' ')
  {
    $params = preg_replace('/\r\n|\n|\r/', '', $params);
    $params = preg_replace('/\t/', '', $params);
    $params = preg_replace('/\ {2,}/', '', $params);
    $params = preg_replace('/(?<!\\\):(\s)+/', ':', $params);
    $params = self::encrypt($params);

    return $params;
  }

  /**
   * Decrypts a param in the shortest possible way
   * 
   * @param string [$params]
   * @return string A decrypted string
   */
  public static function decryptParams($params = '')
  {
    $params = self::decrypt($params);
    $params = self::prepParams($params);

    return $params;
  }
}