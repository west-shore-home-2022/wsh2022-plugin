<?php

namespace WSHWordpress\Utils;

/**
 * Google API Utilities
 *
 * @author erodriguez
 */
class GoogleUtils extends Utils
{
  /**
   * Get Google geocode info for an address
   *
   * @param string $address
   * @param string [$key]
   * @return object|boolean
   */
  public static function geocode($address, $key = null)
  {
    global $_plugin;

    $plugin = $_plugin ? $_plugin : null;

    $key = $plugin ? $plugin->config('api.google') : $key;

    if ($key) {
      $address = trim($address);
      $address = urlencode($address);
      
      // Search by address
      $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key={$key}";
      
      // Search by postal code
      if (is_numeric($address)) {
        $url = "https://maps.googleapis.com/maps/api/geocode/json?components=country:US|postal_code:{$address}&key={$key}";
      }

      $json = file_get_contents($url);
      $response = JsonUtils::decode($json, true);

      if (JsonUtils::isArray($response) && $response['status'] == 'OK' && $response['results']) {
        $result = json_decode(json_encode($response['results'][0]));

        return $result;
      } else if ($plugin) {
        // API responded with a invalid response or an error
        $plugin->error($response);
      }
    } else if ($plugin) {
      // Missing API key
      $plugin->error('Google Maps API key');
    }

    return false;
  }
}