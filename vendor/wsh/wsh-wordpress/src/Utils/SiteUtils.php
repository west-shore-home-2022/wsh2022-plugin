<?php

namespace WSHWordpress\Utils;

use DeviceDetector\DeviceDetector;
use DeviceDetector\Parser\Device\DeviceParserAbstract;

/**
 * Site utilities
 *
 * @author Ed Rodriguez
 */
class SiteUtils extends Utils
{
  /**
   * Parses the user agent and returns os, client and bot information.
   * Used mostly for tracking & logging purposes.
   *
   * <br><b>Usage</b> (script)<br>
   * <code>
   * $info = SiteUtils::getInfo();
   * </code>
   *
   * ----
   *
   * @author Ed Rodriguez
   * @param string [$agent] An optional agent string to parse
   * @return object Returns os, client and bot information
   */
  public static function getInfo ($agent = '')
  {
    $agent = isset($agent) && is_string($agent) ? trim($agent) : null;
    $agent = empty($agent) && isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : $agent;

    $os = (object) [
      'name' => '',
      'version' => ''
    ];

    $client = (object) [
      'name' => '',
      'version'=> ''
    ];

    $bot = (object) [
      'name' => ''
    ];

    if (!empty($agent) && class_exists('DeviceDetector\DeviceDetector')) {
      DeviceParserAbstract::setVersionTruncation(DeviceParserAbstract::VERSION_TRUNCATION_NONE);

      $dd = new DeviceDetector($agent);

      $dd->parse();

      if ($dd->isBot()) {
        // Handles bots, spiders, crawlers,...
        $_bot = $dd->getBot();
        $bot = isset($_bot) && is_array($_bot) ? self::arrayToObject($_bot) : $bot;
      } else {
        $_os = $dd->getOs();
        $os = isset($_os) && is_array($_os) ? self::arrayToObject($_os) : $os;

        $_client = $dd->getClient(); // holds information about browser, feed reader, media player, ...
        $client = isset($_client) && is_array($_client) ? self::arrayToObject($_client) : $client;

        //$_device = $dd->getDevice();
        //$_brand = $dd->getBrand();
        //$_model = $dd->getModel();
      }
    }

    // SET DEFAULTS
    if (!isset($os->name)) {
      $os->name = '';
    }
    if (!isset($os->version)) {
      $os->version = '';
    }

    if (!isset($client->name)) {
      $client->name = '';
    }
    if (!isset($client->version)) {
      $client->version = '';
    }

    if (!isset($bot->name)) {
      $bot->name = '';
    }

    $info = (object) [
      'agent' => $agent,
      'os' => $os,
      'client' => $client,
      'bot' => $bot
    ];

    return $info;
  }
}