<?php

namespace WSHWordpress\Utils;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception as PHPMailer_Exception;

class EmailUtils extends Utils
{
  /**
   * The main email function used to send emails
   *
   * $attachments = [['path' => 'path/to/file', 'as' => 'name.pdf', 'mime' => 'application/pdf']];
   * $attachments = [['data' => {RAW_DATA}, 'as' => 'name.pdf', 'mime' => 'application/pdf']];
   *
   * @param object $plugin
   * @param string|array $params Encapsulated params
   *    {@param string [$config] The email's config}
   *    {@param string [$to] The email's to value}
   *    {@param string [$cc] The email's cc (Carbon Copy) value}
   *    {@param string [$bcc] The email's bcc (Blind Carbon Copy) value}
   *    {@param string [$subject_prepend] The prepend subject value}
   *    {@param string [$subject_append] The append subject value}
   *    {@param string [$subject] The email's subject value}
   *    {@param string [$body] The email's body value}
   *    {@param string [$html] The email's html optional body value}
   *    {@param boolean [$type] The method type in which to send the email}
   *    {@param array [$attachments] Optional file attachments}
   *    {@param string [$replyto] The email's replyto value}
   *    {@param string [$from] The email's from value}
   *    {@param string [$from_name] The email's from name value}
   *    {@param string [$priority] The email's priority value}
   *    {@param string [$template_html] The html email template to use}
   *    {@param string [$template_text] The text email template to use}
   *    {@param boolean [$send=true] To send or not}
   *    {@param boolean [$debug=false] To debug or not}
   *    {@param string [$mode=php] To mode to send with}
   * @return boolean True if the email was sent
   */
  public static function mail($plugin, $params='')
  {
    set_time_limit(0);

    // Custom variable holder for templates
    $vars = [];

    $sent = false;
    $send = true;
    $debug = false;
    $modes = ['php', 'phpmailer'];
    $mode = $plugin->config('mail.mode', $modes[0]);
    $from = $plugin->config('mail.from');
    $from_name = $plugin->config('mail.from_name');

    // Define local variables
    $locals = self::trimExplode(',', '
      config, mode, send, debug, to, cc, bcc, subject_prepend, subject_append, subject,
      body, html, text, type, attachments, replyto, replyto_name, from, from_name,
      priority, template_html, template_text
    ');

    // Initiate local variable instances
    foreach ($locals as $key) {
      if (!isset($$key)) {
        $$key = null;
      }
    }

    // Parse incoming params
    $params = self::prepParams($params);

    // Common template variables
    $params['site_url'] = site_url();
    $params['logo'] = get_template_directory_uri() . '/assets/images/logo.png';

    // Combine params into the local scope
    foreach ($params as $key => $value) {
      if (in_array($key, $locals)) {
        $$key = $value;
      } // Add non-locals to the template variable holder
      else {
        $vars[$key] = $value;
      }
    }

    // Set Mode
    $mode = !in_array($mode, $modes) ? $modes[0] : $mode;

    // Don't send server emails
    if ($plugin) {
      $send = $plugin->config('mail.send', $send);

      if (!is_null($send) && !$send) {
        return true;
      }

      $send = true;
    }

    // Load custom configurations
    if ($config) {
      $plugin_emails = $plugin->load('emails');
      $plugin_config = $plugin->config();
      $plugin_settings = $plugin->settings->all();

      $data = $plugin->find($config, null, $plugin_emails);

      if (is_object($data)) {
        $props = get_object_vars($data);

        // Combine configs into the local scope
        foreach ($props as $key => $value) {
          $value = $plugin->parse($value, $plugin_emails, $plugin_config, $plugin_settings);

          if (in_array($key, $locals)) {
            // Only combine if it doesn't already have a value
            if (!$$key) {
              $$key = $value;
            }
          } // Add non-locals to the template variable holder
          else {
            $vars[$key] = $value;
          }
        }

        // Domain overrides
        $host = @$_SERVER['HTTP_HOST'];

        if ($host && isset($data->domains)) {
          $domains = get_object_vars($data->domains);

          foreach ($domains as $domain => $overrides) {
            $pattern = '/' . preg_quote($domain, '/') . '$/i';

            if (preg_match($pattern, $host)) {
              $overrides = get_object_vars($overrides);

              // Combine configs into the local scope
              foreach ($overrides as $key => $value) {
                $value = $plugin->parse($value, $plugin_emails, $plugin_config, $plugin_settings);

                if (in_array($key, $locals)) {
                  $$key = $value;
                } // Add non-locals to the template variable holder
                else {
                  $vars[$key] = $value;
                }
              }
              break;
            }
          }
        }
      }
    }

    // Replace any dynamic syntax in local string values with template values
    // Example:
    //      $code = 500;
    //      $subject = "Server Error ({$code})";
    foreach ($locals as $key) {
      if (isset($$key) && is_string($$key)) {

        // First find all {$variable} syntax
        $matches = [];
        preg_match_all('/\{\$([a-z0-9_\-]*)\}/i', $$key, $matches);

        if ($matches && $matches[0] && $matches[1]) {

          // For each syntax found replace it with a
          foreach ($matches[1] as $item) {
            $pattern = '/\{\$'.preg_quote($item, '/').'\}/';
            $value2 = (isset($vars[$item]) && (!is_array($vars[$item]) || !is_object($vars[$item]))) ? $vars[$item] : '';

            $$key = preg_replace($pattern, $value2, $$key);
          }
        }
      }
    }

    // FROM NAME
    $from_name = !$from && !$from_name ? $plugin->config('mail.from_name') : $from_name;

    // SMTP
    $smtp = $plugin->config('mail.smtp');

    // SUBJECT
    if ($subject) {
      if ($subject_prepend) {
        $subject = $subject_prepend . $subject;
      }

      if ($subject_append) {
        $subject .= $subject_append;
      }

      // Replaces special html characters with UTF-8 values
      $subject = preg_replace_callback(
        '/&#?[a-z0-9]+;/i',
        function ($matches) {
          return mb_convert_encoding($matches[0], 'UTF-8', 'HTML-ENTITIES');
        },
        $subject
      );
    }

    // Parse HTML Template
    if (!$html && $template_html) {
      $view = $plugin->view($template_html);

      foreach ($vars as $key => $value) {
        $view->{$key} = $value;
      }

      $html = $view->render();
    }

    // Parse Text Template
    if (!$text && $template_text) {
      $view = $plugin->view($template_text);

      foreach ($vars as $key => $value) {
        $view->{$key} = $value;
      }

      $text = $view->render();
    }

    // To
    $to = is_array($to) ? implode(',', $to) : $to;
    
    // Debugging
    if ($debug) {
      $output = '<div style="background:#EEE;padding:20px;font-family:Arial;font-size:10pt;border-bottom:3px solid #000;">';

      foreach ($locals as $key) {
        if (!preg_match('/^(html|debug)$/i', $key) && isset($$key)) {
          if (is_string($$key) || is_numeric($$key) || is_bool($$key) || is_null($$key)) {
            $output .= "<strong>{$key}:</strong> " . $$key . "<br />";
          } elseif ((is_object($$key) || is_array($$key)) && !in_array($key, ['plugin'])) {
            $output .= "<strong>{$key}:</strong><pre>" . print_r($$key, 1) . "</pre><br />";
          }
        }
      }

      $output .= '</div>';

      if ($html) {
        $output .= $html;
      } else if ($text) {
        $output .= $text;
      }

      die($output);
    }

    // Send Email
    else if ($send) {

      // Send via php's built-in mail() method
      if ($mode === 'php') {
        $message = $html ? $html : $text;
        $headers = '';

        if ($from_name && $from) {
          $headers .= "From: {$from_name} <{$from}>\r\n";
        } elseif ($from) {
          $headers .= "From: {$from}\r\n";
        }

        $headers .= $cc ? "CC: {$cc}\r\n" : '';
        $headers .= $bcc ? "BCC: {$bcc}\r\n" : '';
        $headers .= $replyto ? "Reply-to: {$replyto}\r\n" : '';
        $headers .= "MIME-Version: 1.0\r\n";

        if ($attachments) {
          $hash = md5(date('r', time()));
          $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-{$hash}\"\r\n";

          $message = self::buildMultipart($attachments, $hash, $message);
        } else {
          $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        }

        $sent = mail($to, $subject, $message, $headers);
      } else if ($mode === 'phpmailer' && $smtp) {
        $mail = new PHPMailer(true);

        try {
          // Server settings
          if (isset($_GET['test']) && $_GET['test'] === 'phpmailer') {
            $smtp2 = clone $smtp;
            unset($smtp2->password);
            
            echo('<pre>' . print_r($smtp2, 1) . '</pre>');

            $mail->SMTPDebug = SMTP::DEBUG_SERVER;
          }
          
          $mail->isSMTP();

          $mail->Host = $smtp->host;
          $mail->SMTPAuth = isset($smtp->auth);
          
          $mail->Username = $smtp->username;
          $mail->Password = $smtp->password;

          if (isset($smtp->secure)) {
            $secure = PHPMailer::ENCRYPTION_STARTTLS;
            $secure = preg_match('/^ssl$/i', $smtp->secure) ? PHPMailer::ENCRYPTION_SMTPS : $secure;

            $mail->SMTPSecure = $secure;
          }

          if (isset($smtp->verify) && !$smtp->verify) {
            $mail->SMTPOptions = [
              'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
              ]
            ];
          }

          $mail->Port = $smtp->port;

          // From
          if (isset($from)) {
            $mail->setFrom($from, $from_name);
          }

          // To
          $to = self::trimExplode('/(,|;)/', $to);

          foreach ($to as $email) {
            if (self::isValidEmail($email)) {
              $mail->addAddress($email);
            }
          }

          // ReplyTo
          if ($replyto) {
            $mail->addReplyTo($replyto, $replyto_name);
          }

          // CC
          if ($cc) {
            $cc = self::trimExplode('/(,|;)/', $cc);

            foreach ($cc as $email) {
              if (self::isValidEmail($email)) {
                $mail->addCC($email);
              }
            }
          }

          // BCC
          if ($bcc) {
            $bcc = self::trimExplode('/(,|;)/', $bcc);

            foreach ($bcc as $email) {
              if (self::isValidEmail($email)) {
                $mail->addBCC($email);
              }
            }
          }

          // Attachments
          if ($attachments) {
            foreach ($attachments as $attachment) {
              if (file_exists($attachment)) {
                $filename = basename($attachment);
                $mail->addAttachment($attachment, $filename);
              }
            }
          }

          // Subject
          $mail->Subject = $subject;

          // Body
          if ($html) {
            $mail->isHTML(true);

            // HTML Body
            $mail->Body = $html;

            // Text Body
            if ($text) {
              $mail->AltBody = $text;
            }
          } else {
            // Text Body
            $mail->Body = $text;
          }

          $mail->send();

          $sent = 1;
        } catch (PHPMailer_Exception $e) {
          $plugin->error("Email could not be sent. Mailer Error: {$mail->ErrorInfo}");
        }
      }
    }

    return $sent;
  }

  /**
   * Build multipart message with attachments
   *
   * @param array $attachments
   * @param string $hash
   * @param string $html
   * @param string [$text]
   * @return string
   */
  public static function buildMultipart($attachments, $hash, $html, $text = null)
  {
    $message = '';

    ob_start();
    ?>--PHP-mixed-<?="{$hash}\n"?>
Content-Type: multipart/alternative; boundary="PHP-alt-<?=$hash?>"

<?php if ($text) { ?>
--PHP-alt-<?="{$hash}\n"?>
Content-Type: text/plain; charset="iso-8859-1"
Content-Transfer-Encoding: 7bit

<?=$text?>

<?php } ?>
--PHP-alt-<?="{$hash}\n"?>
Content-Type: text/html; charset="iso-8859-1"
Content-Transfer-Encoding: 7bit

<?=$html?>

--PHP-alt-<?=$hash?>--

<?php
  foreach ($attachments as $attachment) {
    if (file_exists($attachment)) {
      $filename = basename($attachment);
      $attachment = chunk_split(base64_encode(file_get_contents($attachment)));
?>
--PHP-mixed-<?="{$hash}\n"?>
Content-Type: application/zip; name="<?=$filename?>"
Content-Transfer-Encoding: base64
Content-Disposition: attachment

<?=$attachment?>
<?php }} ?>
--PHP-mixed-<?=$hash?>--<?php

    $message = ob_get_clean();

    return $message;
  }
}