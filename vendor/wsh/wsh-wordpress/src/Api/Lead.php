<?php

namespace WSHWordpress\Api;

use WSHWordpress\Api\Api;
use WSHWordpress\Models\Lead as LeadModel;
use WSHWordpress\Utils\WufooUtils;

/**
 * Lead API
 *
 * @author Ed Rodriguez
 */
class Lead extends Api
{
  /**
   * Add lead
   *
   * @public
   * @post
   */
  public function add($die = false)
  {
    $plugin = $this->plugin;
    $debug = true;

    $hashkey = $this->value('HandshakeKey');

    // Process request
    if ($hashkey) {
      $model = new LeadModel;
      
      $wufoo_hash = WufooUtils::formhash();
      $wufoo_id = WufooUtils::formid();
      
      // Try to get existing lead record
      if ($wufoo_hash && $wufoo_id) {
        $model = LeadModel::where([
          'wufoo_hash' => $wufoo_hash,
          'wufoo_id' => $wufoo_id
        ])->first();
        
        if ($model) {
          $plugin->info("[TEST] Existing Lead: {$wufoo_hash} (#{$wufoo_id})");
        } else {
          $plugin->info("[TEST] New Lead: {$wufoo_hash} (#{$wufoo_id})");
        }
        
        $model = $model ? $model : new LeadModel;
      }
      
      // Only add new leads
      if ($model->isEmpty()) {
        $model->wufoo_form = WufooUtils::formname();
        $model->wufoo_hash = $wufoo_hash;
        $model->wufoo_id = $wufoo_id;
        $model->first_name = WufooUtils::firstname();
        $model->last_name = WufooUtils::lastname();
        $model->address = WufooUtils::address();
        $model->city = WufooUtils::city();
        $model->state = WufooUtils::state();
        $model->zip = WufooUtils::zip();
        $model->phone = WufooUtils::phone();
        $model->email = WufooUtils::email();
        $model->comments = WufooUtils::comments();
        $model->source = WufooUtils::source();

        // Add lead
        if ($model->wufoo_id) {
          if ($model->add()) {
            if ($this->handleLead($model)) {
              $this->success("Successfully added lead (#{$model->id})");
            } else {
              $this->error("Unable to send lead (#{$model->id}) to vendor");
            }
          } else {
            $this->error("Unable to save lead to database (Wufoo #{$model->wufoo_id}");
            $debug = true;
          }
        } else {
          $this->error("An invalid Wufoo lead was submitted");
          $debug = true;
        }
      } else {
        $this->success('Existing Lead #' . $model->id);
      }
    }

    if (!$this->hasLogs()) {
      $this->error('Unable to add lead');
    }

    // Blank response for WuFoo
    if ($die && $hashkey) {
      $plugin->log($this->response(), 'Info', $debug);

      if (!$this->hasErrors()) {
        http_response_code(200);
        die('');
      }
    }

    return $this->response();
  }

  /**
   * Sends leads to configured vendors
   *
   * @param type $lead
   * @return boolean
   */
  public function handleLead($lead)
  {
    $success = true;

    $plugin = $this->plugin;

    $vendors = $plugin->config('vendors');

    $one_click_contractor = $plugin->config('vendors.one_click_contractors');

    // 1) One Click Contractors
    if ($one_click_contractor) {
    }

    return $success;
  }
}
