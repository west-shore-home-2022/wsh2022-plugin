<?php

namespace WSHWordpress\Api;

use WSHWordpress\Utils\CsvUtils;

class Records extends Api
{
  /**
   * List records
   *
   * Example Link
   * 
   * @users any
   */
  public function index()
  {
    $plugin = $this->plugin;

    if (!$this->hasLogs()) {

      // Get the model dynamically
      $classname = null;
      $model = $this->value('_model');

      if (!$model) {
        $this->error('Missing the required model parameter');
      } else {
        $classname = $plugin->classes('Models', $model);

        if (!$classname || !class_exists($classname)) {
          $this->error('Unable to load the model');
        }
      }

      if (!$this->hasErrors()) {
        $model = new $classname;

        // Request variables
        $page = $this->value('_page', 1);
        $count = $this->value('_count', 30);
        $sort = $model->config->get('sort', 'name');
        $sort = $this->value('_sort', $sort);
        $order = $model->config->get('order', 'ASC');
        $order = $this->value('_order', $order);
        $search = $this->value('_search');
        $all = $this->value('_all');

        // Search columns
        $columns = $model->columns('search');

        // Pagination params
        $params = [
          'columns' => $columns,
          'search' => $search,
          'page' => $page,
          'count' => $count,
          'sort' => $sort,
          'all' => $all,
          'order' => $order
        ];

        // Get custom sql from the model
        $sql = $model->sql('pagination');

        $params['sql'] = $sql;

        // Get pagination
        $response = $classname::pagination($params);

        // Extend record
        foreach ($response->records as &$record) {
          $record->onExtend();
        }

        $this->success($response);
      }
    }

    return $this->response();
  }

  /**
   * Import records
   *
   * @nonce
   * @permissions admin editor
   */
  public function import()
  {
    $plugin = $this->plugin;

    $classname = null;
    $model = $this->value('_model');

    // Import tallies
    $imports = [
      'successes' => [],
      'errors' => []
    ];

    // Validate model
    if (!$this->hasErrors()) {
      if (!$model) {
        $this->error('Missing the required model parameter');
      } else {
        $classname = $plugin->classes('Models', $model);

        if (!$classname || !class_exists($classname)) {
          $this->error('Unable to load the model');
        }
      }
    }

    // Validate file
    if (!$this->hasErrors()) {
      if (!$_FILES || !isset($_FILES['file'])) {
        $this->error('Please select a csv file to import');
      } else {
        $file = (object) $_FILES['file'];

        if ($file->error) {
          $this->error(print_r($file->error, 1));
        } else if (!preg_match('/\.csv$/i', $file->name)) {
          $this->error('Only csv files are allowed for import');
        }
      }
    }

    // Process import
    if (!$this->hasErrors()) {
      $rows = CsvUtils::read($file->tmp_name);

      $count = 1;
      $model = new $classname;

      foreach($rows as $row) {
        $builder = $classname::builder();

        $matches = $model->config->get('import.matches');
        $mappings = $model->config->get('import.mappings');

        if ($matches && $mappings) {
          foreach ($matches as $match) {
            $value = $this->lookup($match, $row, $mappings);
            $builder = $builder->where($match, $value);
          }

          $record = $builder->first();

          if (!$record) {
            $record = new $classname;
          }
        }

        if ($record) {
          $add = $record->isEmpty();

          // Map import fields
          foreach ($mappings as $prop => $array) {
            $value = $this->lookup($prop, $row, $mappings);
            $record->{$prop} = $value;
          }

          // Process request
          if ($add) {
            // Add sale
            if ($record->add()) {
              $imports['successes'][] = $count;
            } else {
              $imports['errors'][] = "#{$count} Unable to add record";
            }
          } else {
            // Update sale
            if ($record->update()) {
              $imports['successes'][] = $count;
            } else {
              $imports['errors'][] = "#{$count} Unable to update record";
            }
          }
        } else {
          $imports['errors'][] = "#{$count} Unable to build record";
        }

        $count++;
      }
    }

    // If no errors then success
    if ($imports['successes']) {
      $total = count($imports['successes']);
      $this->success($total . ' records were successfully imported');
    }

    if ($imports['errors']) {
      $total = count($imports['errors']);
      $this->error($total . ' records were not imported');

      foreach ($imports['errors'] as $error) {
        $message = null;

        if (is_array($error) || is_object($error)) {
          $error = (object) $error;

          if (isset($error->title)) {
            $message = "<strong>{$error->title}</strong> ";
          }

          if (isset($error->message)) {
            $message .= $error->message;
          }
        } else if (is_string($error)) {
          $message = $error;
        }

        if ($message) {
          $this->error($message);
        }
      }
    }

    return $this->response();
  }

  /**
   * Lookups up csv column values by variable column names
   *
   * @param string $lookup
   * @param array $row
   */
  public function lookup($lookup, $row, $mappings = [])
  {
    $value = null;
    $keys = isset($mappings[$lookup]) ? $mappings[$lookup] : [];

    foreach ($keys as $key) {
      if (key_exists($key, $row)) {
        $value = trim($row[$key]);
        break;
      }
    }

    return $value;
  }
}
