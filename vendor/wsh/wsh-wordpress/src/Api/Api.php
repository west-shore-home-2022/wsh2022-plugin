<?php

namespace WSHWordpress\Api;

use WSHWordpress\Traits\Logger;
use WSHWordpress\Utils\Utils;
use WSHWordpress\Utils\UrlUtils;
use WSHWordpress\Utils\CryptUtils;

class Api
{
  use Logger;

  protected $wpdb = null;
  protected $plugin = null;
  protected $request = null;
  protected $route = null;
  protected $params = [];
  protected $keys = [];
  public $ajax = false;
  protected $api = '';

  /**
   * Create the api
   *
   * @param object $plugin
   * @param object [$request]
   */
  public function __construct($plugin = null, $request = null)
  {
    global $wpdb, $_plugin;

    $plugin = $plugin ? $plugin : $_plugin;
    $this->wpdb = $wpdb;
    $this->plugin = $plugin;

    $this->api = $plugin->config('api.url', $this->api);

    $uri = [];

    if ($request) {
      $this->ajax = true;
      $this->request = $request;
      $this->route = $request->get_route();

      $uri = UrlUtils::uriToArray($this->route, 1);
      array_shift($uri);
    }

    $this->params = array_merge($uri, $_REQUEST);
  }

  /**
   * Set a given API param
   *
   * @param string $prop
   * @param mixed $value
   */
  public function set($name, $value)
  {
    $this->params[$name] = $value;
  }

  /**
   * Get a property value from the API params
   *
   * @param string $prop
   * @param [mixed] $default
   * @return mixed
   */
  protected function value($prop, $default = '')
  {
    return $this->plugin->find($prop, $default, $this->params);
  }

  /**
   * Gets the record's wp column values
   *
   * @param object $record
   * @param [string] $table
   * @return array
   */
  public function values($record, $table = '')
  {
    $wpdb = $this->wpdb;

    $values = is_object($record) ? get_object_vars($record) : $record;

    if ($table && is_array($values)) {
      $columns = $wpdb->get_col("DESC {$table}", 0);

      foreach ($values as $column => $value) {
        if (!is_numeric($column) && !in_array($column, $columns)) {
          unset($values[$column]);
        }
      }
    }

    return $values;
  }

  /**
   * Validates required fields
   *
   * @param object $record
   * @param string $type
   * @param array [$fields] Additional required fields
   */
  public function validateRequired($record, $type, $fields = [])
  {
    $required = is_array($type) ? $type : null;
    $record = is_array($record) ? (object) $record : $record;

    if (!$required) {
      if (isset($this->required) && isset($this->required[$type])) {
        $required = $this->required[$type];
      }
    }

    if (is_array($required) && is_object($record)) {
      $required = is_array($fields) ? array_merge($required, $fields) : $required;

      foreach ($required as $require) {
        if (!isset($record->{$require})) {
          $name = Utils::getName($require);
          $this->error("{$name} is a required field");
        } else {
          $value = $record->{$require};
          if (is_string($value)) {
            $value = trim($value);

            if (!strlen($value)) {
              $name = Utils::getName($require);
              $this->error("{$name} is a required field");
            }
          }
        }
      }
    }
  }

  /**
   * Gets the current user
   *
   * @return object
   */
  public function user()
  {
    return $this->plugin->user;
  }

  /**
   * Checks if the user is authorized
   *
   * @return boolean
   */
  public function auth()
  {
    return current_user_can('editor') || current_user_can('administrator');
  }

  /**
   * Checks and determines if a record has been updated
   *
   * @param object $row The record to check
   * @param array $values The new values
   * @return boolean
   */
  public function hasUpdates($row, $values)
  {
    $bool = false;

    foreach ($values as $prop => $value) {
      if ($row->{$prop} != $value) {
        $bool = true;
        break;
      }
    }

    return $bool;
  }

  /**
   * Checks if a nonce is valid
   *
   * @param type $action The nonce action
   * @return boolean
   */
  public function nonce($action = 'wp_rest')
  {
    $bool = false;

    $nonce = null;
    $nonce = isset($_REQUEST['_wpnonce']) ? $_REQUEST['_wpnonce'] : $nonce;
    $nonce = !$nonce && $this->request ? $this->request->get_header('X-WP-Nonce') : $nonce;

    if ($nonce) {
      $bool = wp_verify_nonce($nonce, $action);
    }

    return $bool;
  }

  /**
   * Handles exporting
   *
   * Example Link
   * @link 
   */
  public function export()
  {
    $plugin = $this->plugin;
    $user = $plugin->user;

    $this->ajax = false;
    $this->params['_all'] = '1';

    // Build filename
    $filename = preg_replace('/^.*(\/|\\\)/', '', get_called_class());
    $filename = $this->value('_plural', $filename);
    $filename = 'export-' . strtolower($filename) . '-';
    $filename .= date('Y-m-d-H-i-s');
    $filename .= '.csv';

    // Validate user
    if (!$user) {
      $this->error('You must be logged in to export');
    }

    // Validate this API has an index method
    if (!method_exists($this, 'index')) {
      $this->error('Invalid export request');
    }

    // Get records
    if (!$this->logs()) {
      $response = $this->index();

      if (isset($response->successes)
        && is_array($response->successes)
        && $response->successes
        && isset($response->successes[0]->records)) {

        $this->setLogs([]);

        $records = $response->successes[0]->records;

        if (!$records) {
          $this->error('No records to export');
        }

        if (!$this->hasLogs()) {
          try {
            // Build CSV file
            $columns = [];

            $file = sys_get_temp_dir() . DS . $filename;

            $fp = fopen($file, 'w');

            foreach ($records as $record) {
              $record = get_object_vars($record);

              foreach ($record as $key => &$value) {
                $value = is_object($value) ? get_object_vars($value) : $value;

                if (is_array($value)) {
                  $value = implode(', ', print_r($value, 1));
                }
              }

              // Columns
              if (!$columns) {
                $columns = array_keys($record);
                fputcsv($fp, $columns);
              }

              fputcsv($fp, $record);
            }

            fclose($fp);
          } catch (Exception $ex) {
            $this->error($ex->getMessage());
          }
        }
      }
    }

    // Handle success
    if (!$this->hasLogs()) {
      $params = "api:download,user:{$user->id}";
      $params = CryptUtils::encrypt($params);

      $url = site_url($filename) . "?params={$params}";

      $this->success(['url' => $url]);
    }

    return $this->response();
  }

  /**
   * Build pagination
   *
   * @param array $items
   * @param array|string $columns
   * @return object
   */
  public function pagination($items, $columns = null)
  {
    $records = [];
    $count = $this->value('_count', 30);
    $page = $this->value('_page', 1);
    $search = $this->value('_search');

    if ($search) {
      $array = [];
      $pattern = '/' . preg_quote($search, '/') . '/i';

      foreach ($items as $item) {
        $found = false;

        $props = get_object_vars($item);

        foreach ($props as $prop => $value) {
          if (is_string($value) && preg_match($pattern, $value)) {
            $found = true;
            break;
          }
        }

        if ($found) {
          $array[] = $item;
        }
      }

      $items = $array;
    }

    $total = count($items);
    $pages = ceil($total / $count);

    // Build records
    $i = 1;
    $end = $page * $count;
    $start = ($end - $count) + 1;

    foreach ($items as $item) {
      if ($i >= $start && $i <= $end) {
        $added = false;

        if (is_object($item)) {
          if ($columns) {
            if (method_exists($item, 'object')) {
              $records[] = $item->object($columns);
            } else {
              $columns = Utils::trimExplode(',', $columns);

              $obj = (object) [];

              foreach ($columns as $column) {
                $obj->{$column} = null;

                if (has_property($item, $column)) {
                  $obj->{$column} = $item->{$column};
                }
              }

              $records[] = $obj;
            }

            $added = true;
          } elseif (method_exists($item, 'cleanup')) {
            $records[] = $item->cleanup();
            $added = true;
          }
        }

        if (!$added) {
          $records[] = $item;
        }
      }

      $i++;
    }

    // Build response
    $response = (object) [
      'count' => intval($count),
      'page' => intval($page),
      'pages' => $pages,
      'total' => $total,
      'records' => $records
    ];

    return $response;
  }

  /**
   * Gets the APIs key array
   *
   * @return array
   */
  public function getKeys()
  {
    return $this->keys;
  }

  /**
   * A method for child classes to override when validating keys
   *
   * @param string $key
   * @return boolean
   */
  public function validateKey($key = null) { return false; }
}