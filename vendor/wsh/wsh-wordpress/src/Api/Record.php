<?php

namespace WSHWordpress\Api;

class Record extends Api
{
  /**
   * Save a record
   *
   * @nonce
   * @permissions admin editor
   */
  public function save()
  {
    $plugin = $this->plugin;

    // Request variables
    $model = null;
    $classname = null;
    $record = $this->value('record', null);
    $record = $record && is_array($record) ? (object) $record : $record;

    // Prep record
    $record = is_string($record) ? json_decode(stripslashes(trim($record))) : $record;

    if (is_array($record)) {
      $record = (object) $record;
    }

    // Validate model
    if (!$this->hasErrors()) {
      // Get the model dynamically
      $classname = null;
      $model = $this->value('_model', $model);

      if (!$model) {
        $this->error('Missing the required model parameter');
      } else {
        $classname = $plugin->classes('Models', $model);

        if (!$classname || !class_exists($classname)) {
          $this->error('Unable to load the model');
        }
      }
    }

    // Validate Record
    if (!$this->hasErrors()) {
      if (!$record) {
        $this->error('Record is a required field');
      } else {
        // Fix Vue checkboxes
        $props = [];

        foreach ($props as $prop) {
          if (isset($record->{$prop})) {
            if (preg_match('/^(true|1)$/i', $record->{$prop})) {
              $record->{$prop} = '1';
            } else {
              $record->{$prop} = '0';
            }
          }
        }

        $required = (new $classname)->config->get('required', []);

        $this->validateRequired($record, $required);
      }
    }

    // Process request
    if (!$this->hasErrors()) {
      $row = new $classname($record);

      // Before saving callback
      $row->onBeforeSave($record);

      if ($row->hasLogs()) {
        $this->addLogs($row->logs());
      }

      // Process request
      if (!$this->hasErrors()) {
        if (!$row->isEmpty()) {
          // Update record
          if (!$row->update()) {
            $this->addLogs($row->logs());
          }
        } else {
          // Add record
          if ($row->add()) {
            $record->id = $row->id;
          } else {
            $this->addLogs($row->logs());
          }
        }
      }

      // Extend record
      if (!$this->hasErrors() && !$row->isEmpty()) {
        // After saving callback
        $row->onAfterSave($record);

        if ($row->hasLogs()) {
          $this->addLogs($row->logs());
        }
      }
    }

    // If no errors then success
    if (!$this->hasErrors()) {
      $this->success($record);
    }

    return $this->response();
  }

  /**
   * Delete a record
   *
   * @nonce
   * @permissions admin editor
   */
  public function delete()
  {
    $plugin = $this->plugin;

    // Request variables
    $model = null;
    $classname = null;
    $id = $this->value('id');

    // Validate ID
    if (!$this->hasErrors() && !$id) {
      $this->error('ID is a required field');
    }

    // Validate model
    if (!$this->hasErrors()) {
      // Get the model dynamically
      $classname = null;
      $model = $this->value('_model', $model);

      if (!$model) {
        $this->error('Missing the required model parameter');
      } else {
        $classname = $plugin->classes('Models', $model);

        if (!$classname || !class_exists($classname)) {
          $this->error('Unable to load the model');
        }
      }
    }

    // Process request
    if (!$this->hasErrors()) {
      $row = new $classname($id);

      if (!$row->delete()) {
        $this->addLogs($row->logs());
      }
    }

    // If no errors then success
    if (!$this->hasErrors()) {
      $this->success($id);
    }

    return $this->response();
  }

  /**
   * Toggle a record's active status
   *
   * @nonce
   * @permissions admin editor
   */
  public function active()
  {
    $plugin = $this->plugin;

    // Request variables
    $model = null;
    $classname = null;
    $id = $this->value('id');
    $active = $this->value('active');

    // Validate ID
    if (!$this->hasErrors() && !$id) {
      $this->error('ID is a required field');
    }

    // Validate active
    if (!$this->hasErrors() && !strlen($active)) {
      $this->error('Active is a required field');
    }

    // Validate model
    if (!$this->hasErrors()) {
      // Get the model dynamically
      $classname = null;
      $model = $this->value('_model', $model);

      if (!$model) {
        $this->error('Missing the required model parameter');
      } else {
        $classname = $plugin->classes('Models', $model);

        if (!$classname || !class_exists($classname)) {
          $this->error('Unable to load the model');
        }
      }
    }

    // Process request
    if (!$this->hasErrors()) {
      $row = new $classname($id);

      if (!$row->isEmpty()) {
        $row->active = $active;

        if (!$row->update()) {
          $this->addLogs($row->logs());
        }
      } else {
        $this->error("Unable to fetch record #{$id}");
      }
    }

    // If no errors then success
    if (!$this->hasErrors()) {
      $this->success($id);
    }

    return $this->response();
  }

  /**
   * Update a record's status
   *
   * @nonce
   * @permissions admin editor
   */
  public function status()
  {
    $plugin = $this->plugin;

    // Request variables
    $model = null;
    $classname = null;
    $id = $this->value('id');
    $status = $this->value('status');

    // Validate ID
    if (!$this->hasErrors() && !$id) {
      $this->error('ID is a required field');
    }

    // Validate status
    if (!$this->hasErrors() && !$status) {
      $this->error('Status is a required field');
    }

    // Validate model
    if (!$this->hasErrors()) {
      // Get the model dynamically
      $classname = null;
      $model = $this->value('_model', $model);

      if (!$model) {
        $this->error('Missing the required model parameter');
      } else {
        $classname = $plugin->classes('Models', $model);

        if (!$classname || !class_exists($classname)) {
          $this->error('Unable to load the model');
        }
      }
    }

    // Process request
    if (!$this->hasErrors()) {
      $row = new $classname($id);
      $row->status = $status;

      if (!$row->save()) {
        $this->addLogs($row->logs());
      }
    }

    // If no errors then success
    if (!$this->hasErrors()) {
      $this->success($id);
    }

    return $this->response();
  }
}
