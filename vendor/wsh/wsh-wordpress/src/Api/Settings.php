<?php

namespace WSHWordpress\Api;

use WSHWordpress\Api\Api;

/**
 * Settings API
 *
 * @author Ed Rodriguez
 */
class Settings extends Api
{
  /**
   * Saves plugin settings
   */
  public function save()
  {
    // Scope variables
    $plugin = $this->plugin;

    $success = 0;

    // Request variables
    $settings = $this->value('settings');

    // Validate permissions
    if (!$this->auth()) {
      $this->error('You must be an admin or editor');
    }

    // Validate nonce
    if (!$this->nonce()) {
      $this->error('Your current page session has expired');
    }

    // Validate settings
    if (!$settings) {
      $this->error('No settings were sent');
    } else {
      $settings = base64_decode($settings);
      $settings = json_decode($settings);

      if (!$settings || !is_object($settings)) {
        $this->error('No settings were sent');
      } else {
        $settings = get_object_vars($settings);
      }
    }

    // Process request
    if (!$this->hasErrors()) {
      $success = $plugin->settings->save($settings);

      if ($success) {
        $this->success(1);
      } else {
        $this->error('Unable to update settings');
      }
    }

    return $this->response();
  }
}
