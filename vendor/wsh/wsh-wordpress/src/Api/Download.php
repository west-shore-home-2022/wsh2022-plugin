<?php

namespace WSHWordpress\Api;

use WSHWordpress\Api\Api;

class Download extends Api
{
  /**
   * Downloads a temp file
   */
  public function index()
  {
    $plugin = $this->plugin;
    $user = $plugin->user;

    $filename = preg_replace('/^.*(\/|\\\)/', '', $plugin->uri);
    $id = $this->value('user');

    // Validate file
    if (!$filename || !$id) {
      $this->error('Unable to download file');
    }

    // Validate user
    if (!$this->hasLogs()) {
      if (!$user) {
        $this->error('You must be logged in to access this file');
      } elseif ($user->id !== $id) {
        $this->error('This file is for a different user');
      }
    }

    if (!$this->hasLogs()) {
      $file = sys_get_temp_dir() . DS . $filename;

      if (!file_exists($file)) {
        $this->error('The download period for the file has expired');
      }

      if (!$this->hasLogs()) {
        $quoted = sprintf('"%s"', addcslashes(basename($filename), '"\\'));
        $size = filesize($file);

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $quoted);
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: no-cache');
        header('Content-Length: ' . $size);

        readfile($file);

        // Garbage collection
        if (1) {
          unlink($file);
        }

        die();
      }
    }

    return $this->response();
  }
}
