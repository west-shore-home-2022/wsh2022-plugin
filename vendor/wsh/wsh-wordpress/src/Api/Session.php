<?php

namespace WSHWordpress\Api;

class Session extends Api
{
  /**
   * Refreshes a session so that it doesn't expire
   *
   * Example Link
   * 
   */
  public function refresh()
  {
    $expire = ini_get('session.gc_maxlifetime');

    if (is_numeric($expire)) {
      $expire = intval($expire);

      $refresh = $expire - 60;

      $data = [
        'expire' => [
          'milliseconds' => $expire * 1000,
          'seconds' => $expire,
          'minutes' => $expire / 60
        ],
        'refresh' => [
          'milliseconds' => $refresh * 1000,
          'seconds' => $refresh,
          'minutes' => $refresh / 60
        ],
        'nonces' => [
          'wpnonce' => $this->wp_create_nonce('wp_rest')
        ]
      ];

      $this->success($data);
    } else {
      $this->success(1);
    }

    return $this->response();
  }

  /**
   * Override the default WordPress function
   * Without a nonce in the request the REST API will forcefully downgrade to a logged-out user
   *
   * global $current_user; // Empty
   * wp_get_current_user(); // Empty
   *
   * @param type $action
   * @return type
   */
  protected function wp_create_nonce($action = -1)
  {
    $user = $this->user();
    $uid = (int) $user->ID;
    if ( ! $uid ) {
      /** This filter is documented in wp-includes/pluggable.php */
      $uid = apply_filters( 'nonce_user_logged_out', $uid, $action );
    }

    $token = wp_get_session_token();
    $i = wp_nonce_tick();

    return substr( wp_hash( $i . '|' . $action . '|' . $uid . '|' . $token, 'nonce' ), -12, 10 );
  }
}
