<?php

namespace WSHWordpress\Api;

use WSHWordpress\Api\Api;
use WSHWordpress\Models\Ip as IpModel;
use WSHWordpress\Utils\CacheUtils;
use WSHWordpress\Utils\JsonUtils;

/**
 * IP API
 *
 * @author Ed Rodriguez
 */
class Ip extends Api
{
  /**
   * Check an IP address to determine if it is abusive
   *
   * Example: 
   *
   * @required
   *   - An account at abuseipdb.com
   *   - (Free webmaster account is 5,000 lookups per day)
   * @author Ed Rodriguez
   * @created 2020-10-09
   * @public
   * @any
   */
  public function check()
  {
    $plugin = $this->plugin;
    
    $nocache = CacheUtils::nocache();
    
    // Scope variables
    $data = null;
    $result = null;
    $error = null;
    $success = null;
    $ip = $this->value('ip');
    $max = $this->value('max');
    $key = $this->value('key');
    $columns = $this->value('columns');
    $max = is_numeric($max) && $max ? $max : 90;
    $key = $plugin->config('api.abuseipdb', $key);
    $key = $plugin->config('api.abuseipdb.key', $key);
    
    // Validate IP
    if (!$ip) {
      $this->error('IP is required');
    } else if (!preg_match('/(\d{1,3}\.?){4}/', $ip)) {
      $this->error('IP is invalid');
    }
    
    // Validate Key
    if (!$key) {
      $this->error('An API key is required');
    }
    
    // Process request
    if (!$this->hasLogs()) {
      $plugin->table('wsh_ips');
      
      $url = "https://api.abuseipdb.com/api/v2/check?ipAddress={$ip}&maxAgeInDays={$max}";
      
      // Caching
      $record = IpModel::firstOrNew(['ip' => $ip]);
      
      if (!$record->isEmpty() && !$nocache) {
        $data = $record->object($columns);
        $data->cache = 1;
      } else {
        $ch = curl_init();

        curl_setopt_array($ch, [
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_HTTPHEADER => [
            "Accept: application/json",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Key: {$key}"
          ],
        ]);

        $result = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);

        curl_close($ch);

        $success = $status && $status == 200;
        
        if ($success) {
          $response = JsonUtils::decode($result);
          
          if (JsonUtils::isObject($response) && isset($response->data)) {
            $data = $response->data;
            
            $record->ip = $ip;
            $record->public = $data->isPublic ? 1 : 0;
            $record->version = $data->ipVersion;
            $record->abusive = isset($data->isWhitelisted) && is_bool($data->isWhitelisted) && !$data->isWhitelisted ? 1 : 0;
            $record->score = $data->abuseConfidenceScore;
            $record->country = $data->countryCode;
            $record->type = $data->usageType;
            $record->isp = $data->isp;
            $record->domain = $data->domain;
            $record->hostnames = json_encode($data->hostnames, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
            $record->checks = is_numeric($record->checks) ? $record->checks + 1 : 1;
            $record->reports = $data->totalReports;
            $record->users = $data->numDistinctUsers;
            $record->reported_at = isset($data->lastReportedAt) && is_string($data->lastReportedAt) ? date('Y-m-d H:i:s', strtotime($data->lastReportedAt)) : null;
            $record->save();
            
            $data = $record->object($columns);
            $data->cache = 0;
          }
        }
      }
      
      // Handle response
      if ($data) {
        $this->success($data);
      } else {
        $this->error('Unable to look up IP');
        
        if ($error) {
          $this->error($error);
        }
      }
    }
    
    // Default error
    if (!$this->hasLogs()) {
      $this->error('Unable to look up IP');
    }
    
    return $this->response();
  }
}
