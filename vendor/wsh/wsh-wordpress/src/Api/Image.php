<?php

namespace WSHWordpress\Api;

use WSHWordpress\Api\Api;
use WSHWordpress\Utils\Utils;
use WSHWordpress\Utils\CryptUtils;
use WSHWordpress\Utils\FileUtils;
use WSHWordpress\Utils\UrlUtils;
use WSHWordpress\Models\File;

class Image extends Api
{
  protected $keys = ['wsh' => '9HQsNqp2'];

  protected $debug = false;
  protected $file = null;
  protected $src = null;
  protected $source = null;
  protected $image = null;
  protected $cache = null;
  protected $ext = null;
  protected $inline = true;

  protected $optimizations = [
    'jpg' => 100, // 0 - 100 (75 Default)
    'png' => 0 // 0 - 9 (0 = No Optimization)
  ];

  /**
   * Uploads an image to the Wordpress Media library
   *
   * @protected
   * @return string The uploaded image url
   */
  public function upload()
  {
    $now = time();

    if (!$_FILES) {
      $this->error('No files to upload');
    }

    if (!$this->hasLogs()) {
      foreach ($_FILES as $file) {
        $file = (object) $file;

        $upload_dir = wp_upload_dir();

        $filename = basename($file->name);

        if (wp_mkdir_p($upload_dir['path'])) {
          $path = $upload_dir['path'] . '/' . $filename;
        } else {
          $path = $upload_dir['basedir'] . '/' . $filename;
        }

        // Gets a unique filename
        $path = FileUtils::uniqueName($path);

        $filename = basename($path);

        // Fixes slashes
        if (wp_mkdir_p($upload_dir['path'])) {
          $path = $upload_dir['path'] . '/' . $filename;
        } else {
          $path = $upload_dir['basedir'] . '/' . $filename;
        }

        $fileinfo = wp_check_filetype($filename, null);

        $post = (object) [];
        $post->post_author = 1;
        $post->post_date = date('Y-m-d H:i:s', $now);
        $post->post_date_gmt = gmdate('Y-m-d H:i:s', $now);
        $post->post_status = 'inherit';
        $post->comment_status = 'closed';
        $post->post_excerpt = '';
        $post->ping_status = 'closed';
        $post->post_name = sanitize_file_name($filename);
        $post->post_mime_type = $fileinfo['type'];
        $post->to_ping = '';
        $post->pinged = '';
        $post->post_modified = date('Y-m-d H:i:s', $now);
        $post->post_modified_gmt = gmdate('Y-m-d H:i:s', $now);
        $post->post_content = '';
        $post->post_content_filtered = '';
        $post->post_parent = 0;
        $post->menu_order = 0;
        $post->post_type = 'attachment';
        $post->comment_count = 0;

        $content = file_get_contents($file->tmp_name);

        file_put_contents($path, $content);

        if (file_exists($path)) {
          $id = wp_insert_attachment((array) $post, $path);

          if ($id) {
            require_once(ABSPATH . 'wp-admin/includes/image.php');

            $attach_data = wp_generate_attachment_metadata($id, $path);

            wp_update_attachment_metadata($id, $attach_data);

            $url = wp_get_attachment_url($id);

            if ($url) {
              $this->success($url);
            } else {
              $this->error('Unable to retrieve url');
            }
          } else {
            $this->error('Unable to insert file');
          }
        } else {
          $this->error('Unable to put file');
        }
      }
    }

    if (!$this->hasLogs()) {
      $this->error('Unable to upload file');
    }

    return $this->response();
  }

  /**
   * Manipulates images
   *
   * Example
   * $encrypt = CryptUtils::encrypt('cmds:square-50,id:1');
   */
  public function index()
  {
    $this->debug = isset($_GET['debug']);
    $plugin = $this->plugin;
    $uri = $plugin->uri;
    $uri = preg_replace('/\/{2,}/', '/', $uri);
    $uri = preg_replace('/.*\/wp-json\//i', '', $uri);

    // Parse uri
    $parts = preg_replace('/^\//', '', $uri);
    $parts = explode('/', $parts);
    $parts = array_splice($parts, 2);
    $filename = $parts[0];
    $parts = explode('.', $filename);
    $params = $parts[0];

    // Shrink long filenames to prevent system write errors
    if (strlen($params) > 32) {
      $ext = isset($parts[1]) ? '.' . $parts[1] : null;
      $filename = md5($params) . $ext;
    }

    // Get params
    if ($this->debug || !$this->params) {
      $params = CryptUtils::decrypt($params);

      if (preg_match('/:/', $params)) {
        $this->params = Utils::prepParams($params);
      } else {
        $this->params = UrlUtils::arrayToAssoc(UrlUtils::uriToArray($params));
      }
    }

    $src = null;
    $cmds = $this->value('cmds');
    $id = $this->value('id');
    $uri = $this->value('uri');

    // Set up image
    if ($id) {
      $file = File::find($id);

      if (!$file->isEmpty()) {
        $this->file = $file;
        $src = $plugin->siteRoot . $file->file;
      }
    } elseif ($uri) {
      // Check root
      $src = $plugin->siteRoot . $uri;

      // Check uploads
      if (!file_exists($src)) {
        $src = $plugin->uploadRoot . $uri;
      }

      // Check assets
      if (!file_exists($src)) {
        $src = $plugin->assetRoot . $uri;
      }

      // Check absolute
      if (!file_exists($src)) {
        $src = $uri;
      }
    }

    // Run cmds
    if ($cmds && $src && file_exists($src)) {
      $this->setSource($src);

      $cmds = explode('_', $cmds);

      foreach ($cmds as $args) {
        $args = explode('-', $args);
        $method = array_shift($args);

        if (method_exists($this, $method)) {
          call_user_func_array("self::{$method}", $args);
          $this->source = $this->image;
        }
      }

      // Setup cache
      $cache = preg_replace('/.*\/wp-content\//i', '', $this->src);
      $cache = $plugin->cacheRoot . dirname($cache) . '/' . $filename;
      $this->cache = $cache;

      if (!$this->showCache()) {
        $this->showNew();
      }
    } else {
      $this->showOriginal();
    }

    // Prevent any "headers already sent" warnings
    die();
  }

  /**
   * Scales an image so that its width or height (which ever are the larger of the two) are less than the max
   * This is the most used transformation out all the image transformations.
   *
   * Example: {SITE_ROOT}image/cmds/maxscaled-100/id/1/
   *
   * @param integer $max
   */
  protected function maxscaled($max = null)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      if (($currentWidth > $max) || ($currentHeight > $max)) {
        if ($currentWidth > $currentHeight) {
          $newWidth = $max;
          $newHeight = $currentHeight*($max/$currentWidth);
        } elseif ($currentWidth < $currentHeight) {
          $newWidth = $currentWidth*($max/$currentHeight);
          $newHeight = $max;
        } elseif ($currentWidth == $currentHeight) {
          $newWidth = $max;
          $newHeight = $max;
        }

        $this->image = imagecreatetruecolor($newWidth, $newHeight);
        $this->setTransparency();

        imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
      }
    }
  }

  /**
   * Scales an image by the supplied width & height
   *
   * Example: {SITE_ROOT}image/cmds/scale-100-100/id/1/
   *
   * @param integer $width
   * @param integer $height
   */
  protected function scale($width = null, $height = null)
  {
    if ($width && $height && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);
      $newWidth = $width;
      $newHeight = $height;
      $this->image = imagecreatetruecolor($newWidth, $newHeight);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales an image by the supplied width
   *
   * Example: {SITE_ROOT}image/cmds/width-100/id/1/
   *
   * @param integer $max
   * @param integer [$aspect=0]
   */
  protected function width($max = null, $aspect = 0)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);
      $newWidth = $max;
      $newHeight = ($aspect) ? ($currentHeight*$newWidth) / $currentWidth : $currentHeight;
      $this->image = imagecreatetruecolor($newWidth, $newHeight);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales an image adaptively by the supplied width
   *
   * Example: {SITE_ROOT}image/cmds/adaptive-100/id/1/
   *
   * @param integer $max
   * @param integer [$aspect=1]
   */
  protected function adaptive($max = null, $aspect = 1)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);
      $newWidth = ($currentWidth > $max) ? $max : $currentWidth;
      $newHeight = ($aspect) ? ($currentHeight*$newWidth)/$currentWidth : $currentHeight;
      $this->image = imagecreatetruecolor($newWidth, $newHeight);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales an image by the supplied height
   *
   * Example: {SITE_ROOT}image/cmds/height-100/id/1/
   *
   * @param integer $max
   * @param integer [$aspect=0]
   */
  protected function height($max = null, $aspect = 0)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);
      $newHeight = $max;
      $newWidth = ($aspect) ? ($currentWidth*$newHeight)/$currentHeight : $currentWidth;
      $this->image = imagecreatetruecolor($newWidth, $newHeight);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales an image by the supplied width
   *
   * Example: {SITE_ROOT}image/cmds/rotate-90/id/1/
   *
   * @link http://stackoverflow.com/questions/3012051/how-to-avoid-black-background-when-rotating-an-image-45-degree-using-php
   * @param integer $degrees
   */
  protected function rotate($degrees = null)
  {
    if ($degrees && $this->source) {
      $method = 'imagerotate';

      if (function_exists($method)) {
        $degrees = 360 - $degrees;
        $this->image = imagerotate($this->source, $degrees, 16777215, 0);
      } elseif (method_exists($this, $method) && is_callable([$this, $method])) {
        $this->image = $this->imagerotate($degrees);
      }
    }
  }

  /**
   * Flips an image by the supplied type
   *
   * Example: {SITE_ROOT}image/cmds/flip-horizontal/id/1/
   *
   * @param string $type
   */
  public function flip($type)
  {
    $width = imageSX($this->source);
    $height = imageSY($this->source);
    $src_x = 0;
    $src_y = 0;
    $src_width = $width;
    $src_height = $height;
    $found = 0;
    $success = 0;

    switch ($type) {
      case 'veritcal':
      case 'v':
      case '1':
        $src_y = $height -1;
        $src_height = -$height;
        $found = 1;
        break;
      case 'horizontal':
      case 'h':
      case '2':
        $src_x = $width -1;
        $src_width = -$width;
        $found = 1;
        break;
      case 'both':
      case 'b':
      case '3':
        $src_x = $width -1;
        $src_y = $height -1;
        $src_width = -$width;
        $src_height = -$height;
        $found = 1;
        break;
    }

    if ($found) {
      $this->image = imagecreatetruecolor($width, $height);

      if (imagecopyresampled($this->image, $this->source, 0, 0, $src_x, $src_y , $width, $height, $src_width, $src_height)) {
        $success = 1;
      }
    }

    return $success;
  }

  /**
   * Applies filters to an image
   *
   * Example: {SITE_ROOT}image/cmds/filter-grayscale/id/1/
   *
   * @param string $type
   * @param mixed arg1
   * @param mixed $arg2
   * @param mixed $arg3
   * @param mixed $arg4
   */
  protected function filter($type = null, $arg1 = null, $arg2 = null, $arg3 = null, $arg4 = null)
  {
    $type = strtolower(trim($type));

    if ($type) {

      $this->image = $this->source;

      switch ($type) {
        case 'negate':
          imagefilter($this->image, IMG_FILTER_NEGATE);
          break;
        case 'grayscale':
          imagefilter($this->image, IMG_FILTER_GRAYSCALE);
          break;
        case 'brightness':
          imagefilter($this->image, IMG_FILTER_BRIGHTNESS, $arg1);
          break;
        case 'contrast':
          imagefilter($this->image, IMG_FILTER_CONTRAST, $arg1);
          break;
        case 'colorize':
          imagefilter($this->image, IMG_FILTER_COLORIZE, $arg1, $arg2, $arg3, $arg4);
          break;
        case 'edetect':
        case 'edgedetect':
          imagefilter($this->image, IMG_FILTER_EDGEDETECT);
          break;
        case 'emboss':
          imagefilter($this->image, IMG_FILTER_EMBOSS);
          break;
        case 'gblur':
        case 'gaussianblur':
          imagefilter($this->image, IMG_FILTER_GAUSSIAN_BLUR);
          break;
        case 'sblur':
        case 'selectiveblur':
          imagefilter($this->image, IMG_FILTER_SELECTIVE_BLUR);
          break;
        case 'mremoval':
        case 'meanremoval':
          imagefilter($this->image, IMG_FILTER_MEAN_REMOVAL);
          break;
        case 'smooth':
          imagefilter($this->image, IMG_FILTER_SMOOTH, $arg1);
          break;
        case 'pixalate':
          imagefilter($this->image, IMG_FILTER_PIXELATE, $arg1, $arg2);
          break;
      }
    }
  }

  /**
   * Crops image to a the specified rect dimensions
   *
   * Example: {SITE_ROOT}image/cmds/crop-14-12-118-158/id/1/
   *
   * @param integer $xl
   * @param integer $yl
   * @param integer $x2
   * @param integer $y2
   */
  protected function crop($x1 = null, $y1 = null, $x2 = null, $y2 = null)
  {
    if ((is_numeric($x1) && $x1 >= 0)
        && (is_numeric($y1) && $y1 >= 0)
        && $x2 && $y2 && $this->source) {

      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      $newWidth = $x2 - $x1;
      $newHeight = $y2 - $y1;

      $dst_x = $x1;
      $dst_y = $y1;

      $this->image = imagecreatetruecolor($newWidth, $newHeight);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, 0, 0, $x1, $y1, $currentWidth, $currentHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Crops image from the left
   *
   * Example: {SITE_ROOT}image/cmds/cropleft-100/id/1/
   *
   * @param integer $left
   */
  protected function cropleft($left)
  {
    if ((is_numeric($left) && $left > 0) && $this->source) {

      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      $newWidth = $currentWidth - $left;
      $newHeight = $currentHeight;

      $this->image = imagecreatetruecolor($newWidth, $newHeight);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, 0, 0, $left, 0, $currentWidth, $currentHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales and crops if needed an image to a Square
   *
   * Example: {SITE_ROOT}/wp-json/{PLUGIN_NAME}/image/cmds/square-50/id/1/
   *
   * @param integer $max
   */
  protected function square($max = null)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      if ($currentWidth < $max && $currentHeight < $max) {
        // PREVENT UPSCALING
        if ($currentWidth > $currentHeight) {
          $max = $currentWidth;
        } elseif ($currentWidth < $currentHeight) {
          $max = $currentHeight;
        }
      }

      $_x = 0;
      $_y = 0;

      if ($currentWidth > $currentHeight) {
        $newWidth = $currentWidth*($max/$currentHeight);
        $newHeight = $max;
        $_x = 0 - (($newWidth - $max) / 2);
      } elseif ($currentWidth < $currentHeight) {
        $newWidth = $max;
        $newHeight = $currentHeight*($max/$currentWidth);
        $_y = 0 - (($newHeight - $max) / 2);
      } elseif ($currentWidth == $currentHeight) {
        $newWidth = $max;
        $newHeight = $max;
      }

      $this->image = imagecreatetruecolor($max, $max);
      $this->setTransparency();

      imagecopyresampled($this->image, $this->source, $_x, $_y, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales and crops if needed an image to a Square
   *
   * Example: {SITE_ROOT}/wp-json/{PLUGIN_NAME}/image/cmds/squareleft-50/id/1/
   *
   * @param integer $max
   */
  protected function squareleft($max = null)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      if ($currentWidth < $max && $currentHeight < $max) {
        // PREVENT UPSCALING
        if ($currentWidth > $currentHeight) {
          $max = $currentWidth;
        } elseif ($currentWidth < $currentHeight) {
          $max = $currentHeight;
        }
      }

      $_x = 0;
      $_y = 0;

      if ($currentWidth > $currentHeight) {
        $newWidth = $currentWidth*($max/$currentHeight);
        $newHeight = $max;
      } elseif ($currentWidth < $currentHeight) {
        $newWidth = $max;
        $newHeight = $currentHeight*($max/$currentWidth);
        $_y = 0 - (($newHeight - $max) / 2);
      } elseif ($currentWidth == $currentHeight) {
        $newWidth = $max;
        $newHeight = $max;
      }

      $this->image = imagecreatetruecolor($max, $max);
      $this->setTransparency();

      imagecopyresampled($this->image, $this->source, $_x, $_y, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales and crops if needed an image to a Square
   *
   * Example: {SITE_ROOT}/wp-json/{PLUGIN_NAME}/image/cmds/squareright-50/id/1/
   *
   * @param integer $max
   */
  protected function squareright($max = null)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      if ($currentWidth < $max && $currentHeight < $max) {
        // PREVENT UPSCALING
        if ($currentWidth > $currentHeight) {
          $max = $currentWidth;
        } elseif ($currentWidth < $currentHeight) {
          $max = $currentHeight;
        }
      }

      $_x = 0;
      $_y = 0;

      if ($currentWidth > $currentHeight) {
        $newWidth = $currentWidth*($max/$currentHeight);
        $newHeight = $max;
        $_x = 0 - ($newWidth - $max);
      } elseif ($currentWidth < $currentHeight) {
        $newWidth = $max;
        $newHeight = $currentHeight*($max/$currentWidth);
        $_y = 0 - (($newHeight - $max) / 2);
      } elseif ($currentWidth == $currentHeight) {
        $newWidth = $max;
        $newHeight = $max;
      }

      $this->image = imagecreatetruecolor($max, $max);
      $this->setTransparency();

      imagecopyresampled($this->image, $this->source, $_x, $_y, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales the height less than max and crops the width
   *
   * Example: {SITE_ROOT}image/cmds/maxheightcropwidth-50-100/id/1/
   *
   * @param integer $width
   * @param integer $height
   */
  protected function maxheightcropwidth($width = null, $height = null)
  {
    if ($width && $height && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      $newHeight = $height;
      $newWidth = $currentWidth*($height/$currentHeight);
      $_x = 0 - (($newWidth - $width) / 2);
      $_y = 0;

      $this->image = imagecreatetruecolor($width, $newHeight);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, $_x, $_y, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales the width less than max and crops the height
   *
   * Example: {SITE_ROOT}image/cmds/maxwidthcropheight-50-100/id/1/
   *
   * @param integer $width
   * @param integer $height
   */
  protected function maxwidthcropheight($width = null, $height = null)
  {
    if ($width && $height && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      $newHeight = $currentHeight*($width/$currentWidth);
      $newWidth = $width;
      $_x = 0;
      $_y = 0-(($newHeight - $height) / 2);

      $this->image = imagecreatetruecolor($newWidth, $height);
      $this->setTransparency();
      imagecopyresampled($this->image, $this->source, $_x, $_y, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
    }
  }

  /**
   * Scales an image so that its height are less than the max
   *
   * Example: {SITE_ROOT}image/cmds/maxwidth-50/id/1/
   *
   * @param integer $max
   */
  protected function maxwidth($max = null)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      if ($currentWidth > $max) {
        $newWidth = $max;
        $newHeight = ($currentHeight*$newWidth)/$currentWidth;
        $this->image = imagecreatetruecolor($newWidth, $newHeight);
        $this->setTransparency();
        imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
      }
    }
  }

  /**
   * Scales an image so that its height are less than the max
   *
   * Example: {SITE_ROOT}image/cmds/maxheight-50/id/1/
   *
   * @param integer $max
   */
  protected function maxheight($max = null)
  {
    if ($max && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      if ($currentHeight > $max) {
        $newHeight = $max;
        $newWidth = ($currentWidth*$newHeight)/$currentHeight;
        $this->image = imagecreatetruecolor($newWidth, $newHeight);
        $this->setTransparency();
        imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
      }
    }
  }

  /**
   * Scales an image so that both the width and height are less than or qeual to their max
   *
   * Example: {SITE_ROOT}image/cmds/maxwidthheight-50-50/id/1/
   *
   * @param integer $width
   * @param integer $height
   */
  protected function maxwidthheight($width = null, $height = null)
  {
    if ($width && $height && $this->source) {
      $currentWidth = imageSX($this->source);
      $currentHeight = imageSY($this->source);

      if ($currentWidth <= $width && $currentHeight <= $height) {

      } else {
        $newWidth = $currentWidth;
        $newHeight = $currentHeight;

        // Scale by width/height
        if ($currentWidth > $width) {
          $newWidth = $width;
          $newHeight = floor(($currentHeight * $newWidth) / $currentWidth);
        } elseif ($currentHeight > $height) {
          $newHeight = $height;
          $newWidth = floor(($currentWidth * $newHeight) / $currentHeight);
        }

        // Scale by newWidth/newHeight
        while ($width < $newWidth || $height < $newHeight) {
          if ($width < $newWidth) {
            $newWidth = $width;
            $newHeight = floor(($currentHeight * $newWidth) / $currentWidth);
          } elseif ($height < $newHeight) {
            $newHeight = $height;
            $newWidth = floor(($currentWidth * $newHeight) / $currentHeight);
          }
        }

        $this->image = imagecreatetruecolor($newWidth, $newHeight);
        $this->setTransparency();
        imagecopyresampled($this->image, $this->source, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
      }
    }
  }

  /**
   * Manually rotate an image
   *
   * @param integer $angle
   * @return resource
   */
  protected function imagerotate($angle)
  {
    if (($angle < 0) || ($angle > 360)) {
      return $this->source;
    }

    $img = $this->source;
    $width = imagesx($img);
    $height = imagesy($img);

    switch ($angle) {
      case 90: $newimg = @imagecreatetruecolor($height, $width); break;
      case 180: $newimg = @imagecreatetruecolor($width, $height); break;
      case 270: $newimg = @imagecreatetruecolor($height, $width); break;
      case 0: return $img; break;
      case 360: return $img; break;
    }

    if ($newimg) {
      for ($i = 0; $i < $width; $i++) {
        for ($j = 0; $j < $height; $j++) {
          $reference = imagecolorat($img,$i,$j);

          switch ($angle) {
            case 90: if (!@imagesetpixel($newimg, ($height - 1) - $j, $i, $reference )) { return false; } break;
            case 180: if (!@imagesetpixel($newimg, $width - $i, ($height - 1) - $j, $reference )) { return false; } break;
            case 270: if (!@imagesetpixel($newimg, $j, $width - $i, $reference )) { return false; } break;
          }
        }
      }

      return $newimg;
    }

    return $this->source;
  }

  /**
   * Applies transparencies to the new image from the source image
   */
  protected function setTransparency()
  {
    if (preg_match('/^(gif|png)$/i', $this->ext)) {
      $index = imagecolortransparent($this->source);
      $color = ['red' => 255, 'green' => 255, 'blue' => 255];

      if ($index >= 0) {
        $color = imagecolorsforindex($this->source, $index);
      }

      $index = imagecolorallocate($this->image, $color['red'], $color['green'], $color['blue']);

      imagefill($this->image, 0, 0, $index);
      imagecolortransparent($this->image, $index);
    }
  }

  /**
   * Shows the cached file
   *
   * @return boolean
   */
  protected function showCache()
  {
    $found = false;

    if ($this->debug) {
      return false;
    }

    $file = $this->src;
    $disposition = $this->inline ? 'inline' : 'attachment';
    $filename = basename($file);

    $cacheFile = $this->cache;

    if ($cacheFile && is_file($cacheFile)) {
      $fileDate = filemtime($file);
      $cacheDate = filemtime($cacheFile);

      if ($cacheDate >= $fileDate) {
          $found = true;
          $ext = $this->ext;

          $this->cachedHeaders($cacheFile);

          if (preg_match('/png/', $ext)) {
            header('Content-Type: image/png');
          } else {
            header('Content-Type: image/jpeg');
          }

          header("Content-Disposition: {$disposition}; filename={$filename}");
          readfile($cacheFile);
          $this->cleanup();
      } else {
        @unlink($cacheFile);
      }
    }

    return $found;
  }

  /**
   * Serves the original file to the browser
   */
  protected function showOriginal()
  {
    $file = $this->src;
    $disposition = $this->inline ? 'inline' : 'attachment';
    $filename = basename($file);

    // HEADERS CACHE
    $this->cachedHeaders($file);

    // HEADERS IMAGE (if needed)
    header("Content-Type: image/{$this->ext}");
    header("Content-Disposition: {$disposition}; filename={$filename}");
    readfile($file);
    $this->cleanup();
  }

  /**
   * Serves the newly created file to the browser
   */
  protected function showNew()
  {
    if ($this->image) {
      $this->saveCache();

      $disposition = $this->inline ? 'inline' : 'attachment';
      $filename = basename($this->src);

      if (preg_match('/png/i', $this->ext)) {
        header('Content-Type: image/png');
        header("Content-Disposition: {$disposition}; filename={$filename}");
        imagepng($this->image);
      } else {
        header('Content-Type: image/jpeg');
        header("Content-Disposition: {$disposition}; filename={$filename}");
        imagejpeg($this->image);
      }

      $this->cleanup();
    } else {
      $this->showOriginal();
    }
  }

  /**
   * Validates the image type is a PNG
   *
   * @param string $src
   * @return bool
   */
  public function isPNG($src)
  {
    return $this->isType($src, IMAGETYPE_PNG);
  }

  /**
   * Validates the image type is a JPEG
   *
   * @param string $src
   * @return bool
   */
  public function isJPEG($src)
  {
    return $this->isType($src, IMAGETYPE_JPEG);
  }

  /**
   * Validates the image type is a GIF
   *
   * @param string $src
   * @return bool
   */
  public function isGIF($src)
  {
    return $this->isType($src, IMAGETYPE_GIF);
  }

  /**
   * Validates the image type
   *
   * @param string $src
   * @param integer $imagetype
   * @return bool
   */
  public function isType($src, $imagetype)
  {
    $type = null;

    if (!function_exists('exif_imagetype')) {
      if ((list($width, $height, $_type, $attr) = getimagesize($src)) !== false) {
        $type = $_type;
      }
    } else {
      $type = exif_imagetype($src);
    }

    return $type && $type == $imagetype;
  }

  /**
   * Sets the source image
   */
  protected function setSource($src = null)
  {
    $src = $src ? $src : $this->src;

    if ($src) {
      $source = null;
      $ext = $this->getExt($src);

      if ($this->isLocal($src)) {
        if (file_exists($src)) {
          if (preg_match('/gif/i', $ext) && $this->isGIF($src)) {
            $source = imagecreatefromgif($src);
          } elseif (preg_match('/jpg|jpe|jpeg/i', $ext) && $this->isJPEG($src)) {
            $source = imagecreatefromjpeg($src);
          } elseif (preg_match('/png/i', $ext) && $this->isPNG($src)) {
            $source = imagecreatefrompng($src);
          }
        }
      } else {
        $source = imagecreatefromstring(file_get_contents($src));
      }

      $this->src = $src;
      $this->source = $source;
    }
  }

  /**
   * Cleans up any images that were created
   */
  protected function cleanup()
  {
    if ($this->image && is_resource($this->image)) {
      imagedestroy($this->image);
    }

    if ($this->source && is_resource($this->source)) {
      imagedestroy($this->source);
    }
  }

  /**
   * Determines if a file is local or remote
   *
   * @param string $file
   * @return boolean
   */
  protected function isLocal($file = null)
  {
    $file = trim($file);
    $local = true;

    if ($file) {
      if (strpos($file, '://') == true) {
        $local = false;
      }
    } else {
      if (empty($this->local)) {
        if (strpos($this->src, '://') == true) {
          $local = false;
        }

        $this->local = $local;
      } else {
        $local = $this->local;
      }
    }

    return $local;
  }

  /**
   * Gets the file's extension
   *
   * @param type $file
   * @return type
   */
  protected function getExt($file = '')
  {
      $file = trim($file);

      if ($file) {
        $file = strtolower($file);
        $exts = preg_split('/\./', $file) ;
        $ext = array_pop($exts);

        $this->ext = $ext;
      }

      return $this->ext;
  }

  /**
   * Saves the cache file
   */
  protected function saveCache()
  {
    if ($this->cache && FileUtils::createDir($this->cache)) {
      if (preg_match('/png/', $this->ext)) {
        @imagepng($this->image, $this->cache, $this->optimizations['png']);
      } else {
        imagejpeg($this->image, $this->cache, $this->optimizations['jpg']);
      }
    }
  }

  /**
   * Caches the requested file asset and returns a "Not Modified"
   * header for additional requests if the file hasn't been modified
   * since last retrieved. This method prevents and eliminates server
   * request overhead.
   *
   * Based on "Conditional GET"
   *
   * [Links]
   * http://fishbowl.pastiche.org/2002/10/21/http_conditional_get_for_rss_hackers/
   * http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.9.1
   * http://www.mnot.net/cache_docs/

   * If either of the following conditions exist then a 304 response is returned.
   * 1) HTTP_IF_NONE_MATCH = ETAG
   * 2) HTTP_IF_MODIFIED_SINCE = LAST_MODIFIED_TIME
   */
  protected function cachedHeaders($file)
  {
    $last_modified_time = filemtime($file);
    $etag = md5_file($file);

    header('Last-Modified: '.gmdate('D, d M Y H:i:s', $last_modified_time).' GMT');
    header('Expires: '.gmdate('D, d M Y H:i:s', strtotime('+1 month')).' GMT');
    header('Cache-Control: public');
    header('Pragma: public');
    header('Etag: '.$etag);

    // IF THESE 2 ITEMS ARE PRESENT IN THE REQUEST
    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && isset($_SERVER['HTTP_IF_NONE_MATCH'])) {
      if (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $last_modified_time || trim($_SERVER['HTTP_IF_NONE_MATCH']) == $etag) {
        /* TESTING
        echo(strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']).' (http_if_modified_since)<br />');
        echo($last_modified_time.' (last_modified_time)<br /><br />');
        echo(trim($_SERVER['HTTP_IF_NONE_MATCH']).' (http_if_none_match)<br />');
        echo($etag.' (etag)<br /><br />');
        $this->finalizeRequest('Not Modified!');
        */
        header('HTTP/1.1 304 Not Modified');
      }
    }
  }
}
