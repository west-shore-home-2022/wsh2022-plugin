<?php

namespace WSHWordpress\Api;

use WSHWordpress\Api\Api;

class Tracking extends Api
{
  /**
   * Tracks visitors and users
   */
  public function index()
  {
    if ($this->plugin->track()) {
      $this->success(1);
    } else {
      $this->error(0);
    }

    return $this->response();
  }
}