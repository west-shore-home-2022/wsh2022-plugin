<?php

namespace WSHWordpress\Api;

use Firebase\JWT\JWT;
use WSHWordpress\Api\Api;

class Token extends Api
{
  /**
   * Get a JWT token
   *
   * @user
   * @nonce
   */
  public function get()
  {
    $plugin = $this->plugin;
    $user = $plugin->user();
    $slug = $plugin->config('slug');
    $key = $plugin->config('token.key');

    // Validate key
    if (!$key) {
      $this->error('Missing authorization token key');
    }

    // Build token
    if (!$this->hasErrors()) {
      $iss = $plugin->config('token.issuer', $_SERVER['HTTP_HOST']);
      $aud = $slug;
      $iat = time();
      $nbf = $plugin->config('token.not_before', strtotime('-1 hour'));
      $exp = $plugin->config('token.expiration_time');
      $exp = $exp ? strtotime($exp) : null;

      $token = [
        'iss' => $iss,
        'aud' => $aud,
        'iat' => $iat,
        'nbf' => $nbf
      ];

      if ($exp) {
        $token['exp'] = $exp;
      }

      $token['data'] = [
        'id' => $user->id,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name,
        'email' => $user->email
      ];

      $token = JWT::encode($token, $key);

      $this->success($token);
    }

    return $this->response();
  }

  /**
   * Test a JWT token
   *
   * @token
   */
  public function test()
  {
    $this->success('Token accepted!!!');

    return $this->response();
  }
}
