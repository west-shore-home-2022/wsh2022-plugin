<?php

namespace WSHWordpress\Api;

use WSHWordpress\Api\Api;
use WSHWordpress\Utils\FileUtils;

class Upload extends Api
{
  /**
   * Uploads a file in chunks
   */
  public function chunk()
  {
    $plugin = $this->plugin;

    $file = null;
    $tmp = stripslashes($this->value('tmp'));
    $name = stripslashes($this->value('name'));
    $size = $this->value('size');
    $chunk = @$_REQUEST['chunk'];

    // Validate name
    if (!$name) {
      $this->error('Name is a required param');
    } elseif (!$tmp) {
      $tmp = $name . '-' . time() . '.tmp';
    }

    // Validate chunk
    if (!$chunk) {
      $this->error('Chunk is a required param');
    } else {
      $chunk = base64_decode($chunk);
    }

    if (!$this->hasLogs()) {
      $file = $plugin->cacheRoot . 'chunks' . DS . $tmp;
    }

    // Validate chunk directory
    if (!$this->hasLogs() && !FileUtils::createDir($file)) {
      $this->error('Unable to create upload sub-directory');
    }

    // Process request
    if (!$this->hasLogs()) {
      if (!file_put_contents($file, $chunk, FILE_APPEND)) {
        $this->error('Failed to append file chunk');
      }
    }

    // Handle success
    if (!$this->hasLogs()) {
      $obj = (object) [
        'tmp' => $tmp
      ];

      $this->success($obj);
    }

    return $this->response();
  }
}
