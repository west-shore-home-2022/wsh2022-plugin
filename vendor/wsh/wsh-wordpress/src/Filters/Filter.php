<?php

namespace WSHWordpress\Filters;

use WSHWordpress\Traits\Logger;

/**
 * Filter Base
 *
 * @author Ed Rodriguez
 */
class Filter
{
  use Logger;

  public $plugin;
  public $priority = 10;
  public $accepted_args = 1;

  public function __construct($plugin)
  {
    $this->plugin = $plugin;
  }
}
