<?php

namespace WSHWordpress\Filters;

use WSHWordpress\Plugins\PluginAssets;

/**
 * Asset Version Filter
 *
 * Ensures that the plugin's version is always appended to the src of the plugin's assets.
 * Since other developers can remove it through add_filter methods.
 * This plugin relies on versioning for cache busting.
 *
 * @author Ed Rodriguez
 */
class AssetVersion extends Filter
{
  public static $filter = ['script_loader_src', 'style_loader_src'];
  public $priority = 20;

  /**
   * Runs when the scripts and styles are enqueued
   *
   * @param string $src
   * @return string
   */
  public function emit($src)
  {
    $plugin = $this->plugin;

    $version = $plugin->config('version');

    $items = PluginAssets::$queue;

    // Add versioning
    if ($version && $items && strpos($src, 'ver=') === false) {
      foreach ($items as $item) {
        if (preg_match('/' . preg_quote($item->src, '/') . '/i', $src)) {
          $src = add_query_arg('ver', $version, $src);
        }
      }
    }

    return $src;
  }
}
