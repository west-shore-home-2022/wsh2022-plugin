<?php

namespace WSHWordpress\Tools;

use Google_Client;
use Google_Service_Sheets;
use Google_Service_Sheets_Sheet;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Sheets_SheetProperties;
use Google_Service_Sheets_SpreadsheetProperties;
use Google_Service_Sheets_GridData;
use Google_Service_Sheets_RowData;
use Google_Service_Sheets_CellData;
use Google_Service_Sheets_ExtendedValue;
use WSHWordpress\Traits\Logger;
use WSHWordpress\Utils\Utils;
use Google\Spreadsheet\DefaultServiceRequest;
use Google\Spreadsheet\ServiceRequestFactory;
use Google\Spreadsheet\SpreadsheetService;
use Google\Spreadsheet\Exception\SpreadsheetNotFoundException;
use Google\Spreadsheet\Exception\WorksheetNotFoundException;
use InvalidArgumentException, Exception;

/**
 * Wraps the Google Client
 *
 * PHP Doc
 * @link https://api.kdyby.org/class-Google_Client.html
 * @link https://developers.google.com/resources/api-libraries/documentation/sheets/v4/php/latest/class-Google_Service_Sheets.html
 *
 * Shows how to set up Google_Client
 * @link https://developers.google.com/sheets/api/quickstart/php
 *
 * Set up Oauth 2.0 client
 * @link https://console.cloud.google.com/apis/credentials?project=WSH-169915
 *
 * Remove an approved app
 * @link https://myaccount.google.com/u/0/permissions?pli=1
 *
 * Refreshing tokens documentation
 * @link https://developers.google.com/identity/protocols/OAuth2WebServer#exchange-authorization-code
 */
class GoogleSheets
{
  use Logger;

  public static $config = null;
  public static $config_array = null;
  protected $plugin = null;
  protected $tables = [];

  /**
   * Init Google Sheets tool
   *
   * @param object $plugin
   */
  public function __construct($plugin)
  {
    // Setup API
    $this->plugin = $plugin;

    // Load config
    if (!self::$config) {
      $config = $plugin->root . 'secure' . DS . 'google-client-secrets.json';

      if (file_exists($config)) {
        $json = file_get_contents($config);

        if ($config = json_decode($json, true)) {
          self::$config_array = $config;
          self::$config = json_decode($json);
        }
      }
    }

    // Build required tables
    foreach ($this->tables as $table) {
      $plugin->table($table, $this);
    }
  }

  /**
   * Revokes a client's token
   *
   * @return boolean
   */
  public function revoke()
  {
    $success = false;

    $plugin = $this->plugin;
    $client = $this->client();

    if ($client) {
      if ($client->revokeToken()) {
        $plugin->settings->delete('google');
        $success = true;
      }
    } else {
      $plugin->settings->delete('google');
      $success = true;
    }

    return $success;
  }

  /**
   * Retrieve an access token
   *
   * @return string
   */
  public function token()
  {
    $token = null;
    $plugin = $this->plugin;
    $client = $this->client();

    if ($client) {
      $credentials = $client->getAccessToken();

      // Set token
      if ($credentials && isset($credentials['access_token'])) {
        $token = $credentials['access_token'];
      }
    } else {
      $this->error('Unable to retrieve Google client');
    }

    return $token;
  }

  /**
   * Setup and get the client
   *
   * @return Google_Client
   */
  public function client()
  {
    $client = null;
    $plugin = $this->plugin;

    if ($this->isConfigured()) {
      $client = new Google_Client();
      $client->setAuthConfig(self::$config_array);
      $client->setAccessType('offline');
      $client->setApprovalPrompt('force');
      $client->setIncludeGrantedScopes(true);
      $client->addScope(Google_Service_Sheets::SPREADSHEETS);
      $client->setRedirectUri(site_url('oauth2'));

      $credentials = (array) $plugin->settings->get('google.credentials');

      // Prevent saved error settings
      if (is_array($credentials) && isset($credentials['error'])) {
        $plugin->settings->delete('google.credentials');
        $credentials = false;
      }

      if ($credentials) {
        $error = false;

        try {
          $client->setAccessToken($credentials);
        } catch (InvalidArgumentException $ex) {
          $error = true;
          $plugin->log('InvalidArgumentException: ' . $ex->getMessage());
          $plugin->log($credentials);
        } catch (Exception $ex) {
          $error = true;
          $plugin->log('Exception: ' . $ex->getMessage());
          $plugin->log($credentials);
        }

        // Refresh token
        if ($error || $client->isAccessTokenExpired()) {

          $credentials = $client->refreshToken($client->getRefreshToken());

          // Save new token
          $plugin->settings->save('google.credentials', $credentials);
        }
      }
    }

    return $client;
  }

  /**
   * Authenticates the oauth 2.0 response code
   *
   * @param string $code
   * @return boolean
   */
  public function authenticate($code)
  {
    $success = false;
    $plugin = $this->plugin;
    $client = $this->client();

    if ($client) {
      $client->authenticate($code);

      $credentials = $client->getAccessToken();

      if ($credentials) {
        // Save token setting
        if ($plugin->settings->save('google.credentials', $credentials)) {
          $success = true;
        }
      }
    }

    return $success;
  }

  /**
   * Get the auth url
   *
   * @return type
   */
  public function url()
  {
    $url = null;
    $client = $this->client();

    if ($client) {
      $url = $client->createAuthUrl();
    }

    return $url;
  }

  /**
   * Determine if this feature has been configured
   *
   * @return type
   */
  public function isConfigured()
  {
    return self::$config ? true : false;
  }

  /**
   * Determine if this feature has been authorized
   *
   * @return type
   */
  public function isAuth()
  {
    return $this->plugin->settings->get('google.credentials') ? true : false;
  }

  /**
   * Determine if this feature has been setup
   *
   * @return type
   */
  public function isSetup()
  {
    return $this->isConfigured() && $this->isAuth();
  }

  /**
   * Create a spreadsheet
   *
   * Example:
   * {
   *   "id": null,
   *   "uuid": "_brjmixefo",
   *   "type": "create",
   *   "name": "Test 1",
   *   "worksheets": [{
   *     "id": null,
   *     "name": "Test 2",
   *     "columns": [{
   *       "name": "First Name",
   *       "data": "First Name",
   *       "uuid": "_ktfa5rum7"
   *     }, {
   *       "name": "Last Name",
   *       "data": "Last Name",
   *       "uuid": "_zqtd02l1b"
   *     }]
   *   }]
   * }
   *
   * @param array|object $spreadsheats
   */
  public function create($spreadsheats)
  {
    $results = [];
    $spreadsheats = is_object($spreadsheats) ? [$spreadsheats] : $spreadsheats;

    if (is_array($spreadsheats)) {
      $client = $this->client();

      if ($client) {
        $service = new Google_Service_Sheets($client);

        foreach ($spreadsheats as $_spreadsheet) {
          // 1) Spreadsheet
          $spreadsheet = new Google_Service_Sheets_Spreadsheet;

          // 1a) Spreadsheet props
          $props = new Google_Service_Sheets_SpreadsheetProperties;
          $props->setTitle($_spreadsheet->name);
          $spreadsheet->setProperties($props);

          // 2) Add Sheets
          $worksheets = [];

          foreach ($_spreadsheet->worksheets as $_worksheet) {
            // 2a) Sheet
            $worksheet = new Google_Service_Sheets_Sheet;

            // 2b) Sheet props
            $props = new Google_Service_Sheets_SheetProperties;
            $props->setTitle($_worksheet->name);
            $worksheet->setProperties($props);

            // 2c) Add data
            $data = new Google_Service_Sheets_GridData;
            $row = new Google_Service_Sheets_RowData;

            $values = [];

            foreach ($_worksheet->columns as $key => $_value) {
              $found = false;
              $cell = new Google_Service_Sheets_CellData;
              $value = new Google_Service_Sheets_ExtendedValue;

              if (is_object($_value) && isset($_value->name) && is_string($_value->name)) {
                $_value->name = trim($_value->name);

                if ($_value->name) {
                  $value->setStringValue($_value->name);
                  $found = true;
                }
              } elseif (is_numeric($key) && is_string($_value)) {
                $_value = trim($_value);

                if ($_value) {
                  $value->setStringValue($_value);
                  $found = true;
                }
              }

              if ($found)  {
                $cell->setUserEnteredValue($value);

                $values[] = $cell;
              }
            }

            $row->setValues($values);
            $data->setRowData($row);
            $worksheet->setData($data);

            $worksheets[] = $worksheet;
          }

          // 2d) Set worksheets
          $spreadsheet->setSheets($worksheets);

          // Testing
          //return $spreadsheet->getSheets();

          $response = $service->spreadsheets->create($spreadsheet);

          if ($response instanceof Google_Service_Sheets_Spreadsheet) {
            $results[] = $response;
            $this->success("Spreadsheet \"{$_spreadsheet->name}\" was successfully created");
          } else {
            $this->error("Unable to create spreadsheet \"{$_spreadsheet->name}\"");
          }
        }
      } else {
        $this->error('Google Sheets not setup');
      }
    } else {
      $this->error('Invalid spreadsheet format detected');
    }

    return $results;
  }

  /**
   * Get the columns of a Google Sheet
   *
   * @param object $worksheet
   * @return array
   */
  function getColumns($worksheet)
  {
    $map = [];
    $already_in_map = [];

    $row_one_query = [
      'min-row' => 1,
      'max-row' => 1
    ];

    $cellfeed = $worksheet->getCellFeed($row_one_query);

    if ($cellfeed) {
      $entries = $cellfeed->getEntries();

      foreach ($entries as $cell) {
        $title = trim($cell->getContent());

        if ($title && $title[0] != '_') {
          $array_key = strtolower(preg_replace('/[^A-Z0-9_-]/i', '', $title));

          if ($already_in_map[$array_key]) {
            // if identical column titles, google adds count to end

            $seq = 2;

            while ($already_in_map[$array_key.'_'.$seq]) {
              $seq++;
            }

            $array_key .= "_{$seq}";
          }

          // mark this key as used as used.
          $already_in_map[$array_key] = true;

          $map[$array_key] = $title;
        } else {
          continue;
        }
      }
    }

    return $map;
  }

  /**
   * Gets spreadsheets
   *
   * @return array
   */
  public function spreadsheets()
  {
    $spreadsheets = [];

    $token = $this->token();

    if ($token) {
      $request = new DefaultServiceRequest($token);
      $request->setSslVerifyPeer(false);
      ServiceRequestFactory::setInstance($request);

      $service = new SpreadsheetService();
      $feed = $service->getSpreadsheetFeed();

      $spreadsheets = [];

      foreach ($feed->getEntries() as $sheet) {
        $url = $sheet->getId();
        $url = preg_replace('/\/$/', '', $url);
        $url = explode('/', $url);
        $id = array_pop($url);

        $spreadsheets[] = (object) [
          'id' => $id,
          'name' => ucwords($sheet->getTitle())
        ];
      }
    } else {
      $this->error('Unable to retrieve Google Sheets token');
    }

    return $spreadsheets;
  }

  /**
   * Gets a spreadsheet
   *
   * @param string $spreadsheet_id
   * @return object
   */
  public function spreadsheet($spreadsheet_id)
  {
    $spreadsheet = null;

    $token = $this->token();

    if ($token) {
      $request = new DefaultServiceRequest($token);
      $request->setSslVerifyPeer(false);
      ServiceRequestFactory::setInstance($request);

      $service = new SpreadsheetService();
      $feed = $service->getSpreadsheetFeed();

      $spreadsheet_url = $spreadsheet_id;

      if (!preg_match('/^http/', $spreadsheet_url)) {
        $spreadsheet_url = "https://spreadsheets.google.com/feeds/spreadsheets/private/full/{$spreadsheet_url}";
      }

      try {
        $spreadsheet = $feed->getById($spreadsheet_url);
      } catch (SpreadsheetNotFoundException $ex) {
        $this->error($ex->getMessage());
      }
    } else {
      $this->error('Unable to retrieve Google Sheets token');
    }

    return $spreadsheet;
  }

  /**
   * Get worksheets
   *
   * @param string|object $spreadsheet
   * @return array
   */
  public function worksheets($spreadsheet)
  {
    $worksheets = [];

    $spreadsheet = !is_object($spreadsheet) ? $this->spreadsheet($spreadsheet) : $spreadsheet;

    if ($spreadsheet) {
      $feed = $spreadsheet->getWorksheetFeed();

      foreach ($feed->getEntries() as $sheet) {
        $url = $sheet->getId();
        $url = preg_replace('/\/$/', '', $url);
        $url = explode('/', $url);
        $id = array_pop($url);

        $worksheets[] = (object) [
          'id' => $id,
          'name' => ucwords($sheet->getTitle())
        ];
      }
    } else {
      $this->error('Unable to retrieve worksheets');
    }

    return $worksheets;
  }

  /**
   * Get worksheet
   *
   * @param string|object $spreadsheet
   * @param string $worksheet_id
   * @return array
   */
  public function worksheet($spreadsheet, $worksheet_id)
  {
    $worksheet = null;

    $spreadsheet = !is_object($spreadsheet) ? $this->spreadsheet($spreadsheet) : $spreadsheet;

    if ($spreadsheet) {
      $feed = $spreadsheet->getWorksheetFeed();

      try {
        $worksheet = $feed->getById($worksheet_id);
      } catch (WorksheetNotFoundException $ex) {
        $this->error($ex->getMessage());
      }

      if (!$worksheet) {
        $this->error('Unable to retrieve worksheet');
      }
    } else {
      $this->error('Unable to retrieve spreadsheet');
    }

    return $worksheet;
  }

  /**
   * Get worksheets
   *
   * @param string|object $worksheet
   * @param string|object [$spreadsheet]
   * @return array
   */
  public function columns($worksheet, $spreadsheet = null)
  {
    $columns = [];

    $spreadsheet = !is_null($spreadsheet) && !is_object($spreadsheet) ? $this->spreadsheet($spreadsheet) : $spreadsheet;

    if ($spreadsheet || is_object($worksheet)) {
      $worksheet = !is_object($worksheet) ? $this->worksheet($spreadsheet, $worksheet) : $worksheet;

      if ($worksheet) {
        $columns = [];

        $array = $this->getColumns($worksheet);

        foreach ($array as $column => $name) {
          $columns[] = (object) [
            'column' => $column,
            'name' => $name
          ];
        }

        if ($columns) {
          $this->success($columns);
        } else {
          $this->error('Unable to load remote columns');
        }
      } else {
        $this->error('Unable to retrieve worksheet');
      }
    }

    return $columns;
  }

  /**
   * Insert a row into a worksheet
   *
   * @param object $worksheet
   * @param array $values
   */
  public function insert($worksheet, $values)
  {
    if ($worksheet && $values) {
      try {
        $feed = $worksheet->getListFeed();
        $feed->insert($values);
        $this->success('Appended row to sheet');
      } catch (BadRequestException $ex) {
        $this->error($ex->getMessage());
      } catch (Exception $ex) {
        $this->error('Unable to insert row to sheet');
      }
    } else {
      $this->error('Unable to insert row to sheet');
    }
  }

  /**
   * Converts a number >= 0 into an alpha excel column name.
   * This function is a "Recursion Function" and is used to achieve "AA", "BZ", "CCA", etc...
   *
   * @param integer [$num=0] The column number to convert
   * @param array [$array=array()] An optional array to override the default excel array
   * @param string [$column] The column name which is passed recursively
   * @return string The name of the excel column
   */
  public function getAlnumCol($num = 0, $array = [], $column = '')
  {
    if (!is_array($array) || !$array) {
      $array = Utils::trimExplode(',', 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z');
    }

    $count = count($array);

    if ($num < $count) {
      $column .= $array[$num];
    } else {
      $c = $num / $count;
      $mod = $c % $count;
      $loop = $c / $count;
      $position = $mod == 0 ? $count : $mod;
      $index = $position - 1;

      for ($i = 0; $i < $loop; $i++) {
        $column .= $array[$index];
      }

      $num = $num - ($count * intval($c));
      $column = $this->getAlnumCol($num, $array, $column);
    }

    return $column;
  }
}