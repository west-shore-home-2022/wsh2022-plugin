<?php

namespace WSHWordpress\Tools;

use WSHWordpress\Utils\Utils;
use WSHWordpress\Utils\TestUtils;
use mysqli;

/**
 * Builder Class
 * Handles query caching and driver switching
 *
 * @author Ed Rodriguez
 */
class Builder
{
  public $cache = false;
  public $driver = null;
  public $drivers = ['wordpress', 'native'];
  public $expire = '30 minutes';
  public $cacheDir = null;
  public static $history = [];

  public function __construct() {
    global $_plugin;

    if (!$this->cacheDir && $_plugin) {
      $this->cacheDir = $_plugin->pluginRoot . 'cache' . DIRECTORY_SEPARATOR;
    }
  }

  /**
   * Sets up the driver to use
   *
   * Example
   * TrackingModel::driver('native')
   *    ->groupBy('a.uri')
   *    ->whereNotNull('a.url')
   *    ->whereNull('a.type')
   *    ->where('a.created_at', 'LIKE', "{$year}-{$month}-%")
   *    ->debug(0);
   *
   * @param string|boolean [$value]
   * @return $this
   */
  public function driver($driver)
  {
    $driver = strtolower($driver);

    if (in_array($driver, $this->drivers)) {
      $this->driver = $driver;
    }

    return $this;
  }

  /**
   * Sets up caching
   *
   * Example
   * TrackingModel::cache('1 hour')
   *    ->groupBy('a.uri')
   *    ->whereNotNull('a.url')
   *    ->whereNull('a.type')
   *    ->where('a.created_at', 'LIKE', "{$year}-{$month}-%")
   *    ->debug(0);
   *
   * @param string|boolean [$value]
   * @return $this
   */
  public function cache($value = true)
  {
    if (is_string($value)) {
      $this->expire = $value;

      $this->cache = true;
    } else {
      $this->cache = Utils::getBool($value);
    }

    return $this;
  }

  /**
   * Determines is caching is setup
   *
   * @return boolean
   */
  public function cacheSetup()
  {
    return $this->cache
      && isset($this->cacheDir)
      && is_string($this->cacheDir)
      && is_dir($this->cacheDir);
  }

  /**
   * Retrieve cache
   *
   * @param string $filename
   * @param mixed $data
   * @param boolean [$cache]
   * @return boolean;
   */
  public function getCache($filename, &$data, $cache = true)
  {
    if (!$this->cacheSetup()) {
      return false;
    }

    $file = $this->cacheDir . $filename . '.tmp';

    // Retrieve cache
    if (file_exists($file)) {

      // Clear cache if expired
      if ($cache && isset($this->expire) && is_string($this->expire)) {
        $now = time();
        $date = filemtime($file);
        $expire = $now - (strtotime($this->expire) - $now);

        if ($date < $expire) {
          unlink($file);
          $cache = false;
        }
      }

      if ($cache) {
        $cache = file_get_contents($file);

        $data = unserialize($cache);

        return true;
      }
    }

    return false;
  }

  /**
   * Save cache
   *
   * @param string $filename
   * @param mixed $data
   * @return boolean
   */
  public function saveCache($filename, $data)
  {
    $bool = false;

    if ($this->cacheSetup()
        && is_string($filename)
        && strlen($filename)) {

      $file = $this->cacheDir . $filename . '.tmp';

      $cache = serialize($data);
      $bool = file_put_contents($file, $cache);
    }

    return $bool;
  }

  /**
   * Determine the MySQL driver to use
   *
   * @return string (wordpress|native)
   */
  public function getDriver()
  {
    global $wpdb;

    $driver = 'wordpress';

    if ($this->driver) {
      $driver = $this->driver;
    } else if ($wpdb) {
      $driver = 'wordpress';
    } else if (class_exists('mysqli')
      && defined('DB_HOST')
      && defined('DB_USER')
      && defined('DB_PASSWORD')
      && defined('DB_NAME')) {

      $driver = 'native';
    }

    $this->driver = strtolower($driver);

    return $this->driver;
  }

  /**
   * Query & cache the database directly bypassing WordPress
   *
   * Wordpress Mode Testing
   * http://bathwraps.local/wp-json/WSH-portal/companies/?_page=1&_count=1000&test=speed&driver=wordpress&query=2
   *
   * Native Mode Testing
   * http://bathwraps.local/wp-json/WSH-portal/companies/?_page=1&_count=50&test=speed&driver=native&query=2
   *
   * test=speed
   * driver=wordpress|native
   * query=2
   * reset=1
   *
   * @param string $sql
   * @param boolean [$cache]
   * @return array
   */
  public function get_results($sql)
  {
    $results = [];
    $driver = $this->getDriver();

    $test = isset($_GET['test']) && $_GET['test'] === 'speed' ? true : false;
    $driver = $test && isset($_GET['driver']) ? $_GET['driver'] : $driver;
    $query = $test && isset($_GET['query']) ? $_GET['query'] : 1;
    $reset = $test && isset($_GET['reset']) ? Utils::getBool($_GET['reset']) : false;

    $filename = md5($sql);
    $this->getCache($filename, $results);

    // Run query
    if (!$results) {
      $start = microtime(true);

      if ($driver === 'native') {
        // Create connection
        $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

        mysqli_set_charset($conn, 'utf8');

        // Check connection
        if ($conn->connect_error) {
          die('Connection failed: ' . $conn->connect_error);
        } else {
          $result = $conn->query($sql);

          if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
              $results[] = (object) $row;
            }
          }
        }

        $conn->close();
      } else if ($driver === 'wordpress') {
        global $wpdb;

        $results = $wpdb->get_results($sql);
      }

      // Speed Testing
      if ($test) {
        $sql = trim(preg_replace('/^\s+/m', '', $sql));

        if ($sql) {
          $this::$history[] = $sql;
        }

        $history = $this::$history;

        if (count($history) >= $query) {
          @session_start();
          $end = microtime(true);
          $seconds = $end - $start;
          $milliseconds = $seconds * 1000;
          $total = count($results);

          $test_name = md5($sql);
          $test_name = "tests-speed-{$driver}-{$test_name}";
          $tests = !$reset && isset($_SESSION[$test_name]) && is_array($_SESSION[$test_name]) ? $_SESSION[$test_name] : [];
          $tests[] = [$seconds, $milliseconds];
          $total_tests = count($tests);

          $total_seconds = 0;
          $total_milliseconds = 0;

          foreach ($tests as $array) {
            $total_seconds += $array[0];
            $total_milliseconds += $array[1];
          }

          $average_seconds = $total_seconds / $total_tests;
          $average_milliseconds = $total_milliseconds / $total_tests;

          $data = [
            'Speed Test' => [
              'Total Results' => $total,
              'SQL Query' => "#{$query}",
              'SQL Driver' => ucwords($driver),
              'SQL' => $sql
            ],
            'Test Details' => [
              'Total Tests' => $total_tests,
              'Average Seconds' => $average_seconds,
              'Average Milliseconds' => $average_milliseconds,
              'Tests' => $tests
            ],
            'Query Results' => [$results]
          ];

          $_SESSION[$test_name] = $tests;

          TestUtils::html($data);
        }
      }

      // Save cache
      if ($results) {
        $this->saveCache($filename, $results);
      }
    }

    return $results;
  }
}