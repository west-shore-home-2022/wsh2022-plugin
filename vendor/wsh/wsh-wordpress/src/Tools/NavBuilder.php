<?php

namespace WSHWordpress\Tools;

/**
 * Nav Builder
 *
 * @author erodriguez
 */
class NavBuilder
{
  protected $scope = null;

  public function __construct($scope = null)
  {
    $this->scope = $scope;
  }

  /**
   * Builds navigation
   *
   * @param string $str
   * @return string
   */
  public function nav($params = [])
  {
    $init = true;
    $output = '';
    $items = [];
    $scope = $this->scope;
    $selected = null;
    $templates = [];
    $types = [];
    $tab = '';

    $locals = ['init', 'items', 'scope', 'selected', 'tab', 'templates', 'types'];

    foreach ($params as $name => $value) {
      if (in_array($name, $locals)) {
        $$name = $value;
      }
    }

    if ($items) {
      foreach ($items as $item) {
        $show = true;

        if ($types) {
          $show = in_array($item->type, $types);
        }

        if ($show) {
          $output2 = '';
          $output .= "\n\t{$tab}<li>";

          $item->selected = isset($item->id) && $item->id == $selected;

          if (isset($templates[$item->type])) {
            $html = $templates[$item->type];
            $html = (new Parser($item))->parse($html, $item);

            $output .= $html;
          } else {
            $output .= "<a href=\"#\" data-item=\"{$item->id}\">{$item->name}</a>";
          }

          if ($item->type === 'category' && $scope && method_exists($scope, 'items')) {
            $children = $scope->items($item);
            $t1 = '';

            if ($children) {
              $t1 = $tab . "\t";
              $t2 = $tab . "\t\t";

              $params['init'] = false;
              $params['items'] = $children;
              $params['tab'] = $t2;

              $output2 = $this->nav($params);
            }
          }

          if ($output2) {
            $output .= $output2 . "{$t1}</li>";
          } else {
            $output .= "</li>";
          }
        }
      }
    }

    if ($output) {
      $tag = '<ul>';

      if ($init) {
        if (isset($templates['ul_init'])) {
          $html = $templates['ul_init'];
          $html = (new Parser())->parse($html, $params);

          $tag = $html;
        }
      } else {
        if (isset($templates['ul_child'])) {
          $html = $templates['ul_child'];
          $html = (new Parser())->parse($html, $params);

          $tag = $html;
        }
      }

      $output = "\n{$tab}{$tag}{$output}\n{$tab}</ul>\n";
    }

    return $output;
  }
}