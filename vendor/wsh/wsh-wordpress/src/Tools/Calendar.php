<?php

namespace WSHWordpress\Tools;

/**
 * Query Builder
 *
 * @author Ed Rodriguez
 */
class Calendar
{
  public $month = '';
  public $year = '';
  public $prevMonth = '';
  public $prevYear = '';
  public $nextMonth = '';
  public $nextYear = '';
  public $firstDay = '';
  public $numberDays = '';
  public $monthName = '';
  public $dayOfWeek = '';
  public $weekDays = '';
  public $json = '';
  public $rows = [];
  public $days = [];

  public function __construct($month = null, $year = null)
  {
    $this->setup($month, $year);
  }

  public function setup($month = null, $year = null)
  {
    $this->weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat'];

    $this->month = $month ?  $month : date('m');
    $this->month = (strlen($this->month) == 1) ? '0'.$this->month : $this->month;
    $this->year = $year ? $year : date('Y');

    // PREVIOUS & NEXT
    $this->prevMonth = $this->month-1;
    $this->prevYear = $this->year;
    if ($this->prevMonth == 0) {
        $this->prevMonth = 12;
        $this->prevYear--;
    }
    $this->nextMonth = $this->month+1;
    $this->nextYear = $this->year;
    if ($this->nextMonth == 13) {
        $this->nextMonth = 1;
        $this->nextYear++;
    }

    // FIRST DAY
    $this->firstDay = mktime(0, 0, 0, $this->month, 1, $this->year);

    // NUMBER OF DAYS
    $this->numberDays = date('t', $this->firstDay);

    // MONTH NAME & DAY OF WEEK
    $array = getdate($this->firstDay);
    $this->monthName = $array['month'];
    $this->dayOfWeek = $array['wday'];
  }

  public function prevMonthStr() { return date('F', mktime(0, 0, 0, $this->prevMonth, 1, $this->year)); }
  public function nextMonthStr() { return date('F', mktime(0, 0, 0, $this->nextMonth, 1, $this->year)); }

  public function current()
  {
    return mktime(0, 0, 0, $this->month, 1, $this->year);
  }

  public function prev() { return strtotime('previous month', $this->current()); }
  public function next() { return strtotime('next month', $this->current()); }
}