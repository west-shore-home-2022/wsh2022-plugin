<?php

namespace WSHWordpress\Tools;

use WSHWordpress\Utils\Utils;

/**
 * Parser
 *
 * @author erodriguez
 */
class Parser
{
  protected $scope;

  public function __construct($scope = null)
  {
    $this->scope = $scope;
  }

  /**
   * Parses a string against data
   *
   * @param string $str
   * @return string
   */
  public function parse($str, $data)
  {
    $this->parse_comments($str, $data);
    $this->parse_loops($str, $data);
    $this->parse_includes($str, $data);
    $this->parse_conditions($str, $data);
    $this->parse_local_variables($str, $data);
    $this->parse_variables($str, $data);

    return $str;
  }

  /**
   * Parses comments
   *
   * @param string $str
   * @param object $data
   */
  public function parse_comments(&$str, $data)
  {
    $matches = [];
    $pattern = '/\{\{--(.*)--\}\}/Uis';

    preg_match_all($pattern, $str, $matches);

    if ($matches && isset($matches[0]) && $matches[0]) {
      foreach ($matches[0] as $comment) {
        $pattern = '/' . preg_quote($comment, '/') . '/i';

        $str = preg_replace($pattern, '', $str);
      }
    }
  }

  /**
   * Parses loops
   *
   * @param string $str
   * @param object $data
   */
  public function parse_loops(&$str, $data)
  {
    $matches = [];
    $pattern = '/@foreach\s?\(([a-z0-9_]+)\s+as\s([a-z0-9_]+)\)(.*)@foreach/Uis';

    preg_match_all($pattern, $str, $matches);

    //die('<pre><xmp>' . print_r($matches, 1));

    if ($matches && isset($matches[1]) && $matches[1]) {
      $count = 0;

      foreach ($matches[1] as $prop) {
        $value = '';

        $items = Utils::find($data, $prop);
        $item = Utils::find($data, "{$prop}.{$count}");
        $name = $matches[2][$count];
        $template = $matches[3][$count];
        $pattern = '/' . preg_quote($matches[0][$count], '/') . '/Uis';

        if ($items && is_array($items)) {
          $count2 = 0;

          foreach ($items as $item) {
            $data2 = (object) [];
            $data2->{$name} = $item;
            $data2->index = $count2;
            $data2->count = $count2 + 1;

            $value .= $this->parse($template, $data2);
            $count2++;
          }
        }

        $str = preg_replace($pattern, $value, $str);
        $count++;
      }
    }
  }

  /**
   * Parses includes
   *
   * @param string $str
   * @param object $data
   */
  public function parse_includes(&$str, $data)
  {
    $matches = [];
    $pattern = '/@include\s?\(("|\'){1}([a-z0-9_\-\.\/]+)("|\'){1}\)/Ui';

    preg_match_all($pattern, $str, $matches);

    //die('<pre>' . print_r($matches, 1));

    if ($matches && isset($matches[1]) && $matches[1]) {
      $count = 0;

      foreach ($matches[2] as $prop) {
        $pattern = '/' . preg_quote($matches[0][$count], '/') . '/i';

        $path = preg_replace('/\./', DS, $prop) . '.php';
        $path = $this->scope->abspath($path);

        $value = '';

        if (file_exists($path)) {
          ob_start();

          include($path);

          $value = ob_get_contents();

          ob_get_clean();
        }

        $str = preg_replace($pattern, $value, $str);

        $count++;
      }
    }
  }

  /**
   * Parses conditional statements
   *
   * @param string $str
   * @param object $data
   */
  public function parse_conditions(&$str, $data)
  {
    $matches = [];
    $pattern = '/@if\s?\((.*)\)(.*)@endif/Uis';

    preg_match_all($pattern, $str, $matches);

    if ($matches && isset($matches[1]) && $matches[1]) {
      $count = 0;

      foreach ($matches[1] as $expression) {
        $expression = '$bool = ' . preg_replace('/\$/', '@$this->scope->', $expression) . ';';

        eval($expression);

        $pattern = '/' . preg_quote($matches[0][$count], '/') . '/Uis';

        $true = $matches[2][$count];
        $false = '';

        // Handle else statements
        $parts = preg_split('/\s+@else\s+/', $true);

        if (count($parts) === 2) {
          $true = $parts[0];
          $false = $parts[1];
        }

        $value = $bool ? $true : $false;

        $str = preg_replace($pattern, $value, $str);
        $count++;
      }
    }
  }

  /**
   * Parses local variables
   *
   * @param string $str
   * @param object $data
   */
  public function parse_local_variables(&$str, $data)
  {
    $matches = [];
    $pattern = '/\{\{\s*([a-z0-9_]+)\s*\}\}/i';

    preg_match_all($pattern, $str, $matches);

    if ($matches && isset($matches[1]) && $matches[1]) {
      $count = 0;

      foreach ($matches[1] as $prop) {
        $pattern = '/' . preg_quote($matches[0][$count], '/') . '/i';

        if (isset($this->scope->{$prop})) {
          $value = $this->scope->{$prop};
          $value = !is_null($value) ? $value : '';

          $str = preg_replace($pattern, $value, $str);
        }

        $count++;
      }
    }
  }

  /**
   * Parses variables
   *
   * @param string $str
   * @param object $data
   */
  public function parse_variables(&$str, $data)
  {
    $matches = [];
    $pattern = '/\{\{\s*([a-z0-9_\.\-]+)\s*\}\}/i';

    preg_match_all($pattern, $str, $matches);

    if ($matches && isset($matches[1]) && $matches[1]) {
      $count = 0;

      foreach ($matches[1] as $prop) {
        $pattern = '/' . preg_quote($matches[0][$count], '/') . '/i';
        $value = Utils::find($data, $prop);
        $value = !is_null($value) ? $value : '';

        if ($this->scope->auth) {
          $names = $this->scope->editable['text'];

          if (preg_match('/^(' . $names . ')$/i', $prop) || preg_match('/\.(' . $names . ')$/i', $prop)) {
            // Editable text
            $value = strlen($value) ? $value : 'Editable Text';
            $value = preg_replace('/(?<!>)\n/', '<br />', $value);
            $value = '<span title="Click to Edit" data-edit="' . $prop . '">' . $value . '</span>';
          } elseif (preg_match('/(image)$/i', $prop)) {
            // Editable image
            $operator = !preg_match('/\?/', $value) ? '?' : '&';

            $value .= $operator . 'cms-image=' . $prop;
          }
        }

        $str = preg_replace($pattern, $value, $str);
        $count++;
      }
    }
  }
}