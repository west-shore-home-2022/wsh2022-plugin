<?php

namespace WSHWordpress\Tools;

use WSHWordpress\Traits\Logger;
use WSHWordpress\Models\User;
use WSHWordpress\Models\MailList;
use WSHWordpress\Models\MailListSegment;
use WSHWordpress\Models\MailListUser;
use WSHWordpress\Models\MailListSegmentUser;
use DrewM\MailChimp\MailChimp as MailChimpApi;

class MailChimp
{
  use Logger;

  public $count = 50;
  protected $api = null;
  protected $plugin = null;
  protected $tables = ['wsh_mail_lists', 'wsh_mail_lists_segments', 'wsh_mail_lists_users', 'wsh_mail_lists_segments_users'];

  /**
   * Init MailChimp tool
   *
   * @param object $plugin
   * @param type [$key] The MailChimp API key
   */
  public function __construct($plugin = null, $key = null)
  {
    // Validate plugin
    if (!$plugin) {
      $this->error('Missing plugin injection dependency');
    }

    // Validate key
    if (!$key) {
      $key = $plugin ? $plugin->settings->get('mailchimp.api_key') : $key;

      if (!$key) {
        $this->error('Missing MailChimp API key configuration');
      }
    }

    // Setup API
    if (!$this->hasErrors()) {
      $this->plugin = $plugin;
      $this->api = new MailChimpApi($key);

      // Build required tables
      foreach ($this->tables as $table) {
        $plugin->table($table, $this);
      }
    }
  }

  /**
   * Pulls remote lists and saves them locally
   *
   * - Add lists
   * - Update lists
   * - Delete lists
   *
   * @return WSHWordpress\Collections\Collection
   */
  public function lists()
  {
    $ids = [];
    $count = $this->count;
    $api = $this->api;

    if (!$this->hasErrors()) {
      // Get first list
      $url = "lists?count={$count}&offset=0";
      $result = $api->get($url);

      if (!$api->success()) {
        $this->error('API #1: ' . $api->getLastError());
        $this->error($url);
      }

      if (!$this->hasErrors()) {
        $offset = 0;
        $items = $result['lists'];
        $total = $result['total_items'];
        $pages = ceil($total / $count);

        // Get lists
        for ($page = 0; $page <= $pages; $page++) {
          $offset = $page * $count;

          if ($page > 0) {
            $url = "lists?count={$count}&offset={$offset}";
            $result = $api->get($url);

            if ($api->success()) {
              $items = $result['lists'];
            } else {
              $this->error('API #2: ' . $api->getLastError());
              $this->error($url);
              break;
            }
          }

          // Save lists
          if (!$this->hasErrors() && $items) {
            foreach ($items as $item) {
              $item = (object) $item;

              $ids[] = $item->id;

              $list = MailList::withTrashed()->firstOrNew([
                'list_id' => $item->id
              ]);

              $action = $list->isEmpty() ? ['Added', 'add'] : ['Updated', 'update'];

              $list->name = $item->name;
              $list->count = $item->stats['member_count'];
              $list->type = 'MailChimp';

              // Save list
              if ($list->save()) {
                $this->success("{$action[0]} list #{$list->list_id}");

                // Segments
                $this->segments($list->list_id);
              } else {
                $this->error("Unable to {$action[1]} list #{$list->list_id}");
                $this->addLogs($list->logs());
                break;
              }
            }
          }
        }
      }

      // Delete lists (only delete mailchimp lists)
      $lists = MailList::whereNotNull('list_id')->get();

      foreach ($lists as $list) {
        if (!in_array($list->list_id, $ids)) {
          if ($list->delete()) {
            $this->info("Deleted list #{$list->list_id}");
          } else {
            $this->error("Unable to delete list #{$list->list_id}");
          }
        }
      }
    }

    return MailList::where('type', 'MailChimp')->get();
  }

  /**
   * Pulls remote mail list segments and saves them locally
   *
   * - Add segments
   * - Update segments
   * - Delete segments
   *
   * @param string $list_id The MailChimp list id
   * @return WSHWordpress\Collections\Collection
   */
  public function segments($list_id)
  {
    $ids = [];
    $count = $this->count;
    $api = $this->api;

    if (!$this->hasErrors()) {
      // Get first segment
      $url = "lists/{$list_id}/segments?count={$count}&offset=0";
      $result = $api->get($url);

      if (!$api->success()) {
        $this->error('API #1: ' . $api->getLastError());
        $this->error($url);
      }

      if (!$this->hasErrors()) {
        $offset = 0;
        $items = $result['segments'];
        $total = $result['total_items'];
        $pages = ceil($total / $count);

        // Get segments
        for ($page = 0; $page <= $pages; $page++) {
          $offset = $page * $count;

          if ($page > 0) {
            $url = "lists/{$list_id}/segments?count={$count}&offset={$offset}";
            $result = $api->get($url);

            if ($api->success()) {
              $items = $result['segments'];
            } else {
              $this->error('API #2: ' . $api->getLastError());
              $this->error($url);
              break;
            }
          }

          // Save segments
          if (!$this->hasErrors() && $items) {
            foreach ($items as $item) {
              $item = (object) $item;

              // Only process segments with the type of "saved"
              if ($item->type !== 'saved') {
                continue;
              }

              $ids[] = $item->id;

              $segment = MailListSegment::firstOrNew([
                'segment_id' => $item->id,
                'list_id' => $list_id
              ]);

              $action = $segment->isEmpty() ? ['Added', 'add'] : ['Updated', 'update'];

              $segment->name = $item->name;
              $segment->count = $item->member_count;
              $segment->type = 'MailChimp';

              // Save list
              if ($segment->save()) {
                $this->success("{$action[0]} list segment #{$segment->segment_id}");
              } else {
                $this->error("Unable to {$action[1]} list segment #{$segment->segment_id}");
                $this->addLogs($segment->logs());
                break;
              }
            }
          }
        }
      }

      // Delete segments (only delete mailchimp list segments)
      $segments = MailListSegment::whereNotNull('segment_id')->where('list_id', $list_id)->get();

      foreach ($segments as $segment) {
        if (!in_array($segment->segment_id, $ids)) {
          if ($segment->delete()) {
            $this->info("Deleted list segment #{$segment->segment_id}");
          } else {
            $this->error("Unable to delete list segment #{$segment->segment_id}");
          }
        }
      }
    }

    return MailListSegment::where([
      'type' => 'MailChimp',
      'list_id' => $list_id
    ])->get();
  }

  /**
   * Import a mail list's members
   *
   * @param object $list
   * @return array
   */
  public function listMembers($list)
  {
    $logs = [
     'added' => [],
     'not_added' => []
    ];
    $api = $this->api;
    $count = $this->count;
    $offset = 0;

    // Get first mail list's members
    $url = "lists/{$list->list_id}/members?count={$count}&offset={$offset}";
    $result = $api->get($url);

    if (!$api->success()) {
      $this->error("API #3: ({$list->list_id}) " . $api->getLastError());
      $this->error($url);
    }

    if (!$this->hasErrors()) {
      $offset = 0;
      $items = $result['members'];
      $total = $result['total_items'];
      $pages = ceil($total / $count);

      // Get lists
      for ($page = 0; $page <= $pages; $page++) {
        $offset = $page * $count;

        if ($page > 0) {
          $url = "lists/{$list->list_id}/members?count={$count}&offset={$offset}";
          $result = $api->get($url);

          if ($api->success()) {
            $items = $result['members'];
          } else {
            $this->error("API #4: ({$list->list_id}) " . $api->getLastError());
            $this->error($url);
            break;
          }
        }

        // Build mail list/user relationships
        if ($items) {
          foreach ($items as $item) {
            $item = (object) $item;

            // Get user
            $user = User::where('user_email', $item->email_address)->first();

            if ($user) {
              // Attach users to lists
              if ($list->users()->attach($user->id)) {
                $logs['added'] = $item->email_address;
                //$this->info("Associated user #{$user->id} ({$user->email}) with list #{$list->id}");
              } else {
                $logs['not_added'] = $item->email_address;
                //$this->warning("Unable to associate user #{$user->id} ({$user->email}) with list #{$list->id}");
              }
            } else {
              $logs['not_added'] = $item->email_address;
              //$this->warning("Unable to find a user with {$item->email_address}");
            }
          }
        }
      }
    }

    return $logs;
  }

  /**
   * Import a mail list segment's members
   *
   * @param object $list
   * @param object $segment
   * @return array
   */
  public function segmentMembers($list, $segment)
  {
    $logs = [
     'added' => [],
     'not_added' => []
    ];
    $api = $this->api;
    $count = $this->count;
    $offset = 0;

    // Get first mail list segment's members
    $url = "lists/{$list->list_id}/segments/{$segment->segment_id}/members?count={$count}&offset={$offset}";
    $result = $api->get($url);

    if (!$api->success()) {
      $this->error("API #3: ({$segment->segment_id}) " . $api->getLastError());
      $this->error($url);
    }

    if (!$this->hasErrors()) {
      $offset = 0;
      $items = $result['members'];
      $total = $result['total_items'];
      $pages = ceil($total / $count);

      // Get lists
      for ($page = 0; $page <= $pages; $page++) {
        $offset = $page * $count;

        if ($page > 0) {
          $url = "lists/{$list->list_id}/segments/{$segment->segment_id}/members?count={$count}&offset={$offset}";
          $result = $api->get($url);

          if ($api->success()) {
            $items = $result['members'];
          } else {
            $this->error("API #4: ({$segment->segment_id}) " . $api->getLastError());
            $this->error($url);
            break;
          }
        }

        // Build mail list segment/user relationships
        if ($items) {
          foreach ($items as $item) {
            $item = (object) $item;

            // Get user
            $user = User::where('user_email', $item->email_address)->first();

            if ($user) {
              // Attach users to lists
              if ($segment->users()->attach($user->id)) {
                $logs['added'] = $item->email_address;
                //$this->info("Associated user #{$user->id} ({$user->email}) with list #{$segment->id}");
              } else {
                $logs['not_added'] = $item->email_address;
                //$this->warning("Unable to associate user #{$user->id} ({$user->email}) with list #{$segment->id}");
              }
            } else {
              $logs['not_added'] = $item->email_address;
              //$this->warning("Unable to find a user with {$item->email_address}");
            }
          }
        }
      }
    }

    return $logs;
  }

  /**
   * Sync's remote lists & members to local mail lists and users
   */
  public function sync()
  {
    $ids = [];
    $logs = [
     'added' => [],
     'not_added' => []
    ];
    $count = $this->count;
    $api = $this->api;
    $lists = $this->lists();

    if (!$this->hasErrors()) {

      // Remove all MailChimp mail list relationships
      $ids = MailList::where('type', 'MailChimp')->get()->pluck('id');

      if (MailListUser::whereIn('mail_list_id', $ids)->delete()) {
        $this->info('Removed all old MailChimp list relationships');
      } else {
        $this->error('Unable to remove old MailChimp list relationships');
      }

      if (!$this->hasErrors() && $lists->count()) {

        foreach ($lists as $list) {

          // Skip lists with no list_id
          if (!isset($list->list_id) || !$list->list_id) {
            continue;
          }

          $logs2 = $this->listMembers($list);
          $logs = array_merge($logs, $logs2);

          // Segments
          $segments = $this->segments($list->list_id);

          // Remove all MailChimp segment relationships
          $ids = $segments->pluck('id');

          if ($ids) {
            if (MailListSegmentUser::whereIn('mail_list_segment_id', $ids)->delete()) {
              $this->info('Removed all old MailChimp segment relationships');
            } else {
              $this->error('Unable to remove old MailChimp segment relationships');
            }
          }

          foreach ($segments as $segment) {
            $logs2 = $this->segmentMembers($list, $segment);
            $logs = array_merge($logs, $logs2);
          }
        }
      } else {
        if ($logs['added']) {
          $total = count($logs['added']);
          $this->success("Associated {$total} emails");
        }

        if ($logs['not_added']) {
          $total = count($logs['not_added']);
          $this->warning("Unable to associate {$total} emails");
        }
      }
    }
  }
}