<?php

namespace WSHWordpress\Tools;

use WSHWordpress\Utils\Utils;

/**
 * Query Builder
 *
 * @author Ed Rodriguez
 */
class QueryBuilder extends Builder
{
  protected $conditions = [];
  protected $subqueries = [];
  protected $operators = ['=', '!=', '<>', '>', '<', '>=', '<=', 'like', 'is', 'in'];
  protected $class = null;
  protected $model = null;
  protected $order = null;
  protected $sort = null;
  protected $group = null;
  protected $limit = null;
  protected $sql = null;
  protected $deleted = false;
  protected $debug = false;
  protected $fk = [];

  public function __construct($class)
  {
    $this->class = $class;

    if (is_string($class)) {
      $this->model = new $class;
    }

    parent::__construct();
  }

  /**
   * Return the builder
   *
   * @return $this
   */
  public function builder()
  {
    return $this;
  }

  /**
   * Gets the model's id column name
   *
   * @return string
   */
  public function idColumn()
  {
    return $this->model->idColumn();
  }

  /**
   * Wrapper method for getting the model's table
   *
   * @return string
   */
  public function getTable()
  {
    return $this->table();
  }

  /**
   * Gets the model's table name
   *
   * @return string
   */
  public function table()
  {
    return $this->model->table();
  }

  /**
   * Gets the model's required fields
   *
   * @return array
   */
  public function required()
  {
    return $this->model->required();
  }

  /**
   * Sets up the query builder's where "OR" conditions
   *
   * @global object $wpdb
   * @param array|string $arg1
   * @param string $arg2
   * @param string $arg3
   * @return $this
   */
  public function orWhere($arg1, $arg2 = null, $arg3 = null)
  {
    return $this->where($arg1, $arg2, $arg3, 'OR');
  }

  /**
   * Sets up the query builder's where "AND|OR" conditions
   *
   * @global object $wpdb
   * @param array|string $arg1
   * @param string $arg2
   * @param string $arg3
   * @param string $type=AND|OR
   * @return $this
   */
  public function where($arg1, $arg2 = null, $arg3 = null, $type = 'AND')
  {
    $conditions = [];

    $operator = '=';

    $num = func_num_args();

    if (is_string($arg2) && preg_grep('/' . preg_quote($arg2, '/') . '/i' , $this->operators)) {
      $operator = $arg2;

      $value = $arg3;
    } else {
      $value = $arg2;
    }

    if ($this->table()) {
      $conditions = $arg1;

      if (is_string($arg1)) {
        $conditions = [$arg1 => $value];
      }

      if (is_array($conditions)) {
        foreach ($conditions as $column => $value) {
          if (is_numeric($column) && is_array($value) && count($value) === 3) {
            $column = $value[0];
            $operator = $value[1];
            $value = $value[2];

            $this->conditions[$type][] = (object) [
              'column' => $column,
              'operator' => $operator,
              'value' => $value
            ];
          } else {
            $this->conditions[$type][] = (object) [
              'column' => $column,
              'operator' => $operator,
              'value' => $value
            ];
          }
        }
      }
    }

    return $this;
  }

  /**
   * Alias for adding a where null condition
   *
   * @param string $column
   * @return $this
   */
  public function whereNull($column)
  {
    return $this->where($column, 'is', null);
  }

  /**
   * Alias for adding a where not null condition
   *
   * @param string $column
   * @return $this
   */
  public function whereNotNull($column)
  {
    return $this->where($column, '!=', null);
  }

  /**
   * Alias for adding a where in condition
   *
   * @param string $column
   * @param array $array
   * @return $this
   */
  public function whereIn($column, $array)
  {
    if (is_array($array)) {
      foreach ($array as &$item) {
        $item = esc_sql($item);
      }

      return $this->where($column, 'IN', $array);
    }

    return $this;
  }

  /**
   * Sets debugging
   *
   * @param boolean [$bool]
   * @return $this
   */
  public function debug($bool = true)
  {
    $this->debug = Utils::getBool($bool);

    return $this;
  }

  /**
   * Sets the limit
   *
   * @param integer $limit
   * @return $this
   */
  public function limit($start, $count = null)
  {
    if (is_numeric($start)) {
      $this->limit = (string) $start;

      if ($count) {
        $this->limit .= ', ' . $count;
      }
    }

    return $this;
  }

  /**
   * Sets the order by
   *
   * @param string $order The column name
   * @param string [$sort]
   * @return $this
   */
  public function orderBy($order, $sort = 'ASC')
  {
    if (is_string($order) || is_numeric($order)) {
      $this->order = $order;

      if (is_string($sort) && preg_match('/^(asc|desc)$/i', $sort)) {
        $this->sort = $sort;
      }
    }

    return $this;
  }

  /**
   * Sets the order by
   *
   * @param string $column The column name
   * @return $this
   */
  public function groupBy($column)
  {
    if (is_string($column)) {
      $this->group = $column;
    }

    return $this;
  }

  /**
   * Gets all the records for a model
   *
   * @return type
   */
  public function all()
  {
    $this->conditions = [];

    return $this->get();
  }

  /**
   * Flags the builder to return records with deleted
   *
   * @return $this
   */
  public function withTrashed()
  {
    $this->deleted = true;

    return $this;
  }

  /**
   * Flags the builder to return only records which were deleted
   *
   * @return $this
   */
  public function onlyTrashed()
  {
    $this->conditions['AND'][] = (object) [
      'column' => 'deleted_at',
      'operator' => '!=',
      'value' => null
    ];

    return $this;
  }

  /**
   * Set the custom sql to run
   *
   * @param type $sql
   * @return $this
   */
  public function query($sql = null)
  {
    if ($sql) {
      $this->sql = $sql;
    }

    return $this;
  }

  /**
   * Builds and gets the query results
   *
   * @return \WSHWordpress\Collections\Collection
   */
  public function get()
  {
    global $wpdb;

    $debug = $this->debug;

    $collection = $this->model->newCollection();

    if ($this->table()) {
      $sql = $this->sql;

      if (is_null($sql)) {
        $sql = "SELECT * FROM {$this->table()}";
      } else {
        $sql = preg_replace('/\$?\{\$?table\}/i', $this->table(), $sql);
        $sql = preg_replace('/\$?\{\$?prefix\}/i', $wpdb->prefix, $sql);
      }

      $prefix = null;
      $where = null;
      $group = null;
      $order = null;
      $limit = null;

      // Remove subqueries
      $sql = $this->removeSubqueries($sql);

      $and = preg_match('/\s+WHERE\s+/i', $sql) ? 'WHERE' : null;

      // Get prefix
      if ($this->sql) {
        $matches = [];
        preg_match_all('/FROM\s+`?' . preg_quote($this->table(), '/') . '`?\s+AS\s+([a-b0-9_-]+)/i', $sql, $matches);

        if (isset($matches[1]) && isset($matches[1][0])) {
          $prefix = $matches[1][0].'.';
        }
      }

      // Deleted At (don't retrieve soft deleted records unless specified in condition)
      $found_deleted_at = false;
      $has_deleted_at = $this->model->hasColumn('deleted_at');

      if ($has_deleted_at && !$this->deleted && !preg_match('/(\s+\w\.)?deleted_at\s+/i', $sql)) {
        foreach ($this->conditions as $type => $conditions) {
          foreach ($conditions as $condition) {
            if ($condition->column === 'deleted_at') {
              $found_deleted_at = true;

              break;
            }
          }
        }

        // @note Disabled old way deleted_at before orWhere was implemented
        if (0 && !$found) {
          $this->conditions['AND'][] = (object) [
            'column' => "{$prefix}deleted_at",
            'operator' => '=',
            'value' => null
          ];
        }
      }

      // Where
      foreach ($this->conditions as $type => $conditions) {
        $conditions = !is_array($conditions) ? [$conditions] : $conditions;

        foreach ($conditions as $condition) {
          $and = !$and ? 'WHERE' : $type;
          $column = $condition->column;

          if (!preg_match('/(\.|\()/', $column)) {
            $column = $prefix . '`' . $column . '`';
          }

          if (is_null($condition->value)) {
            $operator = preg_match('/^(=|is)$/i', $condition->operator) ? 'IS' : 'IS NOT';
            $where .= " {$and} {$column} {$operator} NULL";
          } else {
            $value = $condition->value;

            if (preg_match('/in/i', $condition->operator)) {
              if (is_array($value)) {
                foreach ($value as &$val) {
                  $val = esc_sql($val);
                }

                $value = "'" . implode("','", $value) . "'";
              }

              $where .= " {$and} {$column} {$condition->operator} ({$value})";
            } else {
              if (!preg_match('/like/i', $condition->operator)) {
                $value = esc_sql($condition->value);
              }

              $where .= " {$and} {$column} {$condition->operator} '{$value}'";
            }
          }
        }
      }

      // New way deleted_at
      if ($has_deleted_at && !$found_deleted_at) {
        $and = !$and ? 'WHERE' : 'AND';
        $where .= " {$and} {$prefix}deleted_at IS NULL";
      }

      $sql .= $where;

      // Group By
      if (isset($this->group) && !preg_match('/\s+GROUP\s+BY+\s+/i', $sql)) {
        $group = " GROUP BY {$this->group}";
      }

      $sql .= $group;

      // Order By
      if (isset($this->order) && (preg_match('/\./', $this->order) || property_exists($this->model, $this->order) || is_numeric($this->order))) {
        $column = $this->order;

        if (!is_numeric($column) && !preg_match('/\./', $column)) {
          $column = $prefix . $column;
        }

        $order = " ORDER BY {$column}";

        if (isset($this->sort)) {
          $order .= " {$this->sort}";
        }
      }

      $sql .= $order;

      // Limit
      if (isset($this->limit) && !preg_match('/\s+LIMIT\s+\d+\s*(,\s*\d+\s*)?;?$/i', $sql)) {
        $limit = " LIMIT {$this->limit}";
      }

      $sql .= $limit;

      // Add subqueries back
      $sql = $this->addSubqueries($sql);

      // Debugging
      if ($debug) {
        die(print_r($sql, 1));
      }

      $rows = $this->get_results($sql);

      if ($rows) {
        foreach ($rows as &$row) {
          $row = new $this->class($row);
        }

        $collection = $this->model->newCollection($rows);
      }
    }

    return $collection;
  }

  /**
   * Deletes all returned records
   *
   * @return boolean
   */
  public function delete()
  {
    return $this->get()->delete();
  }

  /**
   * Gets the first entry in the collection
   *
   * @return object
   */
  public function first()
  {
    $collection = $this->get();

    return $collection->first();
  }

  /**
   * Gets the first entry in the collection
   *
   * @return object
   */
  public function last()
  {
    $collection = $this->get();

    return $collection->last();
  }

  /**
   * Gets the total entries in the collection
   *
   * @return integer
   */
  public function count()
  {
    $collection = $this->get();

    return $collection->count();
  }

  /**
   * Find a record by its id
   *
   * @param integer $id
   */
  public function find($id)
  {
    $result = $this->model;

    $this->conditions = [];

    if ($id) {
      $this->conditions['AND'][] = (object) [
        'column' => $this->idColumn(),
        'operator' => '=',
        'value' => $id
      ];

      $row = $this->first();

      if ($row) {
        $result = $row;
      }
    }

    return $result;
  }

  /**
   * Performs pagination against a table
   *
   * @param mixed $params
   * @return mixed
   */
  public function pagination($params)
  {
    $columns = [];
    $search = null;
    $sql = null;
    $sort = null;
    $order = null;
    $all = false;
    $page = 1;
    $count = 30;
    $total = 0;
    $props = false;

    $locals = 'columns, search, sql, sort, order, all, page, count, props';

    $locals = Utils::trimExplode(',', $locals);
    $params = Utils::prepParams($params);

    foreach ($params as $key => $value) {
      if (in_array($key, $locals)) {
        $$key = $value;
      }
    }

    // Define select
    if (!$sql) {
      if ($this->sql) {
        $sql = $this->sql;
      } else {
        $sql = "
          SELECT
            *
          FROM {$this->table()}
        ";
      }
    }

    // Remove subqueries
    $sql = $this->removeSubqueries($sql);

    $sql_total = !$this->group ? preg_replace('/SELECT\s+(.*)\s+FROM\s+/Umis', "SELECT COUNT(*) AS total FROM ", $sql) : $sql;

    $sql = $this->addSubqueries($sql);

    $sql2 = '';

    // Build where statements
    if ($columns && $search) {
      $searches = is_string($search) && strlen($search) ? [$search] : [];
      $searches = apply_filters('wsh_search', $searches, $search);

      if ($searches) {
        $sql2 .= !preg_match('/\s+WHERE\s+/i', $sql) ? " WHERE ( " : " AND ( ";

        foreach ($searches as $search) {
          foreach ($columns as $column) {
            $where = !isset($where) ? '' : 'OR';

            if (!preg_match('/\./', $column)) {
              $column = "`{$column}`";
            }

            $sql2 .= " {$where} {$column} LIKE '%{$search}%'";
          }
        }

        $sql2 .= " ) ";
      }
    }

    $sql .= $sql2;
    $sql_total .= $sql2;

    // Debugging
    if (0) {
      die('<pre>' . print_r($sql, 1) . '</pre>');
    }

    // Get total
    $rows = $this->query($sql_total)->get();

    $total = 0;

    // Handle total differently if grouped results
    if ($this->group) {
      $total = count($rows);
    } else {
      $row = $rows->first();
      $total = $row ? intval($row->total) : $total;
    }

    $builder = $this->query($sql)->orderBy($sort, $order);

    // Add limit
    if (!$all) {
      $start = ($page - 1) * $count;
      $builder = $builder->limit($start, $count);
    }

    // Get rows
    $rows = $builder->get();

    // Cleanup
    $records = $rows->toArray($props);

    foreach ($records as &$record) {
      unset($record->table);
      unset($record->config);
      unset($record->logs);
    }

    // Build response
    $pages = $total && $total >= $count ? ceil($total / $count) : 1;

    $response = (object) [];
    $response->count = intval($count);
    $response->page = intval($page);
    $response->pages = intval($pages);
    $response->total = intval($total);
    $response->records = $records;

    return $response;
  }

  /**
   * Remove subqueries during sql processing
   *
   * @param string $sql
   * @return string
   */
  public function removeSubqueries($sql)
  {
    $i = 0;
    $this->subqueries = [];

    preg_match_all('/\(\s*SELECT\s+.*\)/', $sql, $this->subqueries);

    if ($this->subqueries && $this->subqueries[0]) {
      foreach ($this->subqueries[0] as $subquery) {
        $sql = preg_replace('/' . preg_quote($subquery, '/') . '/', "{\$subqueries[{$i}]}", $sql);
        $i++;
      }
    }

    return $sql;
  }

  /**
   * Add subqueries that were removed during sql processing
   *
   * @param string $sql
   * @return string
   */
  public function addSubqueries($sql)
  {
    $i = 0;

   if ($this->subqueries && $this->subqueries[0]) {
      foreach ($this->subqueries[0] as $subquery) {
        $sql = preg_replace('/' . preg_quote("{\$subqueries[{$i}]}", '/') . '/', $subquery, $sql);
        $i++;
      }
    }

    return $sql;
  }

  /**
   * Builds the sql relationship
   *
   * @param object $fk
   * @return $this
   */
  public function belongsToMany($fk)
  {
    $this->fk = $fk;

    $sql = "
      SELECT
        a.*
      FROM {$this->table()} AS a
      LEFT JOIN {$fk->table} AS b
      ON b.{$fk->other} = a.{$this->idColumn()}
      WHERE
        b.{$fk->column} = '{$fk->value}'
    ";

    return $this->query($sql);
  }

  /**
   * Builds the sql relationship
   *
   * @param object $fk
   * @return $this
   */
  public function columnToMany($fk)
  {
    $this->fk = $fk;
    $column = isset($fk->column) ? $fk->column : null;

    $sql = "
      SELECT
        a.*
      FROM {$this->table()} AS a
    ";

    $where = '';

    if ($column) {
      if (isset($fk->values)) {
        $values = $fk->values;
        $values = is_string($values) ? Utils::trimExplode('/(,|\|)/', $values) : $values;

        if (is_array($values) && $values) {
          $or = '';

          foreach ($values as $value) {
            if ($value) {
              $where .= "{$or} a.{$column} = '{$value}' ";
              $or = 'OR';
            }
          }
        }
      }

      if ($where) {
        $where = "WHERE ({$where}) ";
      } else {
        // Handle empty relationships
        $where = "WHERE (a.{$column} IS NULL AND a.{$column} IS NOT NULL) ";
      }
    }

    $sql .= $where;

    return $this->query($sql);
  }

  /**
   * Builds the sql relationship
   *
   * @param object $fk
   * @return $this
   */
  public function hasOne($fk)
  {
    $row = $this->where($fk->column, $fk->value)->first();

    $row = $row ? $row : $this->model;

    if ($row->isEmpty()) {
      $row->{$fk->column} = $fk->value;
    }

    return $row;
  }

  /**
   * Builds the sql relationship
   *
   * @param object $fk
   * @return $this
   */
  public function hasMany($fk)
  {
    return $this->where($fk->column, $fk->value);
  }

  /**
   * Builds the sql relationship
   *
   * @param object $fk
   * @return $this
   */
  public function belongsTo($fk)
  {
    $row = $this->where($this->idColumn(), $fk->value)->first();

    $row = $row ? $row : $this->model;

    return $row;
  }

  /**
   * Detaches a foreign table association
   *
   * @global \WSHWordpress\Tools\type $wpdb
   * @param integer [$id] If empty, detaches all relationships
   * @return boolean
   */
  public function detach($id = null)
  {
    global $wpdb;

    $bool = false;

    if ($this->fk) {
      $fk = $this->fk;

      // ID(s) 1 or [1, 2]
      $ids = [];

      if (is_string($id)) {
        $ids = Utils::trimExplode(',', $id);
      } elseif (is_numeric($id)) {
        $ids = [$id];
      } elseif (is_array($id)) {
        $ids = $id;
      }

      $params = [
        $fk->column => $fk->value,
      ];

      if ($ids) {
        // Detach relationships by ID
        foreach ($ids as $id) {
          if (is_numeric($id)) {
            if ($id) {
              $params[$fk->other] = $id ;
            }

            if ($wpdb->delete($fk->table, $params)) {
              $bool = true;
            }
          }
        }
      } elseif ($wpdb->delete($fk->table, $params)) {
        // Detach all relationships
        $bool = true;
      }
    }

    return $bool;
  }

  /**
   * Attaches a foreign table association
   *
   * @global \WSHWordpress\Tools\type $wpdb
   * @param integer $id
   * @return boolean
   */
  public function attach($id)
  {
    global $wpdb;

    $bool = false;

    if ($this->fk) {
      $fk = $this->fk;

      // ID(s) 1 or [1, 2]
      $ids = [];

      if (is_string($id)) {
        $ids = Utils::trimExplode(',', $id);
      } elseif (is_numeric($id)) {
        $ids = [$id];
      } elseif (is_array($id)) {
        $ids = $id;
      }

      if ($ids) {
        // Get current relationships
        $in = implode("','", $ids);
        $sql = "SELECT * FROM {$fk->table} WHERE {$fk->column} = '{$fk->value}' AND {$fk->other} IN('{$in}')";
        $rows = $this->get_results($sql);

        foreach ($ids as $id) {
          if (is_numeric($id)) {
            $params = [
              $fk->column => $fk->value,
              $fk->other => $id
            ];

            $found = false;

            // Search current relationships
            if ($rows && is_array($rows)) {
              foreach ($rows as $row) {
                if ($row->{$fk->other} == $id) {
                  $found = true;
                  break;
                }
              }
            }

            // Add relationship if needed
            if ($found) {
              $bool = true;
            } elseif ($wpdb->insert($fk->table, $params)) {
              $bool = true;
            }
          }
        }
      }
    }

    return $bool;
  }

  /**
   * Truncates a table
   *
   * @return boolean
   */
  public function truncate()
  {
    global $wpdb;

    $table = $this->table();

    return $wpdb->query("TRUNCATE TABLE {$table}");
  }

  /**
   * Attempts to retrieve a matching record or return a new instance
   *
   * @param array $params
   * @return object
   */
  public function firstOrNew($params)
  {
    $model = $this::where($params)->first();

    if (!$model) {
      $class = $this->class;
      $model = new $class($params);
    }

    return $model;
  }
}