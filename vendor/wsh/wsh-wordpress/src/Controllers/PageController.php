<?php

namespace WSHWordpress\Controllers;

use WSHWordpress\Utils\UrlUtils;

class PageController extends Controller
{
  public function template(&$template)
  {
    $page = null;
    $context = 'Base';

    $plugin = $this->plugin;

    $uri = $plugin->pageUri;

    $segments = UrlUtils::segments($uri);
    $local = $plugin->config('namespace');

    if ($segments) {
      $context = $segments[0];
    }

    $context = trim($context);
    $context = preg_replace('/[^0-9A-Z]/i', ' ', $context);
    $context = ucwords($context);
    $context = preg_replace('/\s/i', '', $context);

    $pages = ['Base'];

    if (!in_array($context, $pages)) {
      array_unshift($pages, $context);
    }

    foreach ($pages as $context) {
      // Check local
      if (!$page && $local) {
        $class = $local . '\\Pages\\' . $context;

        if (class_exists($class)) {
          $page = new $class($plugin);
        }
      }

      // Check core
      if (!$page) {
        $class = 'WSHWordpress\\Pages\\' . $context;

        if (class_exists($class)) {
          $page = new $class($plugin);
        }
      }
    }

    // Execute API
    if ($page) {
      $method = 'template';

      if (method_exists($page, $method)) {
        global $wp_query, $post;

        if ($wp_query->post_count == 0) {
          header('HTTP/1.0 200 OK');
          $wp_query->is_404 = false;
          $wp_query->is_page = true;
          $post = (object) [];
          $post->post_type = 'page';
        }

        $template = $page->{$method}($template);
      }
    }

    return $template;
  }
}