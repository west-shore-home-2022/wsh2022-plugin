<?php

namespace WSHWordpress\Controllers;

use WSHWordpress\Utils\Utils;
use WSHWordpress\Utils\UrlUtils;
use WSHWordpress\Utils\CacheUtils;
use WSHWordpress\Utils\HttpUtils;
use Firebase\JWT\JWT;
use ReflectionMethod;
use WP_REST_Request;
use Exception;

class ApiController extends Controller
{
  /**
   * Register API endpoints
   */
  public function register()
  {
    $this->ajax = false;
    $plugin = $this->plugin;
    $slug = $plugin->config('slug');

    if ($slug) {
      // URI: /wp-json/{slug}/{api}/{params}
      register_rest_route($slug, '/.*', [
        'methods' => 'GET, POST, PUT, PATCH, DELETE',
        'callback' => [$this, 'route']
      ]);
    }

    // URI: /wp-json/wsh/{api}/{params}
    register_rest_route('wsh', '/.*', [
      'methods' => 'GET, POST, PUT, PATCH, DELETE',
      'callback' => [$this, 'route']
    ]);
  }

  /**
   * Route API requests
   *
   * @param WP_REST_Request $request
   */
  public function route(WP_REST_Request $request)
  {
    $result = 0;
    $status = 200;
    $headers = [
      'Cache' => 0
    ];

    $plugin = $this->plugin;
    $this->request = $request;
    $route = $request->get_route();
    $segments = UrlUtils::segments($route);
    $data = array_merge($_POST, $_GET);

    array_shift($segments);

    if ($segments) {
      $api = null;
      $local = $plugin->config('namespace');
      $paths = array_shift($segments);

      $paths = explode('.', $paths);

      $method = count($paths) > 1 ? array_pop($paths) : null;

      foreach ($paths as &$name) {
        $name = $this->name($name);
      }

      // Check local
      if ($local) {
        $class = $local . '\\Api\\' . implode('\\', $paths);

        if (class_exists($class)) {
          $api = new $class($plugin, $request);
        }

        // Check if the namespace's ending method is really a class
        if (!$api || ($method && !method_exists($api, $method))) {
          $class .= '\\' . $this->name($method);

          if (class_exists($class)) {
            $method = null;
            $api = new $class($plugin, $request);
          }
        }
      }

      // Check core
      if (!$api) {
        $class = 'WSHWordpress\\Api\\' . implode('\\', $paths);

        if (class_exists($class)) {
          $api = new $class($plugin, $request);
        }

        // Check if the namespace's ending method is really a class
        if (!$api || ($method && !method_exists($api, $method))) {
          $class .= '\\' . $this->name($method);

          if (class_exists($class)) {
            $method = null;
            $api = new $class($plugin, $request);
          }
        }
      }

      $namespace = $class;

      // Execute API
      if ($api) {
        $method = !$method || !method_exists($api, $method) ? 'index' : $method;

        if (method_exists($api, $method)) {

          // Defaults
          $auth = true;
          $allowed = true;
          $cache = false;
          $access = 'public';
          $token = false;
          $keys = [];
          $nocache = CacheUtils::nocache();
          $expire =  false;
          $type =  strtoupper($request->get_method());
          $types = [];
          $permissions = [];
          $users = [];
          $nonces = null;
          $header_overrides = null;

          // Get method annotations
          $rm = new ReflectionMethod($namespace, $method);
          $comment = $rm->getDocComment();
          preg_match_all('#@(.*?)\n#s', $comment, $annotations);

          // Process annotations
          if ($annotations && isset($annotations[1])) {
            $annotations = $annotations[1];

            foreach ($annotations as $value) {
              $values = Utils::trimExplode('/\s?,?\s+/', trim($value));
              $value =  strtoupper(array_shift($values));

              // @get, @post, @put, etc.
              if (preg_match('/^(GET|POST|OPTIONS|PUT|PATCH|DELETE|ANY)$/', $value)) {
                $types[] = $value;
              }

              // @method post, @methods get, post, etc.
              if ($values && preg_match('/^(METHOD(S)?)$/', $value)) {
                foreach ($values as $val) {
                  $val = strtoupper(preg_replace('/,/', '', $val));

                  if (preg_match('/^(GET|POST|OPTIONS|PUT|PATCH|DELETE|ANY)$/', $val)) {
                    $types[] = $val;
                  }
                }
              }

              // @permissions admin, @permissions admin editor
              if ($values && preg_match('/^(PERMISSION(S)?)$/', $value)) {
                $permissions = $values;
              }

              // @users any, @users admin
              if ($values && preg_match('/^(USER(S)?)$/', $value)) {
                $users = $values;
              }

              // @nonce, @nonce update
              if (preg_match('/^(NONCE(S)?)$/', $value)) {
                $nonces = $values ? $values : true;
              }
              
              // @headers
              if (preg_match('/^(HEADER(S)?)$/', $value)) {
                $header_overrides = $values;
              }

              // @access public, @access private
              if ($values && preg_match('/^(ACCESS)$/', $value)) {
                foreach ($values as $val) {
                  $val = strtoupper(preg_replace('/,/', '', $val));

                  if (preg_match('/^(PUBLIC|PROTECTED|PRIVATE)$/', $val)) {
                    $access = strtolower($val);

                    break;
                  }
                }
              }

              // @public, @protected, @private
              if (preg_match('/^(PUBLIC|PROTECTED|PRIVATE)$/', $value)) {
                $access = strtolower($value);
              }

              // @key
              if ($values && preg_match('/^(KEY)$/', $value)) {
                $keys = $values;
              }

              // @token
              if (preg_match('/^(TOKEN)$/', $value)) {
                $token = true;
              }

              // @cache 1 minute, @cache 5 minutes * 5, @cache (60 * 7) * 2, etc.
              if (preg_match('/^(CACHE)$/', $value)) {
                $cache = true;
                $expire = implode(' ', $values);

                // Seconds
                $expire = preg_replace_callback(
                  '/(\d+)\s*(seconds|second|secs|sec)/i',
                  function ($matches) {
                    if (isset($matches[1]) && is_numeric($matches[1])) {
                      return $matches[1];
                    }
                  },
                  $expire
                );

                // Minutes
                $expire = preg_replace_callback(
                  '/(\d+)\s*(minutes|minute|mins|min)/i',
                  function ($matches) {
                    if (isset($matches[1]) && is_numeric($matches[1])) {
                      return $matches[1] * 60;
                    }
                  },
                  $expire
                );

                // Hours
                $expire = preg_replace_callback(
                  '/(\d+)\s*(hours|hour|hrs|hr)/i',
                  function ($matches) {
                    if (isset($matches[1]) && is_numeric($matches[1])) {
                      return ($matches[1] * 60) * 60;
                    }
                  },
                  $expire
                );

                $expire = preg_replace('/[^0-9\*\+\-\/\(\)]/', '', $expire);

                eval('$expire = ' . $expire . ';');
              }
            }
          }

          // 1) Validate allowed HTTP methods
          if (!$this->hasErrors() && $types) {
            $types = array_unique($types);

            $allowed = false;

            if (in_array($type, $types) || in_array('ANY', $types)) {
              $allowed = true;
            }

            if (!$allowed) {
              $status = 405;
              $this->error("Method not allowed ({$type})");
            }
          }

          if (!$this->hasErrors()) {
            // 2) Validate key or token
            if (!$this->hasErrors() && ($access === 'protected' || $keys || $token)) {
              $auth = false;

              // Validate by key
              if (isset($data['key'])) {
                $key = $data['key'];

                // Validate key against any keys set in the API method's PHPDoc
                if (!$auth) {
                  if ($keys && in_array($key, $keys)) {
                    $auth = true;
                  }
                }

                // Authorization by class keys
                if (!$auth) {
                  $keys = $api->getKeys();

                  if ($keys && in_array($key, $keys)) {
                    $auth = true;
                  }
                }

                // Custom api key validation
                if (!$auth) {
                  $auth = $api->validateKey($key);
                }
              }

              // Validate token based authorization requests
              if (!$auth && $token) {
                $token_key = $plugin->config('token.key');
                $token = HttpUtils::token();

                // Validate token key set up
                if (!$this->hasErrors() && !$token_key) {
                  $status = 401;
                  $this->error('Missing authorization token key');
                }

                // Validate request token
                if (!$this->hasErrors() && !$token) {
                  $status = 401;
                  $this->error('Invalid authorization token');
                }

                if (!$this->hasErrors()) {

                  // Validate & decode JWT token
                  // @link https://packagist.org/packages/firebase/php-jwt
                  try {
                    $decoded = JWT::decode($token, $token_key, ['HS256']);

                    if (is_object($decoded) && isset($decoded->data)) {
                      $auth = true;
                    }
                  } catch (Exception $ex) {
                    $status = 401;

                    $code = $ex->getCode();
                    $message = $ex->getMessage();

                    if (is_string($message) && strlen($message)) {
                      $this->error($message);
                    }

                    if (is_numeric($code) && $code > 0) {
                      $this->error('Error exception code ' . $code);
                    }

                    $this->error('Invalid authorization token');
                  }
                }
              }

              if (!$auth && !$this->hasErrors()) {
                $status = 401;
                $this->error('1) Unauthorized API request');
              }
            }

            // 3) Validate user
            if (!$this->hasErrors() && ($users || $permissions)) {
              $user = $plugin->user();

              // No user logged in
              if (!$user) {
                $status = 401;
                $this->error('2) Unauthorized API request');
              }

              // Validate authorized users
              if (!$this->hasErrors() && $users) {
                if (!preg_grep('/^(any)$/i', $users)) {
                  $found = false;

                  foreach ($users as $username) {
                    $pattern = '/^(' . preg_quote($username, '/') . ')$/i';

                    if (preg_match($pattern, $user->username)) {
                      $found = true;
                      break;
                    }
                  }

                  // No matching user found
                  if (!$found) {
                    $status = 401;
                    $this->error('3) Unauthorized API request');
                  }
                }
              }

              // Validate user permissions
              if (!$this->hasErrors() && $permissions) {
                $found = false;
                $roles = $user->roles;

                $lookups = [
                  'administrator' => 'administrator,admin',
                  'editor' => 'editor,edit',
                  'author' => 'author',
                  'contributor' => 'contributor',
                  'subscriber' => 'subscriber'
                ];

                foreach ($permissions as $permission) {
                  foreach ($lookups as $role => $patterns) {
                    $patterns = Utils::trimExplode(',', $patterns);

                    if ($patterns) {
                      $pattern = '/^(' . implode('|', $patterns) . ')$/i';

                      if (preg_match($pattern, $permission) && in_array($role, $roles)) {
                        $found = true;
                        break;
                      }
                    }
                  }
                }

                // No matching user permissions found
                if (!$found) {
                  $status = 401;
                  $this->error('4) Unauthorized API request');
                }
              }
            }

            // 4) Validate nonce
            if (!$this->hasErrors() && !is_null($nonces)) {
              if (is_bool($nonces)) {
                if (!$this->nonce()) {
                  $status = 401;
                  $this->error('Expired page session');
                }
              } elseif (is_array($nonces)) {
                $found = false;

                foreach ($nonces as $action) {
                  $action = preg_match('/^(any)$/i', $action) ? 'wp_rest' : $action;

                  if ($this->nonce($action)) {
                    $found = true;
                    break;
                  }
                }

                // No matching user nonces found
                if (!$found) {
                  $status = 401;
                  $this->error('Expired page session');
                }
              }
            }

            // 5) Validate a global token override
            if ($this->hasErrors() && $plugin->config('token.global')) {

              // Respond to CORS preflights
              // @link https://www.dinochiesa.net/?p=754
              // @link https://nitstorm.github.io/blog/making-cross-domain-ajax-calls-with-php/
              if (preg_match('/OPTIONS/i', $_SERVER['REQUEST_METHOD'])) {

                // Return only the headers and not the content
                // Only allow CORS if we're doing a GET - i.e. no saving for now
                if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])
                    && preg_match('/GET/i', $_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])
                    && isset($_SERVER['HTTP_ORIGIN'])
                    && $this->isApproved($_SERVER['HTTP_ORIGIN'])) {

                  header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                  header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
                }

                die();
              }

              $status = 200;
              $this->clearLogs('error');

              // Validate token based authorization requests globally
              $token_key = $plugin->config('token.key');
              $token = HttpUtils::token();

              // Validate token key set up
              if (!$this->hasErrors() && !$token_key) {
                $status = 401;
                $this->error('Missing authorization token key');
              }

              // Validate request token
              if (!$this->hasErrors() && !$token) {
                $status = 401;
                $this->error('Invalid authorization token');
              }

              if (!$this->hasErrors()) {

                // Validate & decode JWT token
                // @link https://packagist.org/packages/firebase/php-jwt
                try {
                  $decoded = JWT::decode($token, $token_key, ['HS256']);

                  if (is_object($decoded) && isset($decoded->data)) {
                    $origins = $plugin->config('token.origins', []);

                    if ($origins) {
                      if (!isset($_SERVER['HTTP_ORIGIN'])) {
                        // HTTP Origin required
                        throw new Exception('', 1);
                      } else if (!$this->isApproved($_SERVER['HTTP_ORIGIN'])) {
                        // HTTP Origin not approved
                        throw new Exception('', 2);
                      }
                    }

                    if (isset($_SERVER['HTTP_ORIGIN'])) {
                      header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
                      header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
                    }
                  } else {
                    // Unable to parse token
                    throw new Exception('', 3);
                  }
                } catch (Exception $ex) {
                  $status = 401;

                  $code = $ex->getCode();
                  $message = $ex->getMessage();

                  if (is_string($message) && strlen($message)) {
                    $this->error($message);
                  }

                  if (is_numeric($code) && $code > 0) {
                    $this->error('Error exception code ' . $code);
                  }

                  $this->error('Invalid authorization token');
                }
              }
            }
          }

          // 6) Validate private
          if (!$this->hasErrors() && $access === 'private') {
            $status = 401;
            $this->error('5) Unauthorized API request');
          }

          if (!$this->hasErrors()) {
            // Retrieve cache
            if ($cache) {
              // Generate unique cache key
              $key = $request->get_method() . '|' . $request->get_route() . '|' . serialize([$_POST, $_GET]);
              $key = md5($key);

              // Process caching
              if (!$nocache) {
                $result = get_transient($key);

                $result = $this->isJsonString($result) ? json_decode(json_encode($result)) : $result;

                if ($result) {
                  $headers['Cache'] = 1;
                } else {
                  // Emit API
                  $api->ajax = false;
                  $result = $api->{$method}();

                  // Check for errors
                  if (is_object($result) && isset($result->errors) && $result->errors) {
                    // Delete cache
                    delete_transient($key);
                  } else {
                    // Save cache
                    $value = is_string($result) ? $result : json_encode($result);
                    set_transient($key, $value, $expire);
                  }
                }

                // Check for errors
                if (is_object($result) && isset($result->errors) && $result->errors) {
                  // Delete cache
                  delete_transient($key);
                }
              } else {
                // Delete cache
                delete_transient($key);

                // Emit API
                $api->ajax = false;
                $result = $api->{$method}();

                // Check for errors
                if (is_object($result) && isset($result->errors) && $result->errors) {
                  // Delete cache
                  delete_transient($key);
                } else {
                  // Save cache
                  $value = is_string($result) ? $result : json_encode($result);
                  set_transient($key, $value, $expire);
                }
              }
            }

            if (!$result) {
              $api->ajax = false;
              $result = $api->{$method}();
            }
          }
        } else {
          $status = 404;
          $this->error('API endpoint not found');
        }
      } else {
        $status = 404;
        $this->error('API resource not found');
      }
    }
    
    // Handle errors
    if (!$result) {
      if (!$this->hasErrors()) {
        $status = 400;
        $this->error('API request error');
      }

      $result = $this->response();
    } else {
      if (isset($result->errors) && $result->errors) {
        $status = 200;
      }
    }
    
    // Prep response
    $response = !is_string($result) ? json_encode($result) : $result;

    // Apply callbacks
    $callback = $plugin->value('callback');
    $response = $callback ? "{$callback}({$response})" : $response;

    // Set the HTTP response code header
    http_response_code($status);

    // Apply header overrides
    if ($header_overrides) {
      foreach ($header_overrides as $header) {
        $header = explode(':', $header);
        
        if (count($header) === 2) {
          $name = $header[0];
          $value = urldecode($header[1]);
          $headers[$name] = $value;
        }
      }
    }
    
    // Set additional HTTP response headers
    foreach ($headers as $key => $value) {
      header("{$key}: {$value}");
    }

    // Output response
    die($response);
  }
  
  public function isJsonString($value)
  {
    return is_string($value) && (
              (preg_match('/^\[/', $value) && preg_match('/\]$/', $value))
              || 
              (preg_match('/^\{/', $value) && preg_match('/\}$/', $value))
            );
  }

  public function isApproved($incoming)
  {
    $plugin = $this->plugin;

    $origins = $plugin->config('token.origins', []);

    if (!$origins) {
      return true;
    }

    $approved = false;

    foreach ($origins as $origin) {
      $pattern = '/' . preg_quote($origin, '/') . '/i';

      if (preg_match($pattern, $incoming)) {
        $approved = true;

        break;
      }
    }

    return $approved;
  }

  public function name($name)
  {
    $name = trim($name);
    $name = preg_replace('/(_|-)/', ' ', $name);
    $name = ucwords($name);
    $name = preg_replace('/\s/', '', $name);

    return $name;
  }
}