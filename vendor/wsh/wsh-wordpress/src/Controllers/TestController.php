<?php

namespace WSHWordpress\Controllers;

use ReflectionClass;

/**
 * The test controller
 *
 * @link http://localhost/wp-admin/?page=PLUGIN_SLUG-test
 * @link http://localhost/wp-admin/?page=PLUGIN_SLUG&controller=test
 * @author Ed Rodriguez
 */
class TestController extends Controller
{
  /**
   * The test controller
   *
   * @return string
   */
  public function index()
  {
    // Scope variables
    $wpdb = $this->wpdb;
    $plugin = $this->plugin;
    $user = $plugin->user;

    if (is_admin() && $user && $user->is('admin')) {

      $classes = $plugin->classes('Tests');

      $output = '<div class="cms bootstrap-wrapper" style="margin:30px 0;">';

      // Loop tests
      foreach ($classes as $class) {
        if (!isset($class::$active) || (isset($class::$active) && $class::$active)) {
          $obj = null;
          $reflection = new ReflectionClass($class);
          $methods = $reflection->getMethods();

          // Loop methods
          foreach ($methods as $method) {
            if (preg_match('/^test/i', $method->name)) {
              if (!$obj) {
                $output .= "<h1>{$class}</h1>";
                $obj = new $class($plugin);
              }

              // Trigger test
              ob_start();

              $start = date('Y-m-d g:i:s a');
              $obj->{$method->name}();
              $end = date('Y-m-d g:i:s a');

              $test = ob_get_contents();

              ob_end_clean();

              // Build output
              $output .= "
                <p>
                  <strong>Method:</strong> {$method->name}<br />
                  <strong>Start:</strong> {$start}<br />
                  <strong>End:</strong> {$end}<br />
                </p>
                {$test}<br /><br />
              ";
            }
          }
        }
      }

      $output .= '</div>';

      return $output;
    }

    $this->view = $plugin->view('unauthorized');

    return $this->view->render();
  }
}
