<?php

namespace WSHWordpress\Controllers;

/**
 * Wrapper: The tests controller
 *
 * @link http://localhost/wp-admin/?page=PLUGIN_SLUG-tests
 * @link http://localhost/wp-admin/?page=PLUGIN_SLUG&controller=tests
 * @author Ed Rodriguez
 */
class TestsController extends Controller
{
  /**
   * The tests controller
   *
   * @return string
   */
  public function index()
  {
    return (new TestController($this->plugin))->index();
  }
}
