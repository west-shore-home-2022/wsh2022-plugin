<?php

namespace WSHWordpress\Controllers;

use WSHWordpress\Traits\Logger;

/**
 * Shared controller base
 */
abstract class Controller
{
  use Logger;

  protected $plugin;
  protected $wpdb;
  protected $api = '';

  /**
   * Create the controller
   *
   * @param object $plugin
   */
  public function __construct($plugin)
  {
    global $wpdb;

    $this->plugin = $plugin;
    $this->wpdb = $wpdb;

    $this->api = $plugin->config('api.url', $this->api);
  }

  /**
   * Checks if the user is authorized
   *
   * @return boolean
   */
  public function auth()
  {
    return current_user_can('editor') || current_user_can('administrator');
  }

  /**
   * Checks and determines if a record has been updated
   *
   * @param object $row The record to check
   * @param array $values The new values
   * @return boolean
   */
  public function hasUpdates($row, $values)
  {
    $bool = false;

    foreach ($values as $prop => $value) {
      if ($row->{$prop} != $value) {
        $bool = true;
        break;
      }
    }

    return $bool;
  }

  /**
   * Checks if a nonce is valid
   *
   * @param type $action The nonce action
   * @return boolean
   */
  public function nonce($action = 'wp_rest')
  {
    $bool = false;

    $nonce = null;
    $nonce = isset($_REQUEST['_wpnonce']) ? $_REQUEST['_wpnonce'] : $nonce;
    $nonce = !$nonce && $this->request ? $this->request->get_header('X-WP-Nonce') : $nonce;

    if ($nonce) {
      $bool = wp_verify_nonce($nonce, $action);
    }

    return $bool;
  }
  
  /**
   * The leads page
   *
   * @return string
   */
  public function leads()
  {
    $plugin = $this->plugin;

    $plugin->table('wsh_leads', $this);

    $plugin->assets([
      'assets/vue/Leads.min.js'
    ]);

    $this->view = $plugin->view('leads');
    $this->view->tab = 'leads';
    
    return $this->view->render();
  }

  /**
   * The sources page
   *
   * @return string
   */
  public function sources()
  {
    $plugin = $this->plugin;

    $plugin->table('wsh_sources', $this);

    $plugin->assets([
      'assets/vue/Sources.min.js'
    ]);

    $this->view = $plugin->view('sources');
    $this->view->tab = 'sources';
    
    return $this->view->render();
  }

  /**
   * The tools page
   *
   * @return string
   */
  public function tools()
  {
    $plugin = $this->plugin;

    $plugin->assets([
      'assets/vue/Tools.min.js'
    ]);

    $this->view = $plugin->view('tools');
    $this->view->tab = 'tools';
    $this->view->settings = $plugin->settings->get();
    
    if (isset($this->view->settings->pagespeed) && isset($this->view->settings->pagespeed->search_replace)) {
      $this->view->search_replace = json_encode($this->view->settings->pagespeed->search_replace);
    } else {
      $this->view->search_replace = '[]';
    }

    return $this->view->render();
  }
  
  /**
   * The assets page
   *
   * @return string
   */
  public function assets()
  {
    $plugin = $this->plugin;

    $plugin->assets(['assets/vue/Assets.min.js']);

    $this->view = $plugin->view('assets');
    $this->view->tab = 'assets';

    return $this->view->render();
  }
  
  /**
   * The ips page
   *
   * @return string
   */
  public function ips()
  {
    $plugin = $this->plugin;

    $plugin->table('wsh_ips', $this);
    
    $plugin->assets(['assets/vue/Ips.min.js']);

    $this->view = $plugin->view('ips');
    $this->view->tab = 'ips';

    return $this->view->render();
  }
  
  /**
   * The tracking page
   *
   * @return string
   */
  public function tracking()
  {
    $plugin = $this->plugin;

    $plugin->assets(['assets/vue/Tracking.min.js']);

    $this->view = $plugin->view('tracking');
    $this->view->tab = 'tracking';

    return $this->view->render();
  }

  /**
   * The zips page
   *
   * @return string
   */
  public function zips()
  {
    $plugin = $this->plugin;

    $plugin->table('wsh_zips', $this);

    $plugin->assets(['assets/vue/Zips.min.js']);

    $this->view = $plugin->view('zips');
    $this->view->tab = 'zips';

    return $this->view->render();
  }
}