<?php

namespace WSHWordpress\Templates;

/**
 * A Template is a wrapper for a PHP file.
 */
class PhpTemplate
{
  /**
   * The PHP template file.
   *
   * @var string
   */
  protected $file = null;

  /**
   * The template data, all of which is passed to the PHP template.
   *
   * @var string[]
   */
  protected $data = [];

  /**
   * Create a new template
   *
   * @param string $file The name of the file to render.
   */
  public function __construct($file)
  {
    $this->file = $file;
  }

  /**
   * Magically set a template variable
   *
   * @param string $name  The name of the variable.
   * @param mixed  $value The value of the variable.
   */
  public function __set($name, $value)
  {
    $this->data[$name] = $value;
  }

  /**
   * Find out whether a given item of template data is set.
   *
   * @param string $name The property name.
   * @return boolean
   */
  public function __isset($name)
  {
    return isset($this->data[$name]);
  }

  /**
   * Get an item from this template's data.
   *
   * @param string $name The name of the template variable.
   * @return mixed
   */
  public function __get($name)
  {
    return $this->data[$name];
  }

  /**
   * Render the template and output it.
   *
   * @return void
   */
  public function __toString()
  {
    echo $this->render();
  }

  /**
   * Render the template and return the output.
   *
   * @return string
   */
  public function render()
  {
    $output = '';
    
    if (file_exists($this->file)) {
      ob_start();

      // Initialize template variables
      foreach ($this->data as $_name => $_value) {
        $$_name = $_value;
      }

      include($this->file);

      $output = ob_get_contents();

      ob_end_clean();
    }

    return $output;
  }
}