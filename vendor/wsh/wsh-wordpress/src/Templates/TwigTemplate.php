<?php

namespace WSHWordpress\Templates;

use WSHWordpress\Utils\Utils;
use WSHWordpress\Utils\CryptUtils;
use WSHWordpress\Utils\FileUtils;

/**
 * A Template is a wrapper for a Twig file
 */
class TwigTemplate
{
  protected $_twig = null;

  /**
   * The name of the template to render (if not using a Twig string).
   *
   * @var string
   */
  protected $template_name;

  /**
   * The Twig string to render (if not using a template file).
   *
   * @var string
   */
  protected $template_string;

  /**
   * The template data, all of which is passed to the Twig template.
   *
   * @var string[]
   */
  protected $data;

  /**
   * Paths at which to find templates.
   *
   * @var string[]
   */
  protected static $paths = array();

  /**
   * Create a new template either with a file-based Twig template, or a Twig string.
   *
   * @global type $wpdb
   * @param string|false $template_name   The name of a Twig file to render.
   * @param string|false $template_string A Twig string to render.
   */
  public function __construct($template_name = false, $template_string = false)
  {
    $this->template_name = $template_name;
    $this->template_string = $template_string;
    $this->data = [];
  }

  /**
   * Add a filesystem path under which to look for template files.
   *
   * @param string $new_path The path to add.
   */
  public static function add_path($new_path)
  {
    $path = realpath($new_path);

    if (!in_array($path, self::$paths, true)) {
      self::$paths[] = $path;
    }
  }

  /**
   * Get a list of the filesystem paths searched for template files.
   *
   * @return string[] An array of paths
   */
  public static function get_paths()
  {
    return self::$paths;
  }

  /**
   * Get a list of templates in a given directory, across all registered template paths.
   *
   * @param string $directory The directory to search in.
   */
  public function get_templates($directory)
  {
    $templates = array();

    foreach (self::$paths as $path) {
      $dir = $path . '/' . ltrim($directory, '/');

      foreach (preg_grep('/^[^\.].*\.(twig|html)$/', scandir($dir)) as $file) {
        $templates[] = $directory . '/' . $file;
      }
    }

    return $templates;
  }

  /**
   * Magically set a template variable.
   *
   * @param string $name  The name of the variable.
   * @param mixed  $value The value of the variable.
   */
  public function __set($name, $value)
  {
    $this->data[$name] = $value;
  }

  /**
   * Find out whether a given item of template data is set.
   *
   * @param string $name The property name.
   * @return boolean
   */
  public function __isset($name)
  {
    return isset($this->data[$name]);
  }

  /**
   * Get an item from this template's data.
   *
   * @param string $name The name of the template variable.
   * @return mixed
   */
  public function __get($name)
  {
    return $this->data[$name];
  }

  /**
   * Render the template and output it.
   *
   * @return void
   */
  public function __toString()
  {
    echo $this->render();
  }

  /**
   * Set and get twig
   *
   * @param object $twig
   * @return object
   */
  public function twig($twig = null)
  {
    if ($twig) {
      $this->_twig = $twig;
    } else {
      if (!$this->_twig) {
        $loader = new \Twig_Loader_Filesystem(self::$paths);
        $this->_twig = new \Twig_Environment($loader);
      }
    }

    $this->_twig->addGlobal('_cookie', $_COOKIE);
    $this->_twig->addGlobal('_session', $_SESSION);
    $this->_twig->addGlobal('_post', $_POST);
    $this->_twig->addGlobal('_get', $_GET);

    return $this->_twig;
  }

  /**
   * Render the template and return the output.
   *
   * @return string
   */
  public function render()
  {
    $twig = $this->twig();

    $twig->addExtension(new \Twig_Extension_StringLoader());

    // Add some useful functions to Twig.
    $funcs = array('site_url', 'admin_url', '__', '_e', 'wp_create_nonce');

    foreach ($funcs as $f) {
      $twig->addFunction($f, new \Twig_SimpleFunction($f, $f));
    }

    // Handle wp_nonce_field() differently in order to default it to returning the string.
    $wp_nonce_field = new \Twig_SimpleFunction('wp_nonce_field', function($action = -1, $name = "_wpnonce", $referer = true, $echo = false) {
      return wp_nonce_field($action, $name, $referer, $echo);
    });

    // Handle nonce
    $nonce = new \Twig_SimpleFunction('nonce', function($action = -1) {
      if ($action === 'wp_rest') {
        return wp_create_nonce('wp_rest');
      }

      return wp_create_nonce($action . '-' . get_current_user_id());
    });

    $twig->addFunction($nonce);

    // Handle ini_set
    $in_set = new \Twig_SimpleFunction('in_set', function($values, $value) {
      $found = false;

      if (is_string($values)) {
        $values = explode(',', $values);
      }

      if (is_array($values)) {
        foreach ($values as $val) {
          if (preg_match('/^' . print_r($value, 1) . '$/i', $val)) {
            $found = true;
            break;
          }
        }
      }

      return $found;
    });

    $twig->addFunction($in_set);

    // Check if a column is an associative column type with the suffix _ids
    $twig->addFunction(new \Twig_SimpleFunction('is_type', function ($name, $type) {
      $bool = false;

      if (is_string($name) && is_string($type)) {
        if (preg_match('/^ids$/i', $type)) {
          $bool = preg_match('/_ids$/i', $name);
        }
      }

      return $bool;
    }));

    // Encrypt function
    $twig->addFunction(new \Twig_SimpleFunction('encrypt', function ($str) {
      return CryptUtils::encrypt($str);
    }));

    // Decrypt function
    $twig->addFunction(new \Twig_SimpleFunction('decrypt', function ($str) {
      return CryptUtils::decrypt($str);
    }));

    // Name filter
    $twig->addFilter(new \Twig_SimpleFilter('name', function($str) {
      return Utils::getName($str);
    }));

    // Titlecase Filter
    $filter = new \Twig_SimpleFilter('titlecase', '\\WSHWordpress\\Utils\\Utils::titlecase');
    $twig->addFilter($filter);

    $twig->addFilter(new \Twig_SimpleFilter('get_date_from_gmt', 'get_date_from_gmt'));

    // Strtolower Filter
    $strtolower_filter = new \Twig_SimpleFilter('strtolower', function($str) {
      if (is_array($str)) {
        return array_map('strtolower', $str);
      } else {
        return strtolower($str);
      }
    });

    $twig->addFilter($strtolower_filter);

    // Commas Filter
    $filter = new \Twig_SimpleFilter('commas', function($str) {
      return number_format($str);
    });

    $twig->addFilter($filter);

    // Crop Filter
    $filter = new \Twig_SimpleFilter('cropstring', function($str, $length = 0, $ellipsis = 1, $wordsOnly = 0, $wordBreaks = 1) {
      return Utils::cropString($str, $length, $ellipsis, $wordsOnly, $wordBreaks);
    });

    $twig->addFilter($filter);

    // Filesize Filter
    $filter = new \Twig_SimpleFilter('filesize', function($size, $decimals = 1) {
      return FileUtils::getFileSize($size, $decimals);
    });

    $twig->addFilter($filter);

    // Rating Filter
    $filter = new \Twig_SimpleFilter('rating', function($rating, $type = 'google', $max = 5) {

      $percent = ($rating * 100) / $max;

      $html = '
        <span class="rating-wrapper" title="' . $rating . '">
          <span class="review-rating-off ' . $type . '"><span style="width:' . $percent . '%" class="review-rating-on ' . $type . '"></span></span>
        </span>
      ';

      return $html;
    }, ['is_safe' => ['html']]);

    $twig->addFilter($filter);

    // Enable debugging.
    if (WP_DEBUG) {
      $twig->enableDebug();
      $twig->addExtension(new \Twig_Extension_Debug());
    }

    // Render the template.
    if (!empty($this->template_string)) {
      $template = $twig->createTemplate($this->template_string);
    } else {
      $template = $twig->loadTemplate($this->template_name);
    }

    return $template->render($this->data);
  }
}