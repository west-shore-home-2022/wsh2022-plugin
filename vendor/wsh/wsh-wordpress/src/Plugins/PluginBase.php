<?php

namespace WSHWordpress\Plugins;

use WSHWordpress\Templates\TwigTemplate;
use WSHWordpress\Templates\PhpTemplate;
use WSHWordpress\Utils\FileUtils;
use WSHWordpress\Utils\CryptUtils;
use WSHWordpress\Utils\PatternUtils;
use WSHWordpress\Components\Tracking;

/**
 * Shared plugin base
 */
class PluginBase
{
  public $_config = null;
  public $root = null;
  public $wpdb = null;
  public static $user_id = null;

  public function __construct()
  {
    global $wpdb;

    $this->wpdb = $wpdb;
  }

  /**
   * Get a configuration value by its property path
   *
   * @param string $path The property path
   * @param mixed $default The default value
   */
  public function config($path = '', $default = null)
  {
    $config = $this->_config;

    return $this->find($path, $default, $config);
  }

  /**
   * Find object values by specified property paths
   *
   * @param string $path
   * @param mixed $default
   * @param object $data
   * @return mixed
   */
  public function find($path = '', $default = null, $data)
  {
    $value = null;

    if (is_array($data)) {
      $data = (object) $data;
    }

    if (is_object($data) && $path) {
      $props = explode('.', $path);

      $found = true;
      $temp = $data;

      foreach ($props as $prop) {
        if (is_object($temp) && isset($temp->{$prop})) {
          $temp = $temp->{$prop};
        } elseif (is_array($temp) && isset($temp[$prop])) {
          $temp = $temp[$prop];
        } else {
          $found = false;
          break;
        }
      }

      if ($found) {
        $value = $temp;
      } else {
        $value = $default;
      }
    } else {
      $value = $data;
    }

    // Set default for null and empty strings
    if (!is_null($default)) {
      $val = is_string($value) ? trim($value) : null;

      if (is_null($value) || (is_string($val) && !strlen($val))) {
        $value = $default;
      }
    }

    return $value;
  }

  /**
   * Parses a string against data
   *
   * @param string $str
   * @param object|array $data1
   * @param object|array [$data2]
   * @param object|array [$data3]
   * @return string
   */
  public function parse($str, $data1, $data2 = [], $data3 = [])
  {
    if (!is_string($str)) {
      return $str;
    }

    $matches = [];

    preg_match_all('/\{\{\s*([a-z0-9_\.]+)\s*\}\}/i', $str, $matches);

    if ($matches && isset($matches[1]) && $matches[1]) {
      $count = 0;

      foreach ($matches[1] as $prop) {
        $pattern = '/' . preg_quote($matches[0][$count], '/') . '/i';
        $value = $this->find($prop, null, $data1);

        if (is_null($value) && $data2) {
          $value = $this->find($prop, null, $data2);
        }

        if (is_null($value) && $data3) {
          $value = $this->find($prop, null, $data3);
        }

        $str = preg_replace($pattern, $value, $str);
        $count++;
      }

      return $this->parse($str, $data1, $data2, $data3);
    }

    return $str;
  }

  public function shortcode($filename)
  {
    return $this->view($filename, 'views/shortcodes/');
  }

  /**
   * Override Method: Customize Twig
   *
   * @param object $twig
   * @return object
   */
  public function twig($twig)
  {
    return null;
  }

  /**
   * Converts a file path to a url
   * 
   * @param string $path
   * @param boolean [$ending_slash=false]
   * @return string
   */
  public function url($path, $ending_slash = false)
  {
    $plugin = $this;
    
    $path = preg_replace('/' . preg_quote(DIRECTORY_SEPARATOR, '/') . '/', '/', $path);

    $docRoot = $_SERVER['DOCUMENT_ROOT'];

    $pattern = '/^' . preg_quote($docRoot, '/') . '/i';

    if (!preg_match($pattern, $path)) {
      $docRoot = realpath($plugin->root . '../../../');
      $pattern = '/^' . preg_quote($docRoot, '/') . '/i';
    }

    $path = preg_replace($pattern, '', $path);

    $path = !preg_match('/^\//', $path) ? '/' . $path : $path;
    
    if ($ending_slash) {
      $path = !preg_match('/\/$/', $path) ? $path . '/' : $path;
    }
    
    return $path;
  }
  
  /**
   * Get and build a specified view
   *
   * @param type $filename
   * @return object
   */
  public function view($filename, $dir = 'views/')
  {
    $plugin = $this;
    $ext = FileUtils::getExt($filename);
    $filename = $ext ? preg_replace('/\.' . $ext . '/i', '', $filename) : $filename;
    $docRoot = $_SERVER['DOCUMENT_ROOT'];

    $extensions = ['php', 'html'];
    $extensions = $ext ? [$ext] : $extensions;

    $paths = [
      'theme' => realpath($plugin->themeRoot) . DIRECTORY_SEPARATOR,
      'plugin' => $plugin->root,
      'vendor' => realpath(dirname(dirname(dirname(__FILE__)))) . DIRECTORY_SEPARATOR
    ];

    $view = null;

    foreach ($extensions as $extension) {
      $file = $filename . '.' . $extension;

      if ($view) {
        break;
      }

      // PHP
      if ($extension === 'php') {
        foreach ($paths as $path) {
          $path =  $path . $dir . $file;

          if (file_exists($path)) {
            $view = new PhpTemplate($path);
          }
        }
      }
      // Twig
      else if ($extension === 'html') {
        $view = new TwigTemplate($dir . $file);

        // Add paths
        foreach ($paths as $name => $path) {
          $view->add_path($path);
        }

        // Customize twig
        $twig = $view->twig();
        $twig = $this->twig($twig);

        if ($twig) {
          $view->twig($twig);
        }
      }
    }

    if ($view) {
      $view->plugin = $plugin;
      $view->config = $plugin->_config;
      $view->siteurl = site_url();
      $view->domain = $_SERVER['HTTP_HOST'];

      // Set common variables
      foreach ($paths as $name => $path) {
        $path = $this->url($path, true);

        $dir = 'assets/';

        if ($name === 'plugin') {
          $view->js = !isset($view->js) ? "{$path}{$dir}js/" : $view->js;
          $view->css = !isset($view->css) ? "{$path}{$dir}css/" : $view->css;
          $view->img = !isset($view->img) ? "{$path}{$dir}img/" : $view->img;
          $view->images = !isset($view->images) ? "{$path}{$dir}images/" : $view->images;
        }

        $suffix = ucwords($name);

        $view->{'js'.$suffix} = !isset($view->{'js'.$suffix}) ? "{$path}{$dir}js/" : $view->{'js'.$suffix};
        $view->{'css'.$suffix} = !isset($view->{'css'.$suffix}) ? "{$path}{$dir}css/" : $view->{'css'.$suffix};
        $view->{'img'.$suffix} = !isset($view->{'img'.$suffix}) ? "{$path}{$dir}img/" : $view->{'img'.$suffix};
        $view->{'images'.$suffix} = !isset($view->{'images'.$suffix}) ? "{$path}{$dir}images/" : $view->{'images'.$suffix};
      }
    }

    return $view;
  }

  /**
   * Checks if a nonce is valid
   *
   * @param type $action The nonce action
   * @return boolean
   */
  public function nonce($action)
  {
    $bool = false;

    if (isset($_REQUEST['_nonce'])) {
      $bool = wp_verify_nonce($_REQUEST['_nonce'], $action . '-' . get_current_user_id());
    }

    return $bool;
  }

  /**
   * Checks if a table exists
   *
   * @param string $table The table name
   * @return boolean
   */
  public function hasTable($table)
  {
    return preg_grep('/^(' . preg_quote($table, '/') . ')$/i', $this->tables());
  }

  /**
   * Gets a list of custom tables
   *
   * @return array
   */
  public function tables()
  {
    $prefix = 'wsh_';
    $tables = Plugin::$config->tables;

    if (!$tables) {
      $results = $this->wpdb->get_results("SHOW TABLES LIKE '{$prefix}%'");

      foreach ($results as $result) {
        $vars = array_values(get_object_vars($result));
        $tables[] = $vars[0];
      }

      Plugin::$config->tables = $tables;
    }

    return $tables;
  }

  /**
   * Gets and prepares SQL statements
   *
   * @param string $filename The sql filename
   * @param array $values
   * @return string
   */
  public function sql($filename, $values = [])
  {
    $sql = null;

    $filename .= !preg_match('/\.sql$/i', $filename) ? '.sql' : '';

    // Local SQL
    $file = $this->sqlRoot . $filename;

    // Global SQL
    if (!file_exists($file)) {
      $file = $this->sharedRoot . 'assets' . DS . 'sql' . DS . $filename;
    }

    if (file_exists($file)) {
      $sql = file_get_contents($file);

      if ($values) {
        $sql = $this->wpdb->prepare($sql, $values);
      }
    }

    //die('<pre>' . $sql);

    return $sql;
  }

  /**
   * Checks for a tables and attempts to create it if it doesn't exist
   *
   * @param string $table The table name
   * @param object $scope The scope of the caller
   * @return bool
   */
  public function table($table, $scope = null)
  {
    $errors = [];
    $bool = $this->hasTable($table);

    // Attempt to create table
    if (!$bool) {
      $sql = $this->sql("{$table}-create");

      if ($sql) {
        if (!$this->wpdb->query($sql)) {
          $errors[] = "Unable to create table {$table}";
        }
      } else {
        $errors[] = "Unable to load sql for table {$table}";
      }
    }

    // Update view logs
    if (!$bool && isset($this->view) && $this->view) {
      $response = $this->json('', 'ajax', 0);
      $json = json_encode($response);

      $this->view->logs = $json;
    }

    // Merge errors with scope
    if ($errors && $scope && method_exists($scope, 'error')) {
      foreach ($errors as $error) {
        $scope->error($error);
      }
    }

    return $bool;
  }

  /**
   * Returns a get or post value
   *
   * @param string $name
   * @param mixed [$default]
   * @param mixed [$data]
   * @return type
   */
  public function value($name, $default = null, $data = null)
  {
    $data = !is_null($data) ? $data : array_merge($_GET, $_POST);

    $value = $this->find($name, $default, $data);

    if (is_string($value)) {
      $value = trim($value);
    }

    if (is_null($value) || (is_string($value) && !strlen($value))) {
      $value = $default;
    }

    return $value;
  }

  /**
   * Get the current WordPress user
   *
   * @return object
   */
  public function user()
  {
    $class = null;
    $namespace = $this->config('namespace');

    if ($namespace) {
      $class = $namespace . '\\Models\\User';
    }

    if (!$class || !class_exists($class)) {
      $class = '\\WSHWordpress\\Models\\User';
    }

    $user = null;
    $id = self::$user_id ? self::$user_id : get_current_user_id();

    if ($id) {
      $row = new $class($id);

      if (!$row->isEmpty()) {
        $user = $row;
      }
    }

    return $user;
  }

  /**
   * Check the frontend and backend for a template
   *
   * @param type $filename
   * @return type
   */
  public function path($filename, $uri = false)
  {
    $plugin = $this;
    $path = null;

    // Default Extension (php)
    $filename .= !preg_match('/\.[a-z]+$/', $filename) ? '.php' : '';

    // Frontend
    $file = $plugin->themeRoot . $filename;

    if (file_exists($file)) {
      $path = $file;
    }

    // Backend
    if (!$path) {
      $file = $plugin->pluginRoot . $filename;

      if (file_exists($file)) {
        $path = $file;
      }
    }

    // Shared
    if (!$path) {
      $file = $plugin->sharedRoot . $filename;

      if (file_exists($file)) {
        $path = $file;
      }
    }

    $path = $path ? preg_replace('/(\/|\\\)/', DS, $path) : $path;

    // Return the web absolute uri
    if ($path && $uri) {
      $path = preg_replace('/^' . preg_quote($plugin->siteRoot, '/') . '/i', '/', $path);
      $path = preg_replace('/' . preg_quote(DS, '/') . '+/', '/', $path);
    }

    return $path;
  }

  /**
   * Encrypt params
   *
   * @param string $params
   * @return string
   */
  public function encrypt($params)
  {
    return CryptUtils::encrypt($params);
  }

  /**
   * Builds dynamic properties for daisy chaining
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name)
  {
    if (in_array($name, ['user', 'company']) || method_exists($this, $name)) {
      return $this->getValue($name);
    }

    return null;
  }

  public function getValue($name)
  {
    $var = '_' . $name;

    $value = (isset($this->{$var})) ? $this->{$var} : method_exists($this, $name) ? $this->{$name}() : null;

    if (is_object($value) && $value instanceof QueryBuilder) {
      $value = $value->get();
    }

    return $value;
  }

  /**
   * Protects an array
   *
   * @param array $array
   * @param [integer] $show The length to protect
   * @param [array] $keys The optional keys to protect otherwise protect all keys
   * @return array
   */
  public function protectArray($array, $show = 4, $keys = [])
  {
    $data = [];

    if (isset($array) && is_array($array)) {
      foreach ($array as $key => $value) {
        if (!$keys) {
          $data[$key] = $this->protectString($value, $show);
        } else if (in_array($key, $keys)) {
          $data[$key] = $this->protectString($value, $show);
        } else {
          $data[$key] = $value;
        }
      }
    }

    return $data;
  }

  /**
   * Protects a string by only showing a portion of the string
   *
   * @param string $str The string to protect
   * @param integer $show The amount to show
   * @return string
   */
  function protectString($str, $show = 4)
  {
    if (!is_string($str)) {
      return $str;
    }
    
    $length = strlen($str);
    
    if ($length) {
      $hint = substr($str, 0, $show);
      
      if ($length >= $show) {
        $hint .= str_repeat('-', $length - $show);
      }
      
      return $hint;
    }

    return $str;
  }

  /**
   * Logs an info message
   *
   * @param mixed $msg
   * @return boolean
   */
  public function info($msg)
  {
    return $this->log($msg, 'Info');
  }

  /**
   * Logs an success message
   *
   * @param mixed $msg
   * @return boolean
   */
  public function success($msg)
  {
    return $this->log($msg, 'Success');
  }

  /**
   * Logs an error message
   *
   * @param mixed $msg
   * @return boolean
   */
  public function error($msg)
  {
    return $this->log($msg, 'Error');
  }

  /**
   * Logs a message to the plugin log file
   *
   * @param mixed $msg
   * @param string $type
   * @param boolean $request
   * @return boolean
   */
  public function log($msg, $type = 'Info', $request = false)
  {
    $success = false;
    $msg = !is_string($msg) ? print_r($msg, 1) : $msg;

    $plugin = $this;

    $slug = $plugin->config('slug');

    if ($slug && $msg) {
      $now = date('Y-m-d H:i:s');
      $ip = @$_SERVER['REMOTE_ADDR'];
      $file = "{$plugin->root}{$slug}-log.php";

      $message = "[{$now}] {$type} | {$ip}";
      $message .= " | {$msg}";

      // Email errors
      $flags = FILE_APPEND;
      $max = $plugin->config('log.size', 1); // Max log size in MB
      $key = $plugin->config('log.key', 'wsh'); // Url log key
      $email = $plugin->config('emails.error');

      if ($email && preg_match('/error/i', $type)) {
        $body = $message;
        $body .= "\n\n";
        $body .= print_r($_REQUEST, 1);

        mail($email, $slug, $body);
      }

      if ($request) {
        $message .= "\n" . trim(print_r($_REQUEST, 1));
      }
      
      // Check max log size
      if (file_exists($file) 
        && is_numeric($max) 
        && $max > 0) {

        $size = filesize($file); // Size in Bytes
        $size = round($size / 1024 / 1024, 4); // Convert Bytes to MB
        
        if ($size >= $max) {
          $flags = 0;
        }
      }
      
      // Prepend default php log protection
      if (!file_exists($file) || $flags === 0) {
        $prepend = "<?php if (!isset(\$_GET['{$key}'])) { die('Unauthorized access!'); } ?><pre>\n";
        $message = "{$prepend}{$message}";
      }

      $success = file_put_contents($file, "{$message}\n", FILE_APPEND);
    }

    return $success;
  }

  /**
   * Tracks user activity
   *
   * @param mixed [$params]
   * @return boolean
   */
  public function track($params = null)
  {
    return (new Tracking($this))->process($params);
  }
  
  /**
   * Process database assets
   * 
   * @param string $buffer
   */
  public function pagespeed($buffer)
  {
    $plugin = $this;
    $replacements = [];
    
    // Skip if buffer empty or not html
    if (!isset($_GET['pagespeed'])) {
      return $buffer;
    }

    $pagespeed = $this->settings->get('pagespeed');
    
    if (!$pagespeed) {
      return $buffer;
    }
    
    // 1) Search & Replace
    if (isset($pagespeed->search_replace) && is_array($pagespeed->search_replace) && $pagespeed->search_replace) {
      foreach ($pagespeed->search_replace as $rule) {
        $search = $rule->search;
        $search = preg_quote($search, '/');
        $search = preg_replace('/\n/', '(\n|\r\n)', $search);
        
        $replace = $rule->replace;
        $pattern = '/' . $search . '/i';
        
        $buffer = preg_replace($pattern, $replace, $buffer); 
      }
    }
    
    // 2) Remove body html
    if (isset($pagespeed->remove_body_html) && $pagespeed->remove_body_html) {
      $pattern = PatternUtils::pattern('name:html_tags_opened,tag:body');
      $buffer = preg_replace($pattern, '<body><br /><p style="text-align: center">Contents removed for page speed testing...</p></body>', $buffer);
    }
    
    // 3) Remove head html
    if (isset($pagespeed->remove_head_html) && $pagespeed->remove_head_html) {
      $pattern = PatternUtils::pattern('name:html_tags_opened,tag:head');
      $buffer = preg_replace($pattern, '<head></head>', $buffer);
    }
    
    // 4) Remove all scripts
    if (isset($pagespeed->remove_all_scripts) && $pagespeed->remove_all_scripts) {
      $pattern = PatternUtils::pattern('name:html_tags_opened,tag:script');
      $buffer = preg_replace($pattern, '', $buffer);
    }
    
    // 5) Remove all styles
    if (isset($pagespeed->remove_all_styles) && $pagespeed->remove_all_styles) {
      $pattern = PatternUtils::pattern('name:html_tags_closed,tag:link');
      $buffer = preg_replace($pattern, '', $buffer);
      
      $pattern = PatternUtils::pattern('name:html_tags_opened,tag:style');
      $buffer = preg_replace($pattern, '', $buffer);
    }
    
    // 6) Remove all images
    if (isset($pagespeed->remove_all_images) && $pagespeed->remove_all_images) {
      $pattern = PatternUtils::pattern('name:html_tags_closed,tag:img');
      $buffer = preg_replace($pattern, '', $buffer);
    }
    
    return $buffer;
  }
}