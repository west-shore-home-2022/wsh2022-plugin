<?php

namespace WSHWordpress\Plugins;

use WSHWordpress\Components\Session;
use WSHWordpress\Components\Settings;
use WSHWordpress\Controllers\ApiController;
use WSHWordpress\Controllers\PageController;
use WSHWordpress\Config\YamlConfig;
use WSHWordpress\Utils\CacheUtils;
use WSHWordpress\Utils\CryptUtils;
use WSHWordpress\Utils\FileUtils;
use WSHWordpress\Utils\Utils;
use WP_REST_Request;
use Puc_v4_Factory;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Exception;

/**
 * Shared plugin base
 */
class Plugin extends PluginAssets
{
  public $_config = null;
  public $wpdb = null;

  public $session = null;
  public $settings = null;

  public $root = null;
  public $pluginRoot = null;
  public $assetRoot = null;
  public $docRoot = null;
  public $sqlRoot = null;
  public $templateRoot = null;
  public $viewRoot = null;
  public $themeRoot = null;
  public $sharedRoot = null;
  public $siteRoot = null;
  public $uploadRoot = null;
  public $cacheRoot = null;
  public $pageUri = null;
  public $requestUri = null;
  public $siteUri = null;
  public $uri = null;

  public static $shortcodes = [];

  public static $config = [
    'tables' => [],
    'schemas' => []
  ];

  public $redirect = null;
  public $page = [
    'template' => null,
    'output' => null,
  ];

  public function __construct($file = null)
  {
    self::$config = (object) self::$config;
    $this->page = (object) $this->page;

    if (!defined('DS')) {
      define('DS', DIRECTORY_SEPARATOR);
    }

    $this->root = dirname($file) . DS;
    $this->pluginRoot = $this->root;
    $this->assetRoot = $this->root . 'assets' . DS;
    $this->docRoot =  preg_replace('/(\/|\\\)+$/', '', preg_replace('/(\/|\\\)/', DS, $_SERVER['DOCUMENT_ROOT'])) . DS;
    $this->sqlRoot = $this->assetRoot . 'sql' . DS;
    $this->templateRoot = $this->root . 'templates' . DS;
    $this->viewRoot = $this->root . 'views' . DS;
    $this->themeRoot = get_template_directory() . DS;
    $this->sharedRoot = dirname(dirname(dirname(__FILE__))) . DS ;
    $this->siteRoot = preg_replace('/(\/|\\\)+$/', '', preg_replace('/(\/|\\\)/', DS, ABSPATH)) . DS;
    $this->siteUri = '/' . preg_replace('/(\/|\\\)/', '/', preg_replace('/^' . preg_quote($this->docRoot, '/') . '/i', '', $this->siteRoot));

    $this->requestUri = $_SERVER['REQUEST_URI'];
    $uri = $this->requestUri;
    $parts = explode('?', $uri);
    $this->uri = $parts[0];
    $this->pageUri = preg_replace('/^' . preg_quote($this->siteUri, '/') . '/i', '/', $this->uri);

    $this->session = new Session($this);
    $this->settings = new Settings($this);

    parent::__construct();

    $this->load();
    $this->update($file);
    $this->constants();
    $this->init();
    $this->once();

    $this->session->flash();

    add_filter('nocache_headers', [$this, 'wpHeaders'], 10, 1);
    
    // Get page cache
    $this->getPageCache(true);

    // Manipulate final output
    if ($this->config('tags')) {
      add_action('shutdown', [$this, 'shutdown'], 0);
      add_filter('final_output', [$this, 'tags']);
      add_filter('final_output', [$this, 'savePageCache']);
    }

    // Enqueue optimizations
    ob_start([$this, 'databaseAssets']);
    ob_start([$this, 'pagespeed']);
  }

  /**
   * Triggers shutdown filters
   */
  public function shutdown()
  {
    $final = '';

    // We'll need to get the number of ob levels we're in, so that we can iterate over each, collecting
    // that buffer's output into the final output.
    $levels = ob_get_level();

    for ($i = 0; $i < $levels; $i++) {
        $final .= ob_get_clean();
    }

    // Apply any filters to the final output
    echo apply_filters('final_output', $final);
  }

  /**
   * Remove nocache headers from certain API(s)
   *
   * @param array $headers
   * @return array
   */
  public function wpHeaders($headers)
  {
    if (preg_match('/^\/wp-json\/[a-z0-9_-]+\/image\//', $this->uri)) {
      $year = date('Y', strtotime('+1 year'));

      $headers = [
        'Pragma' => 'public',
        'Cache-Control' => 'public',
        'Expires' => "Wed, 21 May {$year} 00:00:00 GMT"
      ];
    }

    return $headers;
  }

  /**
   * Initialize menus
   */
  public function init()
  {
    $slug = $this->config('slug');
    $alias = $this->config('alias');

    $this->url = admin_url() . "?page={$slug}";

    $GLOBALS['_plugin'] = $this;

    if ($alias) {
      $alias = preg_replace('/^\$/', '', $alias);
      $GLOBALS[$alias] = $this;
    }

    // Hide the wordpress admin bar
    if (!is_admin()) {
      $admin_bar = $this->config('admin_bar');

      if (!is_null($admin_bar) && !$admin_bar) {
        show_admin_bar(false);
      }
    }

    // Determine Upload Root
    $path = $this->config('upload.path');

    if ($path) {
      $path = preg_replace('/(\/|\\\)/', DS, $path);
      $path = $this->siteRoot . preg_replace('/^(\/|\\\)+/', '', $path);
    } else {
      $data = wp_upload_dir();
      $path = preg_replace('/(\/|\\\)/', DS, $data['basedir']);
    }

    $this->uploadRoot = preg_replace('/(\/|\\\)+$/', '', $path) . DS;

    // Determine Cache Root
    $path = $this->config('cache.path');

    if ($path) {
      $path = preg_replace('/(\/|\\\)/', DS, $path);
      $path = $this->siteRoot . preg_replace('/^(\/|\\\)+/', '', $path);
    } else {
      $data = wp_upload_dir();
      $path = preg_replace('/(\/|\\\)/', DS, $data['basedir']);
      $path = dirname($path) . DS . 'cache';
    }

    $this->cacheRoot = preg_replace('/(\/|\\\)+$/', '', $path) . DS;

    // Decrypt request params
    $array = [
      'get' => $_GET,
      'post' => $_POST
    ];

    foreach ($array as $type => $data) {
      $params = isset($data['params']) ? $data['params'] : null;

      if ($params && is_string($params)) {
        $params = CryptUtils::decrypt($params);
        $params = Utils::prepParams($params);

        if (is_array($params)) {
          foreach ($params as $key => $value) {
            if ($type === 'get') {
              $_GET[$key] = $value;
            } elseif ($type === 'post') {
              $_POST[$key] = $value;
            }
          }
        }
      }
    }

    // Admin
    add_action('init', [$this, 'route']);
    add_action('admin_menu', [$this, 'menus']);

    // Assets
    $this->initAssets();

    // Admin Footer
    add_filter('admin_footer_text', [$this, 'admin_footer_text']);

    // Restful API
    add_action('rest_api_init', [$this, 'api']);

    // Handle Custom Pages
    add_action('template_redirect', [$this, 'redirect'] );
    add_filter('template_include', [$this, 'page']);

    // Actions
    $this->actions();

    // Filters
    $this->filters();

    // Shortcodes
    $this->shortcodes();

    // Crons
    $this->crons();
  }

  /**
   * Setup plugin constants
   */
  public function constants() {}

  /**
   * Sets the admin footer text
   */
  public function admin_footer_text()
  {
    $page = isset($_GET['page']) ? $_GET['page'] : '';
    $slug = $this->config('slug');

    if ($page === $slug) {
      $output = $this->config('admin.footer');

      echo $this->parse($output, $this->_config);
    }
  }

  /**
   * Load actions
   */
  public function actions()
  {
    $classes = $this->classes('Actions');

    // Setup actions
    foreach ($classes as $class) {
      if (isset($class::$action)
        && (!isset($class::$active) || (isset($class::$active) && $class::$active))) {
        $actions = Utils::trimExplode(',', $class::$action);

        $obj = new $class($this);

        if (method_exists($obj, 'emit')) {
          foreach ($actions as $action) {
            add_action($action, [$obj, 'emit'], $obj->priority, $obj->accepted_args);
          }
        }
      }
    }
  }

  /**
   * Load filters
   */
  public function filters()
  {
    $classes = $this->classes('Filters');

    // Setup filters
    foreach ($classes as $class) {
      if (isset($class::$filter)
        && (!isset($class::$active) || (isset($class::$active) && $class::$active))) {
        $filters = Utils::trimExplode(',', $class::$filter);

        $obj = new $class($this);

        if (method_exists($obj, 'emit')) {
          foreach ($filters as $filter) {
            add_filter($filter, [$obj, 'emit'], $obj->priority, $obj->accepted_args);
          }
        }
      }
    }
  }

  /**
   * Load shortcodes
   */
  public function shortcodes()
  {
    $classes = $this->classes('Shortcodes');

    // Setup shortcodes
    foreach ($classes as $file => $class) {
      if (!isset($class::$active) || (isset($class::$active) && $class::$active)) {
        $obj = new $class($this);

        if (method_exists($obj, 'run')) {
          $name = preg_replace('/\.php$/i', '', basename($file));

          $tag = isset($obj::$tag) ? $obj::$tag : null;
          $tag = $tag ? $tag : strtolower($name);

          add_shortcode($tag, [$obj, 'run']);

          // Parse name for dashed shortcodes
          if (preg_match('/([A-Z]{1}[a-z]+){2,}/', $name)) {
            $tag = '';
            $i = 1;

            $chars = str_split($name);

            foreach ($chars as $char) {
              if ($i > 1 && preg_match('/[A-Z]/', $char)) {
                $tag .= '-' . $char;
              } else {
                $tag .= $char;
              }

              $i++;
            }

            if ($tag) {
              $tag = strtolower($tag);

              add_shortcode($tag, [$obj, 'run']);
            }
          }
        }
      }
    }
  }

  /**
   * Load crons
   */
  public function crons()
  {
    $classes = $this->classes('Crons');

    $debug = isset($_GET['debug']) && preg_match('/^cron(s)?$/i', $_GET['debug']) && is_admin() ? [] : null;

    // Setup crons
    foreach ($classes as $class) {
      $cron = strtolower(preg_replace('/\\\/', '_', $class));

      if (!isset($class::$active) || (isset($class::$active) && $class::$active)) {
        // Schedule cron
        $obj = new $class($this);

        // Debug next scheduled cron
        if (isset($debug)) {
          $time = wp_next_scheduled($cron);

          if ($time) {
            $next = date('Y-m-d H:i:s', $time);

            $debug[] = [
              'cron_class' => preg_replace('/\\\/', '\\<wbr />', $class),
              'hook_name' => preg_replace('/_/', '_<wbr />', $cron),
              'interval' => $obj->interval,
              'next_run' => $next
            ];
          }
        }

        if (method_exists($obj, 'emit') && is_numeric($obj->start)) {
          if (!wp_next_scheduled($cron)) {
            wp_schedule_event($obj->start, $obj->interval, $cron);
          }

          add_action($cron, [$obj, 'emit']);
        }
      } elseif (isset($class::$active) && !$class::$active) {
        // Unschedule cron
        $time = wp_next_scheduled($cron);

        if ($time) {
          wp_unschedule_event($time, $cron);
        }
      }
    }

    // Display cron debugging
    if (isset($debug)) {
      $view = $this->view('debug');
      $view->head = 'Cron Schedule';
      $view->subhead = date('Y-m-d H:i:s');
      $view->data = $debug;

      die($view->render());
    }
  }

  /**
   * Retrieve class paths
   *
   * @param string $path
   * #param string [$classname]
   * @return array|string
   */
  public function classes($path, $classname = null)
  {
    $classes = [];
    $namespaces = [];
    $namespaces['WSHWordpress'] = dirname(__DIR__) . DS . $path;

    if ($ns = $this->config('namespace')) {
      $namespaces[$ns] = $this->root . 'src' . DS . $path;
    }

    // Build actions
    foreach ($namespaces as $namespace => $path) {
      if (is_dir($path)) {
        $items = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);

        foreach ($items as $name => $item) {
          if ($item->isFile() && preg_match('/\.php$/i', $name)) {

            $lookup = preg_replace('/.*' . preg_quote(DS . 'src' . DS, '/') . '/i', '', $name);
            $class = $namespace . '/' . $lookup;
            $class = preg_replace('/\.php$/i', '', $class);
            $class = preg_replace('/(\\\|\/)+/i', '\\', $class);

            if (class_exists($class)) {
              $classes[$lookup] = $class;
            }
          }
        }
      }
    }

    // Return a specific class by classname
    if ($classname) {
      if ($classes) {
		// 1) No classname conversions
        $classname = ucwords($classname);

        $classes = array_reverse($classes);

        foreach ($classes as $class) {
          if (preg_match('/' . preg_quote($classname, '/') . '$/', $class)) {
            return $class;
          }
        }

      	// 2) Convert Pascal Casing (Ex: contact_form to ContactForm)
        $classname = preg_replace('/_/', ' ', $classname);
        $classname = ucwords($classname);
        $classname = preg_replace('/\s/', '', $classname);

        $classes = array_reverse($classes);

        foreach ($classes as $class) {
          if (preg_match('/' . preg_quote($classname, '/') . '$/', $class)) {
            return $class;
          }
        }
      }

      return null;
    }

    return $classes;
  }

  /**
   * Handle API requests
   */
  public function api()
  {
    $controller = new ApiController($this);
    $controller->register();
  }

  /**
   * Handle custom page requests
   *
   * @param string $template
   * @return string
   */
  public function page($template)
  {
    if ($this->page && isset($this->page->template)) {
      global $post;

      $post_object = get_post(2);

      setup_postdata($GLOBALS['post'] =& $post_object);

      $template = $this->page->template;
    } else {
      $controller = new PageController($this);
      $controller->template($template);
    }

    return $template;
  }

  public function redirect()
  {
    if ($this->redirect) {
      wp_redirect($this->redirect);
      die();
    }
  }

  /**
   * Gets a property from the plugin's page object
   *
   * @param string $prop The property name
   * @param mixed [$default] Optional property default value
   * @return mixed
   */
  public function get($prop, $default = null)
  {
    $value = $default;

    if (isset($this->page->{$prop})) {
      $value = $this->page->{$prop};
    }

    return $value;
  }

  /**
   * Update the plugin
   * Requirements: WordPress >= 3.9.0
   *
   * @param string $file
   */
  public function update($file = null)
  {
    $slug = $this->config('slug');
    $url = $this->config('update.url');
    $consumer_key = $this->config('update.consumer.key');
    $consumer_secret = $this->config('update.consumer.secret');
    $branch = $this->config('update.branch');

    if (function_exists('wp_normalize_path')
        && $slug
        && $url
        && $consumer_key
        && $consumer_secret
        && $branch
        && $file) {

      $updater = Puc_v4_Factory::buildUpdateChecker($url, $file, $slug);

      // Authorization Credentials
      $updater->setAuthentication([
        'consumer_key' => $consumer_key,
        'consumer_secret' => $consumer_secret
      ]);

      // Branch
      $updater->setBranch($branch);
    }
  }

  /**
   * Build admin menus & submenus
   */
  public function menus()
  {
    $menus = $this->config('menus');
    $name = $this->config('name');

    if ($menus) {
      $callback = [$this, 'output'];

      foreach ($menus as $menu) {
        $show = true;

        // Merge menu defaults
        $defaults = [
          'title' => $name,
          'permissions' => null,
          'slug' => null,
          'label' => null,
          'icon' => 'dashicons-admin-generic',
          'capability' => 'read',
          'position' => 30,
          'submenus' => []
        ];

        foreach ($defaults as $prop => $value) {
          if (!isset($menu->{$prop})) {
            $menu->{$prop} = $value;
          }
        }

        // Validate permissions
        $permissions = $menu->permissions;

        if ($permissions) {
          $permissions = implode('|', $permissions);
          $show = $this->user->is($permissions);
        }

        // Validate required
        $required = ['title', 'label', 'slug', 'capability', 'icon'];

        foreach ($required as $prop) {
          if (!isset($menu->{$prop}) || !$menu->{$prop}) {
            $show = false;

            break;
          }
        }

        // Whether to show or not
        if ($show && isset($menu->show)) {
          $cmd = '$show = ' . $menu->show . ';';
          eval($cmd);
        }

        // Build menu
        if ($show) {
          $menu->position = is_numeric($menu->position) ? $menu->position : '';

          $request = $_REQUEST;

          $slug = $menu->slug;
          $length = strlen($slug);

          // Page must start with plugin slug
          if (!isset($request['page']) || substr($request['page'], 0, $length) !== $slug) {
            $page = $slug;
          } else {
            $page = $request['page'];
          }

          add_menu_page(
            $menu->title,
            $menu->label,
            $menu->capability,
            $page,
            $callback,
            $menu->icon,
            $menu->position
          );

          // Build submenus
          $submenus = $menu->submenus;

          if ($submenus) {
            foreach ($submenus as $submenu) {
              $show = true;

              // Merge submenu defaults
              $defaults = [
                'title' => $name,
                'label' => null,
                'capability' => 'read'
              ];

              foreach ($defaults as $prop => $value) {
                if (!isset($submenu->{$prop})) {
                  $submenu->{$prop} = $value;
                }
              }

              // Validate required
              $required = ['title', 'label', 'slug', 'capability'];

              foreach ($required as $prop) {
                if (!isset($submenu->{$prop}) || !$submenu->{$prop}) {
                  $show = false;

                  break;
                }
              }

              // Whether to show or not
              if ($show && isset($submenu->show)) {
                $cmd = '$show = ' . $submenu->show . ';';
                eval($cmd);
              }

              if ($show) {
                add_submenu_page(
                  $menu->slug,
                  $submenu->title,
                  $submenu->label,
                  $submenu->capability,
                  $submenu->slug,
                  $callback
                );
              }
            }
          }
        }
      }
    }
  }

  /**
   * Print the currently-stored output
   */
  public function output()
  {
    echo $this->page->output;
  }

  /**
   * Routes plugin requests to controllers
   */
  public function route()
  {
    self::$user_id = get_current_user_id();

    // Process API Requests
    $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : null;

    if ($api) {
      $request = new WP_REST_Request('GET', '/api/' . $api);

      $controller = new ApiController($this);
      $controller->route($request, [$api]);
    }

    if (!$this->page->output) {
      // Process Request
      $request = $_REQUEST;
      $page = isset($request['page']) ? $request['page'] : null;

      $slug = $this->config('slug');
      $length = strlen($slug);

      // Controller default
      $name = 'admin';

      // Controller by config
      $name = $this->config('controller', $name);

      // Check page is set
      if (!$page) {
        return;
      }

      // Controller by request
      if (isset($request['controller'])) {
        $name = $request['controller'];
      } else {
        // Check for configured slugs
        $slugs = $this->slugs();

        $menu = null;

        foreach ($slugs as $key => $_menu) {
          if (preg_match('/^' . preg_quote($key, '/') . '/', $page)) {
            $menu = $_menu;
            $show = true;

            // Whether to show or not
            if (isset($menu->show)) {
              $cmd = '$show = ' . $menu->show . ';';
              eval($cmd);
            }

            // Controller by menu
            if ($show) {
              if (isset($menu->controller)) {
                $name = $menu->controller;
              } elseif (strlen($page) > strlen($key)) {
                // Controller by suffix
                $name = substr($page, strlen($key) - 1);
              }
            }

            break;
          }
        }

        if (!$menu) {
          return;
        }
      }

      // Get controllers
      $classes = $this->classes('Controllers');
      $classname = ucfirst($name) . 'Controller';

      // Find controller
      foreach ($classes as $class) {
        if (preg_match('/\\\\' . preg_quote($classname, '/') . '$/', $class)) {
          $controller = new $class($this);

          $action = 'index';
          $action = !empty($request['action']) ? $request['action'] : $action;
          $action = isset($menu) && is_object($menu) && isset($menu->action) ? $menu->action : $action;

          unset($request['page'], $request['controller'], $request['action']);

          if (method_exists($controller, $action)) {
            $this->page->output = $controller->{$action}($request);
            break;
          }
        }
      }
    }
  }

  /**
   * Get all the slugs for the plugin
   *
   * @return array
   */
  public function slugs()
  {
    $slugs = [];

    $menus = $this->config('menus');

    if ($menus) {
      foreach ($menus as $menu) {
        if (isset($menu->slug)) {
          $slug = strtolower($menu->slug);
          $slug = preg_replace('/&.*$/', '', $slug);

          if (!isset($slugs[$slug])) {
            $slugs[$slug] = $menu;
          }
        }

        if (isset($menu->submenus)) {
          foreach ($menu->submenus as $submenu) {
            if (isset($submenu->slug)) {
              $slug = strtolower($submenu->slug);
              $slug = preg_replace('/&.*$/', '', $slug);

              if (!isset($slugs[$slug])) {
                if ((!isset($submenu->controller) || !$submenu->controller)
                  && (isset($menu->controller) && $menu->controller)) {
                  $submenu->controller = $menu->controller;
                }

                if ((!isset($submenu->action) || !$submenu->action)
                  && (isset($menu->action) && $menu->action)) {
                  $submenu->action = $menu->action;
                }

                $slugs[$slug] = $submenu;
              }
            }
          }
        }
      }
    }

    return $slugs;
  }

  /**
   * Loads the configuration file
   *
   * @param string $file
   * @return config
   * @throws Exception
   */
  public function load($file = 'config', $required = true)
  {
    $expire = 60 * 60 * 24; // 1 day

    $main_config = $file === 'config';
    $pluginRoot = $this->root;
    $file = $pluginRoot . $file;
    $key = basename(dirname($file)) . '-config-' . md5($file);

    $config = get_transient($key);

    if (!$config || CacheUtils::nocache()) {
      $config = [];

      // 1) Yaml
      // Preferred way but some servers can't parse yaml correctly.
      // Example: www.ehsfl.com
      if (!$config) {
        $ext = '.yml';
        $configFile = file_exists($file . $ext) ? $file . $ext : $pluginRoot . $file . $ext;

        if (file_exists($configFile) && is_file($configFile)) {
          $yaml = new YamlConfig($configFile, false, null);
          $config = $yaml->toArray();
        }
      }

      // 2) JSON
      if (!$config) {
        $ext = '.json';
        $configFile = file_exists($file . $ext) ? $file . $ext : $pluginRoot . $file . $ext;

        if (file_exists($configFile) && is_file($configFile)) {
          $config = json_decode(file_get_contents($configFile));
        }
      }

      // 3) PHP
      if (!$config) {
        $ext = '.php';
        $configFile = file_exists($file . $ext) ? $file . $ext : $pluginRoot . $file . $ext;

        if (file_exists($configFile) && is_file($configFile)) {
          include($configFile);
          //die(json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        }
      }

      if ($config) {
        if (!is_object($config)) {
          $config = json_decode(json_encode($config));
        }

        if (is_object($config)) {
          set_transient($key, $config, $expire);
        } else {
          if ($required) {
            throw new Exception('Plugin configuration is invalid');
          } else {
            return null;
          }
        }
      } else {
        if ($required) {
          throw new Exception('Plugin configuration file missing');
        } else {
          return null;
        }
      }
    }

    if ($main_config) {
      $this->_config = $config;
    }

    return $config;
  }

  /**
   * Runs once unless a plugin reload is triggered
   * Here is where you would want to setup any default plugin tables
   *
   * Reload Example:
   * Append "?reload" to the URL
   */
  public function once()
  {
    $plugin = $this;
    $expire = 0; // Never expire
    $now = date('Y-m-d H:i:s');
    $slug = $plugin->config('slug');
    $key = "{$slug}-once";

    $once = get_transient($key);

    if (!$once || CacheUtils::nocache()) {
      $plugin->table('wsh_meta', $this);
      $plugin->table('wsh_assets', $this);

      set_transient($key, $now, $expire);
    }
  }

  /**
   * Get the last page
   */
  public function lastpage($lastpage = null)
  {
    $lastpage = $lastpage ? $lastpage : home_url('/');
    $referrer = @$_SERVER['HTTP_REFERER'];

    if ($referrer && !preg_match('/' . preg_quote($_SERVER['HTTP_HOST'], '/') . '/i', $referrer)) {
      $lastpage = $referrer;
    }

    $lastpage = isset($this->page->lastpage) ? $this->page->lastpage : $lastpage;

    $lastpage = isset($_REQUEST['redirect']) ? $_REQUEST['redirect'] : $lastpage;

    return $lastpage;
  }

  /**
   * Process tags
   * 
   * @param string $buffer
   */
  public function tags($buffer)
  {
    $plugin = $this;
    
    // @todo Skip wp-json requests
    $classes = $this->classes('Tags');

    if (!$classes) {
      return $buffer;
    }

    // Setup tags
    foreach ($classes as $class) {
      $cron = strtolower(preg_replace('/\\\/', '_', $class));

      if (!isset($class::$active) || (isset($class::$active) && $class::$active)) {
        // Initiate tag
        $obj = new $class($this, $buffer);

        if (method_exists($obj, 'emit')) {
          $buffer = $obj->emit();
        }
      }
    }

    //$buffer = print_r($classes, 1);
    
    return $buffer;
  }

  /**
   * Get page cache
   * 
   * @param boolean $die
   * @return string
   */
  public function getPageCache($die = false)
  {
    $value = null;
    $nocache = CacheUtils::nocache();

    if (isset($_GET['cache'])) {
      $cache = Utils::getBool($_GET['cache']) && !$nocache;
      $name = $_SERVER['REQUEST_URI'];

      if ($cache) {
        $value = $this->cache($name, null, true, 'html');

        // Die output
        if ($die && !is_null($value)) {
          die($value);
        }
      }
    }

    return $value;
  }

  /**
   * Save page cache
   * 
   * @param string $buffer
   * @return string
   */
  public function savePageCache($buffer)
  {
    $plugin = $this;

    if (isset($_GET['cache']) && Utils::getBool($_GET['cache'])) {
      $name = $_SERVER['REQUEST_URI'];

      $this->cache($name, $buffer, true, 'html');
    }
    
    return $buffer;
  }

  /**
   * Get or save cache
   *
   * @param string $name The cache filename
   * @param mixed [$value=null] The cache value to save
   * @param boolean [$md5=false] Whether to md5 the filename in order to attempt to make it unique
   * @param string [$suffix=null] An optional suffix to append to the cache filename
   */
  public function cache($name, $value = null, $md5 = false, $suffix = null)
  {
    $cacheDir = $this->cacheRoot;
    $now = date('Y-m-d H:i:s');

    FileUtils::createDir($cacheDir);

    // Alternative: Try to create a writeable cache directory in the uploads directory
    if (!file_exists($cacheDir) || !is_writeable($cacheDir)) {
      $cacheDir = $this->uploadRoot . 'cache' . DIRECTORY_SEPARATOR;

      FileUtils::createDir($cacheDir);
    }

    // Process cache
    if (file_exists($cacheDir) && is_writeable($cacheDir)) {
      $user = $this->user();

      $name = preg_replace('/(\?|&)reload=?\d*/i', '', $name);
      $name = is_bool($md5) && $md5 ? md5($name) : $name;
      $name .= is_string($suffix) && strlen($suffix) ? ".{$suffix}" : $name;

      $cacheFile = $cacheDir . $name;

      // 1) Get cache
      if ($name && is_null($value)) {
        // Don't get cache when a user is logged in
        if ($user) {
          if (file_exists($cacheFile)) {
            unlink($cacheFile);
          }

          return null;
        }

        $value = null;

        if (file_exists($cacheFile)) {
          $value = file_get_contents($cacheFile);
        }

        return $value;
      }

      // 2) Save cache
      else if ($name && !is_null($value)) {
        // Don't save cache when a user is logged in
        if ($user) {
          if (file_exists($cacheFile)) {
            unlink($cacheFile);
          }

          return false;
        }

        if (is_string($suffix) && strlen($suffix) && $suffix === 'html') {
          $value = "\n<!-- Cache Start ({$now}) -->\n" . trim($value) . "\n<!-- Cache End -->\n";
        }

        return file_put_contents($cacheFile, $value);
      }
    }

    return null;
  }
}