<?php

namespace WSHWordpress\Plugins;

use WSHWordpress\Models\Asset;
use WSHWordpress\Utils\Utils;
use WSHWordpress\Utils\WpUtils;
use WSHWordpress\Utils\CacheUtils;
use WSHWordpress\Utils\CryptUtils;
use WSHWordpress\Utils\FileUtils;
use WSHWordpress\Utils\JsonUtils;
use MatthiasMullie\Minify;

/**
 * Handles embedding plugin assets
 */
class PluginAssets extends PluginBase
{
  public static $queue = [];
  public static $jquery = false;

  public $assets = [
    'frontend' => '',
    'backend' => '
      assets/vendor/jquery-ui/1.12.1/jquery-ui.min.css,
      assets/vendor/jquery-ui/1.12.1/jquery-ui.min.js,
      assets/vendor/promise-polyfill/7.1.2/polyfill.min.js,
      vendor/wsh/wsh-wordpress/assets/js/jquery-extended.min.js,
      vendor/wsh/wsh-wordpress/assets/js/cms.min.js,
      vendor/wsh/wsh-wordpress/assets/vendor/bootstrap/3.3.7/js/bootstrap.min.js,
      vendor/wsh/wsh-wordpress/assets/vendor/bootstrap/3.3.7/bootstrap-hack.min.js,
      vendor/wsh/wsh-wordpress/assets/vendor/lodash/4.13.1/lodash.min.js,
      vendor/wsh/wsh-wordpress/assets/vendor/moment/2.17.1/moment.min.js,
      vendor/wsh/wsh-wordpress/assets/vue/Base.min.js,
      vendor/wsh/wsh-wordpress/assets/vendor/vue/2.1.6/vue.min.js,
      vendor/wsh/wsh-wordpress/assets/vue/Components.min.js,
      vendor/wsh/wsh-wordpress/assets/vendor/trumbowyg/2.8.1/ui/trumbowyg.min.css,
      vendor/wsh/wsh-wordpress/assets/vendor/trumbowyg/2.8.1/trumbowyg.min.js,
      vendor/wsh/wsh-wordpress/assets/css/backend.min.css,
      vendor/wsh/wsh-wordpress/assets/js/backend.min.js,
      assets/css/backend.min.css,
      assets/js/backend.min.js
    ',
    'common' => ''
  ];

  public static $once_global_js = false;

  /**
   * Init plugin frontend or backend assets
   */
  public function initAssets()
  {
    if (is_admin()) {
      // Backend
      add_action('admin_enqueue_scripts', [$this, 'backendAssets']);
      add_action('admin_footer', [$this, 'footer'], 1);
      add_action('admin_enqueue_scripts', [$this, 'enqueue']);
    } else {
      // Frontend
      $this->frontendAssets();
      $this->commonAssets();
      add_action('wp_footer', [$this, 'footer'], 1);
      add_action('wp_footer', [$this, 'enqueue']);
    }
  }

  /**
   * Process backend assets and applies rules
   *
   * @param string $page The current page name
   */
  public function backendAssets($page)
  {
    global $post;

    $posts = [];
    $continue = !$page ? false : true;
    $slugs = $this->slugs();
    $backend_pages = $this->config('backend.pages');
    $backend_assets = $this->config('assets.backend');

    // Build assets
    $assets = Utils::trimExplode(',', $this->assets['backend']);

    if ($backend_assets && is_array($backend_assets)) {
      $assets = array_merge($assets, $backend_assets);
    }

    // Allow certain pages
    $continue = false;

    if ($page) {
      foreach ($slugs as $menu) {
        $pattern = '/_page_' . preg_quote($menu->slug, '/') . '(-.*)?$/i';

        // Allow if the page is an allowed page or if the page matches the plugin slug pattern
        if (preg_match($pattern, $page)) {
          $continue = true;

          break;
        }
      }
    }

    // Allow certain post types
    if ($post && $posts) {
      if (in_array($post->post_type, $posts)) {
        $continue = true;
      }
    }

    if (!$continue) {
      return;
    }

    $deps = ['css' => [], 'js' => []];
    $deps['js'] = WpUtils::is_plugin_active('rest-api/plugin.php') ? ['wp-api'] : [];

    $params = ['group' => 'backend', 'deps' => $deps];

    $this->assets($assets, $params);
    $this->commonAssets();
  }

  /**
   * Combine & append plugin frontend assets
   */
  public function frontendAssets()
  {
    $assets = Utils::trimExplode(',', $this->assets['frontend']);
    $frontend = $this->config('assets.frontend');

    if ($frontend && is_array($frontend)) {
      $assets = array_merge($assets, $frontend);
    }

    $this->assets($assets, 'group:frontend');
  }

  /**
   * Combine & append plugin common assets
   */
  public function commonAssets()
  {
    $assets = Utils::trimExplode(',', $this->assets['common']);
    $common = $this->config('assets.common');

    if ($common && is_array($common)) {
      $assets = array_merge($assets, $common);
    }

    $this->assets($assets, 'group:common');
  }

  /**
   * Wrapper: Append plugin scripts to a queue
   *
   * Example:
   * $plugin->scripts('js/test1.js, js/test2.js');
   *
   * @param string|array $assets
   * @param array [$params]
   */
  public function scripts($assets, $params = null)
  {
    $this->assets($assets, $params);
  }

  /**
   * Wrapper: Append plugin styles to a queue
   *
   * Example:
   * $plugin->styles('css/test1.css, css/test2.css');
   *
   * @param string|array $assets
   * @param array [$params]
   */
  public function styles($assets, $params = null)
  {
    $this->assets($assets, $params);
  }

  /**
   * Appends plugin assets to a queue
   *
   * @param string|array $assets
   * @param array [$params]
   */
  public function assets($assets, $params = null)
  {
    $plugin = $this;

    // Defaults
    $deps = [];
    $overload = true;
    $group = 'inline';
    $footer = true;
    $media = 'all';

    // Locals
    $locals = Utils::trimExplode(',', 'deps, footer, group, media, overload');

    // Params
    $params = Utils::prepParams($params);

    foreach ($params as $key => $value) {
      if (in_array($key, $locals)) {
        if (isset($$key) && Utils::isBool($$key)) {
          $$key = Utils::getBool($value);
        } else {
          $$key = $value;
        }
      }
    }

    $deps = !$deps ? ['js' => [], 'css' => []] : $deps;

    $deps['js'] = $deps['js'] ? $deps['js'] : [];
    $deps['css'] = $deps['css'] ? $deps['css'] : [];

    $paths = Utils::trimExplode(',', $assets);
    $slug = $plugin->config('slug');
    $version = $plugin->config('version');

    foreach ($paths as $path) {
      if (!preg_match('/^(http(s)?:)?\/\//', $path) && !preg_match('/^\//', $path)) {

        if ($overload) {
          $path = $plugin->path($path, true);

          if (!preg_match('/^\//', $path)) {
           $path = '/' . $path;
          }
        } else {
          // Backend Assets
          if (is_admin()) {
            if (!preg_match('/^assets\//', $path)) {
              $path = 'assets/' . $path;
            }

            $path = !preg_match('/^\//', $path) ? '/' . $path : $path;
            $path = '/' . basename($this->root) . $path;
            $path = plugins_url($path);
          // Frontend Assets
          } else {
            if (!preg_match('/^\//', $path)) {
              $themePath = explode('/', get_template_directory_uri());
              $themePath = '/' . implode('/', array_splice($themePath, 3));
              $themePath = preg_replace('/^' . preg_quote($plugin->siteUri, '/') . '/i', '/', $themePath);
              $path =  $themePath . '/' . $path;
            }
          }
        }
      }

      $url = $path;

      $key = $plugin->assetKey($url);
      $queue = null;

      if (!isset(self::$queue[$key])) {
        if (preg_match('/\.js$/i', $url)) {
          $handle = $key;

          $array = isset($deps['js']) ? $deps['js'] : [];

          $queue = (object) [
            'type' => 'script',
            'group' => $group,
            'handle' => $handle,
            'src' => $url,
            'deps' => $array,
            'ver' => $version,
            'in_footer' => $footer
          ];
        } elseif (preg_match('/\.css$/i', $url)) {
          $handle = $key;

          $array = isset($deps['css']) ? $deps['css'] : [];

          $queue = (object) [
            'type' => 'style',
            'group' => $group,
            'handle' => $handle,
            'src' => $url,
            'deps' => $array,
            'ver' => $version,
            'media' => $media
          ];
        }

        if ($queue) {
          self::$queue[$key] = $queue;
        }
      }
    }
  }

  /**
   * Get a unique asset key
   *
   * NOTE: Prevents multiple active plugins which use the same wsh-wordpress
   * framework from loading the same shard assets more than once
   *
   * @param string $url
   * @return string
   */
  public function assetKey($url)
  {
    return md5(strtolower(preg_replace('/^.*\/wsh-wordpress\//i', '{SHARED}', $url)));
  }

  /**
   * Adds global javascript variables to the footer
   */
  public function footer()
  {
    global $wsh_global_footer;
    
    $slug = $this->config('slug');
    
    // Define URLs
    $admin_url = admin_url() . 'admin.php?page=' . $slug;
    $plugin_url = plugins_url(basename($this->root)) . '/';
    $site_url = site_url() . '/';
    $rest_url = site_url() . '/wp-json/' . $slug . '/';
    $global_rest_url = site_url() . '/wp-json/wsh/';
    $theme_url = get_template_directory_uri() . '/';
    $websocket_url = null;
    $wpnonce = wp_create_nonce('wp_rest');

    // Websocket
    $websocket = $this->config('websocket');

    if ($websocket) {
      $protocol = isset($websocket->protocol) ? "{$websocket->protocol}://" : 'ws://';
      $host = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'localhost';
      $host = isset($websocket->host) ? $websocket->host : $host;
      $port = isset($websocket->port) ? ':' . $websocket->port : null;

      $websocket_url = "{$protocol}{$host}{$port}";
    }

    // Plugin scope variables
    $data = [
      'nonces' => [
        'wpnonce' => $wpnonce
      ],
      'urls' => [
        'admin' => $admin_url,
        'plugin' => $plugin_url,
        'site' => $site_url,
        'rest' => $rest_url,
        'theme' => $theme_url,
        'websocket' => $websocket_url
      ]
    ];

    // Global scope variables
    $data_global = [
      'config' => [
        'api' => [],
        'ip' => isset($_GET['ip']) ? $_GET['ip'] : @$_SERVER['REMOTE_ADDR']
      ],
      'nonces' => [
        'wpnonce' => $wpnonce
      ],
      'urls' => [
        'admin' => $admin_url,
        'plugin' => $plugin_url,
        'site' => $site_url,
        'rest' => $global_rest_url,
        'theme' => $theme_url,
        'websocket' => $websocket_url
      ],
      'user' => null
    ];
    
    // Enable global abusiveipdb form api
    if ($this->config('api.abuseipdb.global')) {
      $data_global['config']['api']['abusiveipdb'] = 1;
    }

    $user = $this->user();

    if ($user) {
      $data_global['user'] = (object) [
        'uid' => CryptUtils::encrypt($user->id),
        'name' => $user->name,
        'email' => $user->email,
        'username' => $user->username,
        'first_name' => $user->first_name,
        'last_name' => $user->last_name
      ];
    }
    
    $var = preg_replace('/-/', '_', $slug);
    $data = json_encode($data);
    $data_global = json_encode($data_global);
    
    $output = "<script>
/* <![CDATA[ */
var {$var} = {$data};";
    if (!isset($wsh_global_footer)) {
    $output .="
var wsh = {$data_global};";
    }
    $output .="
/* ]]> */
</script>";
    
    $wsh_global_footer = true;
    
    echo $output;
  }
  
  /**
   * Enqueues plugin assets by group order
   */
  public function enqueue()
  {
    $queue = self::$queue;
    $groups = Utils::trimExplode(',', 'backend, frontend, common, page, inline');

    // Debugging
    //die('<pre>' . print_r($queue, 1) . '</pre>');

    // Process queue
    foreach ($groups as $group) {
      foreach ($queue as $key => $item) {
        if ($item->group === $group) {
          if ($item->type === 'script') {
            // Include jQuery once since some themes don't include it by default
            if (!self::$jquery) {
              $item->deps[] = 'jquery';
              self::$jquery = true;
            }

            wp_enqueue_script($item->handle, $item->src, $item->deps, $item->ver, $item->in_footer);
          } elseif ($item->type === 'style') {
            wp_enqueue_style($item->handle, $item->src, $item->deps, $item->ver, $item->media);
          }
        }
      }
    }
  }
  
  /**
   * Process database assets
   * 
   * @param string $buffer
   */
  public function databaseAssets($buffer)
  {
    $plugin = $this;
    $replacements = [];
    
    // Skip if buffer empty or not html
    if (!$buffer || !preg_match('/<\/(body|head)>/i', $buffer) || isset($_GET['pagespeed'])) {
      return $buffer;
    }

    $optimizations = Asset::where('active', 1)->get();

    // Skip if no optimizations
    if (!$optimizations) {
      return $buffer;
    }

    // Skip if no cache directory
    if (!FileUtils::createDir($plugin->cacheRoot)) {
      return $buffer;
    }
    
    // Process urls
    foreach ($optimizations as $optimization) {
      $optimization->assets = trim($optimization->assets);
      
      // Skip if assets is empty
      if (!$optimization->assets) {
        continue;
      }
      
      // Skip if asset type doesn't match
      if ($optimization->type !== 'global') {
        // Skip if asset is backend but not an admin page
        if ($optimization->type === 'backend' && !is_admin()) {
          continue;
        }
        
        // Skip if asset is frontend but is an admin page
        if ($optimization->type === 'frontend' && is_admin()) {
          continue;
        }
      }
      
      // Replace
      if (preg_match('/^replace/i', $optimization->action)) {
        $this->replaceAssets($buffer, $optimization, $replacements);
      }
      // Combine
      else if (preg_match('/^combine/i', $optimization->action)) {
        $this->combineAssets($buffer, $optimization, $replacements);
      }
    }
    
    // Debugging only if logged in
    $user = $plugin->user();
    
    if ($user) {
      $total = count($replacements);

      $output = "<script>\n";
      $output .= "console.log('Output Buffer: Replaced {$total} files');\n";

      $i = 1;
      foreach ($replacements as $remote => $local) {
        $output .= "console.log('{$i}a) Remote: {$remote}');\n";
        $output .= "console.log('{$i}b) Local: http://' + (location.hostname + '{$local}'));\n";
        $i++;
      }

      $output .= "</script>";
      
      $this->appendToBody($buffer, $output);
    }
    
    return $buffer;
  }
  
  /**
   * Combine assets action
   * 
   * @param type $buffer
   * @param type $optimization
   * @param type $replacements
   */
  public function combineAssets(&$buffer, $optimization, &$replacements)
  {
    // Test mode only
    if (0 && !isset($_GET['test'])) {
      return;
    }
    
    $plugin = $this;
    $cacheRoot = $plugin->cacheRoot;
    $now = date('Y-m-d H:i:s');
    $queue = [];
    
    // Default curl options
    $defaults = [
      'CURLOPT_CONNECTTIMEOUT' => 30, // Max time to connect to server
      'CURLOPT_TIMEOUT' => 60, // Max total execution time
      'CURLOPT_RETURNTRANSFER' => true, // Return result on request
      'CURLOPT_USERAGENT' => "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)",
      'CURLOPT_FOLLOWLOCATION' => true, // Follow any redirects
      'CURLOPT_SSL_VERIFYPEER' => true // Never set to false on production servers
    ];
    
    // Config curl override options
    $options = $plugin->config('curl.combine_assets', (object) []);
    $options = is_object($options) ? get_object_vars($options) : [];
    $options = is_array($options) ? $options : [];
    $options = array_merge($defaults, $options);
    
    $assets = $optimization->assets;
    $attributes = $optimization->attributes;
    $action = $optimization->action;
    $minify = $optimization->minify;

    // Split assets
    $assets = preg_split('/\s+/', $assets);

    // Build attributes
    $attributes = JsonUtils::decode($attributes);
    $attributes = JsonUtils::isArray($attributes) ? $attributes : [];
    
    $array = [];

    foreach ($attributes as $attribute) {
      if (is_object($attribute) && isset($attribute->name) && isset($attribute->value)) {
        $name = preg_replace('/"/', '&quote;', $attribute->name);
        $value = preg_replace('/"/', '&quote;', $attribute->value);

        $array[$name] = $value;
      }
    }

    $attributes = $array;

    // Set up types
    $types = (object) [
      'js' => [],
      'css' => [],
    ];
    
    // Custom asset variables
    $vars = [
      'site(_url|_root)?' => site_url($plugin->url($plugin->siteRoot)),
      'plugin(_url|_root)?' => site_url($plugin->url($plugin->pluginRoot)),
      'shared(_url|_root)?' => site_url($plugin->url($plugin->sharedRoot)),
      'theme(_url|_root)?' => site_url($plugin->url($plugin->themeRoot)),
      'shared_asset(s)?' => site_url($plugin->url($plugin->sharedRoot) . 'assets/'),
      'theme_asset(s)?' => site_url($plugin->url($plugin->themeRoot) . 'assets/'),
      'plugin_asset(s)?' => site_url($plugin->url($plugin->assetRoot))
    ];
    
    // Categorize assets by type
    foreach ($assets as $asset) {
      // Parse custom asset variables
      foreach ($vars as $name => $value) {
        $pattern = '/\{\$(' . $name . ')\}/i';
        $asset = preg_replace($pattern, $value, $asset);
      }
      
      $type = $this->assetType($asset);
      
      if ($type === 'js') {
        $types->js[] = $asset;
      } else if ($type === 'css') {
        $types->css[] = $asset;
      }
    }
    
    // Process asset types
    foreach ($types as $type => $assets) {
      if (!$assets) {
        continue;
      }
      
      // Build unique filename
      $filename = '';
      $files = '';
      
      foreach ($assets as $asset) {
        $filename .= $asset;
        $files .= $asset . ',';
      }
      
      $filename = md5($filename) . '.combined.' . $type;
      
      // Cache file
      $file = $cacheRoot . $filename;
      
      // Cache clearing
      $this->clearAssetCache($file, $optimization);
      
      // Add file to queue
      if (!file_exists($file) && $assets) {
        $queue[$file] = $assets;
        file_put_contents($file, '');
      }
      
      // Append or replace a remote asset with a local asset
      if (!isset($queue[$file]) && file_exists($file) && filesize($file)) {
        $url3 = $this->url($file);
        $output = '';
        
        // Build custom attributes
        $attrs = '';
        $array = $attributes;
        
        // Default attributes
        if ($type === 'css') {
          if (!isset($array['rel'])) {
            $array['rel'] = 'stylesheet';
          }
        }

        foreach ($array as $name => $value) {
          $attrs .= " {$name}=\"{$value}\"";
        }
        
        if ($type === 'js') {
          $output = '<script src="' . $url3 . '"' . $attrs . '></script>';
        } else if ($type === 'css') {
          $output = '<link href="' . $url3 . '"' . $attrs . '/>';
        }
        
        $output = trim($output);
        
        if (!$output) {
          break;
        }
        
        $files = trim($files);
        $files = preg_replace('/,$/', '', $files);
        
        $replacements[$files] = $url3;
          
        if (preg_match('/^combine-head/i', $action)) {
          $this->appendToHead($buffer, $output);
        } else if (preg_match('/^combine-body/i', $action)) {
          $this->appendToBody($buffer, $output);
        } else if (preg_match('/^combine-replace/i', $action)) {
          $found = false;
          
          foreach ($assets as $asset) {
            if ($type === 'js') {
              $pattern = "/
                  <script
                    [^>]*?
                    src=\"?'?" . preg_quote($asset, '/') . "\"?'?
                    [^>]*?
                  >
                  <\/script>
                /xis";
            } else if ($type === 'css') {
              $pattern = "/
                  <link
                    [^>]*?
                    href=\"?'?" . preg_quote($asset, '/') . "\"?'?
                    [^>]*?
                  >
                /xis";
            }
            
            // Testing patterns
            if (0) {
              $matches = [];
            
              preg_match_all($pattern, $buffer, $matches);

              $buffer = ($pattern . "\n\n" . '<pre>' . print_r($matches, 1) . '</pre>');

              return $buffer;
            }
            
            if (preg_match($pattern, $buffer)) {
              if (!$found) {
                $found = true;
                
                $buffer = preg_replace($pattern, $output, $buffer);
              } else {
                $buffer = preg_replace($pattern, '', $buffer);
              }
            }
          }
        }
      }
    }
    
    // Process queue
    if ($queue) {
      session_write_close();
      
      // Retrieve & cache file
      foreach ($queue as $file => $assets) {
        if (file_exists($file) && !filesize($file)) {
          $plugin->log(basename($file));
          
          $contents_combined = '';
          
          foreach ($assets as $asset) {
            $url = $asset;
            
            $pattern = '/' . preg_quote($url, '/') . '/i';
            
            $parts = explode('?', $url);
            array_shift($parts);
            $query = $parts;
            $query = implode('?', $query);
            $query = strlen($query) ? '?' . $query : '';

            $url2 = preg_match('/^\/\//', $url) ? 'https:' . $url : $url;

            $contents = '';
            
            // 1) Try file_get_contents
            // Initially disabled because some servers might not support this method
            // For Example: Godaddy w/Cloudflare Caching
            if (0) {
              $contents = @file_get_contents($url2);
              $contents = $contents && is_string($contents) ? trim($contents) : '';
            }
            
            // 2) Try curl
            if (!strlen($contents)) {
              //$plugin->info($url2);
              $ch = curl_init();
              curl_setopt_array($ch, $options);
              curl_setopt($ch, CURLOPT_URL, $url2);
              
              $result = curl_exec($ch);
              $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
              $error = curl_error($ch);
              
              curl_close($ch);
              
              if ($status === 200) {
                $contents = $result;
              }
            }
            
            if (strlen($contents)) {
              // Convert relative URLs to absolute URLs in CSS content
              if ($type === 'css') {
                $base = dirname($url2) . '/';

                $matches = [];
                $pattern = '/url\(\s*(\'|")?(\.\.\/)*.*(\'|")?\s*\)/Uxis';
                preg_match_all($pattern, $contents, $matches);

                foreach ($matches[0] as $match) {
                  // Skip absolute urls
                  if (preg_match('/:/', $match)) {
                    continue;
                  }

                  $array = [];
                  preg_match_all('/url\(\s*(\'|\")?((\.\.\/)+)/', $match, $array);
                  $baseurl = $base;
                  
                  if (isset($array[2]) && $array[2]) {
                    $num = substr_count($array[2][0], '../');

                    if ($num) {
                      for ($i = 1; $i <= $num; $i++) {
                        $baseurl = dirname($baseurl) . '/';
                      }

                      $replace = preg_replace('/(url\(\s*(\'|\")?)((\.\.\/)+)/', "$1{$baseurl}", $match);
                      $contents = preg_replace('/' . preg_quote($match, '/'). '/', $replace, $contents);
                    }
                  } else {
                    $replace = preg_replace('/url\(\s*(\'|\")?/', "$0{$baseurl}", $match);
                    $contents = preg_replace('/' . preg_quote($match, '/'). '/', $replace, $contents);
                  }
                }
              }
              
              // Minify
              if ($optimization->minify) {
                $this->minifyAsset($contents, $type);
              }

              $contents_combined .= "/* {$url2} */\n" . $contents . "\n\n";
            }
          }
          
          $contents_combined = "/* Cached: {$now} */\n" . trim($contents_combined);
      
          if (strlen($contents_combined)) {
            file_put_contents($file, $contents_combined);
          }
        }
      }
    }
  }
  
  /**
   * Clear asset cache
   * 
   * @param type $file
   * @param type $asset
   */
  public function clearAssetCache($file, $asset)
  {
    if (file_exists($file)) {
      $now = time();
      $expire = $asset->expire;
      $updated_at = $asset->updated_at;
      
      $user = $this->user();
      $nocache = CacheUtils::nocache() && $user;
      $clear_cache = false;

      // Clear when an admin is logged in and a hard refresh is detected
      if (!$clear_cache) {
        if ($nocache) {
          $clear_cache = true;
        }
      }

      // Clear when a file has expired
      if (!$clear_cache) {
        if ($expire) {
          $modified_time = filemtime($file);
          $expire_time = strtotime('+' . $expire, $modified_time);
          
          if ($now >= $expire_time) {
            $clear_cache = true;
          }
        }
      }

      // Clear when records are updated
      if (!$clear_cache) {
        $updated_at = strtotime($updated_at);
        $modified_time = filemtime($file);

        if ($updated_at >= $modified_time) {
          $clear_cache = true;
        }
      }

      // Clear cache
      if ($clear_cache) {
        unlink($file);
      }
    }
    
    return $clear_cache;
  }
  
  /**
   * Determines the asset type
   * 
   * @param string $asset
   * @return string
   */
  public function assetType($asset)
  {
    $type = 'js';
    
    if (preg_match('/\.css/i', $asset)) {
      $type = 'css';
    }
    
    return $type;
  }
  
  /**
   * Replace assets action
   * 
   * @param type $buffer
   * @param type $optimization
   * @param type $replacements
   */
  public function replaceAssets(&$buffer, $optimization, &$replacements)
  {
    $plugin = $this;
    $cacheRoot = $plugin->cacheRoot;
    
    $assets = $optimization->assets;
    $action = $optimization->action;
    $minify = $optimization->minify;

    $assets = preg_split('/\s+/', $assets);

    foreach ($assets as $asset) {
      $url = $asset;
      $type = $this->assetType($asset);
      $pattern = '/' . preg_quote($url, '/') . '/i';

      $parts = explode('?', $url);
      $filename = array_shift($parts);
      $query = $parts;
      $query = implode('?', $query);
      $query = strlen($query) ? '?' . $query : '';

      $filename = basename($filename);
      $filename = md5($url) . '.' . $filename;

      $file = $cacheRoot . $filename;

      $url2 = preg_match('/^\/\//', $url) ? 'https:' . $url : $url;

      // Cache clearing
      $this->clearAssetCache($file, $optimization);

      // Retrieve & cache file
      if (!file_exists($file)) {
        $contents = file_get_contents($url2);

        $contents = trim($contents);
        
        if (strlen($contents)) {
          // Minify
          if ($optimization->minify) {
            $this->minifyAsset($contents, $type);
          }
          
          file_put_contents($file, $contents);
        }
      }

      // Replace remote url with a local url
      if (file_exists($file)) {
        $url3 = $this->url($file);
        $url3 = $url3 . $query;

        $replacements[$url2] = $url3;
        $replace = $url3;

        $buffer = preg_replace($pattern, $replace, $buffer);
      }
    }
  }
  
  /**
   * Minify asset
   * 
   * @todo
   * @param string $contents
   * @return string
   */
  public function minifyAsset(&$contents, $type = '')
  {
    if ($type === 'css') {
      $minifier = new Minify\CSS;
      $minifier->add($contents);
      $contents = $minifier->minify();
    } else if ($type === 'js') {
      $minifier = new Minify\JS;
      $minifier->add($contents);
      $contents = $minifier->minify();
    }
    
    return $contents;
  }
  
  /**
   * Append to the head of the buffer
   * 
   * @param string $buffer
   * @param string $content
   */
  public function appendToHead(&$buffer, $content = '')
  {
    $content = trim($content);
    
    if (strlen($content)) {
      $pattern = '/<\/head>/i';
      $content .= "\n</head>";
      
      $buffer = preg_replace($pattern, $content, $buffer);
    }
  }
  
  /**
   * Append to the body of the buffer
   * 
   * @param string $buffer
   * @param string $replace
   */
  public function appendToBody(&$buffer, $content = '')
  {
    $content = trim($content);
    
    if (strlen($content)) {
      $pattern = '/<\/body>/i';
      $content .= "\n</body>";
      
      $buffer = preg_replace($pattern, $content, $buffer);
    }
  }
}