<?php

namespace WSHWordpress\Components;

use WSHWordpress\Models\Integration;
use WSHWordpress\Tools\GoogleSheets;
use WSHWordpress\Traits\Logger;

/**
 * Integrations Component
 * This component is responsible for managing integration data
 *
 * @author Ed Rodriguez
 */
class Integrations extends Component
{
  use Logger;

  protected $table = null;
  protected $id = null;

  public function __construct($obj, $table = null)
  {
    $this->table = $table ? $table : $obj->table();
    $this->id = $obj->id;

    parent::__construct();
  }

  /**
   * Saves integrations
   *
   * @param string|object|array $integrations
   * @param mixed $value
   * @return boolean
   */
  public function save($integrations, $plugin, $value = null)
  {
    if (is_object($integrations)) {
      $plugin->table('WSH_integrations', $this);

      // Google Sheets Integration
      if (isset($integrations->google_sheets) && is_array($integrations->google_sheets)) {

        // Delete old integrations
        $records = Integration::where([
          'fk_table' => $this->table,
          'fk_id' => $this->id,
          'type' => 'google_sheets'
        ])->get();

        $ids = [];

        foreach ($integrations->google_sheets as $spreadsheet) {
          if (isset($spreadsheet->integration_id) && !in_array($spreadsheet->integration_id, $ids)) {
            $ids[] = $spreadsheet->integration_id;
          }
        }

        foreach ($records as $record) {
          if (!in_array($record->id, $ids)) {
            $record->delete();
          }
        }

        // Add new integrations
        foreach ($integrations->google_sheets as $spreadsheet) {
          // Create Google Sheet
          $api = new GoogleSheets($plugin);

          if ($spreadsheet->type === 'create') {

            // Create Google Sheet
            if ($api->isSetup()) {
              $results = $api->create($spreadsheet);

              $this->addLogs($api->logs());

              // Add Integration
              if (!$api->hasErrors()) {
                $_spreadsheet = $results[0];

                $spreadsheet->type = 'existing';
                $spreadsheet->client_id = GoogleSheets::$config->web->client_id;
                $spreadsheet->id = $_spreadsheet->getSpreadsheetId();
                $worksheets = $api->worksheets($spreadsheet->id);

                if ($worksheets) {
                  $worksheet = $worksheets[0];
                  $spreadsheet->worksheets[0]->id = $worksheet->id;
                  $worksheet = $api->worksheet($spreadsheet->id, $worksheet->id);
                  $columns = $api->columns($worksheet);
                  $total = count($columns);

                  $worksheet = $spreadsheet->worksheets[0];

                  // Bind google sheet column id
                  for ($i = 0; $i < $total; $i++) {
                    $item = $columns[$i];

                    if (isset($worksheet->columns[$i])) {
                      $column = $worksheet->columns[$i];

                      if ($column->name === $item->name) {
                        $column->id = $item->column;
                      }
                    }
                  }

                  $model = new Integration([
                    'type' => 'google_sheets',
                    'data' => json_encode($spreadsheet, JSON_PRETTY_PRINT),
                    'fk_id' => $this->id,
                    'fk_table' => $this->table
                  ]);

                  if (!$model->save()) {
                    $this->error("Unable to save Google Sheets integration \"{$spreadsheet->name}\"");
                  }
                } else {
                  $this->error("Unable to read worksheet for \"{$spreadsheet->name}\"");
                }
              }
            } else {
              $this->error("Google Sheets not enabled, unable to save \"{$spreadsheet->name}\"");
            }
          } elseif ($spreadsheet->type === 'existing') {

            if ($api->isSetup()) {
              if (isset($spreadsheet->integration_id) && is_numeric($spreadsheet->integration_id) && $spreadsheet->integration_id > 0) {
                // Update an existing Google Sheet
                $record = Integration::find($spreadsheet->integration_id);

                $record->data = json_encode($spreadsheet, JSON_PRETTY_PRINT);

                if (!$record->save()) {
                  $this->error("Unable to save Google Sheets integration \"{$spreadsheet->name}\"");
                }
              } else {
                // Add an existing Google Sheet
                $spreadsheet->client_id = GoogleSheets::$config->web->client_id;

                $model = new Integration([
                  'type' => 'google_sheets',
                  'data' => json_encode($spreadsheet, JSON_PRETTY_PRINT),
                  'fk_id' => $this->id,
                  'fk_table' => $this->table
                ]);

                if (!$model->save()) {
                  $this->error("Unable to save Google Sheets integration \"{$spreadsheet->name}\"");
                }
              }
            } else {
              $this->error("Google Sheets not enabled, unable to save \"{$spreadsheet->name}\"");
            }
          }
        }
      }
    }
  }

  /**
   * Gets all integrations
   *
   * @return object
   */
  public function all()
  {
    $integrations = (object) [];

    $records = Integration::where([
      'fk_table' => $this->table,
      'fk_id' => $this->id
    ])->get();

    foreach ($records as $record) {
      $type = $record->type;

      if (!isset($integrations->{$type})) {
        $integrations->{$type} = [];
      }

      $integration = json_decode($record->data);
      $integration->integration_id = $record->id;

      $integrations->{$type}[] = $integration;
    }

    return $integrations;
  }
}