<?php

namespace WSHWordpress\Components;

use WSHWordpress\Models\Meta as MetaModel;

/**
 * Meta Component
 * This component is responsible for managing meta data
 *
 * @author Ed Rodriguez
 */
class Meta extends Component
{
  protected $table = null;
  protected $id = null;

  public function __construct($obj, $table = null)
  {
    $this->table = $table ? $table : $obj->table();
    $this->id = $obj->id;

    parent::__construct();
  }

  /**
   * Gets meta
   *
   * @param string [$key]
   * @return mixed
   */
  public function get($key = null)
  {
    if (!$key) {
      return $this->all();
    }

    $meta = MetaModel::where([
      'fk_table' => $this->table,
      'fk_id' => $this->id,
      'key' => $key
    ])->first();

    // Get meta (not found)
    if ($meta) {
      return $meta->value;
    }

    return null;
  }

  /**
   * Sets meta
   *
   * @param string $key
   * @param mixed $value
   * @return boolean
   */
  public function set($key, $value)
  {
    $meta = MetaModel::firstOrNew([
      'fk_table' => $this->table,
      'fk_id' => $this->id,
      'key' => $key
    ]);

    // Convert objects and arrays to json
    if (is_object($value) || is_array($value)) {
      $value = json_encode($value, JSON_PRETTY_PRINT);
    }

    // Convert boolean to numeric
    if (is_bool($value)) {
      $value = $value ? 1 : 0;
    }

    $value = trim($value);

    $meta->value = $value;

    return $meta->save();
  }

  /**
   * Saves meta
   *
   * @param string|object|array $items
   * @param mixed $value
   * @return boolean
   */
  public function save($items, $value = null)
  {
    // Save single meta
    if (is_string($items)) {
      $items = trim($items);

      if ($items) {
        $items = [$items => $value];
      }
    } elseif (is_object($items)) {
      $items = get_object_vars($items);
    }

    // Save multiple meta
    if (is_array($items)) {
      foreach ($items as $key => $value) {
        if (!is_numeric($key)) {
          $meta = MetaModel::firstOrNew([
            'fk_table' => $this->table,
            'fk_id' => $this->id,
            'key' => $key
          ]);

          // Convert objects and arrays to json
          if (is_object($value) || is_array($value)) {
            $value = json_encode($value, JSON_PRETTY_PRINT);
          }

          // Convert boolean to numeric
          if (is_bool($value)) {
            $value = $value ? 1 : 0;
          }

          $value = trim($value);

          if (!strlen($value) && !$meta->isEmpty()) {
            // Delete meta that is empty
            return $meta->delete();
          } elseif (strlen($value) && $meta->value !== $value) {
            // Save meta
            $meta->value = $value;

            return $meta->save();
          }
        }
      }
    }

    return false;
  }

  /**
   * Deletes meta
   *
   * @param string $key
   * @return boolean
   */
  public function delete($key)
  {
    $meta = MetaModel::firstOrNew([
      'fk_table' => $this->table,
      'fk_id' => $this->id,
      'key' => $key
    ]);

    return $meta->isEmpty() || $meta->delete();
  }

  /**
   * Gets all meta
   *
   * @return object
   */
  public function all()
  {
    $meta = (object) [];

    $records = MetaModel::where([
      'fk_table' => $this->table,
      'fk_id' => $this->id
    ])->get();

    foreach ($records as $record) {
      $value = $record->value;

      if ($this->isJson($value)) {
        $value = json_decode($value);
      }

      $meta->{$record->key} = $value;
    }

    return $meta;
  }

  /**
   * Determines if a value is a json string
   *
   * @param string $value
   * @return boolean
   */
  public function isJson($value)
  {
    $isJson = preg_match('/^\[/', $value) && preg_match('/\]$/', $value);
    $isJson = $isJson ? $isJson : preg_match('/^\{/', $value) && preg_match('/\}$/', $value);

    return $isJson;
  }

  /**
   * Gets meta via magic method
   *
   * @param string $key
   * @return mixed
   */
  public function __get($key)
  {
    $meta = $this->all();

    if ($meta && isset($meta->{$key})) {
      return $meta->{$key};
    }

    return null;
  }

  /**
   * Sets meta via magic method
   *
   * @param string $key
   * @param mixed $value
   * @return mixed
   */
  public function __set($key, $value)
  {
    return $this->set($key, $value);
  }
}