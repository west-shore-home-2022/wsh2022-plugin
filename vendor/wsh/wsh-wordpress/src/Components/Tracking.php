<?php

namespace WSHWordpress\Components;

use WSHWordpress\Models\Meta as MetaModel;
use WSHWordpress\Utils\Utils;
use WSHWordpress\Utils\SiteUtils;
use WSHWordpress\Utils\JsonUtils;
use WSHWordpress\Models\Tracking as TrackingModel;

/**
 * Tracking Component
 * This component is responsible for processing and adding tracking
 *
 * @author erodriguez
 */
class Tracking extends Component
{
  /**
   * Processes tracking
   *
   * @param type $params
   * @return boolean
   */
  public function process($params)
  {
    $success = false;

    $plugin = $this->plugin;
    $user = $plugin->user;

    // Params
    $params = !is_null($params) ? $params : array_merge($_GET, $_POST);
    $params = Utils::prepParams($params);

    $track = Utils::getBool($plugin->value('track', true, $params));

    if (!$track) {
      return $success;
    }

    $from = $plugin->config('tracking.excludes.from');
    $to = $plugin->config('tracking.excludes.to');

    // Request variables
    $title = $plugin->value('title', null, $params);
    $url = $plugin->value('url', null, $params);
    $uri = $url ? preg_replace('/^[a-z]+:\/\/.+\//Ui', '/', $url) : null;

    // Remove query string
    if ($uri) {
      $uri = explode('?', $uri);
      $uri = $uri[0];
    }

    $uri = $plugin->value('uri', $uri, $params);
    $referrer = $plugin->value('referrer', null, $params);
    $type = $plugin->value('type', null, $params);
    $depth = $plugin->value('depth', null, $params);
    $width = $plugin->value('width', null, $params);
    $height = $plugin->value('height', null, $params);
    $data = $plugin->value('data', null, $params);

    // Prep Data
    $json = stripslashes($data);
    $json = JsonUtils::decode($json);

    if (JsonUtils::isValid($json)) {
      $data = JsonUtils::encode($json, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
    }

    $host = $plugin->value('host', 0, $params);
    $resource_id = $plugin->value('resource_id', null, $params);
    $user_id = $plugin->value('user_id', null, $params);
    $meta = $plugin->value('meta', null, $params);

    // Date Override (used while creating dummy data)
    $created_at = $plugin->value('created_at', null, $params);

    $host = Utils::getBool($host);

    // Setup tracking table (if needed)
    $plugin->table('wsh_tracking', $plugin);
    $plugin->table('wsh_meta', $plugin);

    // Excludes IPs
    if (is_array($from)) {
      foreach ($from as $exclude) {
          $exclude = trim($exclude);

          if (isset($_SERVER['REMOTE_ADDR']) && strpos($_SERVER['REMOTE_ADDR'], $exclude) !== false) {
            $track = false;
            break;
          }
      }
    }

    // Exclude DNS
    if (is_array($to)) {
      foreach ($to as $exclude) {
        $exclude = trim($exclude);

        if (isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], $exclude) !== false) {
          $track = false;
          break;
        }
      }
    }

    // Validate one of the required
    if (empty($url) && empty($type)) {
      $track = false;
    }

    if ($track) {
      $tracking = new TrackingModel;

      $tracking->title = $title;
      $tracking->url = $url;
      $tracking->uri = $uri;
      $tracking->referrer = $referrer;
      $tracking->type = $type;
      $tracking->ip = trim($_SERVER['REMOTE_ADDR']);
      //$tracking->host = ($host) ? gethostbyaddr($_SERVER['REMOTE_ADDR']) : ''; // Getting host may cause performance issues

      $info = SiteUtils::getInfo();

      if (isset($info) && is_object($info)) {
        $tracking->agent = $info->agent;
        $tracking->os_name = $info->os->name;
        $tracking->os_version = $info->os->version;
        $tracking->client_name = $info->client->name;
        $tracking->client_version = $info->client->version;
        $tracking->bot_name = $info->bot->name;
      } else {
        $tracking->agent = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '';
      }

      $tracking->depth = $depth;
      $tracking->width = $width;
      $tracking->height = $height;
      $tracking->data = $data;
      $tracking->resource_id = $resource_id;
      $tracking->user_id = $user_id;

      if (!$user_id && $user) {
        $tracking->user_id = $user->id;
      }

      // Date Override (used while creating dummy data)
      if ($created_at) {
        $tracking->created_at = $created_at;
      }

      if ($tracking->add()) {
        $success = true;

        // Add tracking meta
        if ($meta) {
          if (is_array($meta)) {
            foreach ($meta as $key => $value) {
              if (!is_numeric($key)) {

                $model = new MetaModel;
                $model->fk_id = $tracking->id;
                $model->fk_table = $tracking->table();
                $model->key = $key;
                $model->value = $value;

                if ($model->add()) {

                }
              }
            }
          }
        }
      }
    }

    return $success;
  }
}