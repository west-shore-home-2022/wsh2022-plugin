<?php

namespace WSHWordpress\Components;

/**
 * Settings Component
 * This component is responsible for managing plugin settings
 *
 * @author erodriguez
 */
class Settings extends Component
{
  /**
   * Gets all the plugin's settings
   *
   * @return mixed
   */
  public function all()
  {
    return $this->get();
  }

  /**
   * Gets the plugin's settings or a plugin setting by its property path
   *
   * @param string|array|object [$path] The property path
   * @param mixed $default The default value
   * @return mixed
   */
  public function get($path = null, $default = null)
  {
    $plugin = $this->plugin;

    if ($path) {
      $settings = $this->all();

      return $plugin->find($path, $default, $settings);
    }

    $settings = (object) [];

    $wpdb = $plugin->wpdb;
    $slug = $plugin->config('slug');

    if ($slug) {
      $table = $wpdb->prefix . 'options';
      $option_name = $slug . '-settings';
      $sql = "SELECT * FROM {$table} WHERE option_name = '{$option_name}' LIMIT 1";

      $rows = $wpdb->get_results($sql);

      if ($rows) {
        $row = $rows[0];
        $value = $row->option_value;

        if ($value) {
          $json = json_decode($value);

          if (is_object($json)) {
            $settings = $json;
          }
        }
      }
    }

    return $settings;
  }

  /**
   * Saves a plugin setting
   *
   * @param string|object|array $name
   * @param mixed $value
   * @return boolean
   */
  public function save($name = '', $value = null)
  {
    $success = 0;
    $plugin = $this->plugin;
    $wpdb = $plugin->wpdb;
    $table = $wpdb->prefix . 'options';
    $option_name = $plugin->config('slug') . '-settings';
    $settings = $this->get();

    if (is_object($settings)) {
      $items = null;

      if (is_string($name)) {
        $items = [$name => $value];
      } else if (is_array($name)) {
        $items = $name;

        unset($items['_nonce']);
      } else if (is_object($name)) {
        $items = get_object_vars($name);
      }

      if (is_array($items) && $items) {
        foreach ($items as $key => $value) {
          $props = explode('.', $key);

          $i = 1;
          $total = count($props);
          $temp = $settings;

          foreach ($props as $prop) {
            if (isset($temp->{$prop})) {
              if ($i < $total) {
                $temp = $temp->{$prop};
              } else if (is_null($value)) {
                unset($temp->{$prop});
              } else {
                $temp->{$prop} = $value;
              }
            } else {
              if ($i < $total) {
                $temp->{$prop} = (object) [];
                $temp = $temp->{$prop};
              } else if (is_null($value)) {
                unset($temp->{$prop});
              } else {
                $temp->{$prop} = $value;
              }
            }

            $i++;
          }
        }
      }

      // Get settings
      $sql = "SELECT * FROM {$table} WHERE option_name = '{$option_name}' LIMIT 1";

      $rows = $wpdb->get_results($sql);

      $value = json_encode($settings);

      // Save updated settings
      if ($rows) {
        // Update
        $row = $rows[0];

        if ($row->option_value === $value) {
          $success = 1;
        } else {
          $success = $wpdb->update($table,
            ['option_value' => $value],
            ['option_id' => $row->option_id],
            ['%s'],
            ['%d']
          );
        }
      } else {
        // Add
        $success = $wpdb->insert($table,
          [
            'option_name' => $option_name,
            'option_value' => $value
          ],
          ['%s', '%s']
        );
      }
    }

    return $success;
  }

  /**
   * Deletes a plugin setting
   *
   * @param string|object|array $name
   * @return boolean
   */
  public function delete($name = '')
  {
    return $this->save($name, null);
  }
}