<?php

namespace WSHWordpress\Components;

/**
 * Component Base
 *
 * @author erodriguez
 */
class Component
{
  public $plugin;

  public function __construct($plugin = null)
  {
    global $_plugin;

    $this->plugin = $plugin ? $plugin : $_plugin;
  }
}
