<?php

namespace WSHWordpress\Components;

use WSHWordpress\Config\YamlConfig;

/**
 * Config Component
 * This component is responsible for managing config data
 *
 * @author Ed Rodriguez
 */
class Config extends Component
{
  protected $data = [];

  public function __construct($yaml = '')
  {
    parent::__construct();

    $this->data = YamlConfig::parse($yaml);
  }

  /**
   * Gets config
   *
   * @param string [$path]
   * @return mixed
   */
  public function get($path = null, $default = null)
  {
    global $_plugin;

    if ($_plugin) {
      $plugin = $_plugin;

      return $plugin->find($path, $default, $this->data);
    }

    return $default;
  }

  /**
   * Sets config
   *
   * @param string $path
   * @param mixed $value
   * @return boolean
   */
  public function set($path, $value)
  {
    $success = false;

    $_data = $this->data;
    $data = &$_data;

    $paths = explode('.', $path);
    $total = count($paths);

    $count = 1;
    foreach ($paths as $path) {
      if ($count == $total) {
        if (is_array($data)) {
          $data[$path] = $value;
          $success = true;
        } elseif (is_object($data)) {
          $data->{$path} = $value;
          $success = true;
        }

        break;
      }

      if (is_array($data) && isset($data[$path])) {
        $data = &$data[$path];
      } elseif (is_object($data) && isset($data->{$path})) {
        $data = &$data->{$path};
      }
      $count++;
    }

    if ($success) {
      $this->data = $_data;
    }

    return $success;
  }

  /**
   * Deletes config
   *
   * @param string $key
   * @return boolean
   */
  public function delete($path)
  {
    $success = false;

    $_data = $this->data;
    $data = &$_data;

    $paths = explode('.', $path);
    $total = count($paths);

    $count = 1;
    foreach ($paths as $path) {
      if ($count == $total) {
        if (is_array($data)) {
          unset($data[$path]);
          $success = true;
        } elseif (is_object($data)) {
          unset($data->{$path});
          $success = true;
        }

        break;
      }

      if (is_array($data) && isset($data[$path])) {
        $data = &$data[$path];
      } elseif (is_object($data) && isset($data->{$path})) {
        $data = &$data->{$path};
      }
      $count++;
    }

    if ($success) {
      $this->data = $_data;
    }

    return $success;
  }

  /**
   * Gets all config values
   *
   * @return object
   */
  public function all()
  {
    return $this->data;
  }

  /**
   * Gets config values via magic method
   *
   * @param string $key
   * @return mixed
   */
  public function __get($key)
  {
    return $this->get($key);
  }

  /**
   * Sets config values via magic method
   *
   * @param string $path
   * @param mixed $value
   * @return mixed
   */
  public function __set($path, $value)
  {
    return $this->set($path, $value);
  }
}