<?php

namespace WSHWordpress\Components;

/**
 * Session Component
 * This component is responsible for managing plugin sessions
 *
 * @author erodriguez
 */
class Session extends Component
{
  protected $lookup = 'WSH';

  public function __construct($plugin)
  {
    // Start session
    if (!session_id()) {
      @session_start();
    }

    parent::__construct($plugin);

    // Keep session alive
    $this->get();
  }

  /**
   * Gets all the session variables
   *
   * @return object
   */
  public function all()
  {
    return $this->get();
  }

  /**
   * Gets the session or the plugin's session property value
   *
   * @param string [$prop]
   * @return mixed
   */
  public function get($prop = null)
  {
    $lookup = $this->lookup;
    $plugin = $this->plugin;

    if ($prop) {
      $value = null;

      if ($prop) {
        $slug = $plugin->config('slug');
        $prop = "{$slug}-{$prop}";
        $session = $this->get();

        if (isset($session->{$prop})) {
          $value = $session->{$prop};
        }
      }

      return $value;
    }

    if (!isset($_SESSION[$lookup]) || !is_object($_SESSION[$lookup])) {
      $session = (object) [];
    } else {
      $session = $_SESSION[$lookup];
    }

    // Keep the session alive
    $session->time = time();

    $_SESSION[$lookup] = $session;

    return $session;
  }

  /**
   * Sets a plugin's session property value
   *
   * @param string [$prop]
   * @param mixed [$value]
   */
  public function set($prop = null, $value = null)
  {
    $lookup = $this->lookup;
    $plugin = $this->plugin;

    // Portal session state
    if ($prop) {
      $slug = $plugin->config('slug');
      $prop = "{$slug}-{$prop}";
      $session = $this->get();
      $session->{$prop} = $value;
      $_SESSION[$lookup] = $session;
    }
  }

  /**
   * Clears all sessions or the plugin's session by slug
   *
   * @param string [$slug]
   */
  public function clear($slug = null)
  {
    $lookup = $this->lookup;
    $plugin = $this->plugin;

    if ($slug) {
      // Clear by plugin slug only
      $session = $this->get();
      $keys = array_keys(get_object_vars($session));

      foreach ($keys as $key) {
        if (preg_match('/^' . preg_quote($slug, '/') . '-/', $key)) {
          unset($session->{$key});
        }
      }

      $_SESSION[$lookup] = $session;
    } else {
      // Clear all
      unset($_SESSION[$lookup]);
    }
  }

  /**
   * Sets session flash properties or if called empty, performs the following:
   *
   * 1) Stores session props to the plugin's page object
   * 2) Clears the flash session props
   * 3) Returns the flash session prop array
   *
   * @param string [$name]
   * @param mixed [$value]
   * @return array All the previously saved flash props
   */
  public function flash($name = null, $value = null)
  {
    $plugin = $this->plugin;

    if ($name) {
      if (!isset($_SESSION['flash'])) {
        $_SESSION['flash'] = [];
      }

      if (is_string($name)) {
        $_SESSION['flash'][$name] = $value;
      } elseif (is_array($name)) {
        foreach ($name as $key => $val) {
          $_SESSION['flash'][$key] = $val;
        }
      }
    } else {
      $props = isset($_SESSION['flash']) ? $_SESSION['flash'] : [];

      foreach ($props as $prop => $value) {
        $plugin->page->{$prop} = $value;
      }

      // Don't clear session on certain WordPress requests
      if (!preg_match('/(wp-login\.php|wp-json)/', $plugin->uri) && !isset($_GET['errormsg'])) {
        unset($_SESSION['flash']);
      }

      return $props;
    }
  }
}