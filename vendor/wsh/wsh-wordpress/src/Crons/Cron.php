<?php

namespace WSHWordpress\Crons;

use WSHWordpress\Traits\Logger;

/**
 * Cron Base
 *
 * @author Ed Rodriguez
 */
class Cron
{
  use Logger;

  public $start = 'midnight';
  public $interval = 'daily'; // hourly, twicedaily, daily, none

  public function __construct($plugin)
  {
    $this->plugin = $plugin;

    // Convert start string to a unix time
    if (is_string($this->start)) {
      $this->start = strtotime($this->start);
    }
  }
}
