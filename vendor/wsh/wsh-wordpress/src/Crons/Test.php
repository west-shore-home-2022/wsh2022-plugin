<?php

namespace WSHWordpress\Crons;

/**
 * Simple test cron
 *
 * @author Ed Rodriguez
 */
class Test extends Cron
{
  public static $active = false;
  public $start = '+1 minute';
  public $interval = 'hourly';

  /**
   * Outputs to the plugin log
   *
   * @param string $src
   * @return string
   */
  public function emit()
  {
    $plugin = $this->plugin;

    $plugin->log(__FILE__ . ' (Cron)');
  }
}
